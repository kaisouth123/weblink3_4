﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.Weblink.Controller.Models
{
    public class requestMessage
    {

        public requestMessage()
        {
            ServiceFee = new ServiceFee();
        }
        public string ReferenceNumber { get; set; }
        public string PurchaseToken { get; set; }
        public string MerchantCode { get; set; }
        public string MerchantAccountCode { get; set; }
        public string Token { get; set; }
        public string PaymentAccountNumber { get; set; }
        public string ExpirationDate { get; set; }
        public string SecurityCode { get; set; }
        public string BankAccountNum { get; set; }
        public string RoutingNum { get; set; }
        public string TransactionType { get; set; }
        public string Amount { get; set; }
        public string FeeAmount { get; set; }
        public string InvoiceNum { get; set; }
        public string CustomerAccountCode { get; set; }
        public string ZipCode { get; set; }
        public string PONum { get; set; }
        public string AccountHolderName { get; set; }
        public string HolderType { get; set; }
        public string AccountStreet { get; set; }
        public string AccountCity { get; set; }
        public string AccountState { get; set; }
        public string AccountZip { get; set; }
        public string AccountPhone { get; set; }
        public string PaymentType { get; set; }
        public string SoftwareVendor { get; set; }
        public string ClientPublicIPAddress { get; set; }
        public string OriginatingTechnologySource { get; set; }
        public string TransCatCode { get; set; }
        public string TransIndustryType { get; set; }
        public string AcctType { get; set; }
        public string CustomData { get; set; }
        public ServiceFee ServiceFee { get; set; }
    }
}
