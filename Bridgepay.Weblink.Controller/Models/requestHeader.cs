﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.Weblink.Controller.Models
{
    public class requestHeader
    {
        public string ClientIdentifier { get; set; }
        public string RequestType { get; set; }
        public string RequestDateTime { get; set; }
        public string TransactionID { get; set; }
        public requestMessage requestMessage { get; set; }

        public requestHeader()
        {
            requestMessage = new requestMessage();
        }
    }
}
