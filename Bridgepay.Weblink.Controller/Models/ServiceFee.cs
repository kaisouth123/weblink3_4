﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.Weblink.Controller.Models
{
    public class ServiceFee
    {
        public string ServiceFeeID { get; set; }
        public string Amount { get; set; }
        public string PurchaseToken { get; set; }
    }
}
