﻿using Bridgepay.Weblink.Controller.Models;
using Bridgepay.Weblink.Core.Interfaces;
using Bridgepay.Weblink.Core.Models;
using Bridgepay.WebLink.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Xsl;

namespace Bridgepay.Weblink.Controller
{
    public class RequestFormatter : IRequestFormatter
    {

        protected NameValueCollection _data;

        private StringWriter stringWriter;
        private XmlWriter xmlWriter;
        private string _requestType;
        private string _clientIPAddress;

        public RequestFormatter(string requestType, string clientIPAddress)
        {
            _requestType = requestType;
            _clientIPAddress = clientIPAddress;
            stringWriter = new StringWriter();
            xmlWriter = XmlWriter.Create(stringWriter);
        }
        public string FormatRequest(NameValueCollection data, string clientIdentifier, string referenceNumber)
        {
            requestHeader request = new requestHeader();

            request.requestMessage.PurchaseToken = data[Fields.PurchaseToken];

            request.ClientIdentifier = clientIdentifier;
            request.RequestType = _requestType;
            request.TransactionID = (string.IsNullOrEmpty(data[Fields.UserDefinedTransactionId]) ? GetDefaultUserDefinedTransactionId() : data[Fields.UserDefinedTransactionId]);
            request.RequestDateTime = DateTime.Now.ToShortDateString();
            request.requestMessage.AccountCity = data[Fields.City];
            request.requestMessage.AccountHolderName = data[Fields.FullName];
            request.requestMessage.AccountPhone = Helper.RemoveNonDigits(data[Fields.Phone]);
            request.requestMessage.AccountState = data[Fields.State];
            request.requestMessage.AccountStreet = data[Fields.Address1];
            request.requestMessage.AccountZip = data[Fields.ZipCode];
            request.requestMessage.AcctType = GetAccountType(data);
            string principalAmount;
            if (string.IsNullOrWhiteSpace(data[Fields.SvcAmount]))
                principalAmount = Helper.ToBridgeCommAmount(data[Fields.TotalAmt]);
            else
            {
                string totalAmount = Helper.ToBridgeCommAmount(data[Fields.TotalAmt]);
                string serviceAmount = Helper.ToBridgeCommAmount(data[Fields.SvcAmount]);
                principalAmount = (Convert.ToInt32(totalAmount) - Convert.ToInt32(serviceAmount)).ToString();
            }
            request.requestMessage.Amount = principalAmount;
            if (!string.IsNullOrWhiteSpace(data[Fields.FeeAmount]))
                request.requestMessage.FeeAmount = Helper.ToBridgeCommAmount(data[Fields.FeeAmount]);
            request.requestMessage.BankAccountNum = data[Fields.AccountNumber];
            request.requestMessage.ClientPublicIPAddress = _clientIPAddress;
            request.requestMessage.CustomerAccountCode = data[Fields.CustomerAccountCode];
            request.requestMessage.ExpirationDate = Helper.GetExpDate(data);
            request.requestMessage.HolderType = GetHolderType(data);
            request.requestMessage.InvoiceNum = data[Fields.InvoiceNum];
            request.requestMessage.MerchantAccountCode = data[Fields.MerchantAccountCode];
            request.requestMessage.MerchantCode = data[Fields.MerchantCode];
            request.requestMessage.SoftwareVendor = data[Fields.SoftwareVendor];
            request.requestMessage.OriginatingTechnologySource = "Weblink"; ;
            request.requestMessage.PaymentAccountNumber = data[Fields.CardNumber];
            request.requestMessage.PaymentType = data[Fields.PaymentType];
            request.requestMessage.PONum = data[Fields.PONum];
            request.requestMessage.ReferenceNumber = referenceNumber;
            request.requestMessage.RoutingNum = data[Fields.TransitNumber];
            request.requestMessage.SecurityCode = data[Fields.CVNum];
            request.requestMessage.Token = data[Fields.Token];
            request.requestMessage.TransactionType = GetTransactionType(data);
            request.requestMessage.TransCatCode = data[Fields.TransactionCategoryCode] != null ? data[Fields.TransactionCategoryCode].ToUpper() : string.Empty;
            request.requestMessage.CustomData = data[Fields.CustomData];
            if (!string.IsNullOrWhiteSpace(data[Fields.TransactionIndustryType]))
                request.requestMessage.TransIndustryType = data[Fields.TransactionIndustryType].ToUpper();
            else
            {
                if (IsCardTransaction(data))
                    request.requestMessage.TransIndustryType = "EC";
                else
                    request.requestMessage.TransIndustryType = "WEB";
            }
            request.requestMessage.ZipCode = data[Fields.ZipCode];

            if (!string.IsNullOrWhiteSpace(data[Fields.SvcAmount]))
            {
                request.requestMessage.ServiceFee.ServiceFeeID = GetDefaultUserDefinedTransactionId();
                request.requestMessage.ServiceFee.PurchaseToken = data[Fields.svcPurchaseToken];
                request.requestMessage.ServiceFee.Amount = Helper.ToBridgeCommAmount(data[Fields.SvcAmount]); ;
            }

            return FormatWithXSLT(request);
        }


        protected string FormatWithXSLT(requestHeader request)
        {
            XslCompiledTransform xslt = GetXSLTTransformer();

            XmlSerializer xs = new XmlSerializer(request.GetType());
            string xmlString;
            using (StringWriter swr = new StringWriter())
            {
                xs.Serialize(swr, request);
                xmlString = swr.ToString();
            }

            var xd = new XmlDocument();
            xd.LoadXml(xmlString);

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Encoding = new UnicodeEncoding(false, false);
            settings.ConformanceLevel = ConformanceLevel.Fragment;

            string results = string.Empty;

            using (StringWriter textWriter = new StringWriter())
            {
                XmlWriter.Create(textWriter, settings);
                using (XmlWriter.Create(textWriter, settings))
                {
                    xslt.Transform(xd, null, textWriter);
                    results = textWriter.ToString();
                }
            }

            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(results);
            return System.Convert.ToBase64String(plainTextBytes);

            //return results;
        }

        private XslCompiledTransform GetXSLTTransformer()
        {
            string template;
            string xsltBase = string.Empty;
            string templateDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase);

            switch (_requestType)
            {
                case BridgeCommRequestTypes.MerchantInfoRequest:
                    xsltBase = "MerchantInfo";
                    break;
                case BridgeCommRequestTypes.TransactionRequest:
                    xsltBase = "Transaction";
                    break;
                case BridgeCommRequestTypes.VoidRefundRequest:
                    xsltBase = "VoidRefund";
                    break;
                default:
                    break;
            }

            template = string.Concat(templateDirectory, "\\Stylesheets\\", xsltBase, "Request", ".xslt");

            var xslt = new XslCompiledTransform();
            xslt.Load(template);
            return xslt;
        }

        private string GetDefaultUserDefinedTransactionId()
        {
            return string.Format("{0}-{1}",
                DateTime.Now.ToString("yyyyMMddHHmmss"),
                Guid.NewGuid());
        }

        private string GetAccountType(NameValueCollection data)
        {
            // Credit card http://labs.unitedthinkers.com/index.php/UniCharge_Real-Time_HTTPs_Integration#Account_Types

            string acctType = data[Fields.AccountType];

            if (IsCardTransaction(data))
            {
                acctType = "R";
            }
            else
            {
                if (!string.IsNullOrEmpty(acctType))
                {
                    switch (acctType.ToLower())
                    {
                        case "checking":
                            acctType = "C";
                            break;
                        case "saving":
                        case "savings":
                            acctType = "S";
                            break;
                    }
                }
                else
                {
                    // Must be a check trxn and they didnt specify acct type somehow
                    // Default to checking
                    acctType = "C";
                }
            }

            return acctType;
        }

        private string GetHolderType(NameValueCollection data)
        {
            string holderType;

            if (IsCardTransaction(data))
            {
                holderType = data[Fields.HolderType];
            }
            else // Maintain backward compatibility with "CheckType"
            {
                holderType = data[Fields.HolderType] ?? data[Fields.CheckType];
            }

            holderType += string.Empty;

            switch (holderType.ToLower())
            {
                default:
                case "personal":
                    holderType = "P";
                    break;
                case "business":
                    holderType = "O";
                    break;
            }

            return holderType;
        }

        private bool IsCardTransaction(NameValueCollection data)
        {
            string cardType = data[Fields.CardTypeName];
            if (string.IsNullOrEmpty(cardType))
                cardType = data[Fields.WalletCardType];

            if (!string.IsNullOrEmpty(cardType) &&
                !cardType.Equals("echeck", StringComparison.CurrentCultureIgnoreCase))
            {
                return true;
            }

            if (!string.IsNullOrEmpty(data[Fields.SecureFormat]) &&
                !string.IsNullOrEmpty(data[Fields.SecurityInfo]))
            {
                return true;
            }

            if (!string.IsNullOrEmpty(data[Fields.Token]))
            {
                return true;
            }

            return false;
        }

        private string GetTransactionType(NameValueCollection data)
        {
            string transType = string.Empty;
            string passedInTransType = (data[Fields.TransType] + string.Empty).ToLower();

            switch (passedInTransType)
            {
                case "return":
                case "credit":
                    transType = BridgeCommTransTypes.Credit;
                    break;
                case "auth":
                case "sale-auth":
                case "preauth":
                    transType = BridgeCommTransTypes.SaleAuth;
                    break;
                case "sale":
                case "":
                    transType = BridgeCommTransTypes.Sale;
                    break;
            }

            return transType;
        }

    }
}
