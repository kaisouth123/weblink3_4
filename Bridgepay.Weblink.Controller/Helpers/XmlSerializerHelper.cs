﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Bridgepay.Weblink.Controller.Helpers
{
    public class XmlSerializerHelper<T>
    {
        public Type _type;
        private string _requestType;

        public XmlSerializerHelper(string requestType)
        {
            _type = typeof(T);
            _requestType = requestType;
        }

        public string ToXML(object obj)
        {
            using (StringWriter stringWriter = new StringWriter())
            {
                XmlSerializer serializer = new XmlSerializer(_type);
                serializer.Serialize(stringWriter, obj);
                return stringWriter.ToString();
            }
        }

        public T Read(string xmlString)
        {
            T result;
            using (StringReader stringReader = new StringReader(xmlString))
            {
                XmlSerializer deserializer = new XmlSerializer(_type);
                result = (T)deserializer.Deserialize(stringReader);
            }
            return result;
        }
    }
}
