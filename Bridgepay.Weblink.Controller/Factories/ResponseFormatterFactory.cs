﻿
using Bridgepay.Weblink.Controller.ResponseFormatters;
using Bridgepay.Weblink.Core.Interfaces;
using Bridgepay.Weblink.Core.Models;

namespace Bridgepay.Weblink.Controller.Factories
{
    public static class ResponseFormatterFactory
    {
        public static IResponseFormatter CreateResponseFormatter(string requestType, string response)
        {
            if (System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(response)).Contains("ErrorResponse"))
            {
                return new ErrorResponseFormatter(requestType, response);
            }
            else
                switch (requestType)
                {
                    case BridgeCommRequestTypes.MerchantInfoRequest:
                        return new MerchantInfoResponseFormatter(response);
                    case BridgeCommRequestTypes.VoidRefundRequest:
                        return new VoidRefundResponseFormatter(response);
                    case BridgeCommRequestTypes.TransactionRequest:
                        return new TransactionResponseFormatter(response);
                    default:
                        return null;
                }
        }
    }
}