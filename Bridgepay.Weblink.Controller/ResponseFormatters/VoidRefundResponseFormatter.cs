﻿using Bridgepay.Weblink.Controller.Models;
using Bridgepay.Weblink.Core.Interfaces;
using Bridgepay.Weblink.Core.Models;
using System;

namespace Bridgepay.Weblink.Controller.ResponseFormatters
{
    public class VoidRefundResponseFormatter : BaseResponseFormatter
    {

        public VoidRefundResponseFormatter(string response):base(response)
        {

        }
        public override IResponse FormatResponse()
        {
            TransactionResponse transactionResponse = new TransactionResponse();

            dynamic responseObject = DynamicXml.Parse(_response);

            int bridgeCommResult = 0;
            if (!int.TryParse(responseObject.ResponseCode, out bridgeCommResult))
            {
                bridgeCommResult = 99;
            }
            transactionResponse.Result = bridgeCommResult.ToString();
            transactionResponse.Message = responseObject.ResponseDescription;

            if (responseObject.responseMessage != null)
            {
                transactionResponse.HostCode = responseObject.responseMessage.ReferenceNumber;
                transactionResponse.Id = responseObject.responseMessage.GatewayTransId;
            }

            return transactionResponse;

        }
    }
}