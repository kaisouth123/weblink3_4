﻿using Bridgepay.Weblink.Controller.Models;
using Bridgepay.Weblink.Core.Interfaces;
using Bridgepay.Weblink.Core.Models;
using Bridgepay.WebLink.Core.Helpers;
using System;

namespace Bridgepay.Weblink.Controller.ResponseFormatters
{
    public class TransactionResponseFormatter : BaseResponseFormatter
    {

        public TransactionResponseFormatter(string response) :base(response)
        {

        }
        public override IResponse FormatResponse()
        {
            TransactionResponse transactionResponse = new TransactionResponse();

            dynamic responseObject = DynamicXml.Parse(_response);

            int bridgeCommResult = 0;
            if (!int.TryParse(responseObject.ResponseCode, out bridgeCommResult))
            {
                bridgeCommResult = 99;
            }
            transactionResponse.Result = bridgeCommResult.ToString();
            transactionResponse.Message = responseObject.ResponseDescription;

            if (responseObject.responseMessage != null)
            {
                transactionResponse.AuthCode = responseObject.responseMessage.AuthorizationCode ?? string.Empty;
                transactionResponse.CardType = responseObject.responseMessage.CardType ?? string.Empty;
                transactionResponse.ExpDate = responseObject.responseMessage.ExpirationDate ?? string.Empty;
                transactionResponse.HostCode = responseObject.responseMessage.ReferenceNumber ?? string.Empty;
                transactionResponse.Id = responseObject.responseMessage.GatewayTransID ?? string.Empty;
                bool isCommercialCard;
                if (!bool.TryParse(responseObject.responseMessage.IsCommercialCard, out isCommercialCard))
                    isCommercialCard = false;
                transactionResponse.IsCommercialCard = isCommercialCard;

                transactionResponse.Token = responseObject.responseMessage.Token ?? string.Empty;

                if (responseObject.responseMessage.Token != null &&
                    responseObject.responseMessage.Token.Length >= 4)
                {
                    transactionResponse.LastFour = responseObject.responseMessage.Token.Substring(responseObject.responseMessage.Token.Length - 4);
                }


                int originalServiceFeeAmount = 0;
                int authorizedServiceFeeAmount = 0;
                int actualServiceFeeAmount = 0;


                bool serviceFeeExists = responseObject.responseMessage.ServiceFeeResult != null && !string.IsNullOrEmpty(responseObject.responseMessage.ServiceFeeResult.GatewayResult);
                if (serviceFeeExists)
                {
                    originalServiceFeeAmount = TranslateAmount(responseObject.responseMessage.ServiceFeeResult.OriginalAmount);
                    authorizedServiceFeeAmount = TranslateAmount(responseObject.responseMessage.ServiceFeeResult.AuthorizedAmount);
                    if (responseObject.responseMessage.ServiceFeeResult.GatewayResult == "00000")
                        actualServiceFeeAmount = authorizedServiceFeeAmount;
                    else
                        actualServiceFeeAmount = 0;
                }

                int originalPrincipalAmount;
                int authorizedPrincipalAmount;
                int actualPrincipalAmount;

                originalPrincipalAmount = TranslateAmount(responseObject.responseMessage.OriginalAmount);
                authorizedPrincipalAmount = TranslateAmount(responseObject.responseMessage.AuthorizedAmount);
                if (responseObject.responseMessage.GatewayResult == "00000")
                    actualPrincipalAmount = authorizedPrincipalAmount;
                else
                    actualPrincipalAmount = 0;

                int finalOriginalAmount = originalPrincipalAmount + originalServiceFeeAmount;
                int finalAuthorizedAmount = authorizedPrincipalAmount + authorizedServiceFeeAmount;
                int finalActualAmount = actualPrincipalAmount + actualServiceFeeAmount;

                transactionResponse.OriginalAmount = Helper.FromBridgeCommAmount(finalOriginalAmount);
                transactionResponse.AuthorizedAmount = Helper.FromBridgeCommAmount(finalAuthorizedAmount);
                transactionResponse.ActualAmount = Helper.FromBridgeCommAmount(finalActualAmount);

                transactionResponse.ServiceFeeResponse.OriginalAmount = Helper.FromBridgeCommAmount(originalServiceFeeAmount);
                transactionResponse.ServiceFeeResponse.AuthorizedAmount = Helper.FromBridgeCommAmount(authorizedServiceFeeAmount);
                transactionResponse.ServiceFeeResponse.ActualAmount = Helper.FromBridgeCommAmount(actualServiceFeeAmount);

                if (responseObject.responseMessage.ServiceFeeResult != null)
                {
                    transactionResponse.ServiceFeeResponse.AuthCode = responseObject.responseMessage.ServiceFeeResult.AuthorizationCode ?? string.Empty;
                    transactionResponse.ServiceFeeResponse.HostCode = string.Empty;
                    transactionResponse.ServiceFeeResponse.Message = responseObject.responseMessage.ServiceFeeResult.GatewayMessage ?? string.Empty;
                    transactionResponse.ServiceFeeResponse.MessageEx = responseObject.responseMessage.ServiceFeeResult.InternalMessage ?? string.Empty;
                    if (!int.TryParse(responseObject.responseMessage.ServiceFeeResult.GatewayResult, out bridgeCommResult))
                    {
                        bridgeCommResult = 99;
                    }

                    transactionResponse.ServiceFeeResponse.Result = bridgeCommResult.ToString();
                    transactionResponse.ServiceFeeResponse.ServiceID = responseObject.responseMessage.ServiceFeeResult.GatewayTransID ?? string.Empty;
                }
            }
            return transactionResponse;

        }

        private int TranslateAmount(dynamic authorizedAmount)
        {
            int amount;
            if (!int.TryParse(authorizedAmount, out amount))
                amount = 0;
            return amount;
        }
    }
}