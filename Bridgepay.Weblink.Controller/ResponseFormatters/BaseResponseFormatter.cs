﻿using Bridgepay.Weblink.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.Weblink.Controller.ResponseFormatters
{
    public abstract class BaseResponseFormatter : IResponseFormatter
    {

        protected string _response;

        public BaseResponseFormatter(string response)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(response);
            _response = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public abstract IResponse FormatResponse();
    }
}
