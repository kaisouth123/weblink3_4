﻿using Bridgepay.Weblink.Controller.Models;
using Bridgepay.Weblink.Core.Interfaces;
using Bridgepay.Weblink.Core.Models;
using System;

namespace Bridgepay.Weblink.Controller.ResponseFormatters
{
    public class MerchantInfoResponseFormatter : BaseResponseFormatter
    {

        public MerchantInfoResponseFormatter(string response) : base(response) { }
        public override IResponse FormatResponse()
        {

            MerchantInfoResponse merchantInfoResponse = new MerchantInfoResponse();

            dynamic responseObject = DynamicXml.Parse(_response);

            int bridgeCommResult = 0;
            if (!int.TryParse(responseObject.ResponseCode, out bridgeCommResult))
            {
                bridgeCommResult = 99;
            }
            merchantInfoResponse.Result = bridgeCommResult.ToString();
            merchantInfoResponse.Message = responseObject.ResponseDescription;

            if (responseObject.responseMessage != null)
            {
                merchantInfoResponse.MerchantName = responseObject.responseMessage.MerchantName;
                merchantInfoResponse.MID = responseObject.responseMessage.MerchantAccountCode;
                merchantInfoResponse.ExtraData[Fields.MerchantAccountCode] = responseObject.responseMessage.MerchantAccountCode;
                merchantInfoResponse.ExtraData[Fields.MerchantCode] = responseObject.responseMessage.MerchantCode;
                merchantInfoResponse.MessageEx = "Gateway transaction Id: " + responseObject.responseMessage.GatewayTransId; // Eh, why not...

                if (string.IsNullOrEmpty(responseObject.responseMessage.MerchantAccountCode))
                {
                    merchantInfoResponse.Result = "99";
                    merchantInfoResponse.Message = "Could not locate an active user with that login.";
                }
            }

            return merchantInfoResponse;

        }
    }
}