﻿using Bridgepay.Weblink.Controller.Models;
using Bridgepay.Weblink.Core.Interfaces;
using Bridgepay.Weblink.Core.Models;
using System;

namespace Bridgepay.Weblink.Controller.ResponseFormatters
{
    public class ErrorResponseFormatter : BaseResponseFormatter
    {
        private string _requestType;
        public ErrorResponseFormatter(string requestType, string response) : base(response)
        {
            this._requestType = requestType;
        }
        public override IResponse FormatResponse()
        {
            IResponse thisResponse;
            switch (_requestType)
            {
                case BridgeCommRequestTypes.MerchantInfoRequest:
                    thisResponse = new MerchantInfoResponse();
                    break;
                case BridgeCommRequestTypes.TransactionRequest:
                    thisResponse = new TransactionResponse();
                    break;
                case BridgeCommRequestTypes.VoidRefundRequest:
                    thisResponse = new TransactionResponse();
                    break;
                default:
                    throw new InvalidCastException(string.Format("Cannot create standardize request type from error response: {0}", _requestType));
            }

            dynamic responseObject = DynamicXml.Parse(_response);

            int bridgeCommResult = 0;
            if (!int.TryParse(responseObject.ResponseCode, out bridgeCommResult))
            {
                bridgeCommResult = 99;
            }
            thisResponse.Result = bridgeCommResult.ToString();
            thisResponse.Message = responseObject.ResponseDescription;
            thisResponse.Result = "99";

            return thisResponse;

        }
    }
}