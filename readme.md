[![forthebadge](https://forthebadge.com/images/badges/made-with-python.svg)](https://forthebadge.com)

# Table of Contents
- [Installing Git Bash](#installing-git-bash)
- [Contributing](#contributing)
- [License](#license)

# Installing Git Bash

[(Back to top)](#table-of-contents)

Test test 2

1. Go to the [Git Bash Website](https://gitforwindows.org/) and click on the download button. Then save it to your computer somewhere.
2. Once downloaded then open up Git Bash. A terminal will appear and will wait for your input. Here are some start commands to get you up and running with Git Bash.
	
	Prints your current working directory, or location of where you are on your computer.
	```bash	
	pwd
	``` 
	
# Contributing

[(Back to top)](#table-of-contents)

Meticulously crafted by our fellow [contributers](CONTRIBUTING.md).

# License

[(Back to top)](#table-of-contents)


GNU Affero General Public License v3.0 - 2007 - [Kai South](https://www.gnu.org/licenses/). Please have a look at the [LICENSE.md](LICENSE.md) for more details.


[![BridgePay](https://bridgepaynetwork.com/wp-content/uploads/2018/07/BridgePaylogoFinalforMS.png)](https://bridgepaynetwork.com/)