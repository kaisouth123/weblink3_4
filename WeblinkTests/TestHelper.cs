﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace WeblinkTests
{
    public static class TestHelper
    {
        //private const string WeblinkUrl = "http://localhost:58111/weblink.aspx";
        private const string WeblinkUrl = "https://www.bridgepaynetsecuretest.com/weblink3/weblink.aspx";

        public static XElement PostToWeblinkDCP(NameValueCollection data)
        {
            return XElement.Parse(PostToWeblink(data));
        }

        public static string PostToWeblink(NameValueCollection data)
        {
            using (var client = new WebClient())
            {
                byte[] responseBytes = client.UploadValues(WeblinkUrl, data);
                string responseText = Encoding.UTF8.GetString(responseBytes);

                return responseText;
            }
        }
    }
}
