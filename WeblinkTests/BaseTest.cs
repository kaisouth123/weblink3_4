﻿using Bridgepay.Weblink.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using WebLINK.Common;


namespace WeblinkTests
{
    public abstract class BaseTest
    {
        //protected const string Login = "weblinktest1";
        //protected const string Password = "EAAAAF/oYUPSzJZ7TGFZ9bkPjyE75VVBeYEFsiJoXFCxOfDJ";
        protected const string Login = "pgdemo";
        protected const string Password = "Bpn!2015";

        protected Dictionary<string, string> PostValues { get; set; }
        protected Dictionary<string, string> ExpectedValues { get; set; }
        protected List<string> ExpectedFields { get; set; }

        public BaseTest()
        {
            PostValues = new Dictionary<string, string>();
            ExpectedValues = new Dictionary<string, string>();
            ExpectedFields = new List<string>();
            PostValues.Add(Fields.Login, Login);
            //PostValues.Add(Fields.PasswordEncrypted, Password);
            PostValues.Add(Fields.Password, Password);
        }

        public void AddOrUpdatePostValue(string key, string value)
        {
            PostValues[key] = value;
        }

        public abstract void Execute();
        public abstract void AssertMe();

        protected bool IsMockPaymentServer
        {
            get
            {
                var paymentServerType = PostValues.ContainsKey(Fields.PaymentServerType) ? PostValues[Fields.PaymentServerType] : string.Empty;

                return paymentServerType.Equals("Mock", StringComparison.CurrentCultureIgnoreCase);
            }
        }
    }
}
