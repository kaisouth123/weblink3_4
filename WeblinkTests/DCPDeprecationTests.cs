﻿using Bridgepay.Core.Logging;
using Bridgepay.Weblink.Core.Helpers;
using Bridgepay.Weblink.Core.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Specialized;

namespace WeblinkTests
{
    [TestClass]
    public class DCPDeprecationTests
    {
        [TestInitialize]
        public void Setup()
        {
            LogProvider.Configure(null);
        }

        [TestMethod]
        public void PurchaseTokenShouldFailDCPAttempt()
        {
            var helper = new DCPEndOfLifeHelper("alice, bob, carol", "12/31/2021", "2/15/2021");
            var now = new DateTime(2021, 3, 1);

            var form = new NameValueCollection
            {
                { Fields.PurchaseToken, "123ABC" }
            };

        Assert.IsFalse(helper.IsDCPModeAllowed(form, now));
        }

        [TestMethod]
        public void PassThroughUserShouldSucceedDCPAttempt()
        {
            var helper = new DCPEndOfLifeHelper("alice, bob, carol", "12/31/2021", "2/15/2021");
            var now = new DateTime(2021, 3, 1);

            var form = new NameValueCollection
            {
                { Fields.Login, "carol" }
            };

            Assert.IsTrue(helper.IsDCPModeAllowed(form, now));
        }

        [TestMethod]
        public void NonPassThroughUserShouldFailDCPAttempt()
        {
            var helper = new DCPEndOfLifeHelper("alice, bob, carol", "12/31/2021", "2/15/2021");
            var now = new DateTime(2021, 3, 1);

            var form = new NameValueCollection
            {
                { Fields.Login, "dave" }
            };

            Assert.IsFalse(helper.IsDCPModeAllowed(form, now));
        }

        [TestMethod]
        public void DeprecatedPassThroughListShouldFailDCPAttempt()
        {
            var helper = new DCPEndOfLifeHelper("alice, bob, carol", "12/31/2021", "2/15/2021");
            var now = new DateTime(2022, 1, 1);

            var form = new NameValueCollection
            {
                { Fields.Login, "alice" }
            };

            Assert.IsFalse(helper.IsDCPModeAllowed(form, now));
        }

        [TestMethod]
        public void NoPassThroughListDeprecationShouldPassDCPAttempt()
        {
            var helper = new DCPEndOfLifeHelper("alice, bob, carol", null, "2/15/2021");
            var now = new DateTime(2021, 3, 1);

            var form = new NameValueCollection
            {
                { Fields.Login, "alice" }
            };

            Assert.IsTrue(helper.IsDCPModeAllowed(form, now));
        }

        [TestMethod]
        public void DCPNotDeprecatedYetShouldPassDCPAttempt()
        {
            var helper = new DCPEndOfLifeHelper("alice, bob, carol", "12/31/2021", "2/15/2021");
            var now = new DateTime(2021, 2, 1);

            var form = new NameValueCollection
            {
                { Fields.Login, "steve" }
            };

            Assert.IsTrue(helper.IsDCPModeAllowed(form, now));
        }

        [TestMethod]
        public void WhitespaceUserShouldFailDCPAttempt()
        {
            var helper = new DCPEndOfLifeHelper("alice, bob, carol", "12/31/2021", "2/15/2021");
            var now = new DateTime(2021, 3, 1);

            var form = new NameValueCollection
            {
                { Fields.Login, null }
            };

            Assert.IsFalse(helper.IsDCPModeAllowed(form, now));
        }
    }
}
