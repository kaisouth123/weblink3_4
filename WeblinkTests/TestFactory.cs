﻿using Bridgepay.Weblink.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using WebLINK.Common;

namespace WeblinkTests
{
    public static class TestFactory
    {
        public static DCPTest DCPSaleWithWalletCreation()
        {
            DCPTest test = new DCPTest();

            test.AddOrUpdatePostValue(Fields.TotalAmt, "1.00");
            test.AddOrUpdatePostValue(Fields.CardNumber, "4111111111111111");
            test.AddOrUpdatePostValue(Fields.CardExpYear, "2020");
            test.AddOrUpdatePostValue(Fields.CardExpMonth, "12");
            test.AddOrUpdatePostValue(Fields.CVNum, "999");
            test.AddOrUpdatePostValue(Fields.EnableWallet, "1");
            test.AddOrUpdatePostValue(Fields.Address1, "123 N unit test st");
            test.AddOrUpdatePostValue(Fields.City, "Unit testville");
            test.AddOrUpdatePostValue(Fields.State, "IL");
            test.AddOrUpdatePostValue(Fields.ZipCode, "60142");
            test.AddOrUpdatePostValue(Fields.FullName, "Unit Test");
            test.AddOrUpdatePostValue(Fields.CardTypeName, "Visa");
            //test.AddOrUpdatePostValue(Fields.SiteId, Constants.SiteId);
            test.AddOrUpdatePostValue(Fields.SiteId, "b68df443-e633-49ca-be95-d57733f533fb");

            test.AddOrUpdateExpectedValue(Fields.r_Result, "0");
            test.AddOrUpdateExpectedValue(Fields.r_WalletResultCode, "0");

            test.AddExpectedField(Fields.r_WalletToken);
            test.AddExpectedField(Fields.r_WalletKey);
            test.AddExpectedField(Fields.r_WalletMessage);

            return test;
        }

        public static DCPTest DCPSaleViaToken()
        {
            DCPTest test = new DCPTest();

            test.AddOrUpdatePostValue(Fields.TotalAmt, "1.00");
            test.AddOrUpdatePostValue(Fields.Token, Constants.TestCardToken);
            test.AddOrUpdatePostValue(Fields.CardExpYear, "2020");
            test.AddOrUpdatePostValue(Fields.CardExpMonth, "12");
            test.AddOrUpdatePostValue(Fields.CVNum, "999");
            test.AddOrUpdatePostValue(Fields.Address1, "123 N unit test st");
            test.AddOrUpdatePostValue(Fields.City, "Unit testville");
            test.AddOrUpdatePostValue(Fields.State, "IL");
            test.AddOrUpdatePostValue(Fields.ZipCode, "60142");
            test.AddOrUpdatePostValue(Fields.FullName, "Unit Test");
            test.AddOrUpdatePostValue(Fields.CardTypeName, "Visa");

            test.AddOrUpdateExpectedValue(Fields.r_Result, "0");

            return test;
        }

        public static DCPTest DCPSaleViaWallet(XElement response)
        {
            DCPTest test = new DCPTest();

            test.AddOrUpdatePostValue(Fields.TotalAmt, "1.00");
            test.AddOrUpdatePostValue(Fields.EnableWallet, "1");
            test.AddOrUpdatePostValue(Fields.WalletToken, response.GetElementValueOrDefault(Fields.r_WalletToken));
            test.AddOrUpdatePostValue(Fields.WalletKey, response.GetElementValueOrDefault(Fields.r_WalletKey));

            test.AddExpectedField(Fields.WalletCardNumber);
            test.AddExpectedField(Fields.WalletExpDate);
            test.AddExpectedField(Fields.WalletCardType);

            return test;
        }

        public static DCPTest DCPSplitTransactionSale()
        {
            DCPTest test = new DCPTest();

            test.AddOrUpdatePostValue(Fields.TotalAmt, "1.00");
            test.AddOrUpdatePostValue(Fields.CardNumber, "4111111111111111");
            test.AddOrUpdatePostValue(Fields.CardExpYear, "2020");
            test.AddOrUpdatePostValue(Fields.CardExpMonth, "12");
            test.AddOrUpdatePostValue(Fields.CVNum, "999");
            test.AddOrUpdatePostValue(Fields.Address1, "123 N unit test st");
            test.AddOrUpdatePostValue(Fields.City, "Unit testville");
            test.AddOrUpdatePostValue(Fields.State, "IL");
            test.AddOrUpdatePostValue(Fields.ZipCode, "60142");
            test.AddOrUpdatePostValue(Fields.FullName, "Unit Test");
            test.AddOrUpdatePostValue(Fields.CardTypeName, "Visa");
            test.AddOrUpdatePostValue(Fields.TransactionSequenceNum, "1");

            test.AddOrUpdatePostValue(Fields.TransactionSequenceNum + "1", "2");
            test.AddOrUpdatePostValue(Fields.TotalAmt + "1", "1.00");
            test.AddOrUpdatePostValue(Fields.Login + "1", Constants.Login);
            test.AddOrUpdatePostValue(Fields.PasswordEncrypted + "1", Constants.PasswordEncrypted);

            test.AddExpectedField(Fields.r_AuthCode + "1");
            //test.AddExpectedField(Fields.r_HostCode + "1");
            test.AddExpectedField(Fields.r_Id + "1");
            test.AddExpectedField(Fields.r_Token + "1");
            test.AddExpectedField(Fields.r_Message + "1");
            test.AddExpectedField(Fields.r_Result + "1");
            test.AddExpectedField(Fields.r_CardType + "1");
            test.AddExpectedField(Fields.r_LastFour + "1");
            test.AddExpectedField(Fields.r_AuthorizedAmount + "1");

            return test;
        }

        public static DCPTest DCPSplitTransactionSaleViaWallet()
        {
            DCPTest test = new DCPTest();

            test.AddOrUpdatePostValue(Fields.TotalAmt, "1.00");
            test.AddOrUpdatePostValue(Fields.CardNumber, "4111111111111111");
            test.AddOrUpdatePostValue(Fields.CardExpYear, "2020");
            test.AddOrUpdatePostValue(Fields.CardExpMonth, "12");
            test.AddOrUpdatePostValue(Fields.CVNum, "999");
            test.AddOrUpdatePostValue(Fields.Address1, "123 N unit test st");
            test.AddOrUpdatePostValue(Fields.City, "Unit testville");
            test.AddOrUpdatePostValue(Fields.State, "IL");
            test.AddOrUpdatePostValue(Fields.ZipCode, "60142");
            test.AddOrUpdatePostValue(Fields.FullName, "Unit Test");
            test.AddOrUpdatePostValue(Fields.CardTypeName, "Visa");
            test.AddOrUpdatePostValue(Fields.TransactionSequenceNum, "1");
            test.AddOrUpdatePostValue(Fields.EnableWallet, "true");
            test.AddOrUpdatePostValue(Fields.WalletKey, Constants.TestWalletKey);
            test.AddOrUpdatePostValue(Fields.WalletToken, Constants.TestWalletToken);

            test.AddOrUpdatePostValue(Fields.TransactionSequenceNum + "1", "2");
            test.AddOrUpdatePostValue(Fields.TotalAmt + "1", "1.00");
            test.AddOrUpdatePostValue(Fields.Login + "1", Constants.Login);
            test.AddOrUpdatePostValue(Fields.PasswordEncrypted + "1", Constants.PasswordEncrypted);

            test.AddExpectedField(Fields.r_AuthCode + "1");
            //test.AddExpectedField(Fields.r_HostCode + "1");
            test.AddExpectedField(Fields.r_Id + "1");
            test.AddExpectedField(Fields.r_Token + "1");
            test.AddExpectedField(Fields.r_Message + "1");
            test.AddExpectedField(Fields.r_Result + "1");
            test.AddExpectedField(Fields.r_CardType + "1");
            test.AddExpectedField(Fields.r_LastFour + "1");
            test.AddExpectedField(Fields.r_AuthorizedAmount + "1");

            return test;
        }

        public static StandardTest StandardPaymentForm()
        {
            StandardTest test = new StandardTest();

            test.AddOrUpdatePostValue(Fields.Mode, "PaymentForm");

            return test;
        }
    }
}
