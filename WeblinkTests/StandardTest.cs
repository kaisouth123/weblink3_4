﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WeblinkTests
{
    /// <summary>
    /// For performing a non-DCP mode test
    /// </summary>
    public class StandardTest : BaseTest
    {
        protected string Response;

        private List<string> MustExistRegExSearchStrings;
        private List<string> MustNotExistRegExSearchStrings;

        public StandardTest() : base()
        {
            MustExistRegExSearchStrings = new List<string>();
            MustNotExistRegExSearchStrings = new List<string>();
        }

        public override void Execute()
        {
            Response = TestHelper.PostToWeblink(PostValues.ToNameValueCollection());
        }

        public override void AssertMe()
        {
            Assert.IsFalse(string.IsNullOrEmpty(Response));
            Assert.IsTrue(Response.Contains("Weblink"));

            // Ensure all the regex search strings are found
            foreach (var searchString in MustExistRegExSearchStrings)
            {
                Regex re = new Regex(searchString);
                Assert.IsTrue(re.IsMatch(Response));
            }

            // Ensure strings that shouldnt exist, dont
            foreach (var searchString in MustNotExistRegExSearchStrings)
            {
                Regex re = new Regex(searchString);
                Assert.IsFalse(re.IsMatch(Response));
            }
        }

        public void EnsureFieldExists(string fieldName)
        {
            Regex re = new Regex(string.Format("name=[\"']{0}[\"']", fieldName));
            Assert.IsTrue(re.IsMatch(Response), string.Format("Field {0} not found!", fieldName));
        }

        public void EnsureFieldDoesNotExist(string fieldName)
        {
            Regex re = new Regex(string.Format("name=[\"']{0}[\"']", fieldName));
            Assert.IsFalse(re.IsMatch(Response), string.Format("Found field {0}, but it shouldnt exist!", fieldName));
        }

        public void AddMustExistRegexSearchString(string searchString)
        {
            if (!MustExistRegExSearchStrings.Contains(searchString))
            {
                MustExistRegExSearchStrings.Add(searchString);
            }
        }

        public void AddMustNotExistRegexSearchString(string searchString)
        {
            if (!MustNotExistRegExSearchStrings.Contains(searchString))
            {
                MustNotExistRegExSearchStrings.Add(searchString);
            }
        }
    }
}
