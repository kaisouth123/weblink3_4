﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using WebLINK;

namespace WeblinkTests
{
    [TestClass]
    public class IFrameDomainTests
    {
        [TestMethod]
        public void TestSplitIFrameDomainsPositive()
        {
            string iFrameDomains = "   *.muni-link.com,   *.muni-link.lan,\t*.developer,   *.ws.muni-link.local,   local.ws.muni-link.local ;  ";

            var results = weblink.SplitIFrameDomains(iFrameDomains).OrderBy(d => d);

            Assert.AreEqual(6, results.Count());
            Assert.AreEqual(string.Empty, results.First());
            Assert.AreEqual("*.developer", results.Skip(1).First());
            Assert.AreEqual("*.muni-link.com", results.Skip(2).First());
            Assert.AreEqual("*.muni-link.lan", results.Skip(3).First());
            Assert.AreEqual("*.ws.muni-link.local", results.Skip(4).First());
            Assert.AreEqual("local.ws.muni-link.local", results.Skip(5).First());
        }

        [TestMethod]
        public void TestSplitIFrameDomainsNegative()
        {
            string iFrameDomains = "   \t   ";

            var results = weblink.SplitIFrameDomains(iFrameDomains);

            Assert.IsNull(results);
        }

        [TestMethod]
        public void TestGetRootDomainNoSubdomain()
        {
            Uri uri = new Uri("https://google.com/im-a-referrer/so-there");

            string rootDomain = weblink.GetRootDomain(uri.Host);

            Assert.AreEqual("google.com", rootDomain);
        }

        [TestMethod]
        public void TestGetRootDomainMultipleSubdomains()
        {
            Uri uri = new Uri("https://Csi10a.erpsl.inforcloudsuite.com/im-a-referrer/so-there");

            string rootDomain = weblink.GetRootDomain(uri.Host);

            Assert.AreEqual("inforcloudsuite.com", rootDomain);
        }

        [TestMethod]
        public void TestGetIFrameDomainWildcardPositive()
        {
            Uri uri = new Uri("https://Csi10a.erpsl.inforcloudsuite.com/im-a-referrer/so-there");
            string iFrameDomains = "gac-portal.gac.awsdev.infor.com, Mingle-Portal.inforcloudsuite.com, gdeinfor2.com,inforbc.com, infor.com, inforcloudsuite.com, usalvwl00000.infor.com,csi10a.erpsl.inforcloudsuite.com, csimobile.infor.com, apptrix.com, *.inforcloudsuite.com";

            string rootDomain = weblink.GetRootDomain(uri.Host);
            string matchingIFrameDomain = weblink.GetIFrameDomain(iFrameDomains, uri.Host, rootDomain);

            // Note that the wildcard takes precedence, even though an exact match exists.
            Assert.AreEqual("*.inforcloudsuite.com", matchingIFrameDomain);
        }

        [TestMethod]
        public void TestGetIFrameDomainNegative()
        {
            Uri uri = new Uri("https://Csi10a.erpsl.inforcloudsuite.com/im-a-referrer/so-there");
            string iFrameDomains = "  *.invisible.edu , *.inforloudsuite.com,  ford.com;     ; ,,  ";

            string rootDomain = weblink.GetRootDomain(uri.Host);
            string matchingIFrameDomain = weblink.GetIFrameDomain(iFrameDomains, uri.Host, rootDomain);

            Assert.IsNull(matchingIFrameDomain);
        }

        [TestMethod]
        public void TestGetIFrameDomainFQDNPositive()
        {
            Uri uri = new Uri("https://Csi10a.erpsl.inforcloudsuite.com/im-a-referrer/so-there");
            string iFrameDomains = "gac-portal.gac.awsdev.infor.com, Mingle-Portal.inforcloudsuite.com, gdeinfor2.com,inforbc.com, infor.com, inforcloudsuite.com, usalvwl00000.infor.com,csi10a.erpsl.inforcloudsuite.com, csimobile.infor.com, apptrix.com";

            string rootDomain = weblink.GetRootDomain(uri.Host);
            string match = weblink.GetIFrameDomain(iFrameDomains, uri.Host, rootDomain);

            Assert.AreEqual("csi10a.erpsl.inforcloudsuite.com", match);
        }
    }
}
