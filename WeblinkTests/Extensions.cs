﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace WeblinkTests
{
    public static class Extensions
    {
        public static NameValueCollection ToNameValueCollection(this Dictionary<string, string> data)
        {
            NameValueCollection nvc = new NameValueCollection();

            foreach (var item in data)
            {
                nvc.Add(item.Key, item.Value);
            }

            return nvc;
        }

        public static string GetElementValueOrDefault(this XElement element, string elementName, string defaultValue = null)
        {
            XElement el = element.Element(elementName);
            return el != null ? el.Value : defaultValue;
        }
    }
}
