﻿using Bridgepay.Weblink.Core.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Specialized;
using WebLINK;

namespace WeblinkTests
{
    [TestClass]
    public class AuthenticationStatusLogTests
    {
        [TestMethod]
        public void UsernamePasswordProvidedShouldBeYes()
        {
            var form = new NameValueCollection
            {
                { Fields.Login, "myUserName" },
                { Fields.Password, "seekrit" }
            };

            var log = weblink.GetAuthenticationStatusLog(form);

            Assert.AreEqual("AuthenticationStatus: Username Provided: Yes, Username: myUserName, Password Provided: Yes, PasswordEncrypted: No, PurchaseToken Provided: No, Mode: DCP", log);
        }

        [TestMethod]
        public void UsernamePasswordEncryptedProvidedShouldBeTrue()
        {
            var form = new NameValueCollection
            {
                { Fields.Login, "myOtherUserName" },
                { Fields.PasswordEncrypted, "EAAAADpBboQZyGn9dFswVsLrXblvSb9CaO4SPnIBOMqZCKcM" }
            };

            var log = weblink.GetAuthenticationStatusLog(form);

            Assert.AreEqual("AuthenticationStatus: Username Provided: Yes, Username: myOtherUserName, Password Provided: No, PasswordEncrypted: Yes, PurchaseToken Provided: No, Mode: DCP", log);
        }

        [TestMethod]
        public void PurchaseTokenProvidedShouldBeTrue()
        {
            var form = new NameValueCollection
            {
                { Fields.PurchaseToken, "25a58177-b5b1-47f7-a02a-071907009930" }
            };

            var log = weblink.GetAuthenticationStatusLog(form);

            Assert.AreEqual("AuthenticationStatus: Username Provided: No, Username: , Password Provided: No, PasswordEncrypted: No, PurchaseToken Provided: Yes, Mode: DCP", log);
        }

        [TestMethod]
        public void ModeShouldBePaymentForm()
        {
            var form = new NameValueCollection
            {
                { Fields.Mode, "PaymentForm" }
            };

            var log = weblink.GetAuthenticationStatusLog(form);

            Assert.AreEqual("AuthenticationStatus: Username Provided: No, Username: , Password Provided: No, PasswordEncrypted: No, PurchaseToken Provided: No, Mode: PaymentForm", log);
        }
    }
}