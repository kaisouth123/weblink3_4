﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WeblinkTests
{
    //[TestClass]
    public class Webservice
    {
        private const string SettingName = "UnitTestSetting";
        private const string SettingValue = "UnitTestValue";
        private int TempWLID;
        private WeblinkAdmin.weblinkSoapClient client = null;

        private List<int> WeblinkIdsToDelete = new List<int>();
        private List<int> SettingIdsToDelete = new List<int>();

        [TestInitialize]
        public void PrepareTestData()
        {
            client = new WeblinkAdmin.weblinkSoapClient("weblinkSoap");

            TempWLID = client.CreateWeblinkIdForBPN(999999, "UnitTests");

            Assert.AreNotEqual(TempWLID, 0);
            WeblinkIdsToDelete.Add(TempWLID);

            client.AddSettingName(SettingName);
        }

        [TestCleanup]
        public void CleanUp()
        {
            foreach (var settingId in SettingIdsToDelete)
                client.DeleteSetting(settingId);

            foreach (var wlid in WeblinkIdsToDelete)
                client.DeleteWeblinkId(wlid);

            client.DeleteSettingName(SettingName);

            client.Close();
        }

        [TestMethod]
        public void TestAuthorization()
        {
            client.Authorize(TempWLID);

            bool isAuthorized = client.IsAuthorized(TempWLID);
            client.DeAuthorize(TempWLID);

            Assert.AreEqual(isAuthorized, true);
        }

        [TestMethod]
        public void TestAddGlobalSetting()
        {
            int settingId;

            settingId = client.SaveSetting(null, null, SettingName, SettingValue);
            Assert.AreNotEqual(settingId, 0);

            var setting = client.GetDefaultSetting(SettingName);
            Assert.AreEqual(SettingValue, setting);

            client.DeleteSetting(settingId);
        }

        [TestMethod]
        public void TestAddMerchantSetting()
        {
            // Global setting must exist before a merchant specific one can
            int settingId = client.SaveSetting(null, null, SettingName, SettingValue);
            SettingIdsToDelete.Add(settingId);

            settingId = client.SaveSetting(null, TempWLID, SettingName, SettingValue + "1");
            Assert.AreNotEqual(settingId, 0);
            SettingIdsToDelete.Add(settingId);

            var settings = client.GetSettings(TempWLID);
            WeblinkAdmin.Setting setting = null;

            foreach(var s in settings)
                if (s.Name.Equals(SettingName, StringComparison.CurrentCultureIgnoreCase)) 
                {
                    setting = s;
                    break;
                }

            Assert.AreEqual(SettingValue + "1", setting.Value);
        }

        [TestMethod]
        public void TestCreateSystemTemplate()
        {
            client.AddTemplateType("UnitTest", "For unit testing only.");

            int templateId = client.SaveTemplate(null, null, null, "UnitTest", "Contents");
            var template = client.GetTemplate("UnitTest", null, null);
            client.DeleteTemplate(templateId);

            client.DeleteTemplateType("UnitTest");

            Assert.AreEqual(template, "Contents");
        }

        [TestMethod]
        public void TestCreateCustomTemplate()
        {
            client.AddTemplateType("UnitTest", "For unit testing only.");

            int templateId = client.SaveTemplate(null, TempWLID, 1, "UnitTest", "Contents_Custom");
            var template = client.GetTemplate("UnitTest", TempWLID, 1);
            client.DeleteTemplate(templateId);

            client.DeleteTemplateType("UnitTest");

            Assert.AreEqual(template, "Contents_Custom");
        }

        [TestMethod]
        public void TestSaveTemplateHtml()
        {
            client.AddTemplateType("UnitTest", "For unit testing only.");

            int templateId = client.SaveTemplate(null, TempWLID, 1, "UnitTest", "");
            client.SaveTemplateHtml(templateId, "Contents");

            string contents = client.GetTemplate("UnitTest", TempWLID, 1);
            client.DeleteTemplate(templateId);

            Assert.AreEqual(contents, "Contents");

            client.DeleteTemplateType("UnitTest");
        }

        [TestMethod]
        public void TestUpdateTemplate()
        {
            client.AddTemplateType("UnitTest", "For unit testing only.");

            int templateId = client.SaveTemplate(null, TempWLID, 1, "UnitTest", "");

            client.SaveTemplate(templateId, TempWLID, 1, "UnitTest", "Content");

            string content = client.GetTemplateById(templateId);
            client.DeleteTemplate(templateId);
            client.DeleteTemplateType("UnitTest");

            Assert.AreEqual(content, "Content");
        }

        [TestMethod]
        public void TestGetTemplate()
        {
            string template = client.GetTemplate("PaymentForm", null, null);

            Assert.AreNotEqual(template, string.Empty);
        }
    }
}
