﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebLINK.Common;
using Bridgepay.Weblink.Core.Models;

namespace WeblinkTests
{
    public class DCPTest : BaseTest
    {
        public XElement response;

        public override void Execute()
        {
            response = TestHelper.PostToWeblinkDCP(PostValues.ToNameValueCollection());
        }

        public override void AssertMe()
        {
            // Ensure all posted values are returned properly
            foreach (var postedValue in PostValues.Where(PostedFieldValidationFilter))
            {
                if (postedValue.Key.StartsWith(Fields.PasswordEncrypted))
                {
                    continue;
                }
                Assert.AreEqual(postedValue.Value, response.GetElementValueOrDefault(postedValue.Key));
            }

            // Ensure expected values are valid
            foreach (var expectedValue in ExpectedValues)
            {
                Assert.AreEqual(expectedValue.Value, response.GetElementValueOrDefault(expectedValue.Key));
            }

            // Ensure expected fields are present
            foreach (var expectedField in ExpectedFields)
            {
                Assert.IsFalse(string.IsNullOrEmpty(response.GetElementValueOrDefault(expectedField)), 
                    string.Format("Field {0} missing!", expectedField));
            }
        }

        public void EnsureStandardTransactionResponseFieldsExist()
        {
            List<string> FieldsToCheck = new List<string>
            {
                Fields.r_AuthCode,
                //Fields.r_HostCode,
                Fields.r_Message,
                Fields.r_Result,
                Fields.r_Id
            };

            if (!IsMockPaymentServer)
            {
                FieldsToCheck.AddRange(new string[] 
                {
                    Fields.r_Token,
                    Fields.r_AuthorizedAmount,
                    Fields.r_ExpDate,
                    Fields.r_CardType,
                    Fields.r_LastFour
                });
            }

            foreach (var field in FieldsToCheck)
            {
                Assert.IsFalse(string.IsNullOrEmpty(response.GetElementValueOrDefault(field)), string.Format("Field {0} is missing!", field));
            }

        }

        public void AddOrUpdateExpectedValue(string key, string value)
        {
            ExpectedValues[key] = value;
        }

        public void AddExpectedField(string key)
        {
            if (!ExpectedFields.Contains(key))
            {
                ExpectedFields.Add(key);
            }
        }

        public string GetResponseValue(string field)
        {
            return response.GetElementValueOrDefault(field);
        }

        // Returns true if the field should be validated to have the same value coming in as going out.
        private bool PostedFieldValidationFilter(KeyValuePair<string,string> kvp)
        {
            switch (kvp.Key)
            {
                case Fields.PasswordEncrypted:
                case Fields.CardNumber:
                case Fields.CVNum:
                    return false;
                default:
                    return true;
            }
        }

        
    }
}
