﻿using System;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Net;
using System.Text;
using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebLINK.Common;
using Bridgepay.Weblink.Core.Models;

namespace WeblinkTests
{
    [TestClass]
    public class Weblink
    {
        [TestMethod]
        public void TestWeblinkPageLoadsProperly()
        {
            string content = null;
            using (var client = new WebClient())
            {
                content = client.DownloadString(Constants.WeblinkUrl);
            }

            Assert.IsTrue(!string.IsNullOrEmpty(content) && content.Contains("Weblink"));
        }

        [TestMethod]
        public void TestPaymentForm()
        {
            var test = TestFactory.StandardPaymentForm();

            test.AddOrUpdatePostValue("ReceiptReturnUrl", "\\PostToXML.aspx");

            test.Execute();
            test.AssertMe();

            test.EnsureFieldExists(Fields.FullName);
            test.EnsureFieldExists(Fields.Address1);
            test.EnsureFieldExists(Fields.ZipCode);
            test.EnsureFieldExists(Fields.CardTypeName);
            test.EnsureFieldExists(Fields.CardNumber);
            test.EnsureFieldExists(Fields.CardExpMonth);
            test.EnsureFieldExists(Fields.CardExpYear);
            test.EnsureFieldExists(Fields.Address2);
            test.EnsureFieldExists(Fields.City);
            test.EnsureFieldExists(Fields.State);
            test.EnsureFieldExists(Fields.Phone);
            test.EnsureFieldExists(Fields.Email);
            test.EnsureFieldExists(Fields.TotalAmt);
            test.EnsureFieldExists(Fields.CVNum);
            test.EnsureFieldExists(Fields.AccountNumber);
            test.EnsureFieldExists(Fields.TransitNumber);
            test.EnsureFieldExists(Fields.AccountType);
        }

        [TestMethod]
        public void TestPaymentFormWithWalletCreation()
        {
            var test = TestFactory.StandardPaymentForm();

            test.AddOrUpdatePostValue(Fields.EnableWallet, "true");
            test.AddOrUpdatePostValue(Fields.SiteId, Constants.SiteId);

            test.Execute();
            test.AssertMe();

            test.EnsureFieldExists(Fields.SavePreferredPaymentMethod);
        }

        [TestMethod]
        public void TestPaymentFormWithWalletUsage()
        {
            var test = TestFactory.StandardPaymentForm();

            test.AddOrUpdatePostValue(Fields.EnableWallet, "true");
            test.AddOrUpdatePostValue(Fields.WalletKey, Constants.TestWalletKey);
            test.AddOrUpdatePostValue(Fields.WalletToken, Constants.TestWalletToken);

            test.AddMustExistRegexSearchString("id=\"usePreferredPaymentMethod\"");

            test.Execute();
            test.AssertMe();
        }

        [TestMethod]
        public void TestPaymentFormWithJavaSwiping()
        {
            var test = TestFactory.StandardPaymentForm();

            test.AddOrUpdatePostValue(Fields.SwipeMethod, "Java");

            test.AddMustExistRegexSearchString("id=\"swipeButtons\"");

            test.Execute();
            test.AssertMe();
        }

        [TestMethod]
        public void TestTokenizationForm()
        {
            var test = TestFactory.StandardPaymentForm();

            test.AddOrUpdatePostValue(Fields.Mode, "TokenizationForm");

            test.Execute();
            test.AssertMe();

            test.EnsureFieldExists(Fields.FullName);
            test.EnsureFieldExists(Fields.Address1);
            test.EnsureFieldExists(Fields.ZipCode);
            test.EnsureFieldExists(Fields.CardTypeName);
            test.EnsureFieldExists(Fields.CardNumber);
            test.EnsureFieldExists(Fields.CardExpMonth);
            test.EnsureFieldExists(Fields.CardExpYear);

            test.EnsureFieldDoesNotExist(Fields.Address2);
            test.EnsureFieldDoesNotExist(Fields.City);
            test.EnsureFieldDoesNotExist(Fields.State);
            test.EnsureFieldDoesNotExist(Fields.Phone);
            test.EnsureFieldDoesNotExist(Fields.Email);
            test.EnsureFieldDoesNotExist(Fields.TotalAmt);
            test.EnsureFieldDoesNotExist(Fields.CVNum);
            test.EnsureFieldDoesNotExist(Fields.AccountNumber);
            test.EnsureFieldDoesNotExist(Fields.TransitNumber);
            test.EnsureFieldDoesNotExist(Fields.AccountType);
        }

        [TestMethod]
        public void TestSplitTransactionPaymentForm()
        {
            var test = TestFactory.StandardPaymentForm();

            test.AddOrUpdatePostValue(Fields.TransactionSequenceNum, "1");
            test.AddOrUpdatePostValue(Fields.TransactionSequenceNum + "1", "2");
            test.AddOrUpdatePostValue(Fields.Login + "1", Constants.Login);
            test.AddOrUpdatePostValue(Fields.PasswordEncrypted + "1", Constants.PasswordEncrypted);

            test.AddMustExistRegexSearchString("id=\"grandTotal\"");

            test.Execute();
            test.AssertMe();

            test.EnsureFieldExists(Fields.FullName);
            test.EnsureFieldExists(Fields.Address1);
            test.EnsureFieldExists(Fields.ZipCode);
            test.EnsureFieldExists(Fields.CardTypeName);
            test.EnsureFieldExists(Fields.CardNumber);
            test.EnsureFieldExists(Fields.CardExpMonth);
            test.EnsureFieldExists(Fields.CardExpYear);
            test.EnsureFieldExists(Fields.Address2);
            test.EnsureFieldExists(Fields.City);
            test.EnsureFieldExists(Fields.State);
            test.EnsureFieldExists(Fields.Phone);
            test.EnsureFieldExists(Fields.Email);
            test.EnsureFieldExists(Fields.TotalAmt);
            test.EnsureFieldExists(Fields.CVNum);
            test.EnsureFieldExists(Fields.AccountNumber);
            test.EnsureFieldExists(Fields.TransitNumber);
            test.EnsureFieldExists(Fields.AccountType);

            test.EnsureFieldExists(Fields.TotalAmt + "1");
        }

        [TestMethod]
        public void TestDCPSale()
        {
            DCPTest test = new DCPTest();

            test.AddOrUpdatePostValue(Fields.TotalAmt, "1.00");
            test.AddOrUpdatePostValue(Fields.CardNumber, "4111111111111111");
            test.AddOrUpdatePostValue(Fields.CardExpYear, "2020");
            test.AddOrUpdatePostValue(Fields.CardExpMonth, "12");
            test.AddOrUpdatePostValue(Fields.CVNum, "999");
            test.AddOrUpdatePostValue(Fields.CardTypeName, "Visa");
            //test.AddOrUpdatePostValue(Fields.PaymentServerType, "Mock");

            test.AddOrUpdateExpectedValue(Fields.r_Result, "0");

            test.Execute();
            test.EnsureStandardTransactionResponseFieldsExist();
            test.AssertMe();
        }

        [TestMethod]
        public void TestDCPSaleWithWalletCreation()
        {
            DCPTest test = TestFactory.DCPSaleWithWalletCreation();

            test.Execute();
            test.EnsureStandardTransactionResponseFieldsExist();
            test.AssertMe();
        }

        [TestMethod]
        public void TestDCPSaleViaWallet()
        {
            DCPTest testCreateWallet = TestFactory.DCPSaleWithWalletCreation();

            testCreateWallet.Execute();
            testCreateWallet.AssertMe();

            DCPTest testUseWallet = TestFactory.DCPSaleViaWallet(testCreateWallet.response);
            testUseWallet.Execute();

            testUseWallet.AssertMe();
        }

        [TestMethod]
        public void TestDCPTokenSale()
        {
            DCPTest test = TestFactory.DCPSaleViaToken();

            test.Execute();
            test.EnsureStandardTransactionResponseFieldsExist();
            test.AssertMe();
        }

        [TestMethod]
        public void TestDCPSplitTransactionSale()
        {
            DCPTest test = TestFactory.DCPSplitTransactionSale();

            test.Execute();
            test.EnsureStandardTransactionResponseFieldsExist();
            test.AssertMe();
        }

        [TestMethod]
        public void TestDCPSplitTransactionSaleViaWallet()
        {
            DCPTest test = TestFactory.DCPSplitTransactionSaleViaWallet();

            test.Execute();
            test.EnsureStandardTransactionResponseFieldsExist();
            test.AssertMe();
        }
    }
}
