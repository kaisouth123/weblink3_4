﻿using Bridgepay.Weblink.Core.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WeblinkTests
{
    [TestClass]
    public class MaskingTests
    {
        [TestMethod]
        public void PasswordShouldBeMasked()
        {
            const string Password = "ThisIsMyPassword";

            var masked = MaskingHelper.MaskPossiblySensitiveData(Password, "Password");

            Assert.IsNotNull(masked);
            Assert.AreNotEqual("ThisIsMyPassword", masked);
        }

        [TestMethod]
        public void SvcPasswordShouldBeMasked()
        {
            const string Password = "ThisIsMyPassword";

            var masked = MaskingHelper.MaskPossiblySensitiveData(Password, "svcpassword");

            Assert.IsNotNull(masked);
            Assert.AreNotEqual("ThisIsMyPassword", masked);
        }

        [TestMethod]
        public void PasswordEncryptedShouldBeMasked()
        {
            const string Password = "ThisIsMyPassword";

            var masked = MaskingHelper.MaskPossiblySensitiveData(Password, "PasswordEncrypted");

            Assert.IsNotNull(masked);
            Assert.AreNotEqual("ThisIsMyPassword", masked);
        }

        [TestMethod]
        public void NonPasswordShouldNotBeMasked()
        {
            const string Login = "InTheClear";

            var masked = MaskingHelper.MaskPossiblySensitiveData(Login, "Login");

            Assert.IsNotNull(masked);
            Assert.AreEqual("InTheClear", masked);
        }
    }
}
