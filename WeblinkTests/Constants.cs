﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeblinkTests
{
    class Constants
    {
        public const string WeblinkUrl = "http://localhost/weblink/weblink.aspx";
        public const string SiteId = "5765cc3d-a3eb-44b3-938c-47c02762bee6";
        public const string Login = "weblinktest1";
        public const string PasswordEncrypted = "EAAAAF/oYUPSzJZ7TGFZ9bkPjyE75VVBeYEFsiJoXFCxOfDJ";
        public const string TestCardToken = "1000000074051111"; // Equates to 4111111111111111
        public const string TestWalletToken = "U2l0ZUlkPTdmMDFkNGJhLWMyN2QtNDhiNy05YTA0LTZhMjE3ZGFiNjllZCZVc2VyR3VpZD0mV2FsbGV0SWQ9OTAyOGU1ZDItOWNkOC00NWM4LTg2ODktMDNjOTE5NWEyZTE0";
        public const string TestWalletKey = "McDYOWcDOxnO8TkflBlWU5l0lXUC0MVAUeiE4aV1hWyuyWgADU98UuPrn7GFWe5smRTcHQPtLUTe5PKtPXY0yA==";
        
    }
}
