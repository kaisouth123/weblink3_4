﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("WebLINK")]
[assembly: AssemblyDescription("WebLINK 3 Main Assembly")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("BridgePay Networks, LLC")]
[assembly: AssemblyProduct("Weblink 3")]
[assembly: AssemblyCopyright("Copyright © BridgePay Networks 2019")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("e48d1b60-64c5-4f86-be69-09755d5f73e5")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:

// **************************************************************
// IF YOU CHANGE MAJOR OR MINOR, UPDATE TEAMCITY AS WELL!!!!!
// Dont touch Revision or Build. These are set by TeamCity.
// **************************************************************
[assembly: AssemblyVersion("3.14.0.0")]
[assembly: AssemblyFileVersion("3.14.0.0")]
// **************************************************************
// IF YOU CHANGE MAJOR OR MINOR, UPDATE TEAMCITY AS WELL!!!!!
// Dont touch Revision or Build. These are set by TeamCity.
// **************************************************************

[assembly:InternalsVisibleTo("WeblinkTests")]