﻿using System;
using System.Collections.Generic;
using System.Web;
using System.IO;
using System.Text;
using System.Configuration;
using WebLINK.Common;
using AutoMapper;
using System.Collections.Specialized;
using Bridgepay.Weblink.Core.Logging;
using Bridgepay.Weblink.Core.Helpers;

namespace WebLINK
{
    public class Global : System.Web.HttpApplication
    {
        #region DebuggingFlags
        // Setting this to true causes the HTML templates to be loaded from disc locally instead of the DB.
        // This just makes template development way easier
        public static bool DebuggingTemplates = false;
        #endregion

        public static  ICleanLogger Log;

        public const string CryptoSecret = @"d@x:7&!FLT?);7EO3EoaJ+N}7^B;o=W9 wX}L30uuZg2tg4;S!-z0|lZa,RTVr|?";
        public const string WalletKeySalt = @"IYGx2<2YQ.C#^5T (yR;0yW0JfEN7~%>uGu>}d7#TBXD1iU.bk %tMrJ[3[Sz+vR";
        public const string TPI_Triple_DES_Key = @"3K#^Xf1&@vbxylk.hjkf6";
        public const int WeblinkSuccessCode = 0;
        public const int WeblinkGenericErrorCode = 99;
        public const string BpnClientIdentifier = "WebLinkTest";
        public const string PaymentTypeECheck = "eCheck";

        public static Dictionary<string, string> Templates = new Dictionary<string,string>();

        public static weblink PageInstance
        {
            get { return HttpContext.Current.Handler as weblink; }
        }

        static Global()
        {
            // Just in case I forget to turn off this flag...It cannot be true in production!!!
#if !DEBUG
            DebuggingTemplates = false;
#endif
            Bridgepay.Core.Logging.LogProvider.Configure("WebLink3");
            Log = new CleanLogger(Bridgepay.Core.Logging.LogProvider.LoggerInstance);
        }

        #region Events
        protected void Application_Start(object sender, EventArgs e)
        {            
            Log.Debug("Weblink Application_Start()");

            Mapper.CreateMap<WalletService.PaymentMethod, Wallet.PaymentMethod>()
                   .ForMember(m => m.CardHolderAddress1, o => o.MapFrom(m => m.AccountHolderAddress))
                   .ForMember(m => m.CardHolderCity, o => o.MapFrom(m => m.AccountHolderCity))
                   .ForMember(m => m.ContactEmail, o => o.MapFrom(m => m.AccountHolderEmail))
                   .ForMember(m => m.CardHolderName, o => o.MapFrom(m => m.AccountHolderName))
                   .ForMember(m => m.ContactPhone, o => o.MapFrom(m => m.AccountHolderPhone))
                   .ForMember(m => m.CardHolderState, o => o.MapFrom(m => m.AccountHolderState))
                   .ForMember(m => m.CardHolderZip, o => o.MapFrom(m => m.AccountHolderZip))
                   .ForMember(m => m.CardExp, o => o.MapFrom(m => m.ExpirationDate))
                   .ForMember(m => m.CardToken, o => o.MapFrom(m => m.Token))
                   .ForMember(m => m.Id, o => o.MapFrom(m => m.PaymentMethodId));

            LoadTemplates();
        }

        public static void LoadTemplates()
        {
            try
            {
                Templates.Clear();
                DirectoryInfo di = new DirectoryInfo(HttpContext.Current.Server.MapPath("templates"));

                foreach (var file in di.EnumerateFiles())
                {
                    using (StreamReader sr = new StreamReader(file.FullName))
                    {
                        Templates.Add(Path.GetFileNameWithoutExtension(file.Name), sr.ReadToEnd());
                        sr.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("An error occured while loading templates from the local template directory.", ex);
            }

        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            try
            {
                StringBuilder formData = new StringBuilder();

                string conn = null;
                // Need a writeable copy here
                NameValueCollection postData = new NameValueCollection(Request.Form);
                MaskingHelper.MaskSensitiveData(postData);
                foreach (string key in postData)
                {
                    formData.AppendFormat("{2}{0}={1}", key, MaskingHelper.MaskPossiblySensitiveData(postData[key]), conn);
                    conn = "\r\n";
                }

                string errMsg = string.Format("\r\n----------------------------------------------------------------------------------------------\r\nPosted values:\r\n {0}\r\n", formData.ToString());
                //the form data is logged at level DEBUG
                Log.Debug(errMsg);
                //the error is logged at level ERROR
                Log.Error("Error: \r\n", Server.GetLastError());

                string emailAddress = ConfigurationManager.AppSettings["EmailOnErrorAddress"];

                if (!string.IsNullOrEmpty(emailAddress) && Server.GetLastError() != null)
                    Email.SendEmail(emailAddress, "", "", "weblink@" + Environment.MachineName, "Weblink error", Server.GetLastError().ToString(), false);
            }
            catch (Exception) { }
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {
            Log.Debug("Weblink Application_End()");
        } 
        #endregion

    }
}