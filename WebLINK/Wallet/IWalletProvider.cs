﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

namespace WebLINK.Wallet
{
    public interface IWalletProvider
    {
        WalletResponse GetPrimaryPaymentMethod();
        WalletResponse SavePrimaryPaymentMethod(PaymentMethod PaymentMethod, int merchantAccountCode);
    }
}
