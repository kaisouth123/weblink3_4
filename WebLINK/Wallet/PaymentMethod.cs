﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebLINK.Wallet
{
    // Only supporting cards atm, no checks...
    public class PaymentMethod
    {
        public Guid? Id { get; set; }

        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }
        public string CardHolderZip { get; set; }
        public string CardHolderState { get; set; }
        public string CardHolderCity { get; set; }
        public string CardHolderAddress1 { get; set; }
        public string CardHolderAddress2 { get; set; }
        public string CardHolderName { get; set; }

        public string CardType { get; set; }
        public string LastFour { get; set; }
        public string CardExp { get; set; }
        public string CardToken { get; set; }
    }
}