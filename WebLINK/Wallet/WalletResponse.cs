﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebLINK.Wallet
{
    public class WalletResponse
    {
        public PaymentMethod PaymentMethod { get; set; }
        public Guid? WalletId { get; set; }
        public string ResultCode { get; set; }
        public string Message { get; set; }
    }
}