﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using WebLINK.Common;

namespace WebLINK.Wallet.Concrete
{
    /// <summary>
    /// Wallet Provider 
    /// </summary>
    public class BPNWalletProvider : IWalletProvider
    {
        private Guid? WalletId;
        private Guid? SiteId;

        public BPNWalletProvider(Guid? walletId, Guid? siteId)
        {
            WalletId = walletId;
            SiteId = siteId;
        }

        public WalletResponse GetPrimaryPaymentMethod()
        {
            WalletResponse response = new WalletResponse
            {
                ResultCode = Global.WeblinkGenericErrorCode.ToString()
            };

            using (var client = new WalletService.WalletServiceClient())
            {
                var filter = new WalletService.PaymentMethodFilter
                {
                    WalletId = new Guid[] { WalletId.Value },
                };
                var paymentMethod = client.GetPaymentMethodsByFilter(filter, null)
                                          .Entities
                                          .OrderBy(pm => pm.Order)
                                          .FirstOrDefault(pm => pm.PaymentMethodType == WalletService.PaymentMethodType.Cards);

                if (paymentMethod != null)
                {
                    response.PaymentMethod = AutoMapper.Mapper.Map<Wallet.PaymentMethod>(paymentMethod);
                    response.Message = "OK";
                    response.ResultCode = Global.WeblinkSuccessCode.ToString();
                }
                else
                {
                    response.Message = "Primary payment method does not exist.";
                    response.ResultCode = Global.WeblinkGenericErrorCode.ToString();
                }
            }

            return response;
        }

        public WalletResponse SavePrimaryPaymentMethod(PaymentMethod PaymentMethod, int merchantAccountCode)
        {
            WalletResponse response = new WalletResponse();

            using (var client = new WalletService.WalletServiceClient())
            {
                // A wallet id was not provided, so we need to create a new wallet first
                if (!WalletId.HasValue) 
                {
                    var wallet = new WalletService.Wallet
                    {
                        Description = "Weblink user wallet",
                        Name = "Weblink wallet",
                        SiteId = SiteId.Value,
                        IsActive = true,
                    };
                    WalletId = client.CreateWallet(wallet, null, merchantAccountCode).WalletId;
                }

                var newPaymentMethod = new WalletService.PaymentMethod
                {
                    // The old mapper only mapped CardHolderAddress1 to the legacy PaymentMethod.CardHolderAddress.
                    // CardHolderAddress2 was not used; should it be concatenated to AccountHolderAddress here?
                    AccountHolderAddress = PaymentMethod.CardHolderAddress1,
                    AccountHolderCity = PaymentMethod.CardHolderCity,
                    AccountHolderEmail = PaymentMethod.ContactEmail,
                    AccountHolderName = PaymentMethod.CardHolderName,
                    AccountHolderPhone = PaymentMethod.ContactPhone,
                    AccountHolderState = PaymentMethod.CardHolderState,
                    AccountHolderZip = PaymentMethod.CardHolderZip,
                    CardType = PaymentMethod.CardType,
                    ExpirationDate = PaymentMethod.CardExp,
                    LastFour = PaymentMethod.LastFour,
                    PaymentMethodType = WalletService.PaymentMethodType.Cards,
                    Order = 1,
                    Token = PaymentMethod.CardToken,
                    WalletId = WalletId.Value,
                };

                PaymentMethod.Id = client.CreatePaymentMethod(newPaymentMethod).PaymentMethodId;

                response.WalletId = WalletId;
                response.PaymentMethod = PaymentMethod;
                response.ResultCode = "0";
                response.Message = "Ok";
            }

            return response;
        }
    }
}