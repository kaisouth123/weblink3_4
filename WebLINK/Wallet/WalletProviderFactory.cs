﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebLINK.Wallet.Concrete;

namespace WebLINK.Wallet
{
    public class WalletProviderFactory
    {
        public IWalletProvider GetWalletProvider(Guid? walletId, Guid? siteId)
        {
            // We only have one Wallet provider and may never have another, but this factory is here to easily add another.
            return new BPNWalletProvider(walletId, siteId);
        }
    }
}