﻿using Bridgepay.Weblink.Core.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml.Linq;
using WebLINK.Common;

namespace WebLINK.PaymentServer
{
    public class TransactionResponse : BaseResponse
    {
        public string AuthCode { get; set; }
        public string HostCode { get; set; }
        public string CardType { get; set; }
        public bool? IsCommercialCard { get; set; }
        public string Token { get; set; }
        public string TokenKey { get; set; }
        public string CardNumber { get; set; }
        public string ExpDate { get; set; }
        public string NameOnCard { get; set; }
        public string Street { get; set; }
        public string Zip { get; set; }
        public string LastFour { get; set; }
        public string AuthorizedAmount { get; set; }

        public TransactionResponse() { }

        public void MergeResults(ref TransactionData nvc, bool isVoid = false)
        {
            if (isVoid)
            {
                nvc[Fields.rv_AuthCode] = AuthCode;
                nvc[Fields.rv_HostCode] = HostCode;
                nvc[Fields.rv_Id] = Id;
                nvc[Fields.rv_Message] = Message;
                nvc[Fields.rv_Result] = Result.ToString();
            }
            else
            {
                nvc[Fields.r_AuthCode] = AuthCode;
                nvc[Fields.r_HostCode] = HostCode;
                nvc[Fields.r_Message] = Message;
                nvc[Fields.r_MessageEx] = MessageEx;
                nvc[Fields.r_Result] = Result;
                nvc[Fields.r_ExpDate] = ExpDate ?? Helper.GetExpDate(nvc);
                nvc[Fields.r_Id] = Id;
                nvc[Fields.r_NameOnCard] = NameOnCard;
                nvc[Fields.r_Street] = Street;
                nvc[Fields.r_Token] = Token;
                nvc[Fields.r_TokenKey] = TokenKey;
                nvc[Fields.r_Zip] = Zip;
                nvc[Fields.r_CardType] = CardType;
                nvc[Fields.r_LastFour] = LastFour;
                nvc[Fields.r_AuthorizedAmount] = AuthorizedAmount;
            }
        }
    }
}