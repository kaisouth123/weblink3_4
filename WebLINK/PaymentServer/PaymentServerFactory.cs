﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebLINK.Common;

namespace WebLINK.PaymentServer
{
    //At one time we supported multiple payment servers, but alas, that is gone the way of the dodo.
    public class PaymentServerFactory
    {
        public enum PaymentServerTypes
        {
            BPN,
            BridgeComm,
            Mock
        }

        private StateManager stateManager = null;

        public PaymentServerFactory(StateManager stateManager) 
        {
            this.stateManager = stateManager;
	    }

        public IPaymentServer GetPaymentServer(PaymentServerTypes type)
        {
            Type paymentServerType = Type.GetType(string.Format("WebLINK.PaymentServer.Concrete.{0}PaymentServer", type));

            object[] paramaters = null;

            if (type != PaymentServerTypes.Mock)
            {
                paramaters = new object[] { stateManager };
            }

            return (IPaymentServer)Activator.CreateInstance(paymentServerType, paramaters);
        }
    }
}