﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace WebLINK.PaymentServer
{
    public class MerchantInfoResponse : BaseResponse
    {
        public string MID { get; set; }
        public string MerchantName { get; set; }
        public NameValueCollection ExtraData { get; set; }

        public MerchantInfoResponse()
        {
            ExtraData = new NameValueCollection();
        }
    }
}