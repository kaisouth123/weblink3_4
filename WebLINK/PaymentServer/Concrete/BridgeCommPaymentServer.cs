﻿using Bridgepay.Weblink.Controller;
using Bridgepay.Weblink.Controller.Factories;
using Bridgepay.Weblink.Core.Helpers;
using Bridgepay.Weblink.Core.Interfaces;
using Bridgepay.Weblink.Core.Models;
using System.Text;
using WebLINK.Common;

namespace WebLINK.PaymentServer.Concrete
{
    public class BridgeCommPaymentServer : IPaymentServer
    {
        private StateManager stateManager;

        public BridgeCommPaymentServer(StateManager stateManager)
        {
            this.stateManager = stateManager;
        }

        #region "IPayment server implementation"
        public TransactionResponse ProcessTransaction(System.Collections.Specialized.NameValueCollection data)
        {

            TransactionResponse transactionResponse;
            IResponseFormatter responseFormatter;
            IRequestFormatter requestFormatter;
            Global.Log.DebugFormat("ProcessTransaction NameValueCollection:\r\n{0}", MaskingHelper.MaskPossiblySensitiveData(data, false));
            if (stateManager.IsCreatingTokenOnly)
            {
                requestFormatter = new RequestFormatter(BridgeCommRequestTypes.GetTokenRequest, Helper.GetClientIP());
            }
            else
            {
                requestFormatter = new RequestFormatter(BridgeCommRequestTypes.TransactionRequest, Helper.GetClientIP());
                StringBuilder values = new StringBuilder();
                foreach (var keyName in data.AllKeys)
                {
                    values.AppendFormat("{0}:{1}\r\n", keyName,
                        MaskingHelper.MaskSensitiveData(data[keyName], keyName));
                }

                //MerchantName Required by Logging Standards
                if (data[Fields.MerchantName] != null)
                    values.AppendFormat("{0}:{1}\r\n", "MerchantName", data[Fields.MerchantName].ToString());

                Global.Log.DebugFormat("Submitting the following values to Bridgecomm:\r\n{0}", values.ToString());             
            }

            string request = requestFormatter.FormatRequest(data, Global.BpnClientIdentifier, string.Empty);

            using (var client = new BridgeComm.RequestHandlerClient())
            {
                try
                {
                    Global.PageInstance.TimeStampLogger.LogIt("Sending request to Bridgecomm:"+request);
                    string response = client.ProcessRequest(request);
                    responseFormatter = ResponseFormatterFactory.CreateResponseFormatter(BridgeCommRequestTypes.TransactionRequest, response);
                    transactionResponse = responseFormatter.FormatResponse() as TransactionResponse;
                    Global.PageInstance.TimeStampLogger.LogIt("Received response from Bridgecomm:"+response);
                }
                finally
                {
                    client.Close();
                }
            }

            return transactionResponse;
        }
        public MerchantInfoResponse GetMerchantInfo(TransactionData data)
        {
            MerchantInfoResponse merchantInfoResponse;
            IResponseFormatter responseFormatter;

            IRequestFormatter requestFormatter = new RequestFormatter(BridgeCommRequestTypes.MerchantInfoRequest, Helper.GetClientIP());
            string request = requestFormatter.FormatRequest(data, Global.BpnClientIdentifier, string.Empty);

            using (var client = new BridgeComm.RequestHandlerClient())
            {
                try
                {
                    string response = client.ProcessRequest(request);
                    responseFormatter = ResponseFormatterFactory.CreateResponseFormatter(BridgeCommRequestTypes.MerchantInfoRequest, response);
                    merchantInfoResponse = responseFormatter.FormatResponse() as MerchantInfoResponse;
                    //merchantInfoResponse = GetMerchantInfoResponse(resp, data.ActualIndex == 1 ? "" : (data.ActualIndex - 1).ToString());
                }
                finally
                {
                    client.Close();
                }
            }

            return merchantInfoResponse;
        }
        public TransactionResponse VoidTransaction(System.Collections.Specialized.NameValueCollection data)
        {
            TransactionResponse transactionResponse;
            IResponseFormatter responseFormatter;
            IRequestFormatter requestFormatter;

            requestFormatter = new RequestFormatter(BridgeCommRequestTypes.VoidRefundRequest, Helper.GetClientIP());


            string request = requestFormatter.FormatRequest(data, Global.BpnClientIdentifier, data[Fields.r_Id]);

            using (var client = new BridgeComm.RequestHandlerClient())
            {
                try
                {
                    string response = client.ProcessRequest(request);
                    responseFormatter = ResponseFormatterFactory.CreateResponseFormatter(BridgeCommRequestTypes.VoidRefundRequest, response);
                    transactionResponse = responseFormatter.FormatResponse() as TransactionResponse;
                    Global.PageInstance.TimeStampLogger.LogIt("Received response from Bridgecomm");
                }
                finally
                {
                    client.Close();
                }
            }

            return transactionResponse;

        }
        #endregion

        #region "Transaction request and response builders"

        //private Dictionary<string, string> GetTokenRequest(NameValueCollection data)
        //{
        //    Dictionary<string, string> request = GetBaseRequest(data);

        //    request.Add("RequestType", "001");
        //    request.Add("PaymentAccountNumber", data[Fields.CardNumber]);
        //    request.Add("ExpirationDate", Helper.GetExpDate(data));

        //    return request;
        //}

        //private TransactionResponse GetTransactionResponse(TokenObjectResponse tokenObjectResponse)
        //{
        //    TransactionResponse transactionResponse = new TransactionResponse();

        //    transactionResponse.Id = tokenObjectResponse.GatewayTransId;
        //    transactionResponse.Message = tokenObjectResponse.GatewayMessage;
        //    transactionResponse.Result = tokenObjectResponse.GatewayResult == "00000" ? "0" : tokenObjectResponse.GatewayResult;
        //    transactionResponse.Token = tokenObjectResponse.Token;

        //    return transactionResponse;
        //}


       #endregion

    }
}