﻿using Bridgepay.Weblink.Core.Helpers;
using Bridgepay.Weblink.Core.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net;
using System.Text;
using WebLINK.BridgeComm;
using WebLINK.Common;

namespace WebLINK.PaymentServer.Concrete
{
    public class BPNPaymentServer : IPaymentServer
    {
        private StateManager stateManager;

        public BPNPaymentServer(StateManager stateManager)
        {
            this.stateManager = stateManager;
        }

        #region "IPayment server implementation"
        public TransactionResponse ProcessTransaction(System.Collections.Specialized.NameValueCollection data)
        {
            TransactionResponse transactionResponse = new TransactionResponse();

            using (var client = new BridgeComm.RequestHandlerClient())
            {
                try
                {
                    Dictionary<string, string> request;

                    // Need to add support for DCP mode
                    if (stateManager.IsCreatingTokenOnly)
                    {
                        request = GetTokenRequest(data);
                        var response = client.GetToken(request);
                        transactionResponse = GetTransactionResponse(response);
                    }
                    else
                    {
                        request = GetTransactionRequest(data);

                        StringBuilder values = new StringBuilder();
                        foreach (var kvp in request)
                        {
                            values.AppendFormat("{0}:{1}\r\n", kvp.Key,
                                MaskingHelper.MaskSensitiveData(kvp.Value, kvp.Key));
                        }

                        //MerchantName Required by Logging Standards
                        if (data[Fields.MerchantName] != null)
                            values.AppendFormat("{0}:{1}\r\n", "MerchantName", data[Fields.MerchantName].ToString());

                        Global.Log.DebugFormat("Submitting the following values to Bridgecomm:\r\n{0}", values.ToString());
                        Global.PageInstance.TimeStampLogger.LogIt("Sending request to Bridgecomm");

                        var response = client.ProcessWebLinkAuth(request);
                        transactionResponse = GetTransactionResponse(response);

                        Global.PageInstance.TimeStampLogger.LogIt("Received response from Bridgecomm");
                    }
                    
                }
                finally
                {
                    client.Close();
                }
            }

            return transactionResponse;
        }

        public MerchantInfoResponse GetMerchantInfo(TransactionData data)
        {
            MerchantInfoResponse merchantInfoResponse;
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3; // comparable to modern browsers
            using (var client = new BridgeComm.RequestHandlerClient())
            {
                try
                {
                    var resp = client.GetMerchantInfo(GetMerchantInfoRequest(data));
                    merchantInfoResponse = GetMerchantInfoResponse(resp, data.ActualIndex == 1 ? "" : (data.ActualIndex - 1).ToString());
                }
                finally
                {
                    client.Close();
                }
            }

            return merchantInfoResponse;
        }

        public TransactionResponse VoidTransaction(System.Collections.Specialized.NameValueCollection data)
        {
            TransactionResponse transactionResponse = new TransactionResponse();

            using (var client = new BridgeComm.RequestHandlerClient())
            {
                var resp = client.ProcessWebLinkVoidRefund(GetVoidRequest(data));
                transactionResponse = GetTransactionResponse(resp);
                client.Close();
            }

            return transactionResponse;
        }
        #endregion

        #region "Merchant Info request and response builders"
        private Dictionary<string, string> GetMerchantInfoRequest(System.Collections.Specialized.NameValueCollection data)
        {
            Dictionary<string, string> request = GetBaseRequest(data);
            request.Add("RequestType", "011");

            return request;
        }

        private MerchantInfoResponse GetMerchantInfoResponse(MerchantInfoObjectResponse merchantInfoObjectResponse, string fieldSuffix = null)
        {
            MerchantInfoResponse merchantInfoResponse = new MerchantInfoResponse();

            merchantInfoResponse.Result = "0";

            merchantInfoResponse.Message = merchantInfoObjectResponse.GatewayMessage;
            merchantInfoResponse.MerchantName = merchantInfoObjectResponse.MerchantName;
            merchantInfoResponse.MID = merchantInfoObjectResponse.MerchantAccountCode;
            merchantInfoResponse.ExtraData[Fields.MerchantAccountCode + fieldSuffix] = merchantInfoObjectResponse.MerchantAccountCode;
            merchantInfoResponse.ExtraData[Fields.MerchantCode + fieldSuffix] = merchantInfoObjectResponse.MerchantCode;
            merchantInfoResponse.MessageEx = "Gateway transaction Id: " + merchantInfoObjectResponse.GatewayTransId; // Eh, why not...

            if (string.IsNullOrEmpty(merchantInfoObjectResponse.MerchantAccountCode))
            {
                merchantInfoResponse.Result = Global.WeblinkGenericErrorCode.ToString();
                merchantInfoResponse.Message = "Could not locate an active user with that login.";
            }

            return merchantInfoResponse;
        } 
        #endregion

        #region "Transaction request and response builders"
        private Dictionary<string, string> GetTransactionRequest(System.Collections.Specialized.NameValueCollection data)
        {
            string originatingTechnologySource = "Weblink";

            Dictionary<string, string> request = GetBaseRequest(data);

            request.Add("RequestType", "004");
            request.Add("MerchantCode", data[Fields.MerchantCode]);
            request.Add("MerchantAccountCode", data[Fields.MerchantAccountCode]);

            if (!string.IsNullOrEmpty(data[Fields.Token]))
            {
                request.Add("Token", data[Fields.Token]);
            }
            else
            {
                if (!stateManager.IsSecureTransaction &&
                    stateManager.IsCardTransaction)
                {
                    request.Add("PaymentAccountNumber", data[Fields.CardNumber]);
                }
            }

            if (!string.IsNullOrEmpty(Helper.GetExpDate(data)))
            {
                request.Add("ExpirationDate", Helper.GetExpDate(data));
            }
            request.Add("ZipCode", data[Fields.ZipCode]);
            if (!string.IsNullOrEmpty(data[Fields.CVNum]))
            {
                request.Add("SecurityCode", data[Fields.CVNum]);
            }
            request.Add("Amount", Helper.ToBridgeCommAmount(data[Fields.TotalAmt]));
            request.Add("CurrencyCode", "USD");
            request.Add("TransactionType", GetTransactionType(data));
            request.Add("AcctType", GetAccountType(data)); 
            request.Add("InvoiceNum", data[Fields.InvoiceNum]);
            request.Add("PONum", data[Fields.PONum]);
            request.Add("AccountHolderName", data[Fields.FullName]);
            request.Add("HolderType", GetHolderType(data));
            request.Add("AccountStreet", data[Fields.Address1]);
            request.Add("AccountCity", data[Fields.City]);
            request.Add("AccountState", data[Fields.State]);
            request.Add("AccountZip", data[Fields.ZipCode]);
            request.Add("AccountPhone", Helper.RemoveNonDigits(data[Fields.Phone]));
            request.Add("CustomerAccountCode", data[Fields.CustomerAccountCode]);
            request.Add("PaymentType", data[Fields.PaymentType]);
            request.Add("SoftwareVendor", data[Fields.SoftwareVendor]);
            request.Add("clientPublicIPAddress", Helper.GetClientIP());
            request.Add("SettlementDelay", data[Fields.SettlementDelay]);
            // Checks
            if (!stateManager.IsCardTransaction)
            {
                request.Add("BankAccountNum", data[Fields.AccountNumber]);
                request.Add("RoutingNum", data[Fields.TransitNumber]);
            }

            if (!string.IsNullOrEmpty(data[Fields.TransactionCategoryCode]))
            {
                request.Add("TransCatCode", data[Fields.TransactionCategoryCode].ToUpper());
            }

            if (!string.IsNullOrEmpty(data[Fields.TransactionIndustryType]))
            {
                request.Add("TransIndustryType", data[Fields.TransactionIndustryType].ToUpper());
            }
            else
            {
                if (stateManager.IsCardTransaction)
                    request.Add("TransIndustryType", "EC"); //Default to ECommerce for Credit
                else
                    request.Add("TransIndustryType", "WEB"); //Default to WEB for ACH
            }

            // MSR data
            if (stateManager.IsSecureTransaction)
            {
                originatingTechnologySource += "XBAP";
                request.Add("MsrKsn", data[Fields.SecurityInfo]);
                request.Add("SecureFormat", data[Fields.SecureFormat]);
                request.Add("Track1", data[Fields.EncryptedTrack1]);
                request.Add("Track2", data[Fields.EncryptedTrack2]);
                request.Add("securityTechnology", "SecureLink");

                request.Remove("ExpirationDate");
                request.Remove("SecurityCode");
            }

            request.Add("originatingTechnologySource", originatingTechnologySource);

            return request;
        }

        private string GetHolderType(NameValueCollection data)
        {
            string holderType;

            if (stateManager.IsCardTransaction)
            {
                holderType = data[Fields.HolderType];
            }
            else // Maintain backward compatibility with "CheckType"
            {
                holderType = data[Fields.HolderType] ?? data[Fields.CheckType];
            }

            holderType += string.Empty;

            switch (holderType.ToLower())
            {
                default:
                case "personal":
                    holderType = "P";
                    break;
                case "business":
                    holderType = "O";
                    break;
            }

            return holderType;
        }

        private string GetAccountType(NameValueCollection data)
        {
            // Credit card http://labs.unitedthinkers.com/index.php/UniCharge_Real-Time_HTTPs_Integration#Account_Types

            string acctType = data[Fields.AccountType];

            if (stateManager.IsCardTransaction)
            {
                acctType = "R";
            }
            else
            {
                if (!string.IsNullOrEmpty(acctType))
                {
                    switch (acctType.ToLower())
                    {
                        case "checking":
                            acctType = "C";
                            break;
                        case "saving":
                        case "savings":
                            acctType = "S";
                            break;
                    }
                } 
                else 
                {
                    // Must be a check trxn and they didnt specify acct type somehow
                    // Default to checking
                    acctType = "C";
                }
            }
            
            return acctType;
        }

        private Dictionary<string, string> GetTokenRequest(NameValueCollection data)
        {
            Dictionary<string, string> request = GetBaseRequest(data);

            request.Add("RequestType", "001");
            request.Add("PaymentAccountNumber", data[Fields.CardNumber]);
            request.Add("ExpirationDate", Helper.GetExpDate(data));
            return request;
        }

        private TransactionResponse GetTransactionResponse(AuthObjectResponse authObjectResponse)
        {
            TransactionResponse transactionResponse = new TransactionResponse();

            transactionResponse.AuthCode = authObjectResponse.AuthorizationCode;
            transactionResponse.CardType = authObjectResponse.CardType;
            transactionResponse.ExpDate = authObjectResponse.ExpirationDate;
            transactionResponse.HostCode = authObjectResponse.ReferenceNumber;
            transactionResponse.Id = authObjectResponse.GatewayTransId;
            transactionResponse.IsCommercialCard = authObjectResponse.IsCommercialCard;
            transactionResponse.Message = authObjectResponse.GatewayMessage;

            transactionResponse.AVSMessage = authObjectResponse.AVSMessage;
            transactionResponse.AVSResult = authObjectResponse.AVSResult;
            transactionResponse.CVMessage = authObjectResponse.CVMessage;
            transactionResponse.CVResult = authObjectResponse.CVResult;

            int bridgeCommResult = 0;

            // Apprently this can be a string occasionally. Normalize to an int.
            if (!int.TryParse(authObjectResponse.GatewayResult, out bridgeCommResult))
            {
                bridgeCommResult = Global.WeblinkGenericErrorCode;
            }

            transactionResponse.Result = bridgeCommResult.ToString();
            transactionResponse.Token = authObjectResponse.Token;
            transactionResponse.AuthorizedAmount = Helper.FromBridgeCommAmount(authObjectResponse.AuthorizedAmount);

            if (authObjectResponse.Token != null &&
                authObjectResponse.Token.Length >= 4)
            {
                transactionResponse.LastFour = authObjectResponse.Token.Substring(authObjectResponse.Token.Length - 4);
            }

            return transactionResponse;
        }

        private TransactionResponse GetTransactionResponse(VoidRefundObjectResponse voidRefundObjectResponse)
        {
            TransactionResponse transactionResponse = new TransactionResponse();

            transactionResponse.HostCode = voidRefundObjectResponse.ReferenceNumber;
            transactionResponse.Id = voidRefundObjectResponse.GatewayTransId;
            transactionResponse.Message = voidRefundObjectResponse.GatewayMessage;
            transactionResponse.Result = voidRefundObjectResponse.GatewayResult == "00000" ? "0" : voidRefundObjectResponse.GatewayResult;

            return transactionResponse;
        }

        private TransactionResponse GetTransactionResponse(TokenObjectResponse tokenObjectResponse)
        {
            TransactionResponse transactionResponse = new TransactionResponse();

            transactionResponse.Id = tokenObjectResponse.GatewayTransId;
            transactionResponse.Message = tokenObjectResponse.GatewayMessage;
            transactionResponse.Result = tokenObjectResponse.GatewayResult == "00000" ? "0" : tokenObjectResponse.GatewayResult;
            transactionResponse.Token = tokenObjectResponse.Token;

            return transactionResponse;
        }

        private Dictionary<string, string> GetVoidRequest(System.Collections.Specialized.NameValueCollection data)
        {
            Dictionary<string, string> request = GetBaseRequest(data);
            request.Add("MerchantCode", data[Fields.MerchantCode]);
            request.Add("MerchantAccountCode", data[Fields.MerchantAccountCode]);
            request.Add("RequestType", "012");
            request.Add("TransactionType", "void");
            request.Add("ReferenceNumber", data[Fields.r_Id]);
            request.Add("Amount", Helper.ToBridgeCommAmount(data[Fields.TotalAmt]));
            if (data[Fields.InvoiceNum] != null) request.Add("InvoiceNum", data[Fields.InvoiceNum]);
            if (data[Fields.CustomerAccountCode] != null) request.Add("CustomerAccountCode", data[Fields.CustomerAccountCode]);

            return request;
        }

        private Dictionary<string, string> GetBaseRequest(NameValueCollection data)
        {
            Dictionary<string, string> request = new Dictionary<string, string>();

            request.Add("ClientIdentifier", Global.BpnClientIdentifier);
            request.Add("TransactionId", string.IsNullOrEmpty(data[Fields.UserDefinedTransactionId]) ? GetDefaultUserDefinedTransactionId() : data[Fields.UserDefinedTransactionId]);
            request.Add("RequestDateTime", DateTime.Now.ToShortDateString());

            if (string.IsNullOrEmpty(data[Fields.PurchaseToken]))
            { 
                request.Add("User", data[Fields.Login]);
                request.Add("Password", data[Fields.Password]);
            }
            else
            {
                request.Add("PurchaseToken", data[Fields.PurchaseToken]);
            }

            return request;
        }
        #endregion

        private string GetTransactionType(NameValueCollection data)
        {
            string transType = string.Empty;
            string passedInTransType = (data[Fields.TransType] + string.Empty).ToLower();

            switch (passedInTransType)
            {
                case "return":
                case "credit":
                    transType = BridgeCommTransTypes.Credit;
                    break;
                case "auth":
                case "sale-auth":
                case "preauth":
                    transType = BridgeCommTransTypes.SaleAuth;
                    break;
                case "sale":
                case "":
                    transType = BridgeCommTransTypes.Sale;
                    break;
            }

            return transType;
        }

        private string GetDefaultUserDefinedTransactionId()
        {
            return string.Format("{0}-{1}",
                DateTime.Now.ToString("yyyyMMddHHmmss"),
                Guid.NewGuid());
        }

    }
}