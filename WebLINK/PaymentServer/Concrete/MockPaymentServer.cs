﻿using Bridgepay.Weblink.Core.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Web;
using WebLINK.Common;

namespace WebLINK.PaymentServer.Concrete
{
    /// <summary>
    /// A fake payment server that returns different results based on amount ranges.
    /// </summary>
    public class MockPaymentServer : IPaymentServer
    {
        private static Random random = new Random();

        public TransactionResponse ProcessTransaction(NameValueCollection data)
        {
            TransactionResponse response = new TransactionResponse();

            string rawTotalAmount = data[Fields.TotalAmt];
            decimal totalAmount = 0;

            decimal.TryParse(rawTotalAmount, out totalAmount);

            if (data[Fields.Login].Equals("fail", StringComparison.CurrentCultureIgnoreCase))
                response.Result = "1001";
            else if (totalAmount < 10)
                response.Result = "0";
            else if (totalAmount >= 10 && totalAmount < 20)
                response.Result = "13";
            else if (totalAmount >= 20)
                response.Result = "12";

            switch (response.Result)
            {
                case "0":
                    response.Message = "Transaction approved";
                    response.AuthCode = GetRandomAuthCode();
                    response.HostCode = GetRandomNumber(12);
                    response.Id = GetRandomNumber(7);
                    break;
                case "12":
                    response.Message = "Decline";
                    break;
                case "13":
                    response.Message = "Referral";
                    break;
                case "1001":
                    response.Message = "Invalid login";
                    break;
            }

            response.ExpDate = Helper.GetExpDate(data);
            response.CardType = data[Fields.CardTypeName];
            response.LastFour = Helper.GetLastFour(data);

            System.Threading.Thread.Sleep(1000); // Simulate some processing delay
            return response;
        }

        public MerchantInfoResponse GetMerchantInfo(NameValueCollection data)
        {
            return new MerchantInfoResponse
            {
                MerchantName = "Ted's Taxidermy",
                MID = "552",
                Result = "0"
            };
        }

        public TransactionResponse VoidTransaction(NameValueCollection data)
        {
            return new TransactionResponse
            {
                Message = "Transaction voided",
                Result = "0"
            };
        }

        private string GetRandomAuthCode()
        {
            // 65-90 = upper case alphabet
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < 6; i++)
            {
                int x = random.Next(0, 35);
                if (x < 10)
                    sb.Append(x);
                else
                    sb.Append((char)(x + 55));
            }

            return sb.ToString();

        }

        private string GetRandomNumber(int length)
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < length; i++)
            {
                sb.Append(random.Next(0, 9));
            }

            return sb.ToString();
        }



        public MerchantInfoResponse GetMerchantInfo(TransactionData data)
        {
            return new MerchantInfoResponse
            {
                Id = "1",
                MerchantName = "Ted's Taxidermy",
                MID = "1",
                Result = "0"
            };
        }
    }
}