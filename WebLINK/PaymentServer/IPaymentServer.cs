﻿using Bridgepay.Weblink.Controller.Models;
using Bridgepay.Weblink.Core.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using WebLINK.Common;

namespace WebLINK.PaymentServer
{
    //Todo: Add distinct calls for tokenziation operations
    public interface IPaymentServer
    {
        TransactionResponse ProcessTransaction(NameValueCollection data);
        TransactionResponse VoidTransaction(NameValueCollection data);
        MerchantInfoResponse GetMerchantInfo(TransactionData data);
    }
}
