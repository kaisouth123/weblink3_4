﻿using Bridgepay.Weblink.Core.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebLINK.Common;

namespace WebLINK
{
    public partial class emailpost : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string emailAddress = Request.Form[Fields.Email];

            if (string.IsNullOrEmpty(emailAddress) ||
                Request.Form.Count == 0)
            {
                return;
            }

            StringBuilder sb = new StringBuilder("<table border=1>");

            foreach (var key in Request.Form.AllKeys)
            {
                sb.AppendFormat("<tr><th align=left>{0}</th><td align=left>{1}</td></tr>", key, Request.Form[key]);
            }

            sb.Append("</table>");

            string mailServer = ConfigurationManager.AppSettings["SmtpServer"];
            string emailFrom = ConfigurationManager.AppSettings["EmailFrom"];

            SmtpClient client = new SmtpClient(mailServer);
            MailMessage msg = new MailMessage(emailFrom, emailAddress, "Weblink says hello!", sb.ToString());
            msg.IsBodyHtml = true;

            client.Send(msg);
        }
    }
}