﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace WebLINK.Models
{
    public class MsrConfig
    {
        [JsonProperty("device")]
        public string Device { get; set; }

        [JsonProperty("comPortName")]
        public string ComPortName { get; set; }

        [JsonProperty("comBaudRate")]
        public string ComBaudRate { get; set; }

        [JsonProperty("comDataBits")]
        public string ComDataBits { get; set; }

        [JsonProperty("comStopBits")]
        public string ComStopBits { get; set; }

        [JsonProperty("comParity")]
        public string ComParity { get; set; }

        [JsonProperty("usbVendorID")]
        public string UsbVendorId { get; set; }

        [JsonProperty("usbProductID")]
        public string UseProductId { get; set; }
    }
}