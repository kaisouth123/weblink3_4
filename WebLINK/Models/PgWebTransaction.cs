﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebLINK.Models
{
    public class PgWebTransaction
    {
        public int pnRefNum { get; set; }
        public string invNum { get; set; }
        public decimal amount { get; set; }
        public string tenderType { get; set; }
        public string transType { get; set; }
        public string poNum { get; set; }
        public string clerkId { get; set; }

        public string pgWebUrl { get; set; }

        public PgWebTransaction()
        {
            pgWebUrl = "https://pg.bridgepaynetsecuretx.com:8443/paylink/transact.json";
        }

    }
}