﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace WebLINK
{
    public partial class LogPost : System.Web.UI.Page
    {
        static object sync = new object();

        protected void Page_Load(object sender, EventArgs e)
        {
            lock (sync)
            {
                using (StreamWriter sw = new StreamWriter(Server.MapPath("postlog.txt"), true))
                {
                    sw.WriteLine(DateTime.Now.ToString() + " : Begin");

                    foreach (string key in Request.Form.AllKeys)
                    {
                        sw.WriteLine(string.Format("{0} = {1}", key, Request.Form[key]));
                    }

                    sw.Close();
                }
            }
        }
    }
}