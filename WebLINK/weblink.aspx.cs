﻿using Bridgepay.Core.Captcha.Models;
using Bridgepay.Core.Logging;
using Bridgepay.Security.Authentication;
using Bridgepay.Weblink.Core.Helpers;
using Bridgepay.Weblink.Core.Logging;
using Bridgepay.Weblink.Core.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using WebLINK.Common;
using WebLINK.Data;
using WebLINK.Models;
using WebLINK.PaymentServer;
using WebLINK.Templating;
using WebLINK.Validation;
using WebLINK.Wallet;

namespace WebLINK
{
    public partial class weblink : System.Web.UI.Page
    {
        private const string CookieDetectionName = "weblink3_cookie_detection";
        private const string DCPUserPassThroughAppSetting = "DCPUserPassThrough";
        private const string PassThroughDeprecationDateAppSetting = "PassThroughDeprecationDate";
        private const string DCPDeprecationDateAppSetting = "DCPDeprecationDate";
        private const string ErrorRedirectUrl = "error.html";

        #region "Member variables"

        private List<string> RequiredFields = new List<string>(); //Collection of required fields as specified in the template
        private List<string> ReturnFields = new List<string>(); //List of fields to return to the success url - Defaults to all fields
        private List<string> SensitiveFields = new List<string>()
        {
            Fields.CardNumber,
            Fields.CVNum,
            Fields.AccountNumber,
            Fields.TransitNumber,
            Fields.CheckNumber,
            Fields.CardExpMonth,
            Fields.CardExpYear
        }; //List of sensitive fields such as card num, cvv, etc
        private List<ScriptType> HeadScripts = new List<ScriptType>();
        private List<ScriptType> BodyScripts = new List<ScriptType>();
        private List<string> Stylesheets = new List<string>();

        TransactionQueue transactionQueue = null;
        DataProvider dataProvider = null; // Wrapper for the Weblink Db
        StateManager stateManager = null;
        SessionManager sessionManager = null;
        ValidationManager validationManager = null;
        Dictionary<int, TransactionResponse> transactionResults = null;
        IPaymentServer paymentServer = null; // Our interface to the payment server
        IWalletProvider walletProvider = null; // Our interface to the wallet
        public ITimeStampLogger TimeStampLogger = null;
        private static DCPEndOfLifeHelper dcpEndOfLifeHelper;

        private int? PrimaryWLID = null;
        private int? Payment_Form_TID; //Payment form template Id
        private int? Customer_Email_TID;
        private int? Merchant_Email_TID;
        private int? Receipt_TID;
        private string InlineHeadScripts = string.Empty;
        private string InlineBodyScripts = string.Empty;

        private string _userAgent = string.Empty;

        #endregion

        static weblink()
        {
            try
            {
                var dcpUserPassThrough = ConfigurationManager.AppSettings[DCPUserPassThroughAppSetting];
                var passThroughDeprecationDate = ConfigurationManager.AppSettings[PassThroughDeprecationDateAppSetting];
                var dcpDeprecationDate = ConfigurationManager.AppSettings[DCPDeprecationDateAppSetting];
                dcpEndOfLifeHelper = new DCPEndOfLifeHelper(dcpUserPassThrough, passThroughDeprecationDate, dcpDeprecationDate);
            }
            catch (Exception ex)
            {
                LogProvider.LoggerInstance.Error("Failed to load DCP end-of-life settings.", ex);
            }
        }

        #region Events

        protected void Page_Init(object sender, EventArgs e)
        {
            var logMode = ConfigurationManager.AppSettings["LogMode"];

            _userAgent = (Request.UserAgent == null ? "DCP" : Request.UserAgent);

            if (logMode.ToLower() == "showheaders")
            {
                StringBuilder stringBuilder = new StringBuilder();
                foreach (var header in Request.Headers.AllKeys)
                {
                    stringBuilder.AppendFormat("{0}: {1}, ", header, Request.Headers[header]);
                }
                Global.Log.Debug(string.Format("Headers received in request: {0}", stringBuilder.ToString()));

                stringBuilder = new StringBuilder();
                foreach (var parameter in Request.QueryString.AllKeys)
                {
                    stringBuilder.AppendFormat("{0}: {1}, ", parameter, MaskingHelper.MaskPossiblySensitiveData(Request.QueryString[parameter]));
                }
                Global.Log.Debug(string.Format("QueryString parameters received in request: {0}", stringBuilder.ToString()));


                stringBuilder = new StringBuilder();
                foreach (var parameter in Request.Form.AllKeys)
                {
                    stringBuilder.AppendFormat("{0}: {1}, ", parameter, MaskingHelper.MaskPossiblySensitiveData(Request.Form[parameter]));
                }
                Global.Log.Debug(string.Format("Form parameters received in request: {0}", stringBuilder.ToString()));

                stringBuilder = new StringBuilder();
                foreach (var cookie in Request.Cookies.AllKeys)
                {
                    stringBuilder.AppendFormat("{0}: {1}, ", cookie, Request.Cookies[cookie]);
                }
                Global.Log.Debug(string.Format("Cookies found in the request: {0}", stringBuilder.ToString()));
            }

            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3; // comparable to modern browsers
            // Provides a way to refresh the template cache without an iisreset.
            if (Request.QueryString.AllKeys.Any(k => k.Equals("ReloadTemplates")))
            {
                Global.LoadTemplates();
            }

            //If a GET was performed, just output the version #.
            if (Request.RequestType.Equals("GET", StringComparison.CurrentCultureIgnoreCase))
            {
                Response.Write(string.Format("Weblink v{0}", Assembly.GetExecutingAssembly().GetName().Version));
                Response.End();
            }

            Response.Write(string.Format("<!--Weblink v{0}-->\r\n", Assembly.GetExecutingAssembly().GetName().Version));

            sessionManager = new SessionManager(new HttpContextWrapper(Context));
            dataProvider = new DataProvider();

            bool isInitialPost = string.IsNullOrEmpty(Request.Form[Fields.IsPostBack]);

            if (isInitialPost)
            {
                try
                {
                    LogProvider.LoggerInstance.Info(GetAuthenticationStatusLog(Request.Form));                    
                }
                catch (Exception ex)
                {
                    LogProvider.LoggerInstance.Error(nameof(Page_Init), ex);
                }

                // TP: 16662 - DCP mode end-of-life check.
                if (string.IsNullOrWhiteSpace(Request.Form[Fields.Mode]))
                {
                    if (dcpEndOfLifeHelper != null &&
                        !dcpEndOfLifeHelper.IsDCPModeAllowed(Request.Form, DateTime.Now))
                    {
                        LogProvider.LoggerInstance.Warn("DCP mode transaction denied.");
                        try
                        {
                            // Passing endResponse as true prevents Page_Load from executing,
                            // but unfortunately also raises a ThreadAbortException. See:
                            // https://stackoverflow.com/a/4874947
                            Response.Redirect(ErrorRedirectUrl, true);
                            return;
                        }
                        catch (System.Threading.ThreadAbortException)
                        {
                            LogProvider.LoggerInstance.Warn("DCP request aborted.");
                            return;
                        }
                    }
                    else
                    {
                        LogProvider.LoggerInstance.Info("DCP mode transaction allowed.");
                    }
                }

                sessionManager.InitialPostData = Request.Form;
                sessionManager.Data = new NameValueCollection(Request.Form);
                if (_userAgent.ToLower().Contains("os"))
                {
                    Cache[sessionManager.ASPNET_SessionId] = sessionManager.Data;
                }
                if (sessionManager.IsUsingSessionState)
                {
                    HttpCookie cookieDetection = new HttpCookie(CookieDetectionName);
                    cookieDetection.HttpOnly = true; // Must be accessible to Javascript
                    cookieDetection.SameSite = SameSiteMode.None;
                    cookieDetection.Secure = true;
                    Response.SetCookie(cookieDetection);

                    int timeout = General.DefaultSessionTimeoutMinutes;

                    if (!string.IsNullOrEmpty(Request.Form[Fields.SessionTimeout]))
                    {
                        int.TryParse(Request.Form[Fields.SessionTimeout], out timeout);
                    }

                    if (timeout > General.MaxSessionTimeoutMinutes)
                    {
                        timeout = General.MaxSessionTimeoutMinutes;
                    }

                    if (timeout < General.MinSessionTimeoutMinutes)
                    {
                        timeout = General.MinSessionTimeoutMinutes;
                    }

                    Session.Timeout = timeout;
                    sessionManager.EstablishNewSession(new NameValueCollection(Request.Form));
                }
            }
            else
            {
                Global.Log.Info(string.Format("SessionId:{0};Continue:{1};Data:{2};Form:{3}", sessionManager.ASPNET_SessionId, sessionManager.ContinueExistingSession(), MaskingHelper.MaskPossiblySensitiveData(sessionManager.Data, false), MaskingHelper.MaskPossiblySensitiveData(Request.Form)));
                if (sessionManager.IsUsingSessionState && !sessionManager.ContinueExistingSession() && !_userAgent.ToLower().Contains("os"))
                {
                    Global.Log.Info("HandleSessionTimeOut:" + sessionManager.ASPNET_SessionId);
                    HandleSessionTimeOut();
                }
                else
                {
                    if (_userAgent.ToLower().Contains("os"))
                    {
                        sessionManager.Data = (NameValueCollection)Cache[sessionManager.ASPNET_SessionId];
                        Global.Log.Info("NameValueCollection fromCache:" + (NameValueCollection)Cache[sessionManager.ASPNET_SessionId]);
                    }
                    Helper.Merge(sessionManager.Data, Request.Form, true);
                }
            }

            stateManager = new StateManager(sessionManager);
            transactionQueue = new TransactionQueue(sessionManager.Data);
            Global.Log.DebugFormat("Page_Init sessionManager.Data:\r\n{0}", MaskingHelper.MaskPossiblySensitiveData(sessionManager.Data, false));
            if (!stateManager.IsPostBack)
            {
                // Log the posted data for debugging purposes. Masked of course.
                Global.Log.InfoFormat("Incoming initial request for server {0} from {1}", Request.Url.Host, Request.UserHostAddress);
                NameValueCollection debugData = new NameValueCollection(Request.Form);
                MaskingHelper.MaskSensitiveData(debugData);
                Global.Log.Debug(MaskingHelper.MaskPossiblySensitiveData(debugData, false));
                PreProcessPostedData(sessionManager.Data);
            }

            if (sessionManager.IsUsingSessionState)
            {
                DateTime Timeout = sessionManager.GetSessionExpiration();
                TimeSpan tsTimeout = Timeout - DateTime.Now;

                InlineHeadScripts += string.Format("\r\nBP.sessionTimeout = new Date(+new Date() + {0});", tsTimeout.TotalMilliseconds);
            }
            else
            {
                InlineBodyScripts += string.Format("\r\n$('#timer').hide();");
            }

            TimeStampLogger = new TimeStampLogger(Global.Log);
            validationManager = new ValidationManager(sessionManager.Data, stateManager);

            var timeStampMessage = string.Empty;

            if (stateManager.IsDCP)
            {
                timeStampMessage = "Incoming DCP request";
            }
            else if (stateManager.IsPostBack)
            {
                timeStampMessage = "Incoming request from Payment form";
            }
            else
            {
                timeStampMessage = "Incoming initial post";
            }

            TimeStampLogger.LogIt(timeStampMessage);

            int customerEmailTID, merchantEmailTID, paymentFormTID, receiptTID;
            Guid walletId = Guid.Empty;

            if (int.TryParse(sessionManager.InitialPostData[Fields.CustomerEmailTID] + string.Empty, out customerEmailTID))
                Customer_Email_TID = customerEmailTID;
            if (int.TryParse(sessionManager.InitialPostData[Fields.MerchantEmailTID] + string.Empty, out merchantEmailTID))
                Merchant_Email_TID = merchantEmailTID;
            if (int.TryParse(sessionManager.InitialPostData[Fields.PaymentFormTID] + string.Empty, out paymentFormTID))
                Payment_Form_TID = paymentFormTID;
            if (int.TryParse(sessionManager.InitialPostData[Fields.ReceiptTID] + string.Empty, out receiptTID))
                Receipt_TID = receiptTID;

            string returnFields = sessionManager.InitialPostData[Fields.ReturnFields];
            if (!string.IsNullOrEmpty(returnFields))
                ReturnFields = returnFields.Split(',').ToList();
            else
                ReturnFields = Request.Form.AllKeys.ToList<string>();

            if (stateManager.IsPostBack)
            {
                int tempWLID;
                if (int.TryParse(sessionManager.Data[Fields.WLID], out tempWLID))
                {
                    PrimaryWLID = tempWLID;
                }
            }
            else
            {
                //This is always set to true for now to integrate with PGW
                //eventhough cookies are not used at all by PGW
                InlineBodyScripts += "\r\nBP.cookiesEnabled = true;\r\n";
            }

            WalletProviderFactory walletProviderFactory = new WalletProviderFactory();
            walletProvider = walletProviderFactory.GetWalletProvider(transactionQueue.WalletData.WalletId, transactionQueue.WalletData.SiteId);

#if DEBUG
            InlineHeadScripts += "\r\nBP.debug = true;";
#endif
            HeadScripts.Add(ScriptType.Core);
            HeadScripts.Add(ScriptType.jQuery);
            HeadScripts.Add(ScriptType.jQueryUI);
            HeadScripts.Add(ScriptType.PaymentForm);
            HeadScripts.Add(ScriptType.Countdown);

            // Set up the MSR settings. They either were passed in, or we read them from a cookie if exists
            if (stateManager.SwipeMethod == SwipeMethod.java)
            {
                // POST'ed settings take precedence over what might be in the cookie
                var msrConfig = Helper.GetMSRSettingsFromPostData(sessionManager.Data);

                if (!string.IsNullOrEmpty(msrConfig))
                {
                    InlineHeadScripts += "\r\nBP.msrConfig = '" + msrConfig + "';";
                }

                HeadScripts.Add(ScriptType.DeployJava);
                HeadScripts.Add(ScriptType.JavaApplet);
                HeadScripts.Add(ScriptType.JSON2);
                HeadScripts.Add(ScriptType.Amplify);
            }

            if (!string.IsNullOrEmpty(transactionQueue.Data[Fields.AutoInitMSR]))
            {
                InlineBodyScripts += "\r\n$(function(){ BP.handleSwipeButtonClick(); });";
            }
        }

        private void HandleSessionTimeOut()
        {
            string templateHtml = GetTemplateHtml("SessionTimeout", PrimaryWLID, Payment_Form_TID);

            sessionManager.Data = Request.Form;
            WeblinkTemplate template = new WeblinkTemplate(templateHtml, sessionManager, false);

            Response.Write(template.ToString());
            Response.End();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3; // comparable to modern browsers

            if (sessionManager.Data[Fields.IsIframe] != null && sessionManager.Data[Fields.IsIframe].ToLower() == "true")
            {
                if (Session["DomainExist"] != null && Session["DomainExist"].ToString() == "false")
                {
                    sessionManager.Data[Fields.r_Errors] = "Please contact support!";
                    ShowErrorForm();
                    return;
                }
            }
            if (!string.IsNullOrEmpty(sessionManager.Data["isPgWebResponse"]))
            {
                HandlePgWebResponse();
                return;
            }

            if (!validationManager.IsValid())
            {
                foreach (var invalidField in validationManager.InvalidFields)
                {
                    sessionManager.Data[invalidField] = null;
                }

                ShowErrorForm();
                return;
            }

            sessionManager.Data[Fields.TransDate] = DateTime.Now.ToString();

            bool isSuccessfullTransaction = false;

            if (!stateManager.IsPostBack)
            {
                //Acquire Purchase Tokens for each of the user authentications.
                bool purchaseTokensAcquired = AcquirePurchaseTokens();
                //var userLoadSuccessful = LoadUserInfo();
                if (HttpContext.Current.Request.UrlReferrer != null)
                {
                    var isIPhone = _userAgent.ToLower().Contains("iphone");
                    var iFrameDomain = ValidateDomain(HttpContext.Current.Request.UrlReferrer.Host);
                    Global.Log.Info(_userAgent.ToLower());
                    if (Session["DomainExist"] != null && Session["DomainExist"].ToString() == "false")
                    {
                        Response.AddHeader("X-FRAME-OPTIONS", "SAMEORIGIN");
                        Response.AddHeader("Content-Security-Policy", "frame-ancestors 'self'");
                    }
                    else
                    {
                        if (isIPhone)
                        {
                            Response.AddHeader("allow-navigation", HttpContext.Current.Request.UrlReferrer.Host);
                            Response.AddHeader("allow-navigation", HttpContext.Current.Request.UrlReferrer.Authority);
                        }
                        else
                        {
                            // IE 11 doesn't support CSP, but does support an obsolete version of X-FRAME-OPTIONS.
                            // See: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Frame-Options
                            var allowFrom = $"ALLOW-FROM {HttpContext.Current.Request.Url.Scheme}://{iFrameDomain}";
                            Response.AddHeader("X-FRAME-OPTIONS", allowFrom);

                            // TP 15689 - Use the "whitelist" domain matched from the referrer rather than the referrer itself.
                            if (!string.IsNullOrWhiteSpace(iFrameDomain))
                            {
                                // Either a wildcard or exact FQDN match was made.
                                Response.AddHeader("Content-Security-Policy", $"frame-ancestors {iFrameDomain};");
                            }
                            else
                            {
                                // Fallback to the original implementation if neither a wildcard nor exact FQDN match was made.
                                Response.AddHeader("Content-Security-Policy", "frame-ancestors " + HttpContext.Current.Request.UrlReferrer.Host);
                            }
                        }
                    }
                }
                //if (userLoadSuccessful)
                if (purchaseTokensAcquired)
                {
                    var requestSessionId = Request.Cookies["ASP.NET_SessionId"];
                    requestSessionId.SameSite = SameSiteMode.None;
                    requestSessionId.Secure = true;
                    Response.SetCookie(requestSessionId);
                    if (stateManager.SwipeMethod == SwipeMethod.pgweb)
                    {
                        ShowPgWebForm();
                        return;
                    }

                    if (stateManager.IsDCP)
                    {
                        AcceptPayment();
                        isSuccessfullTransaction = !transactionQueue.HasErrors && !validationManager.HasErrors;
                        PerformPostPaymentProcessing(isSuccessfullTransaction);
                    }
                    else
                    {
                        sessionManager.Data[Fields.IsPostBack] = "1";
                        switch (stateManager.Mode)
                        {
                            case Modes.PaymentForm:
                                ShowPaymentForm();
                                break;
                            case Modes.TokenizationForm:
                                ShowTokenizationForm();
                                break;
                        }
                    }
                }
                else
                {
                    MaskingHelper.MaskSensitiveData(sessionManager.Data, false);
                    ShowErrorForm();
                }
            }
            else //Process payment and handle result. A paymentform has been submitted
            {
                AcceptPayment();
                isSuccessfullTransaction = sessionManager.Data[Fields.r_Result] == "0";
                PerformPostPaymentProcessing(isSuccessfullTransaction);
            }

            TimeStampLogger.LogIt("Weblink done.");
        }

        private bool AcquirePurchaseTokens()
        {
            // Load all users associated with this post. This could be multiple users if this is a split transaction.
            // We consider the first user the primary user and whose WLID we'll use for templates/settings, etc.
            int primaryMID = 0;
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3; // comparable to modern browsers
            var paymentServerFactory = new PaymentServerFactory(stateManager);
            paymentServer = paymentServerFactory.GetPaymentServer(stateManager.PaymentServerType);

            MerchantInfoResponse merchantInfoResponse;

            int transactionId = 0;
            foreach (var transactionData in transactionQueue.Transactions)
            {
                string primaryToken = transactionData[Fields.PurchaseToken];
                if (string.IsNullOrWhiteSpace(primaryToken))
                    primaryToken = AcquireToken(transactionData, true);
                if (string.IsNullOrWhiteSpace(primaryToken))
                {
                    Global.Log.Error("AcquirePurchaseTokens(): Unable to acquire purchase token for principal transaction.");
                    validationManager.AddError("Unable to acquire purchase token for principal transaction.");
                    return false;
                }
                else
                {
                    transactionData[Fields.PurchaseToken] = primaryToken;
                    string pTokenName = (transactionId == 0 ? Fields.PurchaseToken : string.Concat(Fields.PurchaseToken, transactionId.ToString()));
                    sessionManager.Data[pTokenName] = primaryToken;
                    transactionId++;
                    transactionData[Fields.Login] = string.Empty;
                    transactionData[Fields.Password] = string.Empty;
                    transactionData[Fields.PasswordEncrypted] = string.Empty;
                }

                Global.Log.InfoFormat("Using principal purchase token {0}", Bridgepay.Core.Logging.EventId.Default, primaryToken);

                if (!string.IsNullOrWhiteSpace(transactionData[Fields.SvcAmount]))
                {
                    string serviceToken = transactionData[Fields.svcPurchaseToken];
                    if (string.IsNullOrWhiteSpace(serviceToken))
                        serviceToken = AcquireToken(transactionData, false);
                    if (string.IsNullOrWhiteSpace(serviceToken))
                    {
                        Global.Log.Error("AcquirePurchaseTokens(): Unable to acquire purchase token for service fee transaction.");
                        validationManager.AddError("Unable to acquire purchase token for service fee transaction.");
                        return false;
                    }
                    else
                    {
                        transactionData[Fields.svcPurchaseToken] = serviceToken;
                        sessionManager.Data[Fields.svcPurchaseToken] = serviceToken;
                        transactionData[Fields.svclogin] = string.Empty;
                        transactionData[Fields.svcpassword] = string.Empty;
                    }

                    Global.Log.InfoFormat("Using service purchase token {0}", Bridgepay.Core.Logging.EventId.Default, serviceToken);
                }
                StringBuilder values = new StringBuilder();
                foreach (var keyName in transactionData.AllKeys)
                {
                    values.AppendFormat("{0}:{1}\r\n", keyName,
                        MaskingHelper.MaskSensitiveData(transactionData[keyName], keyName));
                }
                Global.Log.DebugFormat("transactionData after AcquirePaurchaseToken:\r\n{0}", values.ToString());
                if (transactionData.ActualIndex == 1)
                {
                    try
                    {
                        merchantInfoResponse = paymentServer.GetMerchantInfo(transactionData);
                    }
                    catch (Exception ex)
                    {
                        Global.Log.Error("AcquirePurchaseTokens():", ex);
                        validationManager.AddError("There was a problem retrieving the principal account details.");
                        return false;
                    }
                    if (merchantInfoResponse.Result != Global.WeblinkSuccessCode.ToString())
                    {
                        validationManager.AddError(merchantInfoResponse.Message);
                        return false;
                    }

                    var MID = int.Parse(merchantInfoResponse.MID);
                    primaryMID = MID;

                    if (stateManager.PaymentServerType != PaymentServerFactory.PaymentServerTypes.Mock)
                    {
                        PrimaryWLID = dataProvider.GetWeblinkId(primaryMID, null);
                    }
                    sessionManager.Data[transactionData.GetNormalizedFieldName(Fields.WLID)] = PrimaryWLID.ToString();

                    if (!transactionQueue.Data.Exists(transactionData.GetNormalizedFieldName(Fields.MerchantName)))
                        sessionManager.Data[transactionData.GetNormalizedFieldName(Fields.MerchantName)] = merchantInfoResponse.MerchantName;

                    Helper.Merge(sessionManager.Data, merchantInfoResponse.ExtraData);
                    if (!PrimaryWLID.HasValue && stateManager.PaymentServerType != PaymentServerFactory.PaymentServerTypes.Mock)
                    {
                        validationManager.AddErrorFormat("Unable to locate a WLID for account {0}.", primaryMID);
                        return false;
                    }

                    if (stateManager.PaymentServerType != PaymentServerFactory.PaymentServerTypes.Mock &&
                        !dataProvider.IsAuthorized(PrimaryWLID.Value))
                    {
                        validationManager.AddError("Sorry, this merchant account is not authorized to use Weblink.");
                        return false;
                    }

                }
            }

            LoadPreferredPaymentMethod();

            //Get settings from Weblink Settings_T table
            foreach (var setting in GetEffectiveSettings(PrimaryWLID))
            {
                //Posted settings take precedence!
                if (string.IsNullOrEmpty(transactionQueue.Data[setting.Name]))
                {
                    sessionManager.Data[setting.Name] = setting.Value;
                }
            }

            //If we are displaying the Tokenization form, we cant support eCheck so remove that payment type if present
            if (stateManager.Mode == Modes.TokenizationForm)
            {
                string temp = sessionManager.Data[Fields.PaymentTypes];
                temp = temp.Replace(Global.PaymentTypeECheck, "");
                temp = temp.Replace(",,", ",");
                temp = temp.TrimEnd(new char[] { ',' });
                sessionManager.Data[Fields.PaymentTypes] = temp;
            }
            if (_userAgent.ToLower().Contains("os"))
            {
                Cache[sessionManager.ASPNET_SessionId] = sessionManager.Data;
                Cache["transactionQueue" + sessionManager.ASPNET_SessionId] = transactionQueue;
            }
            return true;
        }

        private string AcquireToken(TransactionData transactionData, bool isPrincipal)
        {
            string user;
            string password;
            string purchaserInfo = transactionData[Fields.CustomerAccountCode];
            string transactionInfo = transactionData[Fields.InvoiceNum];
            string certificationId;
            int actionAmount;
            string token;

            if (isPrincipal)
            {
                user = transactionData[Fields.Login];
                password = transactionData[Fields.Password];
                certificationId = sessionManager.SessionId.ToString();
                int totalAmount = Convert.ToInt32(Helper.ToBridgeCommAmount(transactionData[Fields.TotalAmt]));
                int serviceAmount = Convert.ToInt32(Helper.ToBridgeCommAmount(transactionData[Fields.SvcAmount]));
                actionAmount = totalAmount - serviceAmount;
            }
            else
            {
                user = transactionData[Fields.svclogin];
                password = transactionData[Fields.svcpassword];
                certificationId = sessionManager.SessionId != null ? sessionManager.SessionId.ToString() : Guid.NewGuid().ToString();
                actionAmount = Convert.ToInt32(Helper.ToBridgeCommAmount(transactionData[Fields.SvcAmount]));
            }
            using (var client = new ActionService.ActionServiceClient())
            {
                token = client.AcquirePurchaseToken(user, password, certificationId, actionAmount, purchaserInfo, transactionInfo);
            }

            Global.Log.InfoFormat("Acquired Purchase Token: {0} for {1} on session {2} for amount {3} with Customer Account Code {4} and Invoice Number {5}",
                Bridgepay.Core.Logging.EventId.Default,
                token,
                user,
                certificationId,
                actionAmount,
                purchaserInfo,
                transactionInfo);

            return token;
        }

        private void HandlePgWebResponse()
        {
            MapPGWebResponseFieldsToStandardResponseField();

            var data = new NameValueCollection(sessionManager.Data);
            bool isSuccessfullTransaction = data[Fields.pgweb_ResultCode] == "0";

            MaskingHelper.MaskSensitiveData(data);

            if (!isSuccessfullTransaction)
            {
                var template = new ClientSideRedirectTemplate(data[Fields.FailureUrl], data);
                RenderTemplate(template);
                return;
            }
            else
            {
                PerformPostPaymentProcessing(isSuccessfullTransaction);
            }

        }

        private void MapPGWebResponseFieldsToStandardResponseField()
        {
            string lastFour = string.Empty;

            string temp = sessionManager.Data[Fields.pgweb_BogusAccountNumber];
            if (!string.IsNullOrEmpty(temp) && temp.Length > 3)
            {
                lastFour = temp.Substring(temp.Length - 4);
            }

            sessionManager.Data[Fields.r_AuthCode] = sessionManager.Data[Fields.pgweb_AuthCode];
            sessionManager.Data[Fields.r_AuthorizedAmount] = sessionManager.Data[Fields.pgweb_ApprovedAmount];
            sessionManager.Data[Fields.r_CardType] = sessionManager.Data[Fields.pgweb_CardType];
            sessionManager.Data[Fields.r_ExpDate] = sessionManager.Data[Fields.pgweb_ExpirationDate];
            //sessionManager.Data[Fields.r_HostCode] = sessionManager.Data[Fields.pgweb_RefNum];
            sessionManager.Data[Fields.r_Id] = sessionManager.Data[Fields.pgweb_RefNum];
            sessionManager.Data[Fields.r_LastFour] = lastFour;
            sessionManager.Data[Fields.r_Message] = sessionManager.Data[Fields.pgweb_GatewayMessage];
            sessionManager.Data[Fields.r_MessageEx] = sessionManager.Data[Fields.pgweb_ResultText];
            sessionManager.Data[Fields.r_NameOnCard] = sessionManager.Data[Fields.FullName];
            sessionManager.Data[Fields.r_Result] = sessionManager.Data[Fields.pgweb_ResultCode];
            sessionManager.Data[Fields.TotalAmt] = sessionManager.Data[Fields.pgweb_SubmittedAmount];
            sessionManager.Data[Fields.r_Token] = sessionManager.Data[Fields.pgweb_Token];
        }

        protected void Page_UnLoad(object sender, EventArgs e)
        {
            // Result fields are no longer applicable at this point. If they stay here, they cause validation issues
            // on the next request.
            if (sessionManager != null)
                sessionManager.Data.RemoveResultFields();

            if (dataProvider != null)
                dataProvider.Dispose();
        }

        #endregion

        #region "Page modes"

        private void ShowPgWebForm()
        {
            string templateHtml = GetTemplateHtml("PGWebForm");

            PgWebTransaction pgWebTransaction = BuildPgWebTransaction();

            PgWebTemplate template = new PgWebTemplate(templateHtml, pgWebTransaction);
            template.HeadScripts.Add(ScriptType.Core);
            template.HeadScripts.Add(ScriptType.jQuery);
            template.HeadScripts.Add(ScriptType.JSON2);
            template.HeadScripts.Add(ScriptType.PGWeb);
            template.InlineBodyScript = InlineBodyScripts;

            phBody.Controls.Add(new LiteralControl(template.ToString()));
        }

        private PgWebTransaction BuildPgWebTransaction()
        {
            PgWebTransaction transaction = new PgWebTransaction();

            decimal amount = 0;

            decimal.TryParse(sessionManager.Data[Fields.TotalAmt], out amount);

            transaction.amount = amount;
            transaction.clerkId = string.Empty;
            transaction.invNum = sessionManager.Data[Fields.InvoiceNum];
            transaction.pnRefNum = 0;
            transaction.poNum = sessionManager.Data[Fields.PONum];
            transaction.tenderType = "CREDIT";
            transaction.transType = string.IsNullOrEmpty(sessionManager.Data[Fields.TransType]) ? "SALE" : sessionManager.Data[Fields.TransType];

            return transaction;
        }

        private void ShowPaymentForm()
        {
            string paymentFormTemplateName = "PaymentForm";
            if (transactionQueue.HasMultipleTransactions)
                paymentFormTemplateName = "PaymentFormSplit";

            string templateHtml = GetTemplateHtml(paymentFormTemplateName, PrimaryWLID, Payment_Form_TID);

            PreProcessTemplateHtml(ref templateHtml);

            WeblinkTemplate template = new WeblinkTemplate(templateHtml, sessionManager, true);

            if (stateManager.SwipeMethod == SwipeMethod.java)
            {
                template.Stylesheets.Add(StylesheetType.jQueryUI);

                template.HeadScripts.Add(ScriptType.DeployJava);
                template.HeadScripts.Add(ScriptType.JavaApplet);
            }

            RenderTemplate(template);
        }

        private void ShowTokenizationForm()
        {
            WeblinkTemplate template = new WeblinkTemplate(GetTemplateHtml("TokenizationForm", PrimaryWLID, Payment_Form_TID), sessionManager, true);
            RenderTemplate(template);
        }

        /// <summary>
        /// Shows the error form in cases where a transaction was not successful for any reason. 
        /// "Form" is a bit of a misnomer here. If we are in DCP, we arent rendering any forms really.
        /// </summary>
        private void ShowErrorForm()
        {
            IWeblinkTemplate template = null;

            //Lets make sure we always return r_Result
            if (string.IsNullOrEmpty(sessionManager.Data[Fields.r_Result]))
                sessionManager.Data[Fields.r_Result] = Global.WeblinkGenericErrorCode.ToString();

            // A FailUrl was not provided and we are NOT in DCP mode, so we need to re-display the correct payment form.
            if (string.IsNullOrEmpty(sessionManager.Data[Fields.FailureUrl]) && !stateManager.IsDCP)
            {
                // Dont mask the passwords! Wont be able to re-submit the transaction if so
                MaskingHelper.MaskSensitiveData(sessionManager.Data, true);

                //Remove sensitive fields from the Post. We dont want these values re-populated in the payment form...I guess...

                // Todo: Re-evaluate this. 
                // This causes issues when we clear out values that were posted on the initial call to Weblink.
                // I think we really need to only clear out fields that are actually visibly rendered in the template.
                foreach (string field in SensitiveFields)
                    sessionManager.Data.Remove(field);

                string templateHtml = null;
                string templateName = null;

                if (stateManager.Mode == Modes.TokenizationForm)
                    templateName = "TokenizationForm";
                else
                {
                    if (transactionQueue.HasMultipleTransactions)
                        templateName = "PaymentFormSplit";
                    else
                        templateName = "PaymentForm";
                }

                templateHtml = GetTemplateHtml(templateName, PrimaryWLID, Payment_Form_TID);

                PreProcessTemplateHtml(ref templateHtml);

                template = new WeblinkTemplate(templateHtml, sessionManager, true);

                // If we are doing split transactions then we need to throw the r_Result fields into GeneralErrors instead of relying on 
                // a r_Message placeholder like in the single transaction template.
                if (transactionQueue.HasMultipleTransactions)
                    validationManager.AddResultMessages(transactionQueue);

                template.ErrorMessages = validationManager.GeneralErrors;
                RenderTemplate(template);
            }
            else if (stateManager.IsDCP)
            {
                MaskingHelper.MaskSensitiveData(sessionManager.Data);

                sessionManager.Data[Fields.r_Errors] = validationManager.ToString();

                string url;
                url = Request.Url.AbsoluteUri.ToLower().Replace("/weblink.aspx", "/PostToXml.aspx");

                string contentType = string.Empty;
                string resp = string.Empty;
                resp = Helper.PostData(sessionManager.Data.ToNameValuePairString(), url, out contentType);

                Response.Clear();
                Response.ContentType = contentType;
                Response.Write(resp);
            }
            else
            {
                var postUrl = sessionManager.Data[Fields.FailureUrl];

                MaskingHelper.MaskSensitiveData(sessionManager.Data);
                sessionManager.Data.Add(Fields.r_Errors, validationManager.ToString());

                if (stateManager.IsUsingServerSidePostBacks)
                {
                    var response = Helper.PostData(sessionManager.Data.ToNameValuePairString(), postUrl);

                    Response.Clear();
                    Response.Write(response);
                    Response.End();
                }
                else
                {
                    // 10-15-2014 CRF:
                    // Todo: We should remove certain fields here imo. Username, password, etc. But this isnt the time for that.
                    template = new ClientSideRedirectTemplate(postUrl, sessionManager.Data);
                    RenderTemplate(template);
                }
            }
        }

        private void ShowReceipt()
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3; // comparable to modern browsers
            IWeblinkTemplate template = null;
            var maskedData = new NameValueCollection(sessionManager.Data);
            MaskingHelper.MaskSensitiveData(maskedData);
            //If a ReturnReceiptUrl was not specified, lets default to the referrer - DOES NOT WORK WITH SSL!
            //How about this instead? Best i can come up with at the moment...
            //todo: need to come back to this one!
            if (string.IsNullOrEmpty(sessionManager.Data[Fields.ReturnReceiptUrl]))
                sessionManager.Data[Fields.ReturnReceiptUrl] = "javascript:history.go(-2);";

            if (string.IsNullOrEmpty(sessionManager.Data[Fields.ReceiptUrl])) //If no receipt url was specified, show the receipt template
            {
                string receiptTemplateName = "Receipt";

                if (transactionQueue.HasMultipleTransactions)
                    receiptTemplateName = "ReceiptSplit";

                var templateHtml = GetTemplateHtml(receiptTemplateName, PrimaryWLID, Receipt_TID);

                if (!sessionManager.Data.IsPartialApproval())
                {
                    templateHtml = WeblinkTemplate.RemoveFunctionalityBlock(templateHtml, "PartialApproval");
                }

                template = new WeblinkTemplate(templateHtml, sessionManager, false);

                // Scripts arent needed on receipt page.
                InlineBodyScripts = string.Empty;
                InlineHeadScripts = string.Empty;
                BodyScripts.Clear();
                HeadScripts.Clear();
                RenderTemplate(template);
            }
            else // Post to their url and render the output if any...one would hope so.
            {
                var postUrl = sessionManager.Data[Fields.ReceiptUrl];

                if (stateManager.IsUsingServerSidePostBacks)
                {
                    string contentType;
                    var response = Helper.PostData(maskedData.ToNameValuePairString(), postUrl, out contentType);

                    Response.Clear();
                    Response.ContentType = contentType;
                    Response.Write(response);
                    Response.End();
                }
                else
                {
                    // 10-15-2014 CRF:
                    // Todo: We should remove certain fields here imo. Username, password, etc. But this isnt the time for that.
                    template = new ClientSideRedirectTemplate(postUrl, maskedData);
                    RenderTemplate(template);
                }
            }

            sessionManager.Abandon();
        }

        private void PerformPostPaymentProcessing(bool isSuccessfullTransaction)
        {
            string completeUrl = sessionManager.Data[Fields.CompleteUrl];
            Global.Log.Info("PerformPostPaymentProcessing; completeUrl:" + completeUrl);

            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3; // comparable to modern browsers
            if (isSuccessfullTransaction)
            {
                SavePreferredPaymentMethod();

                var maskedData = new NameValueCollection(sessionManager.Data);

                LogPostTransactionDataToEventViewer(maskedData);

                if (!stateManager.IsDCP)
                {
                    ShowReceipt();
                }
                else
                {
                    string resp;
                    string contentType = string.Empty;


                    resp = Helper.PostData(maskedData.ToNameValuePairString(), Request.Url.AbsoluteUri.ToLower().Replace("/weblink.aspx", "/PostToXml.aspx"), out contentType);

                    Response.ContentType = contentType;
                    Response.Clear();
                    Response.Write(resp);
                }

                SendEmails();
            }
            else
            {
                ShowErrorForm();
            }

            if (!string.IsNullOrEmpty(completeUrl) && (!stateManager.RespondWithLastFailureOnly || isSuccessfullTransaction))
            {
                try
                {
                    var maskedData = new NameValueCollection(sessionManager.Data);
                    MaskingHelper.MaskSensitiveData(maskedData);
                    Helper.PostData(maskedData.ToNameValuePairString(), completeUrl);
                }
                catch (Exception ex)
                {
                    Global.Log.Error("Error while posting to CompleteUrl: " + completeUrl, ex);
                }
            }
        }


        private void AcceptPayment()
        {
            // New it up again to get a fresh copy of data. This needs to be re-factored...sigh
            try
            {
                Global.Log.Info("AcceptPayment");
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3; // comparable to modern browsers
                stateManager = new StateManager(sessionManager);
                transactionResults = new Dictionary<int, TransactionResponse>();
                var paymentServerFactory = new PaymentServerFactory(stateManager);
                paymentServer = paymentServerFactory.GetPaymentServer(stateManager.PaymentServerType);
                var reCaptchaResponse = new RecaptchaResponse() { Success = true };
                var invisible = false;
                if (sessionManager.Data.AllKeys.Contains("invisible"))
                {
                    invisible = true;
                }
                if (sessionManager.Data.AllKeys.Contains("eCaptcha"))
                {
                    var reCaptchaRequest = new RecaptchaRequest()
                    {
                        Url = ConfigurationManager.AppSettings["RecaptchaUrl"],
                        Secret = invisible ? ConfigurationManager.AppSettings["RecaptchaInvisibleSecretKey"] : ConfigurationManager.AppSettings["RecaptchaSecretKey"],
                        Token = sessionManager.Data["eCaptcha"]
                    };

                    reCaptchaResponse = Bridgepay.Core.Captcha.RecaptchaValidation.RecaptchaServerSide(reCaptchaRequest);
                }
                if (reCaptchaResponse.Success)
                {
                    if (_userAgent.ToLower().Contains("os"))
                    {
                        transactionQueue = (TransactionQueue)Cache["transactionQueue" + sessionManager.ASPNET_SessionId];
                    }
                    for (int i = 0; i < transactionQueue.TransactionCount; i++)
                    {
                        var singletonTransData = transactionQueue.Transactions[i];
                        Global.Log.DebugFormat("AcceptPayment singletonTransData:\r\n{0}", MaskingHelper.MaskPossiblySensitiveData(singletonTransData, false));
                        var response = paymentServer.ProcessTransaction(singletonTransData);
                        transactionResults.Add(i, response);
                        // Add the response fields to the current transaction data set
                        response.MergeResults(ref singletonTransData);

                        // Now, merge the transaction results into the original post data so that it all can be returned
                        singletonTransData.MergeTransactionResults(sessionManager.Data);

                        if (response.Result != "0")
                        {
                            if (stateManager.RespondWithLastFailureOnly)
                            {
                                DateTime createDate = DateTime.Now;
                                NameValueCollection maskedData = new NameValueCollection(singletonTransData);
                                MaskingHelper.MaskSensitiveData(maskedData);

                                FailedTransactions_T failedTransaction = new FailedTransactions_T
                                {
                                    ASPNET_SessionId = sessionManager.ASPNET_SessionId,
                                    CreateDate = DateTime.UtcNow,
                                    ExpirationDate = sessionManager.GetSessionExpiration().ToUniversalTime(),
                                    CompleteUrl = sessionManager.Data[Fields.CompleteUrl],
                                    SessionId = sessionManager.SessionId.Value,
                                    TransactionData = maskedData.ToNameValuePairString(),
                                    WLID = Convert.ToInt32(sessionManager.Data.Get(Fields.WLID)),
                                    SessionTimeout = Session.Timeout
                                };

                                dataProvider.InsertFailedTransaction(failedTransaction);
                                sessionManager.HasFailedTransactions = true;
                            }
                            // Dont process subsequent transactions if we fail this one
                            break;
                        }
                        else if (sessionManager.HasFailedTransactions)
                        {
                            // Delete the failed transactions, otherwise the last one will get posted back to the integrator
                            dataProvider.DeleteFailedTransactions(sessionManager.SessionId.Value);
                        }
                    }
                }
                else
                {
                    validationManager.AddError("Please verify you are not a robot!");
                    ShowErrorForm();
                }
                // If we are processing multiple transactions and any have declined, we need to void them.
                if (transactionQueue.HasMultipleTransactions && transactionQueue.HasFailedTransactions)
                {
                    for (int i = 0; i < transactionQueue.Transactions.Count; i++)
                    {
                        TransactionData transaction = transactionQueue.Transactions[i];

                        if (transaction.Id.HasValue && transaction.Result == 0)
                        {
                            var response = paymentServer.VoidTransaction((NameValueCollection)transaction);
                            response.MergeResults(ref transaction, true);
                            // Merge the results of this transaction into our main post data
                            transaction.MergeTransactionResults(sessionManager.Data);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Global.Log.Error("AcceptPayment", ex);
                validationManager.AddError(ex.Message);
                ShowErrorForm();
            }
        }

        private void SavePreferredPaymentMethod()
        {
            // Make sure we are allowed to do this and have all the necessary info first
            if (!stateManager.IsSavingWalletInfo)
            {
                return;
            }

            if (!stateManager.IsDCP)
            {
                if (string.IsNullOrEmpty(Request.Form[Fields.SavePreferredPaymentMethod]) && stateManager.Mode != Modes.TokenizationForm)
                    return;
            }

            // If we've made it this far, we have what we need to save the preferred payment method

            // Build a PaymentMethod object from both the posted data and some of what was returned from the payment server
            var transaction = transactionQueue.Transactions[0];
            PaymentMethod paymentMethod = new PaymentMethod
            {
                CardToken = transaction[Fields.r_Token],
                CardType = transaction[Fields.CardTypeName],
                CardExp = Helper.GetExpDate(transaction),
                LastFour = Helper.GetLastFour(transaction),
                CardHolderAddress1 = transaction[Fields.Address1],
                CardHolderAddress2 = transaction[Fields.Address2],
                CardHolderCity = transaction[Fields.City],
                CardHolderName = transaction[Fields.FullName],
                CardHolderState = transaction[Fields.State],
                CardHolderZip = transaction[Fields.ZipCode],
                ContactEmail = transaction[Fields.Email],
                ContactPhone = transaction[Fields.Phone]
            };

            WalletResponse resp = new WalletResponse
            {
                ResultCode = Global.WeblinkGenericErrorCode.ToString()
            };

            try
            {
                var macString = transaction[Fields.MerchantAccountCode];
                if (string.IsNullOrWhiteSpace(macString))
                    throw new InvalidOperationException(nameof(Fields.MerchantAccountCode) + " not found.");

                int merchantAccountCode = int.Parse(macString);

                resp = walletProvider.SavePrimaryPaymentMethod(paymentMethod, merchantAccountCode);
            }
            catch (Exception ex)
            {
                Global.Log.Error(ex.Message, ex);
                resp.Message = Resources.WalletSaveError;
            }
            sessionManager.Data[Fields.r_WalletMessage] = resp.Message;
            sessionManager.Data[Fields.r_WalletResultCode] = resp.ResultCode;

            if (resp.ResultCode == Global.WeblinkSuccessCode.ToString())
            {
                IBridgepayAuth auth = new WalletAuth(new WalletData
                {
                    SiteId = Guid.Parse(sessionManager.Data[Fields.SiteId]),
                    WalletId = resp.WalletId
                });
                sessionManager.Data[Fields.r_WalletToken] = auth.GenerateAuthToken();
                sessionManager.Data[Fields.r_WalletKey] = auth.GenerateAuthKey();
            }
        }
        private void LogPostTransactionDataToEventViewer(NameValueCollection postData)
        {
            MaskingHelper.MaskSensitiveData(postData);

            StringBuilder values = new StringBuilder();

            foreach (string key in postData.AllKeys)
            {
                values.AppendFormat("{0}:{1}\r\n", key, postData[key]);
            }

            Global.Log.DebugFormat("Received the following values from Bridgecomm:\r\n{0}", values.ToString());
        }
        private string GetWalletSignature(Guid WalletId)
        {
            // Todo: Find a better home for this?
            return string.Format("{0}-{1}", transactionQueue.WalletData.WalletId, transactionQueue.WalletData.SiteId);
        }

        #endregion

        #region Helpers

        private bool LoadUserInfo()
        {
            // Load all users associated with this post. This could be multiple users if this is a split transaction.
            // We consider the first user the primary user and whose WLID we'll use for templates/settings, etc.
            int primaryMID = 0;

            var paymentServerFactory = new PaymentServerFactory(stateManager);
            paymentServer = paymentServerFactory.GetPaymentServer(stateManager.PaymentServerType);

            foreach (var transactionData in transactionQueue.Transactions)
            {
                MerchantInfoResponse merchantInfoResponse;

                try
                {
                    merchantInfoResponse = paymentServer.GetMerchantInfo(transactionData);
                }
                catch (Exception ex)
                {
                    Global.Log.Error("LoadUserInfo():", ex);
                    validationManager.AddErrorFormat("There was a problem retrieving the account details for user {0}.", transactionData[Fields.Login]);
                    return false;
                }

                if (merchantInfoResponse.Result != Global.WeblinkSuccessCode.ToString())
                {
                    validationManager.AddError(merchantInfoResponse.Message);
                    return false;
                }

                var MID = int.Parse(merchantInfoResponse.MID);

                if (transactionData.ActualIndex == 1)
                {
                    primaryMID = MID;

                    if (stateManager.PaymentServerType != PaymentServerFactory.PaymentServerTypes.Mock)
                    {
                        PrimaryWLID = dataProvider.GetWeblinkId(primaryMID, null);
                    }
                    sessionManager.Data[transactionData.GetNormalizedFieldName(Fields.WLID)] = PrimaryWLID.ToString();
                }

                if (!transactionQueue.Data.Exists(transactionData.GetNormalizedFieldName(Fields.MerchantName)))
                    sessionManager.Data[transactionData.GetNormalizedFieldName(Fields.MerchantName)] = merchantInfoResponse.MerchantName;

                Helper.Merge(sessionManager.Data, merchantInfoResponse.ExtraData);

                if (!PrimaryWLID.HasValue && stateManager.PaymentServerType != PaymentServerFactory.PaymentServerTypes.Mock)
                {
                    validationManager.AddErrorFormat("Unable to locate a WLID for user {0}.", transactionData[Fields.Login]);
                    return false;
                }

                if (stateManager.PaymentServerType != PaymentServerFactory.PaymentServerTypes.Mock &&
                    !dataProvider.IsAuthorized(PrimaryWLID.Value))
                {
                    validationManager.AddErrorFormat("Sorry, the merchant account associated to user {0} is not authorized to use Weblink.", transactionData[Fields.Login]);
                    return false;
                }
            }

            LoadPreferredPaymentMethod();

            //Get settings from Weblink Settings_T table
            foreach (var setting in GetEffectiveSettings(PrimaryWLID))
            {
                //Posted settings take precedence!
                if (string.IsNullOrEmpty(transactionQueue.Data[setting.Name]))
                {
                    sessionManager.Data[setting.Name] = setting.Value;
                }
            }

            //If we are displaying the Tokenization form, we cant support eCheck so remove that payment type if present
            if (stateManager.Mode == Modes.TokenizationForm)
            {
                string temp = sessionManager.Data[Fields.PaymentTypes];
                temp = temp.Replace(Global.PaymentTypeECheck, "");
                temp = temp.Replace(",,", ",");
                temp = temp.TrimEnd(new char[] { ',' });
                sessionManager.Data[Fields.PaymentTypes] = temp;
            }

            return true;
        }

        /// <summary>
        /// Loads the wallet payment method details into the transactionQueue object
        /// </summary>
        private bool LoadPreferredPaymentMethod()
        {
            if (string.IsNullOrEmpty(Request.Form[Fields.EnableWallet]))
                return false;

            if (transactionQueue.WalletData.SiteId == null ||
                transactionQueue.WalletData.WalletId == null)
            {
                return false;
            }

            WalletResponse resp = new WalletResponse
            {
                ResultCode = Global.WeblinkGenericErrorCode.ToString()
            };

            try
            {
                resp = walletProvider.GetPrimaryPaymentMethod();

                if (resp.PaymentMethod != null)
                {
                    // Set these fields so we can display them in the payment form when using a preferred payment method
                    sessionManager.Data[Fields.WalletCardNumber] = new string('X', 12) + resp.PaymentMethod.LastFour;
                    sessionManager.Data[Fields.WalletExpDate] = resp.PaymentMethod.CardExp;
                    sessionManager.Data[Fields.WalletCardType] = resp.PaymentMethod.CardType;
                    sessionManager.Data[Fields.ExpDate] = resp.PaymentMethod.CardExp;
                    sessionManager.Data[Fields.Token] = resp.PaymentMethod.CardToken;
                    sessionManager.HasPreferredPaymentMethodLoaded = true;
                }
            }
            catch (Exception exWallet)
            {
                Global.Log.Error("LoadPreferredPaymentMethod", exWallet);
                resp.Message = exWallet.Message;
            }

            sessionManager.Data[Fields.r_WalletMessage] = resp.Message;
            sessionManager.Data[Fields.r_WalletResultCode] = resp.ResultCode;

            return resp.ResultCode.Equals(Global.WeblinkSuccessCode.ToString());
        }

        private void SendEmails()
        {
            try
            {
                string to = null;
                string from = transactionQueue.Data[Fields.EmailFrom];
                if (string.IsNullOrEmpty(from) && stateManager.PaymentServerType != PaymentServerFactory.PaymentServerTypes.Mock)
                    from = dataProvider.GetSetting("EmailFrom", PrimaryWLID.Value);
                if (string.IsNullOrEmpty(from))
                    from = ConfigurationManager.AppSettings["EmailFrom"].ToString();

                from = from ?? "Weblink3";

                string body = null;
                string templateName = null;

                //Send customer email:
                to = transactionQueue.Data[Fields.Email];
                if (!string.IsNullOrEmpty(to))
                {
                    templateName = "CustomerEmail";
                    if (transactionQueue.HasMultipleTransactions)
                    {
                        templateName += "Split";
                    }

                    body = GetTemplateHtml(templateName, PrimaryWLID.Value, Customer_Email_TID);
                    WeblinkTemplate Template = new WeblinkTemplate(body, sessionManager, false);
                    Email.SendEmail(to, "", "", from, "Your payment receipt", Template.ToString(), true);
                }

                //Send merchant email:
                to = transactionQueue.Data[Fields.MerchantEmail];
                if (!string.IsNullOrEmpty(to))
                {
                    templateName = "MerchantEmail";
                    if (transactionQueue.HasMultipleTransactions)
                    {
                        templateName += "Split";
                    }

                    body = GetTemplateHtml(templateName, PrimaryWLID.Value, Merchant_Email_TID);
                    WeblinkTemplate Template = new WeblinkTemplate(body, sessionManager, false);
                    Email.SendEmail(to, "", "", from, "You have received a new payment", Template.ToString(), true);
                }
            }
            catch (Exception ex)
            {
                Global.Log.Error(MaskingHelper.MaskPossiblySensitiveData(ex.Message));
            }
        }

        private void RenderTemplate(IWeblinkTemplate template)
        {
            template.InlineHeadScript = InlineHeadScripts;
            template.InlineBodyScript = InlineBodyScripts;
            template.HeadScripts = HeadScripts;
            phBody.Controls.Add(new LiteralControl(template.ToString()));
            Global.Log.InfoFormat("RenderTemplate returned: {0}", Bridgepay.Core.Logging.EventId.Default, template.ToString());
        }

        /// <summary>
        /// Removes sections of the template based on whether or not they should be rendered or not
        /// </summary>
        /// <param name="templateHtml"></param>
        private void PreProcessTemplateHtml(ref string templateHtml)
        {
            // If there is no preferred payment method, then remove the checkbox from the page
            if (!sessionManager.HasPreferredPaymentMethodLoaded)
                templateHtml = WeblinkTemplate.RemoveFunctionalityBlock(templateHtml, "WalletUse");

            // If the wallet is not enabled, remove the checkbox to save the preferred payment method
            if (!stateManager.AllowWalletSave)
            {
                templateHtml = WeblinkTemplate.RemoveFunctionalityBlock(templateHtml, "WalletSave");
            }

            // If a CancelUrl was not passed in, remove the cancel button
            if (string.IsNullOrEmpty(sessionManager.Data[Fields.CancelUrl]))
            {
                templateHtml = WeblinkTemplate.RemoveFunctionalityBlock(templateHtml, "CancelPayment");
            }

            //If the call to WL contains a token, then we are not showing payment information.
            if (stateManager.IsPlainTokenizedTransaction)
            {
                templateHtml = WeblinkTemplate.RemoveFunctionalityBlock(templateHtml, "PaymentInformation");
            }

            if (stateManager.SwipeMethod == SwipeMethod.none)
            {
                templateHtml = WeblinkTemplate.RemoveFunctionalityBlock(templateHtml, "MSR");
            }

            if (!string.IsNullOrEmpty(sessionManager.Data[Fields.AutoInitMSR]))
            {
                templateHtml = WeblinkTemplate.RemoveFunctionalityBlock(templateHtml, "SwipeButton");
            }
        }

        /// <summary>
        /// Performs any needed pre-processing(modifcation) of the posted data.
        /// </summary>
        /// <param name="PostData"></param>
        private void PreProcessPostedData(NameValueCollection PostData)
        {
            // If a two digit year was passed in, convert to 4 digit.
            var cardExpYear = PostData[Fields.CardExpYear];

            if (!string.IsNullOrEmpty(cardExpYear) && cardExpYear.Length == 2)
            {
                PostData[Fields.CardExpYear] = string.Concat("20", cardExpYear);
            }

            // If no return url was passed in, attempt to get one.
            if (string.IsNullOrEmpty(PostData[Fields.ReturnUrl]))
            {
                if (Request.UrlReferrer != null)
                {
                    PostData[Fields.ReturnUrl] = Request.UrlReferrer.AbsoluteUri;
                }
            }
        }

        private string GetTemplateHtml(string type, int? WLID = null, int? TID = null)
        {
            var templateHtml = string.Empty;

            if ((stateManager != null && stateManager.PaymentServerType == PaymentServerFactory.PaymentServerTypes.Mock) ||
                Global.DebuggingTemplates)
            {
                using (StreamReader sr = new StreamReader(HttpContext.Current.Server.MapPath(string.Format(@"_templates\{0}.html", type))))
                {
                    templateHtml = sr.ReadToEnd();
                    sr.Close();
                }
            }
            else
            {
                templateHtml = dataProvider.GetTemplate(type, WLID, TID);
            }

            return templateHtml;
        }

        private IEnumerable<Settings_T> GetEffectiveSettings(int? wlid)
        {
            if (stateManager.PaymentServerType == PaymentServerFactory.PaymentServerTypes.Mock ||
                Global.DebuggingTemplates)
            {
                return new List<Settings_T>
                {
                    new Settings_T
                    {
                        Name = "PaymentTypes",
                        Value = "AMEX,Discover,Mastercard,Visa,eCheck"
                    },
                    new Settings_T
                    {
                        Name = "ReceiptLogoUrl",
                        Value = "images/bpn_logo.png"
                    }
                }.AsEnumerable();
            }
            else
            {
                return dataProvider.GetEffectiveSettings(wlid.Value);
            }
        }

        #endregion

        #region "Web methods"

        [WebMethod]
        public static string EncryptPassword(string password)
        {
            return Helper.EncryptPassword(HttpUtility.UrlDecode(password));
        }

        [WebMethod]
        public static string DecryptPassword(string encryptedPassword)
        {
            return Helper.DecryptPassword(encryptedPassword);
        }

        [WebMethod]
        public static void SaveMSRSettings(string settings)
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies[General.MSRCookieName];
            if (cookie == null)
            {
                cookie = new HttpCookie(General.MSRCookieName, settings);
                cookie.Expires = DateTime.Now.AddYears(10);
                HttpContext.Current.Response.AppendCookie(cookie);
            }
            else
            {
                cookie.Value = settings;
                cookie.Expires = DateTime.Now.AddYears(10);
                cookie.SameSite = SameSiteMode.None;
                cookie.Secure = true;
                HttpContext.Current.Response.SetCookie(cookie);
            }
        }
        [ScriptMethod, WebMethod]
        public string ValidateDomain(string domain)
        {
            WeblinkDBDataContext db = new WeblinkDBDataContext();
            // The "root domain" is the host with any subdomains removed, e.g. foo.bar.baz.com => baz.com
            var rootDomain = GetRootDomain(domain);
            var existDomain = db.Settings_Ts.Where(s => (s.WLID == null || s.WLID == PrimaryWLID) && s.Name == "iFrameDomains" && s.Value.Contains(rootDomain)).FirstOrDefault();
            if (existDomain != null)
            {
                Session["DomainExist"] = "true";

                // TP 15689 - Return any matching iFrameDomain so it can be added to the CSP header.
                return GetIFrameDomain(existDomain.Value, domain, rootDomain);
            }
            else
            {
                Session["DomainExist"] = "false";
                return null;
            }
        }
        internal static string GetRootDomain(string domain)
        {
            var commonRoots = ConfigurationManager.AppSettings["CommomDomainRoots"].Split(',');
            var rootDomain = domain;
            foreach (string s in commonRoots)
            {
                if (domain.EndsWith(s))
                {
                    int idx = domain.IndexOf(s);
                    int sec = domain.Substring(0, idx - 1).LastIndexOf('.');
                    rootDomain = domain.Substring(sec + 1);
                }
            }
            return rootDomain;
        }

        internal static List<string> SplitIFrameDomains(string iFrameDomains)
        {
            if (string.IsNullOrWhiteSpace(iFrameDomains))
                return null;

            var domains = iFrameDomains.Split(new char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries)
                                       .Select(s => s.Trim().ToLower())
                                       .ToList();

            return domains;
        }

        internal static string GetIFrameDomain(string iFrameDomains, string fqdn, string rootDomain)
        {
            if (string.IsNullOrWhiteSpace(iFrameDomains))
                return null;

            var domains = SplitIFrameDomains(iFrameDomains);
            if (domains == null || domains.Count() == 0)
                return null;

            // Look for a wildcard match first. This code does not support subdomains with wildcards.
            // If the user supplied a wildcard, it's going to take precedence even if there is another
            // domain that's an exact match.
            var wildcardMatch = domains.FirstOrDefault(d => d.Contains($"*.{rootDomain.ToLower()}"));
            if (wildcardMatch != null)
                return wildcardMatch;

            // No wildcard match, so match on the fully-qualified domain name, e.g., foo.bar.baz.com
            return domains.FirstOrDefault(d => d.Contains(fqdn.ToLower()));
        }

        #endregion

        /// <summary>
        /// Logs authentication status data per TP 16425.
        /// </summary>
        internal static string GetAuthenticationStatusLog(NameValueCollection form)
        {
            if (form == null)
            {
                throw new ArgumentNullException(nameof(form));
            }

            var username = form[Fields.Login];
            var usernameProvided = string.IsNullOrWhiteSpace(username) ? "No" : "Yes";
            var passwordProvided = string.IsNullOrWhiteSpace(form[Fields.Password]) ? "No" : "Yes";
            var passwordEncrypted = string.IsNullOrWhiteSpace(form[Fields.PasswordEncrypted]) ? "No" : "Yes";
            var purchaseTokenProvided = string.IsNullOrWhiteSpace(form[Fields.PurchaseToken]) ? "No" : "Yes";
            var mode = string.IsNullOrWhiteSpace(form[Fields.Mode]) ? "DCP" : form[Fields.Mode];

            return $"AuthenticationStatus: Username Provided: {usernameProvided}, Username: {username}, Password Provided: {passwordProvided}, PasswordEncrypted: {passwordEncrypted}, PurchaseToken Provided: {purchaseTokenProvided}, Mode: {mode}";
        }
    }
}