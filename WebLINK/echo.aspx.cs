﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebLINK
{
    /// <summary>
    /// This just echos back anything POST'ed to it.
    /// </summary>
    public partial class echo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var qry = from post in Request.Form.AllKeys.AsEnumerable()
                      select new
                      {
                          Name = post,
                          Value = HttpUtility.UrlDecode(Request.Form[post])
                      };
            rptPost.DataSource = qry;
            rptPost.DataBind();
        }
    }
}