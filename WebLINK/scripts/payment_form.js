﻿(function () {

    BP = {
        paymentForm: {},
        util: {}
    };

    BP.isFormValid = true;
    BP.requiredFields = undefined;
    BP.requiredFieldsCopy = undefined;
    BP.requiredCCFields = undefined;
    BP.requiredCheckFields = undefined;

    BP.debug = false;

    var originalConsoleLog = (window.console && window.console.log) || $.noop;

    window.console = window.console || {};
    window.console.log = function (message) {
        if (BP.debug && originalConsoleLog.call) {
            originalConsoleLog.call(this, message);
        }
    };

    BP.root = document.getElementById("appRoot").href;

    $(function () {

        BP.timerId =
            countdown(
                function (ts) {
                    var timeLeftEl = document.getElementById('timeLeft');

                    if (timeLeftEl) {
                        var timeLeft = ts.toHTML() || "0 seconds";
                        document.getElementById('timeLeft').innerHTML = timeLeft
                    }

                    if (ts.minutes === 0 && ts.seconds === 0) {
                        clearInterval(BP.timerId);
                        var returnUrl = document.getElementById('ReturnUrl').value;
                        BP.paymentForm.handleTimeout(returnUrl);
                    }
                },
                BP.sessionTimeout,
                countdown.MINUTES | countdown.SECONDS);

        BP.paymentForm.updatePaymentMethod();
        $("#container").fadeIn();
        $("input:first").focus();
        $("input[name='TotalAmt']").change(function () {
            var $amt = $(this);
            if ($amt.val() != '')
                $amt.val(BP.util.formatCurrency($amt.val()));
            calculateGrandTotal();
        });
        $("input, select").blur(function () {
            BP.paymentForm.validateField($(this).attr("name"));
        });

        $("button[type=submit]").on('click', function (e) {
            $(":button").prop("disabled", true);
            //$('.loading-mask').css("display","block");
            e.preventDefault();
            //grecaptcha.execute();
            //grecaptcha.getResponse();
            if (BP.paymentForm.validateForm()) {
                $(e.currentTarget).attr("disabled", "disabled");
                clearInterval(BP.timerId);
                $("form").submit();
                return;
            }
            $(":button").prop("disabled", false);
            //$('.loading-mask').hide();
        });

        $("button.cancel").on('click', function (e) {
            e.preventDefault();
            $(":button").prop("disabled", true)
            var cancelUrl = $("input[name='CancelUrl']").val();

            if (cancelUrl) {
                var tokens = cancelUrl.split(' ');
                cancelUrl = tokens[0];

                if (tokens.length > 1 &&
                    tokens[1].toLowerCase() === 'confirm' &&
                    !confirm('Click OK to cancel this transaction.')) {
                    return;
                }

                window.location = cancelUrl;
            }
            $(":button").prop("disabled", false);
        });

        $("button[type='reset']").click(function (e) {
            e.preventDefault();
            $(":button").prop("disabled", true)
            $("input[type!='hidden'], select")
                .each(function () {
                    var $el = $(this);
                    if (!$el.is('[readonly]')) {
                        $el.val('');
                        $el.prop('disabled', false);
                    }
                });

            BP.paymentForm.updatePaymentMethod();
            $("span.formError").remove();
            $(":button").prop("disabled", false)
        });

        $("#usePreferredPaymentMethod").click(function (e) {
            var $el = $(this);
            var $cardType = $('[name=CardTypeName]');
            var $cardNumber = $('[name=CardNumber]');
            var isChecked = $el.is(':checked');
            if (isChecked) {
                $cardType.val(document.getElementById('tg_WalletCardType').value);
                BP.paymentForm.updatePaymentMethod();

                var expDate = document.getElementById('tg_WalletExpDate').value;
                // Expiration dates are currently split into pairs of comboboxes and hidden fields.
                // Set the values of both to the expiration date parts of the card on file.
                $('[name=CardExpMonth], [name=ComboCardExpMonth]').val(expDate.substring(0, 2));
                $('[name=CardExpYear], [name=ComboCardExpYear]').val(expDate.substring(2, 4));

                var maskedCardNumber = document.getElementById('tg_WalletCardNumber').value;

                $cardNumber.val(maskedCardNumber);

                $("div.preferred-payment-method").hide()
                    .find('input[type=checkbox]').attr('checked', false);
                $('[name=CVNum]').focus();
            } else {
                $cardType.val('');
                BP.paymentForm.updatePaymentMethod();
            }
            $cardType.attr('disabled', isChecked ? 'disabled' : null);
            $cardNumber.attr('disabled', isChecked ? 'disabled' : null);
            // Disable the card expiration comboboxes, but not the hidden fields so they'll be included in the posted form.
            $('[name=ComboCardExpMonth]').attr('disabled', isChecked ? 'disabled' : null);
            $('[name=ComboCardExpYear]').attr('disabled', isChecked ? 'disabled' : null);
        });

        calculateGrandTotal();
    });

    BP.paymentForm.handleTimeout = function (url) {
        if (url) {
            location.replace(url);
        } else {
            alert('Your session has timed out.');
        }
    }

    BP.paymentForm.updatePaymentMethod = window.updatePaymentMethod = function () {
        var $e = $("select[name='CardTypeName']");
        $(".cc,.check").hide();

        var st = $e.find('option:selected').attr('st');
        if (st != null)
            $("." + st).show();

        //ugh...this handles the case where CardTypeName is not rendered. ie processing a txn via a token
        //default to cc since checks arent supported with tokenization
        if (!$e.length)
            st = 'cc';

        //reset required fields
        if (BP.requiredFieldsCopy && BP.requiredFields)
            BP.requiredFields = BP.requiredFieldsCopy.slice(0);

        if (BP.requiredFields) {
            switch (st) {
                case 'check':
                    BP.requiredFields = BP.requiredFields.concat(BP.requiredCheckFields);
                    break;
                case 'cc':
                    BP.requiredFields = BP.requiredFields.concat(BP.requiredCCFields);
                    break;
            }
        }

        //need to update the required_fields hidden input too!
        var $requiredFields = $("input[name='required_fields']");
        $requiredFields.val(BP.requiredFields == null ? null : BP.requiredFields.join(','));

        BP.paymentForm.clearPaymentInformationForm();
    }

    BP.paymentForm.clearPaymentInformationForm = function () {
        $(".payment-details")
            .find('input[type=text], select')
            .not('[name=CardTypeName]')
            .each(function () {
                var $el = $(this);
                $el.val('');
            });
    }

    BP.paymentForm.validateForm = function () {

        BP.isFormValid = true;

        $.each(BP.requiredFields, function (i, val) {
            BP.paymentForm.validateField(val);
        });

        // Special case for TotalAmt now due to split transactions.
        // This is not ideal!
        // Todo: Populate RequiredFields collection for split transactions properly
        $('input[name^="TotalAmt"]').each(function () {
            var $el = $(this);
            if ($el.val() == '' || $el.val() == '0.00') {
                addError($el.attr('name'), "*");
                isFormValid = false;
                $el.focus();
            }
        });
        if (BP.isFormValid && $("[name='eCaptcha']").length > 0 && $("input[name='invisible']").length <= 0) {
            if (grecaptcha != null && grecaptcha.getResponse().length > 0)
                $("[name='eCaptcha']").val(grecaptcha.getResponse());
            else {
                BP.isFormValid = false;
                addError("eCaptcha", "Please verify you are not a robot!");
            }
        }
        if (!BP.isFormValid) {
            $(document.forms[0]).find('.validation-error:first').focus();
            alert('Please provide the missing information.');
            $('.loading-mask').hide();
        }
        if ($("select[name='CardTypeName']").find('option:selected').attr('st') == 'cc') {
            var todayDate = new Date();

            var currentMonth = todayDate.getMonth();
            var currentYear = todayDate.getFullYear();

            var cardYear = $("[name*='CardExpYear']").val();
            var cardMonth = $("[name*='CardExpMonth']").val();
            if (cardYear.length == 2) {
                // If the year is a string you want 2028 here, not 200028.
                cardYear = 2000 + parseInt(cardYear, 10);
            }

            var expirationDate = new Date(cardYear, cardMonth - 1)
            var currentDate = new Date(currentYear, currentMonth);

            if (currentDate > expirationDate) {
                BP.isFormValid = false;
                addError("btnCardExpYear", "Invalid expiration date");
            }
        }
        return BP.isFormValid;
    }

    BP.paymentForm.validateField = function (fieldName) {
        var isFieldValid = true;
        var $input = $("[name^='" + fieldName + "']:not(:hidden)");

        if ($input.length) {
            $input.parent().find("span.formError").remove();
            $input.removeClass("validation-error");

            if ((jQuery.inArray(fieldName, BP.requiredFields) != -1) && $input.val() == '') {
                isFieldValid = false;
                addError(fieldName, "*");
            }

            // Only validate the routing/pan if we are not using a preferred payment method
            if (!$("#usePreferredPaymentMethod").is(':checked')) {
                if (fieldName.toLowerCase() == "cardnumber" &&
                    $input.val() != "" &&
                    !BP.util.checkCreditCard($input.val())) {
                    isFieldValid = false;
                }

                if (fieldName.toLowerCase() == "transitnum" && $input.val() != "" & !BP.util.checkABA($input.val())) {
                    isFieldValid = false;
                    addError(fieldName, "Invalid routing #");
                }
            }

            if (fieldName.toLowerCase() == "billtozip" && !BP.util.zipCodeValidator($input.val())) {
                isFieldValid = false;
                addError(fieldName, "Invalid Zip");
            }

            if (fieldName.toLowerCase() == "billtophone" && !BP.util.phoneNumberValidator($input.val())) {
                isFieldValid = false;
                addError(fieldName, "Invalid Phone");
            }
        }

        if (!isFieldValid)
            BP.isFormValid = false;
    }

    BP.paymentForm.createOrUpdateHiddenField = function (fieldName, value) {
        var input = $("input[name='" + fieldName + "']").get(0);

        if (!input) {
            input = document.createElement("input");
            document.forms[0].appendChild(input);
        }

        input.setAttribute("type", "hidden");
        input.setAttribute("name", fieldName);
        input.setAttribute("value", value);
    };

    BP.util.formatCurrency = function (num) {
        num = num.toString().replace(/$|,/g, "");
        if (isNaN(num))
            num = "0";

        sign = (num == (num = Math.abs(num)));
        num = Math.floor(num * 100 + 0.50000000001);
        cents = num % 100;
        num = Math.floor(num / 100).toString();

        if (cents < 10)
            cents = "0" + cents;

        for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
            num = num.substring(0, num.length - (4 * i + 3)) + "," +
                num.substring(num.length - (4 * i + 3));

        return (((sign) ? "" : "-") + num + "." + cents);
    }

    function addError(inputName, message) {
        var $input = $("[name='" + inputName + "']");
        $input.addClass("validation-error");
        if ($input.parent().find("span.formError").length == 0)
            $input.after("<span class='formError'>" + message + "</span>");
    }

    function calculateGrandTotal() {
        var total = 0.0;

        $('input.amount').each(function () {
            var $el = $(this);
            var amt = parseFloat($el.val().replace(/,/g, ""))
            total += isNaN(amt) ? 0 : amt;
        });

        $('#grandTotal').html(BP.util.formatCurrency(total));
    }

    BP.util.Mod10 = function (value) {
        // accept only digits, dashes or spaces
        if (/[^0-9-\s]+/.test(value)) return false;

        // The Luhn Algorithm. It's so pretty.
        var nCheck = 0, nDigit = 0, bEven = false;
        value = value.replace(/\D/g, "");

        for (var n = value.length - 1; n >= 0; n--) {
            var cDigit = value.charAt(n),
                nDigit = parseInt(cDigit, 10);

            if (bEven) {
                if ((nDigit *= 2) > 9) nDigit -= 9;
            }

            nCheck += nDigit;
            bEven = !bEven;
        }

        return (nCheck % 10) == 0;
    }

    BP.util.checkABA = function (s) {

        var i, n, t;

        // First, remove any non-numeric characters.

        t = "";
        for (i = 0; i < s.length; i++) {
            c = parseInt(s.charAt(i), 10);
            if (c >= 0 && c <= 9)
                t = t + c;
        }

        // Check the length, it should be nine digits.

        if (t.length != 9)
            return false;

        // Now run through each digit and calculate the total.

        n = 0;
        for (i = 0; i < t.length; i += 3) {
            n += parseInt(t.charAt(i), 10) * 3
                + parseInt(t.charAt(i + 1), 10) * 7
                + parseInt(t.charAt(i + 2), 10);
        }

        // If the resulting sum is an even multiple of ten (but not zero),
        // the aba routing number is good.

        if (n != 0 && n % 10 == 0)
            return true;
        else
            return false;
    }
    BP.util.checkCreditCard = function () {
        var ccNum = $("[name='CardNumber']").val();
        var ccType = $("[name='CardTypeName']").val();
        var visaRegEx = /^(?:4[0-9]{12}(?:[0-9]{3})?)$/;
        var mastercardRegEx = /^(?:5[1-5][0-9]{14})$/;
        var mastercard2RegEx = /^2(?:22[1-9]|2[3-9][0-9]|[3-6][0-9]{2}|7[0-1][0-9]|720)[0-9]{12}$/;
        var amexpRegEx = /^(?:3[47][0-9]{13})$/;
        var discovRegEx = /^(?:6(?:011|5[0-9][0-9])[0-9]{12})$/;
        var dinersRegEx = /^3(?:0[0-5]|[68][0-9])[0-9]{11}$/;
        var isValid = false;
        if (ccType == "Visa") {
            isValid = visaRegEx.test(ccNum);
        }
        else if (ccType == "Mastercard") {
            isValid = mastercardRegEx.test(ccNum);
            //if is invalid it is possible it is mastercard 2
            if (!isValid) {
                isValid = mastercard2RegEx.test(ccNum);
            }
        } else if (ccType == "AMEX") {
            isValid = amexpRegEx.test(ccNum);
        } else if (ccType == "Discover") {
            isValid = discovRegEx.test(ccNum);
        } else if (ccType == "Diners") {
            isValid = dinersRegEx.test(ccNum);
        }
        if (!isValid) { addError("CardNumber", "Invalid " + ccType + " number!"); }
        return isValid;
    }
    $(function () {
        pageLoad();
        $("select[name='CardExpYear']").combobox();
        $("select[name='CardExpMonth']").combobox();

        var mask = "<div class='loading-mask'> <span class='loading-text'>Loading...</span><div class='loading-image'><div class='loading-color'> </div></div></div>";
        $('body').append(mask);
        if ($('#html_element').html() != null) {
            var addCallback = document.createElement('script');
            //Recaptcha Site Key
            addCallback.innerText = "var onloadCallback = function () {$('#html_element').html(''); grecaptcha.render('html_element', {'sitekey': '6LdRUPYUAAAAAGyxaor-QeaIdotLD8DdLmDKUMhs'});};";
            var imported = document.createElement('script');
            imported.src = "https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit";
            imported.async = "async defer";
            if (!document.head.contains(addCallback))
                document.head.appendChild(addCallback);
            if (!document.head.contains(imported))
                document.head.appendChild(imported);
            if ($("input[name='eCaptcha']") == null || $("input[name='eCaptcha']") == undefined || $("input[name='eCaptcha']").length == 0) {
                var eCaptchahtml = "<input type='hidden' name='eCaptcha' value='' />";
                $('.payment-details').append(eCaptchahtml);
            }
        }
        var keys = [9, 13, 16, 17, 18, 37, 38, 39, 40];
        if ($("input[name='ExpirationDateLogic']") != null && $("input[name='ExpirationDateLogic']") != undefined && $("input[name='ExpirationDateLogic']").length > 0 && $("input[name='ExpirationDateLogic']").val().toLowerCase() == 'mouseonly') {
            $("[name*='ComboCardExpMonth']").keydown(function (e) {
                if (keys.indexOf(e.keyCode) < 0) {
                    alert('Please use the dropdown controls to select the expiration date.');
                    $("[name*='CardExpMonth']").prop("selectedIndex", 0); return false;
                }
            });
            $("[name*='ComboCardExpYear']").keydown(function (e) {
                if (keys.indexOf(e.keyCode) < 0) {
                    alert('Please use the dropdown controls to select the expiration date.');
                    $("[name*='CardExpYear']").prop("selectedIndex", 0); return false;
                }
            });
        }
    });
    function inIframe() {
        try {
            return window.self !== window.top;
        } catch (e) {
            return true;
        }
    }
    function makeElement(id, type, value) {
        var newElement = document.createElement('input');
        newElement.type = type;
        newElement.value = value;
        newElement.id = id;
        newElement.name = id;
        return newElement;
    }
    //Invisible
    function validate(event) {
        //event.preventDefault();
        if (!BP.paymentForm.validateForm()) {
            return false;
        }
        else {
            grecaptcha.execute();
        }
    }
    function onload() {
        var onsubmit = document.createElement("script");
        onsubmit.innerText = 'function onSubmit(token) { $("[name=\'eCaptcha\']").val(grecaptcha.getResponse()); $("form").submit();}'
        document.head.appendChild(onsubmit);
        var imported = document.createElement('script');
        imported.src = "https://www.google.com/recaptcha/api.js";
        imported.async = "async";
        imported.defer = "defer";
        if (!document.head.contains(imported))
            document.head.appendChild(imported);
        //Invisible Recaptcha Site Key
        var recaptchaHtml = '<div id="recaptcha" class="g-recaptcha" data-sitekey="6LfpT_YUAAAAAJHBi5jrR7UWl7vDOjxgErZXhLYF" data-callback="onSubmit" data-size="invisible"></div>';
        $('.payment-details').append(recaptchaHtml);
        if ($("input[name='eCaptcha']") == null || $("input[name='eCaptcha']") == undefined || $("input[name='eCaptcha']").length == 0) {
            var eCaptchaHtml = "<input type='hidden' name='eCaptcha' value='' />";
            $('.payment-details').append(eCaptchaHtml);
        }
        if ($("input[name='invisible']") == null || $("input[name='invisible']") == undefined || $("input[name='invisible']").length == 0) {
            var invisibleHtml = "<input type='hidden' name='invisible' value='true' />";
            $('.payment-details').append(invisibleHtml);
        }
        var element = $("button[type='submit']");
        element.off("click");
        element.attr("type", "button");
        element.on('click', function (e) { validate(); });
    }
    function pageLoad() {
        (function ($) {
            $.widget("custom.combobox", {
                _create: function () {
                    var self = this,
                        select = this.element.hide(),
                        selected = select.children(":selected"),
                        value = selected.val() ? selected.text() : "";
                    var input = this.input = $("<input>")
                        .insertAfter(select)
                        .val(value)
                        .attr("name", "Combo" + select.attr('name'))
                        .autocomplete({
                            delay: 0,
                            minLength: 0,
                            source: function (request, response) {
                                var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                                response(select.children("option").map(function () {
                                    var text = $(this).text();
                                    if (this.value && (!request.term || matcher.test(text)))
                                        return {
                                            label: text,
                                            value: text,
                                            option: this
                                        };
                                }));
                            },
                            select: function (event, ui) {
                                ui.item.option.selected = true;
                                self._trigger("selected", event, {
                                    item: ui.item.option
                                });
                                select.find('option[text = "' + ui.item.option.value + '"]').attr('selected', 'selected');
                            },
                            change: function (event, ui) {
                                if (!ui.item) {
                                    var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex($(this).val()) + "$", "i"),
                                        valid = false;
                                    select.children("option").each(function () {
                                        if ($(this).text().match(matcher)) {
                                            this.selected = valid = true;
                                            return false;
                                        }
                                    });
                                    if (!valid) {
                                        // remove invalid value, as it didn't match anything
                                        addError("btnCardExpYear", "Invalid expiration date");
                                        $(this).val("");
                                        select.val("");
                                        return false;
                                    }
                                    else {
                                        $("[name='btnCardExpYear']").parent().find("span.formError").remove();
                                    }
                                }
                            }
                        })
                        .addClass("validation-error custom-combobox-input");
                    this.button = $("<button type='button'>&nbsp;</button>")
                        .attr("tabIndex", -1)
                        .attr("title", "Show All Items")
                        .attr("name", "btn" + select.attr('name'))
                        .insertAfter(input)
                        .button({
                            icons: {
                                primary: "ui-icon-triangle-1-s"
                            },
                            text: false
                        })
                        .removeClass("ui-corner-all ui-button ui-widget ui-state-default ui-button-icon-only")
                        .addClass("custom-combobox-toggle")
                        .click(function () {
                            if (input.autocomplete("widget").is(":visible")) {
                                input.autocomplete("close");
                                return;
                            }

                            input.autocomplete("search", "");
                            input.focus();
                        });
                },

                destroy: function () {
                    this.input.remove();
                    this.button.remove();
                    this.element.show();
                    $.Widget.prototype.destroy.call(this);
                }
            });
        })(jQuery);
    }
    $(function () {
        $('head').append('<link rel="stylesheet" href="style/custom-theme/jquery-ui-1.10.4.custom.css" type="text/css" />');
        var captcha = $(".captcha");
        var dispalyCaptcha = $('#html_element');
        if (captcha.length > 0 && dispalyCaptcha.length <= 0) {
            onload();
        }
        var url = document.referrer;
        if (url != null && url.match(/:\/\/(.[^/]+)/) != null && url.match(/:\/\/(.[^/]+)/).length > 1) {
            var ref = url.match(/:\/\/(.[^/]+)/)[1];
            var form = $('.payment-details');
            if (form != null && form != 'undefined') {
                form.append(makeElement('RequestDomain', 'hidden', ref));
                if (inIframe()) {
                    form.append(makeElement('IsIframe', 'hidden', 'true'));
                }
                else
                    form.append(makeElement('IsIframe', 'hidden', 'false'));
            }
        }

    });

    BP.util.regexValidator = function (regexValue, valueToValidate) {

        return regexValue.test(valueToValidate);
    }

    BP.util.zipCodeValidator = function (zipCodeValue) {

        var zipCodeRegexValue = /^(?:[0-9]{5}(?:[- ]?[0-9]{4})?)?$/;

        return BP.util.regexValidator(zipCodeRegexValue, zipCodeValue);
    }

    BP.util.phoneNumberValidator = function (phoneNumberValue) {

        var phoneNumberRegexValue = /^(?:[1-9][0-9]{9})?$/;

        return BP.util.regexValidator(phoneNumberRegexValue, phoneNumberValue);
    }



})();