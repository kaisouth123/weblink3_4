﻿(function (context) {

    var transData = context.pgWebTransactionData;

    $(function () {
        if (BP.cookiesEnabled) {
            $.ajax({
                type: "POST",
                url: transData.pgWebUrl,
                data: transData
            }).done(function (json) {

                for (var prop in json.transactionResponseDTO) {
                    json.transactionResponseDTO['pgweb_' + prop] = json.transactionResponseDTO[prop];
                    delete json.transactionResponseDTO[prop];
                }

                json.transactionResponseDTO.isPGWebResponse = true;
                json.transactionResponseDTO.tg_IsPostBack = true;

                var data = json.transactionResponseDTO;

                // Serialize any object properties...
                for (var prop in data) {
                    if (data.hasOwnProperty(prop) &&
                        typeof data[prop] === 'object') {
                        data[prop] = JSON.stringify(data[prop]);
                    }
                }

                var form = createFormFromObject(data);
                form.submit();
            }).fail(function (resp) {
                alert('An error occurred while contacting Pay Guardian.');
            });
        }
    })
    

    // Creates a form tag with hidden inputs based on the keys/values of the passed in object
    function createFormFromObject(obj) {

        var form = document.createElement('form');
        form.method = 'post';

        for (var key in obj) {
            if (obj.hasOwnProperty(key)) {
                var input = document.createElement('input');
                input.name = key;
                input.type = 'hidden';
                input.value = obj[key];
                form.appendChild(input);
            }
        }

        document.body.appendChild(form);

        return form;
    }

})(window);

