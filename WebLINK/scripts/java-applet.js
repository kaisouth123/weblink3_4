﻿(function () {

    // Stringified version of the effective MSR settings
    BP.msrConfig = undefined;

    // Returns a configuration object in the correct format for the MSR of the current values in the
    // configuration dialog.
    BP.getMsrSettingsFromForm = function () {
        return {
            device: $("#msrDevice").val(),
            comPortName: $("#msrComPort").val(),
            comBaudRate: $("#msrBaudRate").val(),
            comDataBits: $("#msrDataBits").val(),
            comStopBits: $("#msrStopBits").val(),
            comParity: $("#msrParityBits").val(),
            usbVendorID: "",
            usbProductID: ""
        };
    };

    // Displays the MSR config dialog
    BP.showMsrConfig = function () {
        var objConfig;
        
        try {
            objConfig = JSON.parse(BP.msrConfig);
        } catch (e) { }

        objConfig = objConfig || {};

        $("#msrDevice").val(objConfig.device);
        $("#msrComPort").val(objConfig.comPortName);
        $("#msrBaudRate").val(objConfig.comBaudRate);
        $("#msrDataBits").val(objConfig.comDataBits);
        $("#msrStopBits").val(objConfig.comStopBits);
        $("#msrParityBits").val(objConfig.comParity);

        $("#msrConfigMessage").hide();
        $("div.ui-dialog button:contains('Save')").button('disable');
        BP.msrConfigDialog.dialog("open");
    };

    // Saves the MSR config to a cookie and locally in BP.msrConfig
    BP.saveMsrConfig = function (e) {
        var settings = BP.getMsrSettingsFromForm();

        var saveButton = $(e.currentTarget);

        BP.msrConfig = settings;
        amplify.store("msrConfig", settings);
        BP.msrConfigDialog.dialog('close');
    };

    // Tests the msr settings to ensure they are valid and enables the save button if so
    BP.testMSR = function () {

        var $saveButton = $("div.ui-dialog button:contains('Save')");
        var $message = $("#msrConfigMessage");

        if (BP.isConfigValid(BP.getMsrSettingsFromForm())) {
            $saveButton.button('enable');
            $message.text('Card swiper test successful!');
        } else {
            $saveButton.button('disable');
            $message.text('Card swiper test failed. Check settings.');
        }

        $message.fadeIn();

    };

    // Checks to see if a provided config is valid by testing it against the applet
    BP.isConfigValid = function (config) {
        var result = false;

        config = config || BP.msrConfig;

        if (BP.loadConfig(config)) {
            if (BP.getMSR()) {
                result = true;
            }
            BP.abandonInput();
        }

        return result;
    };

    // The first step in getting the applet up and running. Downloads, sets up the applet tag, etc
    BP.initializeApplet = function () {
        var attributes = {
            id: 'HWApplet',
            code: 'BPHWApplet.Applet.HWApp',
            archive: 'HWApplet.jar',
            MAYSCRIPT: 'true'
        };

        var parameters = { jnlp_href: BP.root + '/applets/HWApplet.jnlp', cache_option: 'no' };
        deployJava.runApplet(attributes, parameters, '1.7');
    };

    // Registers the applet callback functions
    BP.registerAppletEvents = function () {
        try {
            BP.msr.RegisterForMSR('MSRCompleteEvent');
            BP.msr.RegisterForPIN('MSRCompleteEvent');
            return true;
        } catch (e) {
            console.log(e);
            return false;
        }
    };

    // Loads the provided config into the applet
    BP.loadConfig = function (config) {
        if ($.isPlainObject(config)) {
            config = JSON.stringify(config);
        }
        if (config) {
            var result = BP.msr.LoadSetup(config);
            return BP.isSuccessResult(result);
        } else {
            return false;
        }
    };

    // Readys the MSR for swiping
    BP.getMSR = function () {
        BP.abandonInput();
        var result = BP.msr.GetMSR();
        return BP.isSuccessResult(result);
    };

    // Closes the MSR. Must call getMSR again to swipe.
    BP.abandonInput = function () {
        try {
            var result = BP.msr.AbandonInput();
            return BP.isSuccessResult(result);
        } catch (e) {
            return false;
        }
    };

    // Loads the current config into the MSR and readys it for swiping
    BP.initializeMsr = function () {
        var result = false;

        result = BP.msrConfig &&
            BP.loadConfig(BP.msrConfig) &&
            BP.getMSR();

        return result;
    };

    BP.isSuccessResult = function (msrResponse) {
        try {
            var obj = JSON.parse(msrResponse);
            return obj && obj.resultCode === 0;
        } catch (e) {
            return false;
        }
    };

    BP.handleSwipe = function (data) {
        if ($.isPlainObject(data)) {
            BP.setPaymentMethodViaCardnumber(data.maskedCard);
            BP.paymentForm.updatePaymentMethod();

            BP.requiredFields = BP.requiredFields.filter(IsRequiredForSwipe);

            var cardNumber = $("input[name='CardNumber']");
            var billToName = $("input[name='BillToName']");
            var cardTypeName = $("select[name='CardTypeName']");
            var cardExpMonth = $("select[name='CardExpMonth']");
            var cardExpYear = $("select[name='CardExpYear']");
            var cvNum = $("input[name='CVNum']");

            cardNumber.val(data.maskedCard);
            cardNumber.prop("disabled", true);

            billToName.val(data.custName);
            billToName.prop("disabled", true);
            cardTypeName.prop("disabled", true);
            cardExpYear.prop("disabled", true);
            cardExpMonth.prop("disabled", true);
            cvNum.prop("disabled", true);

            BP.paymentForm.createOrUpdateHiddenField("EncryptedTrack1", data.encTrack1);
            BP.paymentForm.createOrUpdateHiddenField("EncryptedTrack2", data.encTrack2);
            BP.paymentForm.createOrUpdateHiddenField("SecurityInfo", data.securityInfo);
            BP.paymentForm.createOrUpdateHiddenField("SecureFormat", data.secureFormat);
        }
    };

    BP.setPaymentMethodViaCardnumber = function (cardNumber) {

        var cardType;

        // really damn simplistic and not full-proof by any stretch of the imagination!
        switch (cardNumber.charAt(0)) {
            case '3':
                cardType = "AMEX";
                break;
            case '5':
                cardType = "Mastercard";
                break;
            case '6':
                cardType = "Discover";
                break;
            case '4':
            default:
                cardType = "Visa";
                break;
        }

        $("select[name='CardTypeName']").val(cardType);
    };

    BP.handleSwipeButtonClick = function () {
        if (BP.isConfigValid()) {
            BP.initializeMsr();
        } else {
            BP.showMsrConfig();
        }
    };

    function IsRequiredForSwipe(element) {
        return !element.equalsAny(['CardNumber', 'CardTypeName', 'CardExpMonth', 'CardExpYear', 'CVNum']);
    }

    BP.initializeApplet();

})();

$(function () {

    // Handler for anytime a configuration value changes in the config dialog
    $("#msrConfig input, #msrConfig select").change(function () {
        $("div.ui-dialog button:contains('Save')").button('disable');
        $("#msrConfigMessage").fadeOut();
    });

    $("#btnConfigureSwipe").click(function (e) {
        e.preventDefault();
        BP.showMsrConfig();
    });

    $("#btnSwipe").click(function (e) {
        e.preventDefault();
        BP.handleSwipeButtonClick();
    });

    BP.msrConfigDialog = $("#msrConfig").dialog({
        autoOpen: false,
        height: 240,
        width: 350,
        modal: true,
        buttons: [
            {
                text: 'Save',
                click: function (e) {
                    BP.saveMsrConfig(e);
                    BP.initializeMsr();
                },
                disabled: true
            },
            {
                text: 'Test Device',
                click: BP.testMSR
            },
            {
                text: 'Cancel',
                click: function () {
                    BP.msrConfigDialog.dialog("close");
                }
            }
        ]
    });

    BP.msr = document.getElementById('HWApplet');

    window.MSRCompleteEvent = function (data) {
        var objData;

        try {
            objData = JSON.parse(data);
        } catch (e) {

        }

        if (objData && objData.resultCode === 0) {
            BP.handleSwipe(objData);
        }

        BP.getMSR();
    };

    BP.registerAppletEvents();

    if (!BP.msrConfig) {
        BP.msrConfig = JSON.stringify(amplify.store("msrConfig"));
    }
        
});
