﻿String.prototype.equalsAny = function (values) {
    for (var i = 0; i < values.length; i++) {
        if (values[i] == this) { return true; }
    }
    return false;
};

if (!Array.prototype.filter) {
    Array.prototype.filter = function (fun /*, thisArg */) {
        "use strict";

        if (this === void 0 || this === null)
            throw new TypeError();

        var t = Object(this);
        var len = t.length >>> 0;
        if (typeof fun !== "function")
            throw new TypeError();

        var res = [];
        var thisArg = arguments.length >= 2 ? arguments[1] : void 0;
        for (var i = 0; i < len; i++) {
            if (i in t) {
                var val = t[i];

                // NOTE: Technically this should Object.defineProperty at
                //       the next index, as push can be affected by
                //       properties on Object.prototype and Array.prototype.
                //       But that method's new, and collisions should be
                //       rare, so use the more-compatible alternative.
                if (fun.call(thisArg, val, i, t))
                    res.push(val);
            }
        }

        return res;
    };
}

(function (BP) {

    BP.checkForCookies = function () {
        if (document.cookie.indexOf('weblink3_cookie_detection') === -1) {
            $('body').html('<div class="no-cookies"><img src="style/images/32/exclamation.png" border="0" /><span>Cookies are required for this page to operate properly. Please enable cookies and try again.</span></div>');
            return false;
        } else { return true; }
    };

})(window.BP = window.BP || {});