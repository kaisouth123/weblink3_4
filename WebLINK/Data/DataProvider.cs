﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace WebLINK.Data
{
    /// <summary>
    /// Wrapper class for the Weblink database
    /// </summary>
    public class DataProvider : IDisposable
    {
        private WeblinkDBDataContext db = null;

        public readonly WeblinkDBDataContext UnderlyingProvider;

        public DataProvider()
        {
            db = new WeblinkDBDataContext();
            UnderlyingProvider = db;
        }

        public DataProvider(string connString)
        {
            db = new WeblinkDBDataContext(connString);
            UnderlyingProvider = db;
        }

        public int? GetWeblinkId(int MID, string ServerGroup = null)
        {
            return (from WLID in db.WeblinkIDs_Ts
                       where WLID.MID == MID
                       && (string.IsNullOrEmpty(ServerGroup) || WLID.ServerGroup == ServerGroup)
                       select WLID.WLID).FirstOrDefault();

        }

        public bool IsAuthorized(int MID, string ServerGroup = null)
        {
            int? WLID = GetWeblinkId(MID, ServerGroup);
            if (WLID.HasValue)
                return IsAuthorized(WLID.Value);
            else
                return false;
        }

        public bool IsAuthorized(int WLID)
        {
            return db.Authorization_Ts.Any(a => a.WLID == WLID);
        }

        /// <summary>
        /// Returns a single setting value. Set WLID to 0 to retrieve the default setting value explicitly.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="WLID"></param>
        /// <returns></returns>
        public string GetSetting(string type, int WLID)
        {
            StringBuilder SQL = new StringBuilder();
            SQL.Append("SELECT TOP 1 * ");
            SQL.Append("FROM   Settings_T ");
            SQL.Append("WHERE  1         =1 ");
            if (WLID != 0)
                SQL.Append("AND    (WLID = {0} OR WLID IS NULL) ");
            else
                SQL.Append("AND    WLID IS NULL ");
            SQL.Append("AND    name      = {1} ");
            SQL.Append("ORDER BY WLID DESC");

            var setting = db.ExecuteQuery<Settings_T>(SQL.ToString(), WLID, type).FirstOrDefault();

            return setting == null ? string.Empty : setting.Value;
        }

        /// <summary>
        /// Set Name to null to return all settings.
        /// </summary>
        /// <param name="WLID"></param>
        /// <param name="Name"></param>
        /// <returns></returns>
        public IEnumerable<Settings_T> GetEffectiveSettings(int WLID, string Name = null)
        {
            StringBuilder SQL = new StringBuilder();
            SQL.Append("SELECT   s1.name AS Name, s1.Id, ");
            SQL.Append("         COALESCE( ");
            SQL.Append("                   (SELECT value ");
            SQL.Append("                   FROM    settings_t s2 ");
            SQL.Append("                   WHERE   s2.name = s1.name ");
            SQL.Append("                   AND     s2.WLID  = {0} ");
            SQL.Append("                   ) ");
            SQL.Append("                  , s1.value) AS Value, ");
            SQL.Append("         CAST(CASE ISNULL( ");
            SQL.Append("                           (SELECT value ");
            SQL.Append("                           FROM    settings_t s2 ");
            SQL.Append("                           WHERE   s2.name = s1.name ");
            SQL.Append("                           AND     s2.WLID  = {0} ");
            SQL.Append("                           ) ");
            SQL.Append("                           , 1) ");
            SQL.Append("                  WHEN '1' ");
            SQL.Append("                  THEN 1 ");
            SQL.Append("                  ELSE 0 ");
            SQL.Append("         END AS bit) AS IsGlobal ");
            SQL.Append("FROM     settings_t s1 ");
            SQL.Append("WHERE    s1.WLID IS NULL ");
            if (!string.IsNullOrEmpty(Name))
                SQL.Append("AND s1.Name = {1} ");
            SQL.Append("ORDER BY Name");

            return db.ExecuteQuery<Settings_T>(SQL.ToString(), WLID, Name ?? "");
        }

        public string GetTemplate(string type, int? WLID = null, int? TID = null)
        {
            string templateHtml = null;

                var template = db.Templates_Ts.FirstOrDefault(t => t.Type == type &&
                    (WLID == null && t.WLID == null || t.WLID == WLID) &&
                    (TID == null && t.TID == null || t.TID == TID));

                // Fall back to default template
                if (template == null && TID == null)
                    template = db.Templates_Ts.FirstOrDefault(t => t.Type == type &&
                        t.WLID == null &&
                        t.TID == null);
                templateHtml = template == null ? null : template.Template;

            return templateHtml;
        }

        public IEnumerable<Templates_T> GetMerchantTemplates(int WLID)
        {
            StringBuilder SQL = new StringBuilder();
            SQL.Append("SELECT   COALESCE( ");
            SQL.Append("                   (SELECT id ");
            SQL.Append("                   FROM    templates_T t2 ");
            SQL.Append("                   WHERE   t2.type      = t1.type ");
            SQL.Append("                   AND     t2.WLID       = {0} ");
            SQL.Append("                   AND     t2.TID IS NULL ");
            SQL.Append("                   ) ");
            SQL.Append("                  , t1.id) AS id, ");
            SQL.Append("         t1.type                , ");
            SQL.Append("         COALESCE( ");
            SQL.Append("                   (SELECT template ");
            SQL.Append("                   FROM    templates_T t2 ");
            SQL.Append("                   WHERE   t2.type      = t1.type ");
            SQL.Append("                   AND     t2.WLID       = {0} ");
            SQL.Append("                   AND     t2.TID IS NULL ");
            SQL.Append("                   ) ");
            SQL.Append("                  , t1.template) AS Template, ");
            SQL.Append("         CAST(CASE ISNULL( ");
            SQL.Append("                           (SELECT template ");
            SQL.Append("                           FROM    templates_t t2 ");
            SQL.Append("                           WHERE   t2.type      = t1.type ");
            SQL.Append("                           AND     t2.WLID       = {0} ");
            SQL.Append("                           AND     t2.TID IS NULL ");
            SQL.Append("                           ) ");
            SQL.Append("                           , 1) ");
            SQL.Append("                  WHEN '1' ");
            SQL.Append("                  THEN 1 ");
            SQL.Append("                  ELSE 0 ");
            SQL.Append("         END AS bit) AS IsDefault ");
            SQL.Append("FROM     templates_t t1 ");
            SQL.Append("WHERE    t1.WLID IS NULL ");
            SQL.Append("ORDER BY t1.type");

            return db.ExecuteQuery<Templates_T>(SQL.ToString(), WLID);
        }

        public IEnumerable<Templates_T> GetCustomTemplates(int WLID)
        {
            return db.Templates_Ts.Where(t => t.WLID == WLID && t.TID != null);
        }

        public void InsertFailedTransaction(FailedTransactions_T transaction)
        {
            db.FailedTransactions_Ts.InsertOnSubmit(transaction);
            db.SubmitChanges();
        }

        public void DeleteFailedTransactions(Guid sessionId)
        {
            string sql = "DELETE FROM FailedTransactions_T WHERE SessionId = {0}";
            db.ExecuteCommand(sql, sessionId);
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}