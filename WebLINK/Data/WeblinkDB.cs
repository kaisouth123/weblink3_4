namespace WebLINK.Data
{
    partial class Templates_T
    {
        public bool IsDefault { get; set; }
    }

    partial class Settings_T
    {
        public bool IsGlobal { get; set; }
    }
}
