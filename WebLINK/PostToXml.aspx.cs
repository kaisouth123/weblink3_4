﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

namespace WebLINK
{
    /// <summary>
    /// Outputs a simple xml document of all POST'ed values.
    /// </summary>
    public partial class PostToXml : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            XDocument doc = new XDocument();
            XElement data = new XElement("Data");

            foreach (string key in Request.Form)
                data.Add(new XElement(key, Request.Form[key]));

            doc.Add(data);

            Response.Clear();
            Response.ContentType = "text/xml";
            Response.Write(doc.ToString());
        }
    }
}