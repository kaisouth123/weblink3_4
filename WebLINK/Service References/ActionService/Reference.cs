﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebLINK.ActionService {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://bridgepaynetsecuretx.com/actionservice", ConfigurationName="ActionService.IActionService")]
    public interface IActionService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://bridgepaynetsecuretx.com/actionservice/IActionService/AcquirePurchaseToken" +
            "", ReplyAction="http://bridgepaynetsecuretx.com/actionservice/IActionService/AcquirePurchaseToken" +
            "Response")]
        string AcquirePurchaseToken(string userName, string passWord, string certificationId, int transactionAmount, string purchaserInfo, string transactionInfo);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://bridgepaynetsecuretx.com/actionservice/IActionService/AcquirePurchaseToken" +
            "", ReplyAction="http://bridgepaynetsecuretx.com/actionservice/IActionService/AcquirePurchaseToken" +
            "Response")]
        System.Threading.Tasks.Task<string> AcquirePurchaseTokenAsync(string userName, string passWord, string certificationId, int transactionAmount, string purchaserInfo, string transactionInfo);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IActionServiceChannel : WebLINK.ActionService.IActionService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class ActionServiceClient : System.ServiceModel.ClientBase<WebLINK.ActionService.IActionService>, WebLINK.ActionService.IActionService {
        
        public ActionServiceClient() {
        }
        
        public ActionServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public ActionServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ActionServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ActionServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public string AcquirePurchaseToken(string userName, string passWord, string certificationId, int transactionAmount, string purchaserInfo, string transactionInfo) {
            return base.Channel.AcquirePurchaseToken(userName, passWord, certificationId, transactionAmount, purchaserInfo, transactionInfo);
        }
        
        public System.Threading.Tasks.Task<string> AcquirePurchaseTokenAsync(string userName, string passWord, string certificationId, int transactionAmount, string purchaserInfo, string transactionInfo) {
            return base.Channel.AcquirePurchaseTokenAsync(userName, passWord, certificationId, transactionAmount, purchaserInfo, transactionInfo);
        }
    }
}
