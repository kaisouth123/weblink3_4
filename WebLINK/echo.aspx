﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="echo.aspx.cs" Inherits="WebLINK.echo" EnableViewState="false" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
            table {border-collapse: collapse;}
            table th {background-color: Silver;}
            td, th {padding: 1px 5px; border: 1px solid black;}
    </style>
    <meta content="UTF-8" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h1>Posted values:</h1>
        <asp:Repeater ID="rptPost" runat="server">
            <HeaderTemplate>
                <table cellspacing="0" id="echo">
                    <tr>
                        <th>Name</th>
                        <th>Value</th>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><%#Eval("Name") %></td>
                    <td><%#Server.HtmlEncode((string)Eval("Value")) %></td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
    </form>
</body>
</html>
