﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Configuration;
using System.Data;
using WebLINK.Data;
using WebLINK.Common;
using System.Collections.Specialized;

namespace WebLINK.admin
{
    /// <summary>
    /// Summary description for weblink
    /// </summary>
    [WebService(Namespace = "https://boarding.itstgate.com/weblink/admin/weblink.asmx")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class weblink : System.Web.Services.WebService
    {

        DataProvider db = null;

        public weblink()
        {
            db = new DataProvider();
        }

        private void closeDB()
        {
            if (db != null)
                db.Dispose();
        }

        [WebMethod]
        public int CreateWeblinkIdForBPN(int MID, string BpnDomain)
        {
            WeblinkIDs_T weblinkId = new WeblinkIDs_T
            {
                BPNDomain = BpnDomain,
                MID = MID
            };

            db.UnderlyingProvider.WeblinkIDs_Ts.InsertOnSubmit(weblinkId);
            db.UnderlyingProvider.SubmitChanges();
            closeDB();

            return weblinkId.WLID;
        }

        [WebMethod]
        public int CreateWeblinkIdForTGate(int MID, string ServerGroup)
        {
            WeblinkIDs_T weblinkId = new WeblinkIDs_T
            {
                ServerGroup = ServerGroup,
                MID = MID
            };

            db.UnderlyingProvider.WeblinkIDs_Ts.InsertOnSubmit(weblinkId);
            db.UnderlyingProvider.SubmitChanges();
            closeDB();

            return weblinkId.WLID;
        }

        [WebMethod]
        public void DeleteWeblinkId(int WeblinkId)
        {
            var wlid = db.UnderlyingProvider.WeblinkIDs_Ts.FirstOrDefault(w => w.WLID == WeblinkId);

            if (wlid != null)
            {
                db.UnderlyingProvider.WeblinkIDs_Ts.DeleteOnSubmit(wlid);
                db.UnderlyingProvider.SubmitChanges();
            }

            closeDB();
        }

        /// <summary>
        /// Returns a Weblink ID for a given Bridgepay domain and MID
        /// </summary>
        /// <param name="MID"></param>
        /// <param name="BPNDomain"></param>
        /// <returns>Nullable int</returns>
        [WebMethod]
        public int? GetWeblinkIdForBPN(int MID, string BPNDomain)
        {
            return db.GetWeblinkId(MID, BPNDomain);
        }

        /// <summary>
        /// Returns a Weblink ID for a given TGate server group and MID
        /// </summary>
        /// <param name="serverGroup"></param>
        /// <param name="MID"></param>
        /// <returns>Nullable int</returns>
        [WebMethod]
        public int? GetWeblinkIdForTGate(string serverGroup, int MID)
        {
            int? rv = db.GetWeblinkId(MID, serverGroup);
            closeDB();
            return db.GetWeblinkId(MID, serverGroup);
        }

        [WebMethod]
        public string GetTemplate(string type, int? weblinkId = null, int? templateId = null)
        {
            string rv = db.GetTemplate(type, weblinkId, templateId);
            closeDB();
            return (rv);
        }

        [WebMethod]
        public string GetTemplateById(int TemplateId)
        {
            var template = db.UnderlyingProvider.Templates_Ts.FirstOrDefault(t => t.Id == TemplateId);
            string rv = template == null ? string.Empty : template.Template;
            closeDB();
            return rv;
        }

        [WebMethod]
        public List<Template> GetMerchantTemplates(int WeblinkId)
        {
            List<Template> rv = db.GetMerchantTemplates(WeblinkId).Select(t => new Template
                {
                    TemplateHtml = t.Template,
                    TemplateId = t.Id,
                    TID = t.TID,
                    Type = t.Type,
                    WeblinkId = t.WLID
                }).ToList();
            closeDB();
            return rv;
        }

        [WebMethod]
        public List<Template> GetCustomTemplates(int WeblinkId)
        {
            List<Template> rv = db.GetCustomTemplates(WeblinkId).Select(t => new Template
            {
                TemplateHtml = t.Template,
                TemplateId = t.Id,
                TID = t.TID,
                Type = t.Type,
                WeblinkId = t.WLID
            }).ToList();
            closeDB();
            return rv;
        }

        [WebMethod]
        public int SaveTemplate(int? TemplateId, int? WeblinkId, int? TID, string Type, string Template)
        {
            int newTemplateId;
            Templates_T template = new Templates_T();

            if (TemplateId.HasValue)
                template = db.UnderlyingProvider.Templates_Ts.FirstOrDefault(t => t.Id == TemplateId);
            else
                db.UnderlyingProvider.Templates_Ts.InsertOnSubmit(template);

            if (template == null)
                throw new Exception("Could not locate a template with the provided TemplateId");

            template.WLID = WeblinkId;
            template.TID = TID;
            template.Type = Type;
            template.Template = Template;
            
            db.UnderlyingProvider.SubmitChanges();
            newTemplateId = template.Id;

            closeDB();

            return (newTemplateId);
        }

        [WebMethod]
        public void SaveTemplateHtml(int TemplateId, string TemplateHtml)
        {
            var template = db.UnderlyingProvider.Templates_Ts.FirstOrDefault(t => t.Id == TemplateId);

            if (template == null)
                throw new Exception("Could not locate a template with the provided TemplateId");

            template.Template = TemplateHtml;
            db.UnderlyingProvider.SubmitChanges();

            closeDB();
        }

        [WebMethod]
        public void DeleteTemplate(int TemplateId)
        {
            var template = db.UnderlyingProvider.Templates_Ts.FirstOrDefault(t => t.Id == TemplateId);

            if (template == null)
                throw new Exception("Could not locate a template with the provided TemplateId");

            db.UnderlyingProvider.Templates_Ts.DeleteOnSubmit(template);
            db.UnderlyingProvider.SubmitChanges();

            closeDB();
        }

        [WebMethod]
        public List<TemplateType> GetTemplateTypes()
        {
            List<TemplateType> rv = new List<TemplateType>();
            rv = db.UnderlyingProvider.TemplateTypes_Ts.Select(tt => new TemplateType { Type = tt.TemplateType, Description = tt.Description }).ToList();
            closeDB();
            return rv;
        }

        [WebMethod]
        public void AddTemplateType(string TemplateType, string Description)
        {
            TemplateTypes_T templateType = new TemplateTypes_T
            {
                TemplateType = TemplateType,
                Description = Description
            };

            db.UnderlyingProvider.TemplateTypes_Ts.InsertOnSubmit(templateType);
            db.UnderlyingProvider.SubmitChanges();
            closeDB();
        }

        [WebMethod]
        public void DeleteTemplateType(string TemplateType)
        {
            var tt = db.UnderlyingProvider.TemplateTypes_Ts.FirstOrDefault(t => t.TemplateType == TemplateType);
            if (tt != null)
            {
                db.UnderlyingProvider.TemplateTypes_Ts.DeleteOnSubmit(tt);
                db.UnderlyingProvider.SubmitChanges();
            }
            closeDB();
        }

        [WebMethod]
        public void AddSettingName(string value)
        {
            SettingNames_T settingName = new SettingNames_T
            {
                Name = value
            };

            db.UnderlyingProvider.SettingNames_Ts.InsertOnSubmit(settingName);
            db.UnderlyingProvider.SubmitChanges();
            closeDB();
        }

        [WebMethod]
        public void DeleteSettingName(string value)
        {
            var setting = db.UnderlyingProvider.SettingNames_Ts.FirstOrDefault(s => s.Name == value);
            if (setting != null)
            {
                db.UnderlyingProvider.SettingNames_Ts.DeleteOnSubmit(setting);
                db.UnderlyingProvider.SubmitChanges();
            }
            closeDB();
        }

        [WebMethod]
        public int SaveSetting(int? SettingId, int? WeblinkId, string Name, string Value)
        {
            int newSettingId;

            if (!SettingId.HasValue)
            {
                db.UnderlyingProvider.Settings_Ts.DeleteAllOnSubmit(db.UnderlyingProvider.Settings_Ts.Where(s => 
                    (!WeblinkId.HasValue || s.WLID == WeblinkId)
                        && s.Name == Name));
                db.UnderlyingProvider.SubmitChanges();
            }

            Data.Settings_T setting = new Data.Settings_T();
            if (SettingId.HasValue)
                setting = db.UnderlyingProvider.Settings_Ts.FirstOrDefault(s => s.Id == SettingId);
            else
                db.UnderlyingProvider.Settings_Ts.InsertOnSubmit(setting);

            if (setting == null)
                throw new Exception("Could not locate a setting record with that id.");

            setting.Name = Name;
            setting.Value = Value;
            setting.WLID = WeblinkId;
            db.UnderlyingProvider.SubmitChanges();
            newSettingId = setting.Id;

            closeDB();
            return newSettingId;
        }

        [WebMethod]
        public string GetDefaultSetting(string Name)
        {
            var setting = db.GetSetting(Name, 0);
            closeDB();
            return (setting);
        }

        [WebMethod]
        public List<Setting> GetSettings(int WeblinkId)
        {
            List<Setting> rv = db.GetEffectiveSettings(WeblinkId).Select(s => new Setting
            {
                IsGlobal = s.IsGlobal,
                Name = s.Name,
                SettingId = s.Id,
                Value = s.Value
            }).ToList();
            closeDB();
            return rv;
        }

        [WebMethod]
        public List<string> GetSettingNames()
        {
            List<string> rv = db.UnderlyingProvider.SettingNames_Ts.Select(s => s.Name).ToList();
            closeDB();
            return (rv);
        }

        [WebMethod]
        public void DeleteSetting(int SettingId)
        {
            var setting = db.UnderlyingProvider.Settings_Ts.FirstOrDefault(s => s.Id == SettingId);

            if (setting != null)
            {
                db.UnderlyingProvider.Settings_Ts.DeleteOnSubmit(setting);
                db.UnderlyingProvider.SubmitChanges();
            }

            closeDB();
        }

        [WebMethod]
        public void DeleteSettings(int WeblinkId)
        {
            db.UnderlyingProvider.Settings_Ts.DeleteAllOnSubmit(db.UnderlyingProvider.Settings_Ts.Where(s => s.WLID == WeblinkId));
            db.UnderlyingProvider.SubmitChanges();
            closeDB();
        }

        [WebMethod]
        public void Authorize(int WeblinkId)
        {
            Authorization_T auth = db.UnderlyingProvider.Authorization_Ts.FirstOrDefault(a => a.WLID == WeblinkId);

            if (auth == null)
            {
                auth = new Authorization_T
                    {
                        WLID = WeblinkId
                    };
                db.UnderlyingProvider.Authorization_Ts.InsertOnSubmit(auth);
                db.UnderlyingProvider.SubmitChanges();
            }
            closeDB();
        }

        [WebMethod]
        public void DeAuthorize(int WeblinkId)
        {
            Authorization_T auth = db.UnderlyingProvider.Authorization_Ts.FirstOrDefault(a => a.WLID == WeblinkId);

            if (auth != null)
            {
                db.UnderlyingProvider.Authorization_Ts.DeleteOnSubmit(auth);
                db.UnderlyingProvider.SubmitChanges();
            }
            closeDB();
        }

        [WebMethod]
        public bool IsAuthorized(int WeblinkId)
        {
            return db.IsAuthorized(WeblinkId);
        }
    }

    public struct TemplateType
    {
        public string Type { get; set; }
        public string Description { get; set; }
    }

    public struct Template
    {
        public int TemplateId { get; set; }
        public string Type { get; set; }
        public string TemplateHtml { get; set; }
        public int? WeblinkId { get; set; }
        public int? TID { get; set; }
        public bool IsDefault { get; set; }
    }

    public struct Setting
    {
        public int SettingId { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public bool IsGlobal { get; set; }
    }
}
