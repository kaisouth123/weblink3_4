﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebLINK.Templating
{
    public class TemplatePaymentMethod
    {
        public enum PaymentType
        {
            Credit,
            ACH
        }

        public string Name { get; set; }
        public string Value { get; set; }
        public PaymentType Type { get; set; }
    }
}