﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace WebLINK.Templating
{
    public enum ScriptType
    {
        PaymentForm,
        jQuery,
        jQueryUI,
        DeployJava,
        JavaApplet,
        Core,
        JSON2,
        Countdown,
        Amplify,
        PGWeb
    }

    public enum StylesheetType
    {
        PaymentForm,
        Receipt,
        SexyButtons,
        jQueryUI
    }

    public abstract class BaseTemplate : IWeblinkTemplate
    {
        public List<ScriptType> HeadScripts { get; set; }
        public List<ScriptType> BodyScripts { get; set; }
        public List<StylesheetType> Stylesheets { get; set; }
        public string InlineHeadScript { get; set; }
        public string InlineBodyScript { get; set; }
        public List<string> ErrorMessages { get; set; }

        protected const string HeadScriptsTagRegExPattern = @"<!--\s*%%\s*HEAD_SCRIPTS\s*%%\s*-->";
        protected const string BodyScriptsTagRegExPattern = @"<!--\s*%%\s*BODY_SCRIPTS\s*%%\s*-->";
        protected const string StylesTagRegExPattern = @"<!--\s*%%\s*STYLES\s*%%\s*-->";

        public BaseTemplate()
        {
            BodyScripts = new List<ScriptType>();
            HeadScripts = new List<ScriptType>();
            Stylesheets = new List<StylesheetType>();
            ErrorMessages = new List<string>();
        }

        static Dictionary<ScriptType, string> ScriptTypeMap;
        static Dictionary<StylesheetType, string> StylesheetTypeMap;

        static BaseTemplate()
        {
            ScriptTypeMap = new Dictionary<ScriptType, string>
            {
                { ScriptType.DeployJava, "scripts/deployJava.js" },
                { ScriptType.JavaApplet, "scripts/java-applet.js" },
                { ScriptType.jQuery, "scripts/jquery-3.5.1.min.js" },
                { ScriptType.jQueryUI, "scripts/jquery-ui-1.12.1.min.js" },
                { ScriptType.PaymentForm, "scripts/payment_form.js" },
                { ScriptType.Core, "scripts/core.js" },
                { ScriptType.JSON2, "scripts/json2.js" },
                { ScriptType.Countdown, "scripts/countdown.js" },
                { ScriptType.Amplify, "scripts/amplify.min.js" },
                { ScriptType.PGWeb, "scripts/pgweb.js" }
            };

            StylesheetTypeMap = new Dictionary<StylesheetType, string>
            {
                { StylesheetType.PaymentForm, "style/payment_form.css" },
                { StylesheetType.Receipt, "style/receipt.css" },
                { StylesheetType.SexyButtons, "style/sexybuttons.css" },
                { StylesheetType.jQueryUI, "style/custom-theme/jquery-ui-1.10.4.custom.css" }
            };
        }

        protected string GenerateScriptTags(List<ScriptType> scripts)
        {
            StringBuilder sb = new StringBuilder();

            foreach (var scriptType in scripts)
            {
                var path = ScriptTypeMap[scriptType];
                sb.AppendFormat("<script src=\"{0}\" type=\"text/javascript\"></script>\r\n", path);
            }

            return sb.ToString();
        }

        protected string GenerateStylesheetLinks(List<StylesheetType> stylesheets)
        {
            StringBuilder sb = new StringBuilder();

            foreach (var stylesheetType in stylesheets)
            {
                var path = StylesheetTypeMap[stylesheetType];
                sb.AppendFormat("<link href=\"{0}\" type=\"text/css\" rel=\"stylesheet\" />\r\n", path);
            }

            sb.AppendFormat("<link rel=\"home\" id=\"appRoot\" href=\"{0}\">\r\n", HttpContext.Current.Request.ApplicationPath);

            return sb.ToString();
        }
    }
}