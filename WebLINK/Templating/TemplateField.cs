﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace WebLINK.Templating
{
    public class TemplateField
    {
        public string Name = string.Empty;
        public string Value = string.Empty;
        public bool Required = false;
        public bool Disabled = false;
        public string ReplacePattern = string.Empty;
        public bool NamePlaceHolder = false; //True if this placeholder was present inside a name attribute
        public bool InputPlaceHolder = false; //True if this placeholder was rendered as or associated to an input control
        public bool TemplatePlaceHolder = false; // True if this placeholder is for a template

        public TemplateField(string name)
            : this(name, string.Empty, false) { }

        public TemplateField(string name, bool required)
            : this(name, string.Empty, required) { }

        public TemplateField(string name, string value)
            : this(name, value, false) { }

        public TemplateField(string name, string value, bool required)
        {
            this.Name = name;
            this.Required = required;
            this.Value = value;
        }

        public void ParseOptions(string options)
        {
            options = (options + string.Empty).ToLower();
            options = options.TrimStart(new char[] { '{' });
            options = options.TrimEnd(new char[] { '}' });

            string[] tokens = options.Split('|');

            foreach (string option in tokens)
            {
                if (option == "required")
                    this.Required = true;

                //Left this as an example if we need to add support for more "options"
                Regex re = new Regex(@"label=([^\|}]+)");
            }
        }
    }
}