﻿using Bridgepay.Weblink.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebLINK.Common;

namespace WebLINK.Templating.Formatters.Concrete
{
    public class GenericAcctNumFormatter : BaseFormatter
    {
        public GenericAcctNumFormatter(WeblinkTemplate template) : base(template) { }

        public override string ToString(int? transactionSeqNo = null)
        {
            string rv = template.Data[Fields.CardNumber];
            if (string.IsNullOrEmpty(rv))
                rv = template.Data[Fields.AccountNumber];

            return rv;
        }
    }
}