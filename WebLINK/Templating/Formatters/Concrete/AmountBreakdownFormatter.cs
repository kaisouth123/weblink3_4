﻿using Bridgepay.Weblink.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebLINK.Common;

namespace WebLINK.Templating.Formatters.Concrete
{
    public class AmountBreakdownFormatter : BaseFormatter
    {

        public AmountBreakdownFormatter(WeblinkTemplate template) : base(template) { }

        public override string ToString(int? transactionSeqNo = default(int?))
        {
            string totalValue = template.Data[Fields.TotalAmt];
            string prnDescription = template.Data[Fields.Description];
            string prnValue = string.Empty;

            string feeValue = template.Data[Fields.FeeAmount];
            string feeDescription = template.Data[Fields.FeeDescription];

            string svcValue = template.Data[Fields.SvcAmount];
            string svcDescription = template.Data[Fields.SvcDescription];

            // Format value as currency for display purposes
            decimal feeValueDecimal = 0;
            if (decimal.TryParse(feeValue, out feeValueDecimal))
            {
                feeValue = feeValueDecimal.ToString("###,###,###.00");
            }

            decimal svcValueDecimal = 0;
            if (decimal.TryParse(svcValue, out svcValueDecimal))
            {
                svcValue = svcValueDecimal.ToString("###,###,###.00");
            }

            decimal prnValueDecimal = 0;
            decimal totalValueDecimal = 0;
            if (decimal.TryParse(totalValue, out totalValueDecimal))
            {
                prnValueDecimal = totalValueDecimal - (feeValueDecimal + svcValueDecimal);
                prnValue = prnValueDecimal.ToString("###,###,###.00");
            }

            string feeDivString = string.Empty;
            string svcDivString = string.Empty;
            string prnDivString = string.Empty;


            if (feeValueDecimal != 0)
                feeDivString = CreateField(feeDescription, feeValue, Fields.FeeAmount, template.IsForm);

            if (svcValueDecimal != 0)
                svcDivString = CreateField(svcDescription, svcValue, Fields.SvcAmount, template.IsForm);

            if (prnValueDecimal != 0)
                prnDivString = CreateField(prnDescription, prnValue, "prn-amount", template.IsForm);

            return string.Concat(prnDivString, feeDivString, svcDivString);
        }

        private string CreateField(string description, string value, string fieldId, bool displayAsField)
        {

            TagBuilder div = new TagBuilder("div");
            TagBuilder label = new TagBuilder("label");
            TagBuilder data;
            TagBuilder tableRow = new TagBuilder("tr");
            TagBuilder tableHeader = new TagBuilder("th");
            TagBuilder tableData = new TagBuilder("td");

            label.SetInnerText(string.Concat((string.IsNullOrEmpty(description) ? "Principal" : description), ":"));

            if (displayAsField)
            {
                data = new TagBuilder("input");
                data.Attributes.Add("type", "text");
                data.Attributes.Add("value", value);
                data.Attributes.Add("size", "12");
                data.Attributes.Add("disabled", "disabled");
                data.Attributes.Add("readonly", "readonly");
                data.AddCssClass("field");
                data.AddCssClass("amount");
                div.AddCssClass("field");
                div.InnerHtml = string.Concat(label.ToString(), data.ToString());
                return div.ToString();
            }
            else
            {
                data = new TagBuilder("span");
                data.Attributes.Add("class", "amount");
                data.SetInnerText(value);
                tableData.InnerHtml = data.ToString();
                tableHeader.SetInnerText(description);
                tableRow.InnerHtml = string.Concat(tableHeader.ToString(), tableData.ToString());
                return tableRow.ToString();
            }

        }
    }
}