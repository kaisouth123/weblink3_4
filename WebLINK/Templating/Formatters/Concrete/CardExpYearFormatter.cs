﻿using Bridgepay.Weblink.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using WebLINK.Common;

namespace WebLINK.Templating.Formatters.Concrete
{
    public class CardExpYearFormatter : BaseFormatter
    {
        public CardExpYearFormatter(WeblinkTemplate template) : base(template) { }

        public override string ToString(int? transactionSeqNo = null)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<select name=\"CardExpYear\" class=\"field\">");
            sb.Append("\t<option value=''>- - - -</option>\r\n");
            int startYear = DateTime.Now.Year-2000;
            for (int i = startYear; i < startYear + 21; i++)
            {
                string value = i.ToString();
                string selected = null;
                if (template.Data[Fields.CardExpYear] == value)
                    selected = "selected='selected'";
                sb.AppendFormat("\t<option {0}>{1}</option>\r\n", selected, i.ToString());
            }
            sb.AppendLine("</select>");
            return sb.ToString();
        }
    }
}