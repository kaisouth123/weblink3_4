﻿using Bridgepay.Weblink.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebLINK.Common;

namespace WebLINK.Templating.Formatters.Concrete
{
    public class GrandTotalFormatter : BaseFormatter
    {
        public GrandTotalFormatter(WeblinkTemplate template) : base(template) { }

        public override string ToString(int? transactionSeqNo = null)
        {
            decimal grandTotal = 0;
            for (int i = 1; i <= template.TransactionQueue.TransactionCount; i++)
            {
                decimal amount = 0;
                decimal.TryParse(template.Data[TransactionData.GetNormalizedFieldName(Fields.TotalAmt, i)], out amount);

                grandTotal += amount;
            }

            TagBuilder span = new TagBuilder("span");

            span.AddCssClass("grand-total");
            span.SetInnerText(grandTotal.ToString("C"));

            return span.ToString();
        }
    }
}