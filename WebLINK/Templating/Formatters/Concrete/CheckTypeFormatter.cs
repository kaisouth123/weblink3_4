﻿using Bridgepay.Weblink.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using WebLINK.Common;

namespace WebLINK.Templating.Formatters.Concrete
{
    public class CheckTypeFormatter : BaseFormatter
    {
        public CheckTypeFormatter(WeblinkTemplate template) : base(template) { }

        public override string ToString(int? transactionSeqNo = null)
        {
            string rv = null;
            if (template.IsForm)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("<select name=\"CheckType\" class=\"field\">");
                sb.AppendLine("\t<option>Personal</option>\r\n");
                sb.AppendLine("\t<option>Business</option>\r\n");
                sb.AppendLine("</select>");
                rv = sb.ToString();
            }
            else
            {
                rv = string.Format("<span class=\"checkType\">{0}</span>", template.Data[Fields.CheckType]);
            }

            return rv;
        }
    }
}