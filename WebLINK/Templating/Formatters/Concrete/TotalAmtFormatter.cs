﻿using Bridgepay.Weblink.Core.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using WebLINK.Common;

namespace WebLINK.Templating.Formatters.Concrete
{
    public class TotalAmtFormatter : BaseFormatter
    {
        private string fieldName;

        public TotalAmtFormatter(WeblinkTemplate template) : base(template) 
        {
            fieldName = Fields.TotalAmt;
        }

        public override string ToString(int? transactionSeqNo = null)
        {
            string value = template.Data[Fields.TotalAmt];

            if (transactionSeqNo.HasValue)
            {
                fieldName = TransactionData.GetNormalizedFieldName(Fields.TotalAmt, transactionSeqNo.Value);
                value = template.Data[fieldName];
            }

            // Format value as currency for display purposes
            decimal decimalValue = 0;
            if (decimal.TryParse(value, out decimalValue))
            {
                value = decimalValue.ToString("###,###,###.00");
            }

            if (template.IsForm)
            {
                string disabled = null;
                var templateField = template.TemplateFields.GetTemplateField(Fields.TotalAmt);

                if (this.ShouldBeDisabled(transactionSeqNo))
                    disabled = "disabled='disabled' readonly='readonly'";

                return string.Format("<input type='text' value='{0}' name='{1}' {2} class='field amount' size='12' />", value, fieldName, disabled);
            }
            else
            {
                return string.Format("<span class='amount'>{0}</span>", value);
            }
        }

        public override bool ShouldBeDisabled(int? transactionSeqNo = null)
        {
            // Disable if they passed in an amount up front
            return !string.IsNullOrEmpty(template.SessionManager.InitialPostData[fieldName]);
        }
    }
}