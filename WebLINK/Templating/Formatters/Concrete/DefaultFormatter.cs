﻿using Bridgepay.Weblink.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebLINK.Common;

namespace WebLINK.Templating.Formatters.Concrete
{
    public class DefaultFormatter : BaseFormatter
    {
        WebLINK.Templating.TemplateField field = null;

        public DefaultFormatter(WeblinkTemplate template, WebLINK.Templating.TemplateField field) : base(template) 
        {
            this.field = field;
        }

        public override string ToString(int? transactionSeqNo = null)
        {
            string value = template.Data[field.Name] + string.Empty;

            if (template.TransactionQueue.IsRepeatedField(field.Name) && transactionSeqNo.HasValue)
                value = template.Data[TransactionData.GetNormalizedFieldName(field.Name, transactionSeqNo.Value)] + string.Empty;

            return HttpUtility.HtmlEncode(value);
        }
    }
}