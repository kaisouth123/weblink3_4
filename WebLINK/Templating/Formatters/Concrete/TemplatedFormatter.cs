﻿using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace WebLINK.Templating.Formatters.Concrete
{
    /// <summary>
    /// A template based formatter meant to be used for embedded templates inside the main template. The use-case is for
    /// split transactions only so far.
    /// </summary>
    public class TemplatedFormatter : BaseFormatter
    {
        WebLINK.Templating.TemplateField field = null;
        FieldFormatterFactory fieldFormatterFactory = null;

        public TemplatedFormatter(WeblinkTemplate template, WebLINK.Templating.TemplateField field): base(template) 
        {
            this.field = field;
            fieldFormatterFactory = new FieldFormatterFactory(template);
        }

        public override string ToString(int? transactionSeqNo = null)
        {
            // Grab the template out of the html...
            string pattern = string.Format(WeblinkTemplate.EmbeddedTemplateRegExPattern, field.Name);
            var match = Regex.Match(template.Html, pattern, RegexOptions.Singleline);
            string templateText = match.Groups[1].Value.Trim();

            List<WebLINK.Templating.TemplateField> templateFields = template.GetTemplateFields(templateText);

            StringBuilder sb = new StringBuilder();
            string temp = templateText;

            foreach (var transactionData in template.TransactionQueue.Transactions)
            {
                foreach (WebLINK.Templating.TemplateField subField in templateFields)
                {
                    IFieldFormatter formatter = fieldFormatterFactory.GetFormatter(subField);
                    
                    // Make sure we use the ActualIndex below! This ensures we get the correct value if they pass in the second transaction
                    // in the sequence first, etc
                    temp = Regex.Replace(temp, subField.ReplacePattern, formatter.ToString(transactionData.ActualIndex), RegexOptions.IgnoreCase | RegexOptions.Singleline);
                }

                sb.Append(temp);
                temp = templateText;
            }

            // Todo: Should check for duplicates here!
            template.SubTemplateFields.AddRange(templateFields);

            return sb.ToString();
        }
    }
}