﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace WebLINK.Templating.Formatters.Concrete
{
    public class ValidationErrorsFormatter : BaseFormatter
    {
        public ValidationErrorsFormatter(WeblinkTemplate template) : base(template) { }

        public override string ToString(int? transactionSeqNo = null)
        {
            string rv = string.Empty;
            if (template.ErrorMessages.Count != 0)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("<ul class='error_messages'>\r\n");
                foreach (string msg in template.ErrorMessages)
                    sb.AppendFormat("\t<li>{0}</li>\r\n", msg);
                sb.AppendLine("</ul>\r\n");
                rv = sb.ToString();
            }

            return rv;
        }
    }
}