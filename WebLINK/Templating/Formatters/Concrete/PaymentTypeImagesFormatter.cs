﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace WebLINK.Templating.Formatters.Concrete
{
    public class PaymentTypeImagesFormatter : BaseFormatter
    {
        public PaymentTypeImagesFormatter(WeblinkTemplate template) : base(template) { }

        public override string ToString(int? transactionSeqNo = null)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var paymentType in template.PaymentMethods.Where(pm => !pm.Value.Equals("Preferred")))
            {
                sb.AppendFormat("<img src='images/{0}.gif' class='payment_method_logo' />", paymentType.Value);
            }
            return sb.ToString();
        }
    }
}