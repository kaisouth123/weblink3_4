﻿using Bridgepay.Weblink.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using WebLINK.Common;

namespace WebLINK.Templating.Formatters.Concrete
{
    public class CardTypeNameFormatter : BaseFormatter
    {
        public CardTypeNameFormatter(WeblinkTemplate template) : base(template) { }

        public override string ToString(int? transactionSeqNo = null)
        {
            string selectedValue = template.Data[Fields.CardTypeName] + string.Empty;
            string selected = string.Empty;
            string rv = string.Empty;

            if (template.IsForm)
            {
                if (template.PaymentMethods.Count != 0)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("<select name=\"CardTypeName\" class=\"field\" onchange=\"updatePaymentMethod();\">");
                    sb.AppendFormat("\t<option value=''>Select...</option>\r\n");
                    string st = null;
                    foreach (var paymentType in template.PaymentMethods)
                    {
                        switch (paymentType.Type)
                        {
                            case TemplatePaymentMethod.PaymentType.Credit:
                                st = "cc";
                                break;
                            case TemplatePaymentMethod.PaymentType.ACH:
                                st = "check";
                                break;
                        }
                        if (selectedValue.Equals(paymentType.Value, StringComparison.CurrentCultureIgnoreCase))
                            selected = "selected=\"selected\"";
                        else
                            selected = string.Empty;
                        sb.AppendFormat("\t<option value='{0}' {3} st='{1}'>{2}</option>\r\n", paymentType.Value, st, paymentType.Name, selected);
                    }
                    sb.AppendLine("</select>");
                    rv = sb.ToString();
                }
            }
            else
            {
                rv = string.Format("<span class=\"cardTypeName\">{0}</span>", template.Data[Fields.CardTypeName]);
            }

            return rv;
        }
    }
}