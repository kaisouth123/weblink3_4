﻿using Bridgepay.Weblink.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using WebLINK.Common;

namespace WebLINK.Templating.Formatters.Concrete
{
    public class AcctTypeFormatter : BaseFormatter
    {
        public AcctTypeFormatter(WeblinkTemplate template) : base(template) { }

        public override string ToString(int? transactionSeqNo = null)
        {
            string rv = null;
            if (template.IsForm)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("<select name=\"AcctType\" class=\"field\">");
                sb.AppendLine("\t<option>Checking</option>\r\n");
                sb.AppendLine("\t<option>Savings</option>\r\n");
                sb.AppendLine("</select>");
                rv = sb.ToString();
            }
            else
            {
                rv = string.Format("<span class=\"accountType\">{0}</span>", template.Data[Fields.AccountType]);
            }

            return rv;
        }
    }
}