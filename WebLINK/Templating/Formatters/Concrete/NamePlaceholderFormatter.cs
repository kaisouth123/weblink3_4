﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebLINK.Templating.Formatters.Concrete
{
    public class NamePlaceHolderFormatter : BaseFormatter
    {
        private WebLINK.Templating.TemplateField field = null;

        public NamePlaceHolderFormatter(WeblinkTemplate template, WebLINK.Templating.TemplateField field)
            : base(template)
        {
            this.field = field;
        }

        public override string ToString(int? transactionSeqNo = null)
        {
            return field.Name + string.Empty;
        }
    }
}