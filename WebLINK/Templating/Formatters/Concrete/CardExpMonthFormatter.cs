﻿using Bridgepay.Weblink.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using WebLINK.Common;

namespace WebLINK.Templating.Formatters.Concrete
{
    public class CardExpMonthFormatter : BaseFormatter
    {
        public CardExpMonthFormatter(WeblinkTemplate template) : base(template) { }

        public override string ToString(int? transactionSeqNo = null)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<select name=\"CardExpMonth\" class=\"field\">");
            sb.Append("\t<option value=''>- -</option>\r\n");
            for (int i = 1; i <= 12; i++)
            {
                string value = i.ToString().PadLeft(2, '0');
                string selected = null;
                if (template.Data[Fields.CardExpMonth] == value)
                    selected = "selected='selected'";
                sb.AppendFormat("\t<option {0}>{1}</option>\r\n", selected, value);
            }
            sb.AppendLine("</select>");
            return sb.ToString();
        }
    }
}