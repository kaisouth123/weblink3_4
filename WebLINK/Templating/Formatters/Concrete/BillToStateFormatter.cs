﻿using Bridgepay.Weblink.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using WebLINK.Common;

namespace WebLINK.Templating.Formatters.Concrete
{
    public class BillToStateFormatter : BaseFormatter
    {
        public BillToStateFormatter(WeblinkTemplate template) : base(template) { }

        public override string ToString(int? transactionSeqNo = null)
        {
            StringBuilder sb = new StringBuilder();
            string rv = null;

            if (template.IsForm)
            {
                sb.AppendLine("<select name=\"BillToState\" class=\"field\">");
                sb.AppendLine(GetStateOptions(template.Data[Fields.State]));
                sb.AppendLine("</select>");
                rv = sb.ToString();
            }
            else
            {
                rv = string.Format("<span class=\"billToState\">{0}</span>", template.Data[Fields.State]);
            }

            return rv;
        }

        private string GetStateOptions(string selectedValue)
        {
            StringBuilder sb = new StringBuilder();

            Dictionary<string, string> states = new Dictionary<string, string>() {
                {"AL", "Alabama"},{"AK", "Alaska"},{"AZ", "Arizona"},{"AR", "Arkansas"},{"CA", "California"},{"CO", "Colorado"},{"CT", "Connecticut"},{"DE", "Delaware"},{"DC", "Dist of Columbia"},{"FL", "Florida"},{"GA", "Georgia"},{"HI", "Hawaii"},{"ID", "Idaho"},{"IL", "Illinois"},{"IN", "Indiana"},{"IA", "Iowa"},{"KS", "Kansas"},{"KY", "Kentucky"},{"LA", "Louisiana"},{"ME", "Maine"},{"MD", "Maryland"},{"MA", "Massachusetts"},{"MI", "Michigan"},{"MN", "Minnesota"},{"MS", "Mississippi"},{"MO", "Missouri"},{"MT", "Montana"},{"NE", "Nebraska"},{"NV", "Nevada"},{"NH", "New Hampshire"},{"NJ", "New Jersey"},{"NM", "New Mexico"},{"NY", "New York"},{"NC", "North Carolina"},{"ND", "North Dakota"},{"OH", "Ohio"},{"OK", "Oklahoma"},{"OR", "Oregon"},{"PA", "Pennsylvania"},{"RI", "Rhode Island"},{"SC", "South Carolina"},{"SD", "South Dakota"},{"TN", "Tennessee"},{"TX", "Texas"},{"UT", "Utah"},{"VT", "Vermont"},{"VA", "Virginia"},{"WA", "Washington"},{"WV", "West Virginia"},{"WI", "Wisconsin"},{"WY", "Wyoming"},{"AE", "Europe AF (AE)"},{"AP", "Pacific AF (AP)"},{"AA", "Americas AF (AA)"}, {"AB", "Alberta" }, { "BC", "British Columbia" }, { "MB", "Manitoba" }, { "NB", "New Brunswick" }, { "NL", "Newfoundland/abrador" }, { "NT", "Northwest Territories" }, { "NS", "Nova Scotia" }, { "NU", "Nunavut" }, { "ON", "Ontario" }, { "PE", "Prince Edward Island" }, { "QC", "Quebec" }, { "SK", "Saskatchewan" }, { "YT", "Yukon" }                    };

            sb.AppendLine("<option value=''>Select...</option>");
            foreach (KeyValuePair<string, string> state in states)
            {
                if (selectedValue != null && state.Key.ToLower() == selectedValue.ToLower())
                    sb.AppendFormat("<option selected=\"selected\" value='{0}'>{1}</option>", state.Key, state.Value);
                else
                    sb.AppendFormat("<option value='{0}'>{1}</option>", state.Key, state.Value);
            }

            return (sb.ToString());
        }
    }
}