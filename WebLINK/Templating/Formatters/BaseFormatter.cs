﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebLINK.Templating.Formatters
{
    public abstract class BaseFormatter : IFieldFormatter
    {
        protected WeblinkTemplate template = null;

        public BaseFormatter(WeblinkTemplate template)
        {
            this.template = template;
        }

        public override string ToString()
        {
            return ToString(null);
        }

        public virtual bool ShouldBeDisabled(int? transactionSeqNo = null)
        {
            return false;
        }

        public abstract string ToString(int? transactionSeqNo = null);
    }
}