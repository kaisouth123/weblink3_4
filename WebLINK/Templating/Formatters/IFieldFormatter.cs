﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebLINK.Templating.Formatters
{
    public interface IFieldFormatter
    {
        string ToString(int? transactionSeqNo = null);
    }
}
