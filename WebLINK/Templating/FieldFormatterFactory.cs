﻿using System;
using WebLINK.Templating.Formatters;
using WebLINK.Templating.Formatters.Concrete;

namespace WebLINK.Templating
{
    /// <summary>
    /// Returns the correct Formatter class based on the field being processed
    /// </summary>
    public class FieldFormatterFactory
    {
        WeblinkTemplate template = null;

        public FieldFormatterFactory(WeblinkTemplate template)
        {
            this.template = template;
        }

        public IFieldFormatter GetFormatter(TemplateField field)
        {
            // A couple specific formatters are checked for up front, then we fall back to the named formatters and then finally fall back to the
            // default formatter
            if (field.TemplatePlaceHolder)
            {
                return (IFieldFormatter)Activator.CreateInstance(typeof(TemplatedFormatter), template, field);
            }
            else if (field.NamePlaceHolder)
            {
                return (IFieldFormatter)Activator.CreateInstance(typeof(NamePlaceHolderFormatter), template, field);
            }
            else
            {
                string typeName = string.Format("WebLINK.Templating.Formatters.Concrete.{0}Formatter", field.Name);
                Type type = Type.GetType(typeName, false, true);

                if (type != null)
                    return (IFieldFormatter)Activator.CreateInstance(type, template);
                else
                    return (IFieldFormatter)Activator.CreateInstance(typeof(DefaultFormatter), template, field);
            }

        }
    }
}