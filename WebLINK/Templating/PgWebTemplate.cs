﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using WebLINK.Models;

namespace WebLINK.Templating
{
    public class PgWebTemplate : BaseTemplate
    {

        private string templateHtml;
        private PgWebTransaction pgWebTransaction;

        public PgWebTemplate(string templateHtml, PgWebTransaction pgWebTransaction) : base()
        {
            this.templateHtml = templateHtml;
            this.pgWebTransaction = pgWebTransaction;
        }

        public override string ToString()
        {
            // Create the script includes
            Regex re = new Regex(HeadScriptsTagRegExPattern);

            InlineHeadScript += getPgWebTransactionDataScript();
            InlineHeadScript += GenerateScriptTags(HeadScripts);

            templateHtml = re.Replace(templateHtml, InlineHeadScript);

            re = new Regex(BodyScriptsTagRegExPattern);
            var bodyScripts = GenerateScriptTags(BodyScripts);

            if (!string.IsNullOrEmpty(InlineBodyScript))
            {
                bodyScripts += "\r\n<script>" + InlineBodyScript + "</script>";
            }

            templateHtml = re.Replace(templateHtml, bodyScripts);

            return templateHtml;
        }

        private string getPgWebTransactionDataScript()
        {
            return string.Format("<script>window.pgWebTransactionData = {0};</script>", Newtonsoft.Json.JsonConvert.SerializeObject(pgWebTransaction));
        }
        
    }
}