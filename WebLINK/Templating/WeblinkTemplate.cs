﻿using Bridgepay.Weblink.Core.Helpers;
using Bridgepay.Weblink.Core.Models;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.HtmlControls;
using WebLINK.Common;
using WebLINK.Templating.Formatters;

namespace WebLINK.Templating
{


    public class WeblinkTemplate : BaseTemplate
    {
        private const string PlaceholderRegExPattern = @"(name=[""']?|value=[""']?)?<!--[\[{](\w+)[]}](required)?-->";
        public const string EmbeddedTemplateRegExPattern = @"<script.*?type=[""']text/template[""'] id=[""']{0}[""'].*?>(.*)</script>";
        private const string FileSystemTemplateRegExPattern = @"<!--\s*{{\s*(\w+)\s*}}\s*-->";


        private FieldFormatterFactory fieldFormatterFactory = null;

        public SessionManager SessionManager = null;

        // Default values for fields when an integrator does not pass in a value
        public Dictionary<string, string> FieldDefaults { get; set; }

        private string html = null;
        public string Html
        {
            get { return html; }
            set { html = value; }
        }

        private bool isForm = false; //True if we are rendering a form, otherwise we are rendering an email, receipt, etc
        public bool IsForm
        {
            get { return isForm; }
            set { isForm = value; }
        }

        private TransactionQueue transactionQueue = null;
        public TransactionQueue TransactionQueue
        {
            get { return transactionQueue; }
            set { transactionQueue = value; }
        }

        private NameValueCollection data = null;
        public NameValueCollection Data
        {
            get { return data; }
            set { data = value; }
        }

        private List<HtmlInputHidden> hiddenFields = new List<HtmlInputHidden>();
        public List<HtmlInputHidden> HiddenFields
        {
            get { return hiddenFields; }
            set { hiddenFields = value; }
        }

        private Dictionary<string, string> paymentTypeNames = new Dictionary<string, string>()
        {
            {"AMEX", "American Express"},
            {"Diners","Diner's Club"},
            {"Discover", "Discover"},
            {"Mastercard", "Mastercard"},
            {"Visa", "Visa"},
            {"eCheck", "Electronic Check"},
        };
        public Dictionary<string, string> PaymentTypeNames
        {
            get { return paymentTypeNames; }
            set { paymentTypeNames = value; }
        }

        private List<TemplatePaymentMethod> paymentMethods = new List<TemplatePaymentMethod>();
        /// <summary>
        /// Used as the official list of payment methods when rendering payment forms, etc
        /// </summary>
        public List<TemplatePaymentMethod> PaymentMethods
        {
            get { return paymentMethods; }
            set { paymentMethods = value; }
        }

        public List<TemplateField> RequiredFields = new List<TemplateField>();
        public List<TemplateField> TemplateFields = new List<TemplateField>(); // All fields found in the template
        public List<TemplateField> SubTemplateFields = new List<TemplateField>(); // All fields found in sub templates

        private List<string> InputFields = new List<string>(); // Contains a list of all fields that were rendered as inputs
        private string[] PaymentFields = { "CardNumber", "CardExpMonth", "CardExpYear", "CVNum", "CheckNum", "TransitNum", "AccountNum", "CheckType", "AcctType" };
        private List<string> ExemptFields = new List<string>();

        // Add more to this list to support additional style overrides
        private NameValueCollection Styles = new NameValueCollection() {
            { "Style_Page_BackgroundImage", "body {{background-image: url({0});}}" },
            { "Style_Page_BackgroundColor", "body {{background-color: {0};}}" },
            { "Style_Form_BackgroundColor", ".paymentForm {{background-color: {0};}}" },
            { "Style_FontFamily", "body {{font-family: {0};}}" },
            { "Style_Heading_FontColor", ".header {{color: {0};}}" },
            { "Style_Heading_FontSize", ".header {{font-size: {0};}}" },
            { "Style_Heading_BackgroundColor", ".header {{background: {0};}}" },
            { "Style_Instruction_FontColor", ".instruction {{color: {0};}}" },
            { "Style_Instruction_FontSize", ".instruction {{font-size: {0};}}" },
            { "Style_Label_FontColor", ".label {{color: {0};}}" },
            { "Style_Label_FontSize", ".label {{font-size: {0};}}" },
            { "Style_Field_FontColor", ".field {{color: {0};}}" },
            { "Style_Field_FontSize", ".field {{font-size: {0};}}" },
        };

        public WeblinkTemplate(string html, SessionManager sessionManager, bool isForm) : base()
        {
            this.SessionManager = sessionManager;
            this.Html = html;
            this.data = sessionManager.Data;
            this.transactionQueue = new TransactionQueue(data);
            this.IsForm = isForm;
            StringBuilder values = new StringBuilder();
            foreach (var transactionData in transactionQueue.Transactions)
            {                
                foreach (var keyName in transactionData.AllKeys)
                {
                    values.AppendFormat("{0}:{1}\r\n", keyName,
                        MaskingHelper.MaskSensitiveData(transactionData[keyName], keyName));
                }
            }
            Global.Log.DebugFormat("transactionQueue after WeblinkTemplate:\r\n{0}\rsessionManager.Data after WeblinkTemplate:\r\n{1}", values.ToString(), MaskingHelper.MaskPossiblySensitiveData(sessionManager.Data, false));            
            InitPaymentMethods();
            InitTemplateFields();

            fieldFormatterFactory = new FieldFormatterFactory(this);
            FieldDefaults = new Dictionary<string, string>
            {
                //{ Fields.Description, "Total" }
            };

        }

        #region Static properties / ctor

        #endregion

        #region "Static methods"
        /// <summary>
        /// Removes any HTML that is surrounded by comment blocks in the format of <!-- blockName --> html here <!-- /blockName -->
        /// </summary>
        /// <param name="templateHtml"></param>
        /// <param name="blockName"></param>
        /// <returns></returns>
        public static string RemoveFunctionalityBlock(string templateHtml, string blockName)
        {
            if (string.IsNullOrEmpty(templateHtml))
                return null;

            string startBlockRegEx = string.Format(@"<!--\s*{0}\s*-->", blockName);
            string endBlockRegEx = string.Format(@"<!--\s*/{0}\s*-->", blockName);

            Match startCommentBlockMatch = Regex.Match(templateHtml, startBlockRegEx);
            Match endCommentBlockMatch;

            if (startCommentBlockMatch.Success &&
                (endCommentBlockMatch = Regex.Match(templateHtml, endBlockRegEx)).Success)
            {
                while (startCommentBlockMatch.Success && endCommentBlockMatch.Success)
                {
                    int startTagIndex = startCommentBlockMatch.Index;
                    int endTagIndex = endCommentBlockMatch.Index;

                    templateHtml = templateHtml.Remove(startTagIndex, endTagIndex - startTagIndex + endCommentBlockMatch.Length);

                    startCommentBlockMatch = Regex.Match(templateHtml, startBlockRegEx);
                    endCommentBlockMatch = Regex.Match(templateHtml, endBlockRegEx);
                }
            }

            return templateHtml;
        }
        #endregion

        // Populates the PaymentMethod collection from what is specified in the Fields.PaymentTypes field
        private void InitPaymentMethods()
        {
            string paymentTypes = transactionQueue.Data[Fields.PaymentTypes] + string.Empty;

            if (!string.IsNullOrEmpty(paymentTypes))
            {
                foreach (string paymentType in paymentTypes.Split(new char[] { ',' }))
                {
                    TemplatePaymentMethod templatePaymentMethod = new TemplatePaymentMethod
                    {
                        Name = PaymentTypeNames[paymentType],
                        Value = paymentType,
                        Type = TemplatePaymentMethod.PaymentType.Credit
                    };

                    if (paymentType.Equals(Global.PaymentTypeECheck, StringComparison.CurrentCultureIgnoreCase))
                        templatePaymentMethod.Type = TemplatePaymentMethod.PaymentType.ACH;

                    PaymentMethods.Add(templatePaymentMethod);
                }
            }
        }

        /// <summary>
        /// Searches through the template HTML looking for pattern and adds fields to the TemplateFields collection.
        /// </summary>
        /// <param name="pattern"></param>
        public List<TemplateField> GetTemplateFields(string html)
        {
            List<TemplateField> templateFields = new List<TemplateField>();

            if (string.IsNullOrEmpty(html))
                return templateFields;

            foreach (Match match in Regex.Matches(html, PlaceholderRegExPattern))
            {
                TemplateField field = new TemplateField(match.Groups[2].Value);
                field.ReplacePattern = Regex.Escape(match.Value);

                string group1 = match.Groups[1].Value + string.Empty;
                string FieldName = match.Groups[2].Value + string.Empty;
                if (group1.ToLower().StartsWith("name"))
                {
                    field.InputPlaceHolder = true;
                    field.NamePlaceHolder = true;
                }
                else if (group1.ToLower().StartsWith("value"))
                {
                    field.InputPlaceHolder = true;
                }
                else if (FieldName.StartsWithAny(new List<string>
                {
                    Fields.CardTypeName,
                    Fields.CardExpMonth,
                    Fields.CardExpYear,
                    Fields.CheckType,
                    Fields.AccountType,
                    Fields.State,
                    Fields.TotalAmt
                }))
                {
                    field.InputPlaceHolder = true;
                }

                if (match.Value.Substring(match.Value.IndexOf(FieldName) - 1, 1) == "{")
                    field.TemplatePlaceHolder = true;

                //We dont want to replace the name=" or value=" portion of the html!
                if (field.ReplacePattern.StartsWith(group1))
                    field.ReplacePattern = field.ReplacePattern.Substring(group1.Length);

                if (!string.IsNullOrEmpty(match.Groups[3].Value))
                    field.Required = true;

                if (!templateFields.Contains(field))
                    templateFields.Add(field);
            }

            return templateFields;
        }

        public override string ToString()
        {
            string temp = Html;

            foreach (TemplateField field in TemplateFields)
            {
                IFieldFormatter formatter = fieldFormatterFactory.GetFormatter(field);
                string fieldValue = formatter.ToString();

                // Set field defaults if no value was provided and a default exists
                // For now we are not going to make a distinction between null and string.empty. IFieldFormatter.ToString() currently
                // does not return nulls anyway and this is simpler. This shouldnt be an issue for now.
                if (string.IsNullOrEmpty(fieldValue) &&
                    FieldDefaults.ContainsKey(field.Name))
                {
                    fieldValue = FieldDefaults[field.Name];
                }

                temp = Regex.Replace(temp, field.ReplacePattern, fieldValue, RegexOptions.IgnoreCase);
            }

            // Identify all the non disabled input fields. These will be exempt from rendering accompanying hidden fields.
            // Since disabled inputs dont actually get posted, we need to make this distinction. If we didnt render
            // a complimentary hidden input for a disabled field, it would never make it back to the server.
            ExemptFields = GetNonDisabledInputFields(temp);

            //Locate the closing form tag and insert the hidden fields right before it
            int index = temp.ToLower().LastIndexOf("</form>");
            if (index != -1)
            {
                if (SessionManager.IsUsingSessionState)
                {
                    temp = temp.Insert(index, GetHiddenFieldsHtmlForSessions());
                }
                else
                {
                    temp = temp.Insert(index, GetHiddenFieldsHtmlForNonSessions());
                }
            }

            //Add the required fields js array if needed
            index = temp.ToLower().LastIndexOf("</head>");
            if (index != -1)
                temp = temp.Insert(index, GetJSRequiredFields());

            index = temp.ToLower().LastIndexOf("</head>");
            if (index != -1)
                temp = temp.Insert(index, GetStyles());

            // Process file system templates
            Regex re = new Regex(FileSystemTemplateRegExPattern);
            foreach (Match match in re.Matches(temp))
            {
                string templateName = match.Groups[1].Value;
                if (Global.Templates.ContainsKey(templateName))
                {
                    var templateString = Global.Templates[templateName];
                    templateString = templateString.Replace("[HOST]", HttpContext.Current.Request.Url.Host);
                    temp = temp.Replace(match.Value, templateString);
                }
            }

            // Create the script includes
            re = new Regex(HeadScriptsTagRegExPattern);
            var headScripts = GenerateScriptTags(HeadScripts);

            if (!string.IsNullOrEmpty(InlineHeadScript))
            {
                headScripts += "\r\n<script>" + InlineHeadScript + "</script>";
            }

            temp = re.Replace(temp, headScripts);

            re = new Regex(BodyScriptsTagRegExPattern);
            var bodyScripts = GenerateScriptTags(BodyScripts);

            if (!string.IsNullOrEmpty(InlineBodyScript))
            {
                bodyScripts += "\r\n<script>" + InlineBodyScript + "</script>";
            }

            temp = re.Replace(temp, bodyScripts);

            re = new Regex(StylesTagRegExPattern);
            temp = re.Replace(temp, GenerateStylesheetLinks(Stylesheets));

            return (temp);
        }

        private List<string> GetNonDisabledInputFields(string html)
        {
            HtmlAgilityPack.HtmlDocument htmlDoc = new HtmlAgilityPack.HtmlDocument();
            htmlDoc.LoadHtml(html);
            List<string> fields = new List<string>();

            IEnumerable<HtmlNode> inputs = htmlDoc.DocumentNode.SelectNodes("//input[@type='text']");
            var selects = htmlDoc.DocumentNode.SelectNodes("//select");
            if (inputs != null && selects!=null)
                inputs = inputs.Concat(selects);
            else
                inputs = htmlDoc.DocumentNode.SelectNodes("//select");

            if (inputs == null)
                inputs = Enumerable.Empty<HtmlNode>();

            foreach (HtmlNode input in inputs)
            {
                var name = input.Attributes["name"];
                if (input.Attributes["disabled"] == null)
                    fields.Add(name.Value);
            }

            return fields;
        }

        private void InitTemplateFields()
        {
            TemplateFields.AddRange(GetTemplateFields(Html));

            List<TemplateField> exceptionFields = new List<TemplateField>();

            // If this is a SL transaction then Card # and ExpDate are not required no matter what.
            StateManager stateManager = new StateManager(SessionManager);
            if (stateManager.IsSecureTransaction)
            {
                exceptionFields.Add(TemplateFields.FirstOrDefault(tf => tf.Name.Equals(Fields.CardExpMonth)));
                exceptionFields.Add(TemplateFields.FirstOrDefault(tf => tf.Name.Equals(Fields.CardExpYear)));
                exceptionFields.Add(TemplateFields.FirstOrDefault(tf => tf.Name.Equals(Fields.CardNumber)));
            }

            // If this is a tokenized transaction for Bridgecomm, then dont require CVV
            if (stateManager.IsPlainTokenizedTransaction)
            {
                exceptionFields.Add(TemplateFields.FirstOrDefault(tf => tf.Name.Equals(Fields.CVNum)));
            }

            RequiredFields = TemplateFields
                .Where(field => field.Required)
                .Except(exceptionFields)
                .ToList();
        }

        private string GetHiddenFieldsHtmlForSessions()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendFormat("<input type=\"hidden\" name=\"{0}\" value=\"1\" />\r\n", Fields.IsPostBack);

            List<string> fields = new List<string>
            {
                Fields.WalletCardType,
                Fields.WalletExpDate,
                Fields.WalletCardNumber,
                Fields.ReturnUrl,
                Fields.CancelUrl,
                Fields.ExpirationDateLogic
            };

            foreach (var field in fields)
            {
                sb.AppendFormat("<input type=\"hidden\" name=\"{0}\" id=\"{0}\" value=\"{1}\" />\r\n",
                    field,
                    data[field]);
            }

            return sb.ToString();
        }

        private string GetHiddenFieldsHtmlForNonSessions()
        {
            StringBuilder sb = new StringBuilder();

            //Pass through the required fields, since we need them to do server side validation on submit
            //Need to do this like so because we arent currently allowing the user to specify the required fields through a form field.
            //Instead, they are determined from the template itself.

            //Dont include the payment form fields here since we dont truly know which of these fields are required since the user selects check vs cc in a dropdown at "runtime"
            this.data[Fields.RequiredFields] = RequiredFields
                .Where(field => !field.Name.EqualsAny(PaymentFields))
                .Select(field => field.Name)
                .Distinct()
                .Join(",");

            if (this.data[Fields.RequiredFields].StartsWith(","))
                this.data[Fields.RequiredFields] = this.data[Fields.RequiredFields].Substring(1);

            foreach (string field in this.data)
            {
                bool exempt = false;

                if (string.IsNullOrEmpty(this.data[field]))
                    exempt = true;

                if (ExemptFields.Contains(field))
                    exempt = true;

                // Dont include result fields. This causes problems when a transaction fails at the payment form and
                // a fail url is not specified. In other words, when we re-render the payment form to show the error.
                // Only do this it we are rendering a form. We want these rendered in the receipt in case an integrator wants
                // to be able to do a post from the receipt page or something goofy like that I suppose...
                if (IsForm && field.StartsWithAny(new List<string> { "r_", "rv_" }))
                    exempt = true;

                // Just in case they pass these in, dont render them...ever!
                if (field.EqualsAny(Fields.Login, Fields.Password, Fields.PasswordEncrypted,Fields.PurchaseToken))
                    exempt = true;

                if (!exempt)
                    sb.AppendFormat("\t<input type='hidden' name='{0}' value='{1}' />\r\n", field, HttpUtility.HtmlEncode(data[field]));
            }

            return (sb.ToString());
        }

        private string GetStyles()
        {
            IndentedStringBuilder sb = new IndentedStringBuilder();
            bool data = false;

            sb.IndentLevel = 2;
            sb.AppendLine("<style type='text/css'>");
            sb.IndentLevel++;
            foreach (string key in Styles.AllKeys)
            {
                if (!string.IsNullOrEmpty(this.data[key]))
                {
                    data = true;
                    sb.AppendFormat(Styles[key] + "\r\n", this.data[key]);
                }
            }
            sb.IndentLevel--;
            sb.Append("</style>\r\n");

            return (data ? sb.ToString() : string.Empty);
        }

        private string GetJSRequiredFields()
        {
            IndentedStringBuilder sb = new IndentedStringBuilder();

            string conn = null;

            sb.AppendLine("<script type='text/javascript'>");
            sb.Append("\twindow.BP = window.BP || {};\n");
            sb.Append("\tBP.requiredFields = [");
            foreach (TemplateField field in RequiredFields.Where(field => !field.Name.EqualsAny(PaymentFields)))
            {
                sb.AppendFormat("{0}'{1}'", conn, field.Name);
                conn = ", ";
            }

            // Hack - Fix this by parsing the subtemplate fields ahead of time and adding them as required
            if (RequiredFields.Find(fld => fld.Name.Equals(Fields.TotalAmt, StringComparison.CurrentCultureIgnoreCase)) == null)
                sb.AppendFormat("{0}'{1}'", conn, Fields.TotalAmt);

            sb.Append("];\n");
            sb.AppendLine("\tBP.requiredFieldsCopy = BP.requiredFields.slice(0);\n");

            //Required fields for credit and check:
            conn = null;
            sb.Append("\tBP.requiredCCFields = [");
            foreach (TemplateField field in RequiredFields.Where(fld => fld.Name.EqualsAny("CardNumber", "CVNum", "ExpDate", "CardExpMonth", "CardExpYear")))
            {
                sb.AppendFormat("{0}'{1}'", conn, field.Name);
                conn = ", ";
            }
            sb.AppendLine("];");

            conn = null;
            sb.Append("\tBP.requiredCheckFields = [");
            foreach (TemplateField field in RequiredFields.Where(fld => fld.Name.EqualsAny("CheckNum", "TransitNum", "AccountNum", "CheckType", "AccountType")))
            {
                sb.AppendFormat("{0}'{1}'", conn, field.Name);
                conn = ", ";
            }
            sb.AppendLine("];");

            sb.AppendLine("</script>");

            return sb.ToString();
        }

    }
}