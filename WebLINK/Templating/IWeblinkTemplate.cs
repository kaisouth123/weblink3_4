﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebLINK.Templating
{
    interface IWeblinkTemplate
    {
        List<ScriptType> HeadScripts { get; set; }
        List<ScriptType> BodyScripts { get; set; }
        List<StylesheetType> Stylesheets { get; set; }
        string InlineHeadScript { get; set; }
        string InlineBodyScript { get; set; }
        List<string> ErrorMessages { get; set; }

        string ToString();
    }
}
