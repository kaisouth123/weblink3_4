﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Web;

namespace WebLINK.Templating
{
    public class ClientSideRedirectTemplate : IWeblinkTemplate
    {
        private string RedirectUrl;
        private NameValueCollection Data;

        // Yes, a string literal...
        private const string template =
@"<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">
<html xmlns=""http://www.w3.org/1999/xhtml\"">
<head>
    <title></title>
<!-- %%STYLES%% -->
   <link href=""style/payment_form.css"" rel=""stylesheet"" type=""text/css"" />
</head>
<body onload=""document.theForm.submit();""  >
    <form method=""post"" action=""{0}"" name=""theForm"">
        <div id=""processingRequest"" style=""background-color:
  {2};font-family:{3}; color:{4};background-image:{5}"">
            <img src=""style/images/ajax-loader.gif"" />
            Processing could take a few minutes to complete.<br>
            <em>Please do not click Back, Refresh, or close your browser!<em>
        </div>
{1}
    </form>
</body>
</html>";

        public List<ScriptType> HeadScripts { get; set; }
        public List<ScriptType> BodyScripts { get; set; }
        public List<StylesheetType> Stylesheets { get; set; }
        public string InlineHeadScript { get; set; }
        public string InlineBodyScript { get; set; }
        public List<string> ErrorMessages { get; set; }

        public ClientSideRedirectTemplate(string redirectUrl, NameValueCollection data)
        {
            RedirectUrl = redirectUrl;
            Data = data;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var key in Data.AllKeys)
            {
                sb.AppendFormat("\t<input type='hidden' name='{0}' value='{1}' />\r\n", key, Data[key]);
            }

            return string.Format(template, RedirectUrl, sb.ToString(),string.IsNullOrEmpty(Data["Style_Page_BackgroundColor"])? "#F9F3D6": Data["Style_Page_BackgroundColor"], string.IsNullOrEmpty(Data["Style_FontFamily"]) ? "" : Data["Style_FontFamily"], string.IsNullOrEmpty(Data["Style_Heading_FontColor"]) ? "#000" : Data["Style_Heading_FontColor"], string.IsNullOrEmpty(Data["Style_Page_BackgroundImage"]) ? "none":Data["Style_Page_BackgroundImage"]);
        }
        
    }
}