<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:output method="xml" indent="yes"/>
   <xsl:template match="/">
      <requestHeader>
         <ClientIdentifier><xsl:value-of select="requestHeader/ClientIdentifier"/></ClientIdentifier>
         <TransactionID><xsl:value-of select="requestHeader/TransactionID"/></TransactionID>
         <RequestDateTime><xsl:value-of select="requestHeader/RequestDateTime"/></RequestDateTime>
         <RequestType>004</RequestType>
		 <requestMessage>
			<PurchaseToken><xsl:value-of select="requestHeader/requestMessage/PurchaseToken"/></PurchaseToken>
		    <MerchantCode><xsl:value-of select="requestHeader/requestMessage/MerchantCode"/></MerchantCode>
			<MerchantAccountCode><xsl:value-of select="requestHeader/requestMessage/MerchantAccountCode"/></MerchantAccountCode>
			<xsl:if test="requestHeader/requestMessage/AcctType = 'R'">
			  <xsl:if test="requestHeader/requestMessage/Token != ''">
			    <Token><xsl:value-of select="requestHeader/requestMessage/Token"/></Token>
			  </xsl:if>
			  <xsl:if test="requestHeader/requestMessage/PaymentAccountNumber != ''">
			    <PaymentAccountNumber><xsl:value-of select="requestHeader/requestMessage/PaymentAccountNumber"/></PaymentAccountNumber>
			  </xsl:if>
			  <xsl:if test="requestHeader/requestMessage/ExpirationDate != ''">
			    <ExpirationDate><xsl:value-of select="requestHeader/requestMessage/ExpirationDate"/></ExpirationDate>
			  </xsl:if>
			  <xsl:if test="requestHeader/requestMessage/SecurityCode != ''">
			    <SecurityCode><xsl:value-of select="requestHeader/requestMessage/SecurityCode"/></SecurityCode>
			  </xsl:if>
			</xsl:if>
			<xsl:if test="requestHeader/requestMessage/AcctType != 'R'">
              <BankAccountNum><xsl:value-of select="requestHeader/requestMessage/BankAccountNum"/></BankAccountNum>
              <RoutingNum><xsl:value-of select="requestHeader/requestMessage/RoutingNum"/></RoutingNum>
			</xsl:if>
			<TransactionType><xsl:value-of select="requestHeader/requestMessage/TransactionType"/></TransactionType>
			<Amount><xsl:value-of select="requestHeader/requestMessage/Amount"/></Amount>
			<xsl:if test="requestHeader/requestMessage/InvoiceNum != ''">
			  <InvoiceNum><xsl:value-of select="requestHeader/requestMessage/InvoiceNum"/></InvoiceNum>
			</xsl:if>
			<xsl:if test="requestHeader/requestMessage/CustomerAccountCode != ''">
			  <CustomerAccountCode><xsl:value-of select="requestHeader/requestMessage/CustomerAccountCode"/></CustomerAccountCode>
			</xsl:if>
			<xsl:if test="requestHeader/requestMessage/ZipCode != ''">
			  <ZipCode><xsl:value-of select="requestHeader/requestMessage/ZipCode"/></ZipCode>
			</xsl:if>
			<xsl:if test="requestHeader/requestMessage/PONum != ''">
              <PONum><xsl:value-of select="requestHeader/requestMessage/PONum"/></PONum>
			</xsl:if>
            <AccountHolderName><xsl:value-of select="requestHeader/requestMessage/AccountHolderName"/></AccountHolderName>
            <HolderType><xsl:value-of select="requestHeader/requestMessage/HolderType"/></HolderType>
            <AccountStreet><xsl:value-of select="requestHeader/requestMessage/AccountStreet"/></AccountStreet>
            <AccountCity><xsl:value-of select="requestHeader/requestMessage/AccountCity"/></AccountCity>
            <AccountState><xsl:value-of select="requestHeader/requestMessage/AccountState"/></AccountState>
            <AccountZip><xsl:value-of select="requestHeader/requestMessage/AccountZip"/></AccountZip>
			<xsl:if test="requestHeader/requestMessage/AccountPhone != ''">
              <AccountPhone><xsl:value-of select="requestHeader/requestMessage/AccountPhone"/></AccountPhone>
			</xsl:if>
			<xsl:if test="requestHeader/requestMessage/PaymentType != ''">
              <PaymentType><xsl:value-of select="requestHeader/requestMessage/PaymentType"/></PaymentType>
			</xsl:if>
            <OriginatingSoftwareVendor><xsl:value-of select="requestHeader/requestMessage/OriginatingSoftwareVendor"/></OriginatingSoftwareVendor>
            <ClientPublicIPAddress><xsl:value-of select="requestHeader/requestMessage/ClientPublicIPAddress"/></ClientPublicIPAddress>
            <OriginatingTechnologySource><xsl:value-of select="requestHeader/requestMessage/OriginatingTechnologySource"/></OriginatingTechnologySource>
			<xsl:if test="requestHeader/requestMessage/TransCatCode != ''">
              <TransCatCode><xsl:value-of select="requestHeader/requestMessage/TransCatCode"/></TransCatCode>
			</xsl:if>
			<xsl:if test="requestHeader/requestMessage/FeeAmount != ''">
			  <FeeAmount><xsl:value-of select="requestHeader/requestMessage/FeeAmount"/></FeeAmount>
			</xsl:if>
			<xsl:if test="requestHeader/requestMessage/TransIndustryType != ''">
              <TransIndustryType><xsl:value-of select="requestHeader/requestMessage/TransIndustryType"/></TransIndustryType>
			</xsl:if>
			<xsl:if test="requestHeader/requestMessage/AcctType != ''">
              <AcctType><xsl:value-of select="requestHeader/requestMessage/AcctType"/></AcctType>
			</xsl:if>
			<xsl:if test="requestHeader/requestMessage/CustomData != ''">
              <CustomData persist='true'><xsl:value-of select="requestHeader/requestMessage/CustomData"/></CustomData>
			</xsl:if>
			<xsl:if test="requestHeader/requestMessage/ServiceFee/Amount != ''">
              <ServiceFee>
                <ServiceFeeID><xsl:value-of select="requestHeader/requestMessage/ServiceFee/ServiceFeeID"/></ServiceFeeID>
				<PurchaseToken><xsl:value-of select="requestHeader/requestMessage/ServiceFee/PurchaseToken"/></PurchaseToken>
                <Amount><xsl:value-of select="requestHeader/requestMessage/ServiceFee/Amount"/></Amount>
              </ServiceFee>
			</xsl:if>
		</requestMessage>
    </requestHeader>
</xsl:template>
</xsl:stylesheet>


