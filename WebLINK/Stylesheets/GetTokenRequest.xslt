<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:output method="xml" indent="yes"/>
   <xsl:template match="/">
      <requestHeader>
         <ClientIdentifier><xsl:value-of select="requestHeader/ClientIdentifier"/></ClientIdentifier>
         <TransactionID><xsl:value-of select="requestHeader/TransactionID"/></TransactionID>
         <RequestDateTime><xsl:value-of select="requestHeader/RequestDateTime"/></RequestDateTime>
         <RequestType>001</RequestType>
		 <requestMessage>
			<PurchaseToken><xsl:value-of select="requestHeader/requestMessage/PurchaseToken"/></PurchaseToken>
		    <PaymentAccountNumber><xsl:value-of select="requestHeader/requestMessage/PaymentAccountNumber"/></PaymentAccountNumber>
			<ExpirationDate><xsl:value-of select="requestHeader/requestMessage/ExpirationDate"/></ExpirationDate>
		 </requestMessage>
    </requestHeader>
</xsl:template>
</xsl:stylesheet>