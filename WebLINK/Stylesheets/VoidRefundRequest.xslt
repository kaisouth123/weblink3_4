<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:output method="xml" indent="yes"/>
   <xsl:template match="/">
      <requestHeader>
         <ClientIdentifier><xsl:value-of select="requestHeader/ClientIdentifier"/></ClientIdentifier>
         <TransactionID><xsl:value-of select="requestHeader/TransactionID"/></TransactionID>
         <RequestDateTime><xsl:value-of select="requestHeader/RequestDateTime"/></RequestDateTime>
         <RequestType>012</RequestType>
		 <requestMessage>
			<PurchaseToken><xsl:value-of select="requestHeader/requestMessage/PurchaseToken"/></PurchaseToken>
		    <MerchantCode><xsl:value-of select="requestHeader/requestMessage/MerchantCode"/></MerchantCode>
			<MerchantAccountCode><xsl:value-of select="requestHeader/requestMessage/MerchantAccountCode"/></MerchantAccountCode>
			<TransactionType><xsl:value-of select="requestHeader/requestMessage/TransactionType"/></TransactionType>
			<ReferenceNumber><xsl:value-of select="requestHeader/requestMessage/ReferenceNumber"/></ReferenceNumber>
			<Amount><xsl:value-of select="requestHeader/requestMessage/Amount"/></Amount>
			<InvoiceNum><xsl:value-of select="requestHeader/requestMessage/InvoiceNum"/></InvoiceNum>
			<CustomerAccountCode><xsl:value-of select="requestHeader/requestMessage/CustomerAccountCode"/></CustomerAccountCode>
		 </requestMessage>
    </requestHeader>
</xsl:template>
</xsl:stylesheet>