﻿using Bridgepay.Weblink.Core.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace WebLINK.Common
{
    public class SessionManager
    {
        private const string SessionCookieName = "wl3_session_id";
        private const string SessionExpirationKeyName = "session_expiration";
        private const int SessionTimeoutMinutes = 10;

        HttpSessionStateBase Session;
        HttpContextBase Context;
        HttpResponseBase Response;
        HttpRequestBase Request;

        private bool isValid = false;
        public bool IsValid
        {
            get { return isValid; }
        }

        private bool isUsingSessionState = false;
        public bool IsUsingSessionState { get { return isUsingSessionState; } }

        public SessionManager(HttpContextBase httpContext)
        {
            this.Context = httpContext;
            this.Session = httpContext.Session;
            this.Response = httpContext.Response;
            this.Request = httpContext.Request;
            isUsingSessionState = true;
        }

        public bool HasFailedTransactions
        {
            get
            {
                if (Session["HasFailedTransactions"] == null)
                    return false;
                else
                    return (bool)Session["HasFailedTransactions"];
            }
            set
            {
                Session["HasFailedTransactions"] = value;
            }
        }

        public bool IsNewSession
        {
            get
            {
                return string.IsNullOrEmpty(GetCookieSessionId());
            }
        }

        public bool SessionHasTimedOut
        {
            get
            {
                return GetSessionExpiration() < DateTime.Now;
            }
        }

        public bool HasPreferredPaymentMethodLoaded
        {
            get
            {
                if (Session["HasPreferredPaymentMethodLoaded"] == null)
                {
                    return false;
                }
                else
                {
                    return (bool)Session["HasPreferredPaymentMethodLoaded"];
                }
            }
            set
            {
                Session["HasPreferredPaymentMethodLoaded"] = value;
            }
        }

        public NameValueCollection Data
        {
            get
            {
                if (Session["PostData"] != null)
                {
                    return Session["PostData"] as NameValueCollection;
                }

                var data = new NameValueCollection();
                Session["PostData"] = data;
                return data;
            }
            set
            {
                Session["PostData"] = value;
            }
        }

        public NameValueCollection InitialPostData
        {
            get
            {
                if (Session["InitialPostData"] != null)
                {
                    return Session["InitialPostData"] as NameValueCollection;
                }

                var data = new NameValueCollection();
                Session["InitialPostData"] = data;
                return data;
            }
            set
            {
                Session["InitialPostData"] = value;
            }
        }

        public string ASPNET_SessionId
        {
            get
            {
                return Session.SessionID;
            }
        }

        public Guid? SessionId
        {
            get
            {
                if (Session["SessionId"] != null)
                {
                    return Guid.Parse(Convert.ToString(Session["SessionId"]));
                }

                return null;
            }
            private set
            {
                Session["SessionId"] = value;
            }
        }

        /// <summary>
        /// Abandons the session and clears the session cookies
        /// </summary>
        public void Abandon()
        {
            Session.Abandon();

            var aspNetSessionCookie = Response.Cookies["ASP.NET_SessionId"];

            if (aspNetSessionCookie != null)
            {
                aspNetSessionCookie.Value = string.Empty;
                aspNetSessionCookie.Expires = DateTime.Now.AddYears(-30);
            }

            var wlSessionCookie = Response.Cookies[SessionCookieName];

            if (wlSessionCookie != null)
            {
                wlSessionCookie.Value = string.Empty;
                wlSessionCookie.Expires = DateTime.Now.AddYears(-30);
            }
        }

        public void EstablishNewSession(NameValueCollection postData)
        {
            Data = postData;
            SetCookieSessionId(Session.SessionID);
            SetOrUpdateSessionTimeout();
            SessionId = Guid.NewGuid();
        }

        public bool ContinueExistingSession()
        {
            bool success = true;
            Global.Log.Info("SessionHasTimedOut:" + SessionHasTimedOut.ToString());
            Global.Log.Info("Session.SessionID:" + Session.SessionID);
            if (SessionHasTimedOut)
            {
                success = false;
            }

            if (!this.GetCookieSessionId().Equals(Session.SessionID))
            {
                success = false;
            }

            if (!success)
            {
                Abandon();
            }

            SetOrUpdateSessionTimeout();

            return success;
        }

        private string GetCookieSessionId()
        {
            string sessionId = string.Empty;

            HttpCookie sessionIdCookie = Request.Cookies[SessionCookieName];

            if (sessionIdCookie != null)
            {
                Bridgepay.Core.Cryptography.SimpleAES crypto = new Bridgepay.Core.Cryptography.SimpleAES();

                try
                {
                    Global.Log.Info("SessionCookie:" + sessionIdCookie.Value);
                    sessionId = crypto.DecryptString(sessionIdCookie.Value);
                }
                catch (Exception ex)
                {
                    sessionId = string.Empty;
                    Global.Log.Error("There was an error decrypting the session id cookie.", ex);
                }
            }

            return sessionId;
        }

        private void SetCookieSessionId(string sessionId)
        {
            HttpCookie sessionCookie = new HttpCookie(SessionCookieName);
            Bridgepay.Core.Cryptography.SimpleAES crypto = new Bridgepay.Core.Cryptography.SimpleAES();

            sessionCookie.Value = crypto.EncryptToString(sessionId);
            sessionCookie.SameSite = SameSiteMode.None;
            sessionCookie.Secure = true;
            Response.SetCookie(sessionCookie);
        }

        private void SetOrUpdateSessionTimeout()
        {
            Session[SessionExpirationKeyName] = DateTime.Now.AddMinutes(Session.Timeout);
        }

        public DateTime GetSessionExpiration()
        {
            DateTime timeout = DateTime.MinValue;

            if (Session[SessionExpirationKeyName] != null)
            {
                timeout = (DateTime)Session[SessionExpirationKeyName];
            }

            return timeout;
        }

    }
}