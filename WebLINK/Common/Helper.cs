﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Specialized;
using System.Text;
using System.Security.Cryptography;
using System.Net;
using System.IO;
using Microsoft.Win32;
using System.Text.RegularExpressions;
using WebLINK.Models;
using Newtonsoft.Json;
using Bridgepay.WebLink.Models;
using Bridgepay.Weblink.Core.Models;

namespace WebLINK.Common
{
    public static class Helper
    {
        public static string GetPostData(NameValueCollection form)
        {
            StringBuilder sb = new StringBuilder();
            string conn = string.Empty;

            form = new NameValueCollection(form);

            foreach (string key in form.Keys)
            {
                string value = form[key];
                sb.AppendFormat("{2}{0}={1}", key, value, conn);
                conn = "&";
            }

            return (sb.ToString());
        }

        public static string GetTransHash(TransactionQueue transactionQueue)
        {
            SimpleHash simpleHash = new SimpleHash(SimpleHash.HashAlgorithms.SHA512, Global.CryptoSecret);
            return simpleHash.ComputeHash(GetSimpleTransHash(transactionQueue));
        }

        public static string EncryptTransaction(TransactionQueue transactionQueue)
        {
            return Crypto.EncryptStringAES(GetSimpleTransHash(transactionQueue), Global.CryptoSecret);
        }

        public static string DecryptTransaction(string hash)
        {
            return Crypto.DecryptStringAES(hash, Global.CryptoSecret);
        }

        public static bool IsTransactionHashValid(string originalHash, string currentHash)
        {
            NameValueCollection originalData = originalHash.ToNameValueCollection();
            NameValueCollection currentData = currentHash.ToNameValueCollection();

            foreach (string key in originalData)
            {
                if (!originalData[key].Equals(currentData[key], StringComparison.CurrentCultureIgnoreCase))
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Returns a simple hash of the fields of all transactions that would likely be tampered with.
        /// </summary>
        /// <param name="transactionQueue"></param>
        /// <returns></returns>
        public static string GetSimpleTransHash(TransactionQueue transactionQueue)
        {
            // Grab all of our amounts and username/passwords and the mid. Those probably cover the most likely fields to be tampered with.
            NameValueCollection data = transactionQueue.Data;
            NameValueCollection tempCollection = new NameValueCollection();

            tempCollection.Add(Fields.MerchantAccountCode, data[Fields.MerchantAccountCode]);

            foreach (string key in data.Keys)
            {
                if (key.StartsWithAny(new List<string>
                {
                    Fields.TotalAmt,
                    Fields.Login,
                    Fields.PasswordEncrypted,
                    Fields.Password
                }) && !string.IsNullOrEmpty(data[key]))
                    tempCollection.Add(key, data[key]);
            }

            return tempCollection.ToNameValuePairString();
        }
        public static string GetLastFour(NameValueCollection post)
        {
            string lastFour = post[Fields.CardNumber];
            if (!string.IsNullOrEmpty(lastFour))
            {
                if (lastFour.Length > 4)
                    lastFour = lastFour.Substring(lastFour.Length - 4);
            }
            return lastFour;
        }

        public static string GetExpDate(NameValueCollection post)
        {
            string expDate = post[Fields.ExpDate];

            if (string.IsNullOrEmpty(expDate))
            {
                expDate = post[Fields.CardExpMonth];
                string expYear = post[Fields.CardExpYear] + string.Empty;
                if (expYear.Length == 4)
                    expDate += expYear.Substring(2, 2);
                else if (expYear.Length == 2)
                    expDate += expYear;
            }

            return (expDate);
        }

        /// <summary>
        /// Similar to bool.TryParse except that this treats "1", "on", and "T" as boolean true.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public static bool TryParseBoolEX(string value, out bool result)
        {
            bool rv = false;

            if (!string.IsNullOrEmpty(value))
            {
                switch (value.ToLower())
                {
                    case "1":
                    case "on":
                    case "t":
                        value = "True";
                        break;
                }
            }

            rv = bool.TryParse(value, out result);

            return (rv);
        }

        public static string GetXmlNodeValue(string xml, string tag)
        {
            string rv = string.Empty;

            if (tag.StartsWith("<"))
                tag = tag.TrimStart(new char[] { '<' });

            if (tag.EndsWith(">"))
                tag = tag.TrimEnd(new char[] { '>' });

            int start = xml.IndexOf("<" + tag + ">");
            int end = xml.IndexOf("</" + tag + ">");

            if (start != -1 && end != -1)
                rv = xml.Substring(start + tag.Length + 2, (end - (start + tag.Length + 2)));

            return (rv);
        }

        public static string PostData(string postData, string url)
        {
            string contentType = string.Empty;
            return PostData(postData, url, out contentType);
        }

        public static string PostData(string postData, string url, out string contentType)
        {
            string rv = string.Empty;

            byte[] bytes = Encoding.Default.GetBytes(postData);
            WebRequest request = WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = bytes.Length;
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(bytes, 0, bytes.Length);
            dataStream.Close();

            WebResponse response = null;
            try
            {
                response = request.GetResponse();
            }
            catch (WebException we)
            {
                response = we.Response;
            }
            catch (Exception) { throw; }
            dataStream = response.GetResponseStream();
            StreamReader sr = new StreamReader(dataStream);

            rv = sr.ReadToEnd();
            sr.Close();
            dataStream.Close();

            contentType = response.ContentType;

            response.Close();

            Global.Log.InfoFormat("PostData URL: {0} - Content Type: {1} - Response: {2}", Bridgepay.Core.Logging.EventId.Default, url, contentType, rv);

            return (rv);
        }

        public static string GETData(NameValueCollection nvc, string url)
        {
            string rv = string.Empty;
            url += "?" + nvc.ToNameValuePairString();

            WebRequest request = WebRequest.Create(url);
            request.Method = "GET";
            Stream dataStream = null;
            WebResponse response = null;

            try
            {
                response = request.GetResponse();
            }
            catch (WebException ex)
            {
                response = ex.Response;
            }

            dataStream = response.GetResponseStream();
            StreamReader sr = new StreamReader(dataStream);

            rv = sr.ReadToEnd();
            sr.Close();
            dataStream.Close();
            response.Close();

            return (rv);
        }

        public static string Base64Encode(string value)
        {
            if (string.IsNullOrEmpty(value))
                return null;
            else
                return Convert.ToBase64String(Encoding.Default.GetBytes(value));
        }

        public static string Base64Decode(string value)
        {
            if (string.IsNullOrEmpty(value))
                return null;
            else
                return Encoding.Default.GetString(Convert.FromBase64String(value));
        }

        /// <summary>
        /// Merges the passed in collection to the collection and returns a new resultant collection. Will not overwrite
        /// null values in the original collection
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        public static void Merge(NameValueCollection first, NameValueCollection second, bool overwriteNulls = false)
        {
            for (int i = 0; i < second.Count; i++)
            {
                if (string.IsNullOrEmpty(first[second.GetKey(i)]) || overwriteNulls)
                    first[second.GetKey(i)] = second.Get(i);
            }
        }

        public static NameValueCollection AppendSuffixIndexToResultFields(NameValueCollection col, int index)
        {
            NameValueCollection newCol = new NameValueCollection();

            foreach (string key in col.Keys)
            {
                string newKey = key;
                if (key.StartsWith("r_"))
                    newKey = key + index.ToString();
                newCol.Add(newKey, col[key]);
            }

            return newCol;
        }

        public static string GetRegistryValue(string subkey, string name)
        {
            RegistryKey key = null;
            string v = null;
            try
            {
                key = Registry.LocalMachine;
                key = key.OpenSubKey(subkey);
                v = (string)key.GetValue(name);
            }
            catch { }
            finally
            {
                if (key != null) try { key.Close(); }
                    catch { }
            }
            return v;
        }

        public static string EncryptPassword(string password)
        {
            return Crypto.EncryptStringAES(password, Global.CryptoSecret);
        }

        public static string DecryptPassword(string encryptedPassword)
        {
            return Crypto.DecryptStringAES(encryptedPassword, Global.CryptoSecret);
        }

        public static string ToBridgeCommAmount(string amount)
        {
            decimal amt = 0;

            decimal.TryParse(amount, out amt);

            return amt.ToString("#.00").Replace(".", string.Empty);
        }

        public static string FromBridgeCommAmount(int amount)
        {
            return (amount / 100d).ToString("#.00");
        }

        public static void DumpToFile(NameValueCollection data)
        {
            StreamWriter sw = new StreamWriter(@"c:\temp\data.txt", true);

            foreach (string key in data.Keys)
            {
                sw.WriteLine(string.Format("{0} = {1}", key, data[key]));
            }

            sw.Close();
        }

        public static string RemoveWalletFunctionality(string templateHtml)
        {
            Match startCommentBlockMatch = Regex.Match(templateHtml, @"<!--\s*(\w+)\s*-->");
            Match endCommentBlockMatch;
            if (startCommentBlockMatch.Success &&
                (endCommentBlockMatch = Regex.Match(templateHtml, string.Format(@"<!--\s*/{0}\s*-->", startCommentBlockMatch.Groups[1].Value))).Success)
            {
                while (startCommentBlockMatch.Success && endCommentBlockMatch.Success)
                {
                    int startTagIndex = startCommentBlockMatch.Index;
                    int endTagIndex = endCommentBlockMatch.Index;
                    templateHtml = templateHtml.Remove(startTagIndex, endTagIndex - startTagIndex + endCommentBlockMatch.Length);
                    startCommentBlockMatch = Regex.Match(templateHtml, @"<!--\s*(\w+)\s*-->");
                    endCommentBlockMatch = Regex.Match(templateHtml, string.Format(@"<!--\s*/{0}\s*-->", startCommentBlockMatch.Groups[1].Value));
                }
            }
            return templateHtml;
        }

        public static string GetClientIP()
        {
            //string ipList = HttpContext.Current.Request.ServerVariables["HTTP_X_CLUSTER_CLIENT_IP"];

            string ipList = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipList))
            {
                return ipList.Split(',')[0];
            }

            return HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
        }

        public static string GetMSRSettingsFromPostData(NameValueCollection postData)
        {
            if (!string.IsNullOrEmpty(postData[Fields.MSRDevice]))
            {
                return string.Format(General.MSRConfigJsonFormat,
                    postData[Fields.MSRDevice],
                    postData[Fields.MSRComPort],
                    postData[Fields.MSRBaudRate],
                    postData[Fields.MSRDataBits],
                    postData[Fields.MSRStopBits],
                    postData[Fields.MSRParity],
                    string.Empty,
                    string.Empty);
            }
            else
            {
                return null;
            }
        }

        public static string GetMSRSettingsFromCookie()
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies[General.MSRCookieName];

            if (cookie != null)
            {
                return cookie.Value;
            }
            else
            {
                return null;
            }
        }

        public static string RemoveNonDigits(string requestMessage)
        {
            if (requestMessage == null) return string.Empty;
            return new string(requestMessage.Where(c => char.IsDigit(c)).ToArray());
        }

    }
}