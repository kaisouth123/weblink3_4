﻿using System.Collections.Generic;
using System.Text;
using WebLINK.Templating;
using System.ServiceModel;

namespace WebLINK.Common.Extensions
{
    public static class WL3Extensions
    {
        public static TemplateField GetTemplateField(this List<TemplateField> fields, string field)
        {
            return (fields.Find(fld => fld.Name.ToLower() == field.ToLower()));
        }

        public static string GetSummary(this FaultException<BPNWalletService.WalletFault> ex)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(ex.GetType().Name);
            sb.AppendLine(ex.Detail.Message);
            sb.AppendLine(ex.Detail.StackTrace);
            if (ex.Detail.ValidationErrors != null)
            {
                sb.AppendLine("Validations errors:");
                foreach (var err in ex.Detail.ValidationErrors)
                {
                    sb.AppendFormat("{0} : {1}\r\n", err.PropertyName, err.ErrorMessage);
                }
            }

            foreach (var key in ex.Data)
            {
                sb.AppendFormat("{0} : {1}\r\n", key, ex.Data[key]);
            }

            return sb.ToString();
            
        }

    }
}