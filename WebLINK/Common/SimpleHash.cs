﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace WebLINK.Common
{
    public class SimpleHash
    {
        public enum HashAlgorithms
        {
            SHA1,
            SHA256,
            SHA384,
            SHA512,
            MD5
        }

        private HashAlgorithms Algorithm;
        private byte[] saltBytes;

        public SimpleHash(HashAlgorithms Algorithm, string Salt)
        {
            if (string.IsNullOrEmpty(Salt))
                throw new ArgumentException("Salt cannot be blank!");

            this.Algorithm = Algorithm;
            saltBytes = Encoding.UTF8.GetBytes(Salt);
        }

        public string ComputeHash(string plainText)
        {
            // Convert plain text into a byte array.
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            // Allocate array, which will hold plain text and salt.
            byte[] plainTextWithSaltBytes =
                    new byte[plainTextBytes.Length + saltBytes.Length];

            // Copy plain text bytes into resulting array.
            for (int i = 0; i < plainTextBytes.Length; i++)
                plainTextWithSaltBytes[i] = plainTextBytes[i];

            // Append salt bytes to the resulting array.
            for (int i = 0; i < saltBytes.Length; i++)
                plainTextWithSaltBytes[plainTextBytes.Length + i] = saltBytes[i];

            // Because we support multiple hashing algorithms, we must define
            // hash object as a common (abstract) base class. We will specify the
            // actual hashing algorithm class later during object creation.
            HashAlgorithm hash;

            // Make sure hashing algorithm name is specified.
            //if (Algorithm == null)
            //    Algorithm = "";

            // Initialize appropriate hashing algorithm class.
            switch (Algorithm)
            {
                case HashAlgorithms.SHA1:
                    hash = new SHA1Managed();
                    break;

                case HashAlgorithms.SHA256:
                    hash = new SHA256Managed();
                    break;

                case HashAlgorithms.SHA384:
                    hash = new SHA384Managed();
                    break;

                case HashAlgorithms.SHA512:
                    hash = new SHA512Managed();
                    break;

                default:
                    hash = new MD5CryptoServiceProvider();
                    break;
            }

            // Compute hash value of our plain text with appended salt.
            byte[] hashBytes = hash.ComputeHash(plainTextWithSaltBytes);

            // Create array which will hold hash and original salt bytes.
            byte[] hashWithSaltBytes = new byte[hashBytes.Length +
                                                saltBytes.Length];

            // Copy hash bytes into resulting array.
            for (int i = 0; i < hashBytes.Length; i++)
                hashWithSaltBytes[i] = hashBytes[i];

            // Append salt bytes to the result.
            for (int i = 0; i < saltBytes.Length; i++)
                hashWithSaltBytes[hashBytes.Length + i] = saltBytes[i];

            // Convert result into a base64-encoded string.
            string hashValue = Convert.ToBase64String(hashWithSaltBytes);

            // Return the result.
            return hashValue;
        }
    }
}