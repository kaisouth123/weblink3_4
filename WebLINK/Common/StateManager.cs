﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Web;
using Bridgepay.Security.Authentication;
using Bridgepay.Weblink.Core.Models;

namespace WebLINK.Common
{

    public enum Modes
    {
        PaymentForm,
        TokenizationForm
    }

    public enum SwipeMethod
    {
        none,
        java,
        pgweb
    }

    /// <summary>
    /// Normalizes the state of Weblink into a simple object
    /// </summary>
    public class StateManager
    {
        private NameValueCollection data;
        private TransactionQueue transactionQueue;
        private SessionManager sessionManager;

        private Modes mode;
        public Modes Mode
        {
            get { return mode; }
        }

        private SwipeMethod swipeMethod;
        public SwipeMethod SwipeMethod
        {
            get { return swipeMethod; }
            set { swipeMethod = value; }
        }

        WebLINK.PaymentServer.PaymentServerFactory.PaymentServerTypes paymentServerType;
        public WebLINK.PaymentServer.PaymentServerFactory.PaymentServerTypes PaymentServerType
        {
            get { return paymentServerType; }
            set { paymentServerType = value; }
        }

        public StateManager(SessionManager sessionManager)
        {
            this.sessionManager = sessionManager;
            this.data = sessionManager.Data;
            transactionQueue = new TransactionQueue(this.data);

            string strMode = data[Fields.Mode] + string.Empty;

            if (string.IsNullOrEmpty(strMode) || strMode.Equals("debugdcp", StringComparison.CurrentCultureIgnoreCase)) // No longer officially supported, just here for backwards compatability.
            {
                isDCP = true;
            }
            else
            {
                Enum.TryParse(strMode, out mode);
            }

            // Determine payment server type
            string strPaymentServerType = ConfigurationManager.AppSettings["PaymentServerType"];
            if (!string.IsNullOrEmpty(data[Fields.PaymentServerType]))
                strPaymentServerType = data[Fields.PaymentServerType];

            // Note! Enum.TryParse will always succeed for integer based enums when parsing any integer! Hence, the use of IsDefined now.
            if (string.IsNullOrEmpty(strPaymentServerType) || !Enum.IsDefined(typeof(WebLINK.PaymentServer.PaymentServerFactory.PaymentServerTypes), strPaymentServerType))
            {
                paymentServerType = PaymentServer.PaymentServerFactory.PaymentServerTypes.BPN;
            } else
            {
                paymentServerType = (WebLINK.PaymentServer.PaymentServerFactory.PaymentServerTypes)
                    Enum.Parse(typeof(WebLINK.PaymentServer.PaymentServerFactory.PaymentServerTypes), strPaymentServerType);
            }

            // Determine swipe method
            string strSwipeMethod = data[Fields.SwipeMethod];
            if (!string.IsNullOrEmpty(strSwipeMethod))
            {
                if (!Enum.TryParse<Common.SwipeMethod>(strSwipeMethod.ToLower(), out swipeMethod))
                {
                    SwipeMethod = Common.SwipeMethod.none;
                }
            }
            else
            {
                swipeMethod = Common.SwipeMethod.none;
            }
        }

        /// <summary>
        /// True if we are posting back from the Payment form or other form such as the tokenization form, etc
        /// </summary>
        public bool IsPostBack
        {
            get
            {
                return HttpContext.Current.Request.RequestType.Equals("POST", StringComparison.CurrentCultureIgnoreCase) &&
                    !string.IsNullOrEmpty(HttpContext.Current.Request.Form[Fields.IsPostBack]);
            }
        }

        /// <summary>
        /// Returns true if no amounts were passed in
        /// </summary>
        public bool IsDonationMode
        {
            get
            {
                foreach (string key in data.Keys)
                {
                    if (key.StartsWith(Fields.TotalAmt) && !string.IsNullOrEmpty(data[key]))
                        return false;
                }

                return true;
            }
        }

        private bool isDCP = false;
        public bool IsDCP
        {
            get { return isDCP; }
        }

        public bool IsUsingPreferredPaymentMethod
        {
            get 
            {
                // If we are in DCP mode and wallet data was provided, we assume they want to use it!
                bool rv = !string.IsNullOrEmpty(data[Fields.UsePreferedPaymentMethod]) || 
                    (isDCP && 
                    transactionQueue.WalletData.IsValid() &&
                    transactionQueue.WalletData.WalletId != null &&
                    transactionQueue.WalletData.SiteId != null);

                return rv;
            }
        }

        public bool IsCreatingWallet
        {
            get
            {
                bool dcpModeCreation = isDCP &&
                    !string.IsNullOrEmpty(data[Fields.SiteId]) &&
                    !string.IsNullOrEmpty(data[Fields.EnableWallet]);

                bool nonDcpModeCreation = !isDCP &&
                    !string.IsNullOrEmpty(data[Fields.SiteId]) &&
                    !string.IsNullOrEmpty(data[Fields.EnableWallet]) &&
                    Mode == Modes.TokenizationForm;

                return dcpModeCreation || nonDcpModeCreation;
            }
        }

        // True if we have all the data required to allow a customer to save their preferred payment method
        public bool AllowWalletSave
        {
            get
            {
                // Enable wallet must be selected and either SiteId supplied or both WalletToken and WalletKey
                bool allowWalletSave = false;

                allowWalletSave = !string.IsNullOrEmpty(sessionManager.InitialPostData[Fields.EnableWallet]);

                allowWalletSave &= !string.IsNullOrEmpty(sessionManager.InitialPostData[Fields.SiteId]) ||
                    (!string.IsNullOrEmpty(sessionManager.InitialPostData[Fields.WalletToken]) &&
                    !string.IsNullOrEmpty(sessionManager.InitialPostData[Fields.WalletKey]));

                return allowWalletSave;
            }
        }

        public bool IsSavingWalletInfo
        {
            get
            {
                return IsCreatingWallet ||
                    (!string.IsNullOrEmpty(data[Fields.EnableWallet]) &&
                    !string.IsNullOrEmpty(data[Fields.SavePreferredPaymentMethod]));
            }
        }

        public bool HasWalletError
        {
            get
            {
                string walletResultCode = data[Fields.r_WalletResultCode] + string.Empty;

                return !(walletResultCode.Equals(string.Empty) ||
                    walletResultCode.Equals(Global.WeblinkSuccessCode.ToString()));
            }
        }

        public bool IsCardTransaction
        {
            get
            {
                string cardType = data[Fields.CardTypeName];
                if (string.IsNullOrEmpty(cardType))
                    cardType = data[Fields.WalletCardType];

                if (!string.IsNullOrEmpty(cardType) &&
                    !cardType.Equals("echeck", StringComparison.CurrentCultureIgnoreCase))
                {
                    return true;
                }

                if (!string.IsNullOrEmpty(data[Fields.SecureFormat]) &&
                    !string.IsNullOrEmpty(data[Fields.SecurityInfo]))
                {
                    return true;
                }

                if (!string.IsNullOrEmpty(data[Fields.Token]))
                {
                    return true;
                }

                return false;
            }
        }

        public bool IsSecureTransaction
        {
            get
            {
                bool isSlTrxn = false;

                if (!string.IsNullOrEmpty(data[Fields.SecurityInfo]) &&
                    !string.IsNullOrEmpty(data[Fields.EncryptedTrack1]))
                {
                    isSlTrxn = true;
                }

                return isSlTrxn;
            }
        }

        public bool IsCreatingTokenOnly
        {
            get
            {
                string token = data[Fields.Token] + string.Empty;

                return mode == Modes.TokenizationForm ||
                    (token.Equals("create", StringComparison.CurrentCultureIgnoreCase)
                    && string.IsNullOrEmpty(data[Fields.TotalAmt]));
            }
        }

        /// <summary>
        /// Returns true if a token is being used to process this transaction rather than a PAN
        /// </summary>
        public bool IsPlainTokenizedTransaction
        {
            get
            {
                string token = data[Fields.Token] + string.Empty;

                bool isWalletDataPresent = !string.IsNullOrEmpty(data[Fields.EnableWallet]);

                return !string.IsNullOrEmpty(token) &&
                    !token.Equals("create", StringComparison.CurrentCultureIgnoreCase) &&
                    !isWalletDataPresent;
            }
        }

        public bool RespondWithLastFailureOnly
        {
            get
            {
                return !string.IsNullOrEmpty(data[Fields.RespondWithLastFailureOnly]);
            }
        }

        public bool IsUsingServerSidePostBacks
        {
            get
            {
                return !string.IsNullOrEmpty(sessionManager.InitialPostData[Fields.UseServerSidePostBacks]);
            }
        }
    }
}