﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Util;
using Microsoft.Security.Application;

namespace WebLINK.Common
{
    public class AntiXssEncoder : HttpEncoder
    {
        protected override void HtmlEncode(string value, System.IO.TextWriter output)
        {
            var encoded = Microsoft.Security.Application.Encoder.HtmlEncode(value);

            encoded = encoded.Replace("javascript", "");
            output.Write(encoded);
        }

        protected override void HtmlAttributeEncode(string value, System.IO.TextWriter output)
        {
            output.Write(Microsoft.Security.Application.Encoder.HtmlAttributeEncode(value));
        }

        protected override string UrlPathEncode(string value)
        {
            return Microsoft.Security.Application.Encoder.UrlPathEncode(value);
        }

        protected override byte[] UrlEncode(byte[] bytes, int offset, int count)
        {
            var toEncode = Encoding.Default.GetString(bytes);
            var encoded = Microsoft.Security.Application.Encoder.UrlEncode(toEncode);

            return Encoding.Default.GetBytes(encoded);
        }

        protected override void HtmlDecode(string value, System.IO.TextWriter output)
        {
            base.HtmlDecode(value, output);
        }
    }
}