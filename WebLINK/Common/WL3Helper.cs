﻿using System.Linq;
using System.Web;
using System.Collections.Specialized;
using System.Text.RegularExpressions;

namespace WebLINK.Common.Helpers
{
    public static class WL3Helper
    {
        public static string RemoveWalletFunctionality(string templateHtml)
        {
            Match startCommentBlockMatch = Regex.Match(templateHtml, @"<!--\s*(\w+)\s*-->");
            Match endCommentBlockMatch;
            if (startCommentBlockMatch.Success &&
                (endCommentBlockMatch = Regex.Match(templateHtml, string.Format(@"<!--\s*/{0}\s*-->", startCommentBlockMatch.Groups[1].Value))).Success)
            {
                while (startCommentBlockMatch.Success && endCommentBlockMatch.Success)
                {
                    int startTagIndex = startCommentBlockMatch.Index;
                    int endTagIndex = endCommentBlockMatch.Index;
                    templateHtml = templateHtml.Remove(startTagIndex, endTagIndex - startTagIndex + endCommentBlockMatch.Length);
                    startCommentBlockMatch = Regex.Match(templateHtml, @"<!--\s*(\w+)\s*-->");
                    endCommentBlockMatch = Regex.Match(templateHtml, string.Format(@"<!--\s*/{0}\s*-->", startCommentBlockMatch.Groups[1].Value));
                }
            }
            return templateHtml;
        }

        public static string GetClientIP()
        {
            //string ipList = HttpContext.Current.Request.ServerVariables["HTTP_X_CLUSTER_CLIENT_IP"];

            string ipList = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipList))
            {
                return ipList.Split(',')[0];
            }

            return HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
        }

        public static string GetMSRSettingsFromPostData(NameValueCollection postData)
        {
            if (!string.IsNullOrEmpty(postData[Fields.MSRDevice]))
            {
                return string.Format(General.MSRConfigJsonFormat,
                    postData[Fields.MSRDevice],
                    postData[Fields.MSRComPort],
                    postData[Fields.MSRBaudRate],
                    postData[Fields.MSRDataBits],
                    postData[Fields.MSRStopBits],
                    postData[Fields.MSRParity],
                    string.Empty,
                    string.Empty);
            }
            else
            {
                return null;
            }
        }

        public static string GetMSRSettingsFromCookie()
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies[General.MSRCookieName];

            if (cookie != null)
            {
                return cookie.Value;
            }
            else
            {
                return null;
            }
        }

        public static string RemoveNonDigits(string requestMessage)
        {
            if (requestMessage == null) return string.Empty;
            return new string(requestMessage.Where(c => char.IsDigit(c)).ToArray());
        }

    }
}