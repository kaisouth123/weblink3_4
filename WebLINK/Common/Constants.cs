﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebLINK.Common
{
    public static class Fields
    {
        public const string Login = "Login";
        public const string Password = "Password";
        public const string PasswordEncrypted = "PasswordEncrypted";
        public const string PurchaseToken = "PurchaseToken";

        public const string Mode = "Mode";
        public const string ReturnFields = "ReturnFields";
        public const string RequiredFields = "RequiredFields";
        public const string Email = "BillToEmail";
        public const string EmailFrom = "EmailFrom";
        public const string MerchantEmail = "MerchantEmail";
        public const string ReceiptUrl = "ReceiptUrl";
        public const string ReturnReceiptUrl = "ReturnReceiptUrl";
        public const string CompleteUrl = "CompleteUrl";
        public const string FailureUrl = "FailUrl";
        public const string CancelUrl = "CancelUrl";
        public const string ReturnUrl = "ReturnUrl";
        public const string PaymentFormTID = "PaymentFormTID";
        public const string ReceiptTID = "ReceiptTID";
        public const string CustomerEmailTID = "CustomerEmailTID";
        public const string MerchantEmailTID = "MerchantEmailTID";
        public const string TotalAmt = "TotalAmt";
        public const string Description = "Description";
        public const string MerchantName = "MerchantName";
        public const string CardNumber = "CardNumber";
        public const string CardExpMonth = "CardExpMonth";
        public const string CardExpYear = "CardExpYear";
        public const string CardTypeName = "CardTypeName";
        public const string ExpDate = "ExpDate";
        public const string CVNum = "CVNum";
        public const string InvoiceNum = "InvoiceNum";
        public const string PONum = "PONum";
        public const string FullName = "BillToName";
        public const string Address1 = "BillToStreet";
        public const string Address2 = "BillToStreet2";
        public const string City = "BillToCity";
        public const string State = "BillToState";
        public const string ZipCode = "BillToZip";
        public const string Phone = "BillToPhone";
        public const string TransitNumber = "TransitNum";
        public const string AccountNumber = "AccountNum";
        public const string CheckNumber = "CheckNum";
        public const string CheckType = "CheckType";
        public const string HolderType = "HolderType";
        public const string TransType = "TransType";
        public const string AccountType = "AcctType";
        public const string MICR = "MICR";
        public const string DL = "DL";
        public const string SS = "SS";
        public const string DOB = "DOB";
        public const string StateCode = "StateCode";
        public const string TransactionSequenceNum = "TransSequenceNo";
        public const string GrandTotal = "GrandTotal"; // For split transactions. Receipt page only.
        public const string MagData = "MagData";
        public const string SwipeMethod = "SwipeMethod";
        public const string UserDefinedTransactionId = "UserDefinedTransactionId";
        public const string RespondWithLastFailureOnly = "RespondWithLastFailureOnly";
        // Some customers have the ReceiptUrl and/or FailUrl posting back to internal sites. Cant do that with server
        // side posting. So we now have this as an opt-in function.
        public const string UseServerSidePostBacks = "UseServerSidePostBacks";

        // SecureLink fields
        public const string EncryptedTrack1 = "EncryptedTrack1";
        public const string EncryptedTrack2 = "EncryptedTrack2"; // optional
        public const string SecurityInfo = "SecurityInfo";
        public const string SecureFormat = "SecureFormat";

        public const string Style_Page_BackgroundImage = "Style_Page_BackgroundImage";
        public const string Style_Page_BackgroundColor = "Style_Page_BackgroundColor";
        public const string Style_Form_BackgroundColor = "Style_Form_BackgroundColor";
        public const string Style_FontFamily = "Style_FontFamily";
        public const string Style_Heading_FontColor = "Style_Heading_FontColor";
        public const string Style_Heading_FontSize = "Style_Heading_FontSize";
        public const string Style_Heading_BackgroundColor = "Style_Heading_BackgroundColor";
        public const string Style_Instruction_FontColor = "Style_Instruction_FontColor";
        public const string Style_Instruction_FontSize = "Style_Instruction_FontSize";
        public const string Style_Label_FontColor = "Style_Label_FontColor";
        public const string Style_Label_FontSize = "Style_Label_FontSize";
        public const string Style_Field_FontColor = "Style_Field_FontColor";
        public const string Style_Field_FontSize = "Style_Field_FontSize";
        public const string ReceiptLogoUrl = "ReceiptLogoUrl";

        public const string TenderType = "TenderType";
        public const string RedirectUrl = "RedirectUrl";
        
        public const string TransDate = "TransDate";
        public const string Token = "Token";

        // Wallet fields
        public const string WalletToken = "WalletToken";
        public const string SiteId = "SiteId";
        public const string SavePreferredPaymentMethod = "SavePreferredPaymentMethod";
        public const string UsePreferedPaymentMethod = "UsePreferedPaymentMethod";
        public const string EnableWallet = "EnableWallet";
        public const string WalletCardType = "tg_WalletCardType";
        public const string WalletCardNumber = "tg_WalletCardNumber";
        public const string WalletExpDate = "tg_WalletExpDate";
        public const string WalletKey = "WalletKey";
        public const string r_WalletResultCode = "r_WalletResult";
        public const string r_WalletMessage = "r_WalletMessage";
        public const string r_WalletToken = "r_WalletToken";
        public const string r_WalletKey = "r_WalletKey";

        //BridgeComm fields
        public const string PaymentAccountNumber = "PaymentAccountNumber";
        public const string SecurityCode = "SecurityCode";

        // System fields
        public const string IsPostBack = "tg_IsPostBack";
        public const string WLID = "tg_WLID";
        public const string TransactionHash = "tg_TransHash";
        public const string MerchantCode = "tg_MerchantCode";
        public const string MerchantAccountCode = "tg_MerchantAccountCode";
        public const string PaymentTypes = "PaymentTypes";
        public const string PaymentServerType = "PaymentServerType";
        public const string SessionTimeout = "SessionTimeout";

        // Misc Bridgecomm specific fields
        public const string PaymentType = "PaymentType";
        public const string CustomerAccountCode = "CustomerAccountCode";
        public const string TransactionCategoryCode = "TransCatCode";
        public const string TransactionIndustryType = "TransIndustryType";

        // MSR fields
        public const string MSRComPort = "MSRComPort";
        public const string MSRBaudRate = "MSRBaudRate";
        public const string MSRDataBits = "MSRDataBits";
        public const string MSRStopBits = "MSRStopBits";
        public const string MSRParity = "MSRParity";
        public const string MSRDevice = "MSRDevice";
        public const string AutoInitMSR = "AutoInitMSR";

        // Result fields
        public const string r_Result = "r_Result";
        public const string r_Message = "r_Message";
        public const string r_MessageEx = "r_MessageEx";
        public const string r_Token = "r_Token";
        public const string r_TokenKey = "r_TokenKey";
        public const string r_ExpDate = "r_ExpDate";
        public const string r_NameOnCard = "r_NameOnCard";
        public const string r_Street = "r_Street";
        public const string r_Zip = "r_Zip";
        public const string r_AuthCode = "r_AuthCode";
        public const string r_HostCode = "r_HostCode";
        public const string r_Id = "r_Id";
        public const string r_ExtData = "r_ExtData";
        public const string r_CardType = "r_CardType";
        public const string r_Errors = "r_Errors";
        public const string r_LastFour = "r_LastFour";
        public const string r_AuthorizedAmount = "r_AuthorizedAmount";

        // Result fields of a system void
        public const string rv_Result = "rv_Result";
        public const string rv_Message = "rv_Message";
        public const string rv_AuthCode = "rv_AuthCode";
        public const string rv_HostCode = "rv_HostCode";
        public const string rv_Id = "rv_Id";

        // PGWeb result fields
        public const string pgweb_ResultText = "pgweb_resultTxt";
        public const string pgweb_ResultCode = "pgweb_resultCode";
        public const string pgweb_Message = "pgweb_message";
        public const string pgweb_RefNum = "pgweb_refNum";
        public const string pgweb_AuthCode = "pgweb_authCode";
        public const string pgweb_AvsResponse = "pgweb_avsResponse";
        public const string pgweb_CvResponse = "pgweb_cvResponse";
        public const string pgweb_TimeStamp = "pgweb_timeStamp";
        public const string pgweb_SubmittedAmount = "pgweb_submittedAmount";
        public const string pgweb_ApprovedAmount = "pgweb_approvedAmount";
        public const string pgweb_BogusAccountNumber = "pgweb_bogusAccountNumber";
        public const string pgweb_CardType = "pgweb_cardType";
        public const string pgweb_IsCommercialCard = "pgweb_isCommercialCard";
        public const string pgweb_ExpirationDate = "pgweb_expirationDate";
        public const string pgweb_GatewayMessage = "pgweb_gatewayMessage";
        public const string pgweb_InternalMessage = "pgweb_internalMessage";
        public const string pgweb_RemainingAmount = "pgweb_remainingAmount";
        public const string pgweb_ISOCountryCode = "pgweb_isoCountryCode";
        public const string pgweb_ISOTransactionDate = "pgweb_isoTransactionDate";
        public const string pgweb_ISORequestDate = "pgweb_isoRequestDate";
        public const string pgweb_NetworkReferenceNumber = "pgweb_networkReferenceNumber";
        public const string pgweb_MerchantCategoryCode = "pgweb_merchantCategoryCode";
        public const string pgweb_NetworkMerchantId = "pgweb_networkMerchantId";
        public const string pgweb_NetworkTerminalId = "pgweb_networkTerminalId";
        public const string pgweb_StreetMatchMessage = "pgweb_streetMatchMessage";
        public const string pgweb_AVSMessage = "pgweb_avsmessage";
        public const string pgweb_CVMessage = "pgweb_cvmessage";
        public const string pgweb_Token = "pgweb_token";
        
    }

    public static class BridgeCommTransTypes
    {
        public const string Sale = "sale";
        public const string SaleAuth = "sale-auth";
        public const string Credit = "credit";
    }

    public static class General
    {
        public const int DefaultSessionTimeoutMinutes = 10;
        public const int MinSessionTimeoutMinutes = 1;
        public const int MaxSessionTimeoutMinutes = 30;
        public const string MSRCookieName = "MSRConfig";
        public static readonly string MSRConfigJsonFormat;

        static General()
        {
            MSRConfigJsonFormat = "{{ \"device\": \"{0}\", \"comPortName\": \"{1}\", \"comBaudRate\": \"{2}\", \"comDataBits\": \"{3}\", \"comStopBits\": \"{4}\", \"comParity\": \"{5}\", \"usbVendorID\": \"{6}\", \"usbProductID\": \"{7}\" }}";
        }
    }
}