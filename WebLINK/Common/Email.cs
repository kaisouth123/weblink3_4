﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Threading;
using System.Web;

namespace WebLINK.Common
{
    public static class Email
    {
        public static void SendEmail(string to, string cc, string bcc, string from, string subject, string body, bool isBodyHtml)
        {
            MailMessage msg = new MailMessage();
            string[] To = new string[] { };
            string[] CC = new string[] { };
            string[] BCC = new string[] { };

            msg.IsBodyHtml = isBodyHtml;

            if (!string.IsNullOrEmpty(to))
                To = to.Split(new char[] { ',', ';' });

            if (!string.IsNullOrEmpty(cc))
                CC = cc.Split(new char[] { ',', ';' });

            if (!string.IsNullOrEmpty(bcc))
                BCC = bcc.Split(new char[] { ',', ';' });

            foreach (string address in To)
            {
                MailAddress ma = new MailAddress(address);
                msg.To.Add(ma);
            }

            foreach (string address in CC)
            {
                MailAddress ma = new MailAddress(address);
                msg.CC.Add(ma);
            }

            foreach (string address in BCC)
            {
                MailAddress ma = new MailAddress(address);
                msg.Bcc.Add(ma);
            }

            msg.From = new MailAddress(from);
            msg.Subject = subject;
            msg.Body = body;

            ThreadPool.QueueUserWorkItem(cb =>
                {
                    using (var client = new SmtpClient(GetSMTPServer()))
                    {
                        try
                        {
                            client.Send(msg);
                        }
                        catch (Exception ex)
                        {
                            Global.Log.Error("Error sending email: ", ex);
                        }
                    }
                });
        }

        private static string GetSMTPServer()
        {
            string smtpServer = null;
            smtpServer = ConfigurationManager.AppSettings["SMTPServer"];

            if (string.IsNullOrEmpty(smtpServer))
                throw new Exception("Unable to retrieve SMTP server address. Please make sure an " +
                    "appSetting named \"smtpServer\" exists in the application config file.");

            return smtpServer;
        }
    }
}