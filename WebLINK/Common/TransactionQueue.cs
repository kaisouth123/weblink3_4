﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Web;
using Bridgepay.Security.Authentication;
using Bridgepay.Weblink.Core.Models;

namespace WebLINK.Common
{
    /// <summary>
    /// Provides a nice wrapper for a set of transactions posted to weblink. Initially created to handle split transactions, but
    /// it works with a single transaction just as well.
    /// </summary>
    public class TransactionQueue
    {
        

        private List<TransactionData> transactions = null;
        public List<TransactionData> Transactions
        {
            get 
            {
                // Need to re-evaluate everytime since Data could be modified at anytime
                PopulateTransactions();
                return transactions; 
            }
        }

        public WalletData WalletData { 
            get
            {
                WalletData walletData = new WalletData();

                if (!string.IsNullOrEmpty(data[Fields.WalletToken]) &&
                !string.IsNullOrEmpty(data[Fields.WalletKey]))
                {
                    walletData = new WalletData(data[Fields.WalletToken]);
                    IBridgepayAuth walletAuth = GetBridgePayAuth(walletData.WalletId, walletData.SiteId);

                    if (!walletAuth.Validate())
                    {
                        walletData = new WalletData();
                    }
                }
                else
                {
                    Guid siteId;

                    if (Guid.TryParse(data[Fields.SiteId], out siteId))
                    {
                        walletData = new WalletData
                        {
                            SiteId = siteId
                        };
                    }
                }

                return walletData;
            }
        }

        private NameValueCollection data = null;
        public NameValueCollection Data
        {
            get { return data; }
            set { data = value; }
        }

        /// <summary>
        /// Returns the number of transactions that are present in the data set
        /// We only look at the TransSequenceNo fields and if theres a something there, we consider that a transaction
        /// </summary>
        public int TransactionCount
        {
            get
            {
                int rv = 0;

                foreach (string key in data.Keys)
                {
                    if (key.StartsWith(Fields.TransactionSequenceNum))
                        rv++;
                }

                return rv == 0 ? 1 : rv;
            }
        }

        public bool HasMultipleTransactions
        {
            get { return TransactionCount > 1; }
        }

        public bool HasErrors
        {
            get { return HasValidationErrors || HasFailedTransactions; }
        }

        public bool HasValidationErrors
        {
            get
            {
                foreach (string key in data.Keys)
                {
                    // If we have any validation errors, return true
                    if (key.StartsWith(Fields.r_Errors))
                    {
                        if (!string.IsNullOrEmpty(data[key]))
                            return true;
                    }
                }

                return false;
            }
        }

        public bool HasFailedTransactions
        {
            get
            {
                foreach (string key in data.Keys)
                {
                    // If we have any non approvals, return true
                    if (key.StartsWith(Fields.r_Result))
                    {
                        if (!string.IsNullOrEmpty(data[key]))
                        {
                            int resultCode;
                            int.TryParse(data[key], out resultCode);

                            if (resultCode != 0)
                                return true;
                        }
                    }
                }

                return false;
            }
        }

        public bool IsRepeatedField(string field)
        {
            // Safest way to do this
            bool rv = field.StartsWithAny(new List<string>
            {
                Fields.TransactionSequenceNum,
                Fields.TotalAmt,
                Fields.Description,
                Fields.PurchaseToken,
                Fields.Login,
                Fields.Password,
                Fields.PasswordEncrypted,
                Fields.MerchantName,
                Fields.MerchantAccountCode,
                Fields.MerchantCode,
                Fields.WLID,
                Fields.UserDefinedTransactionId,
                "r_" // Need to include result fields as well for receipts, etc
            });

            return rv;
        }

        public TransactionQueue(NameValueCollection data)
        {
            this.data = data;
        }

        /// <summary>
        /// Removes result fields from the data store. (r_XXXXXX) fields
        /// </summary>
        public void RemoveResultFields()
        {
            for (int i = data.Count - 1; i >= 0; i--)
            {
                string key = data.GetKey(i);
                if (key.StartsWith("r_"))
                    data.Remove(key);
            }
        }

        // Sets the wallet token and auth key
        public void SetWalletToken(Guid? WalletId, Guid? SiteId)
        {
            IBridgepayAuth authHandler = GetBridgePayAuth(WalletId, SiteId);

            data[Fields.r_WalletToken] = authHandler.GenerateAuthToken();
            data[Fields.r_WalletKey] = authHandler.GenerateAuthKey();
        }

        private void PopulateTransactions()
        {
            transactions = new List<TransactionData>();

            if (!HasMultipleTransactions)
            {
                TransactionData trans = new TransactionData(data, Global.CryptoSecret);
                trans.ActualIndex = 1;
                transactions.Add(trans);
            }
            else
            {
                for (int transactionSeqNo = 1; transactionSeqNo <= TransactionCount; transactionSeqNo++)
                {
                    NameValueCollection tempData = new NameValueCollection(data.Except(key => !IsRepeatedField(key)));
                    foreach (string key in data.Keys)
                    {
                        string value = data[key];

                        // If the current field is one we are expecting multiples of, grab the value of the correct one.
                        if (IsRepeatedField(key.TrimTrailingDigits()))
                        {
                            string baseKey = key.TrimTrailingDigits();
                            string actualKey = TransactionData.GetNormalizedFieldName(baseKey, transactionSeqNo);
                            if (key == actualKey)
                            {
                                value = data[actualKey];
                                tempData[baseKey] = value;
                            }
                        }
                    }

                    TransactionData transactionData = new TransactionData(tempData, Global.CryptoSecret);
                    transactionData.ActualIndex = transactionSeqNo;
                    transactions.Add(transactionData);
                }

                transactions.Sort(TransactionComparer);
            }
        }

        private WalletData GetWalletData(Guid? walletId, Guid? siteId)
        {
            return new WalletData
            {
                SiteId = siteId,
                WalletId = walletId
            };
        }

        private IBridgepayAuth GetBridgePayAuth(Guid? walletId, Guid? siteId)
        {
            return new WalletAuth(GetWalletData(walletId, siteId));
        }

        private int TransactionComparer(TransactionData transX, TransactionData transY)
        {
            int x = transX.TransactionSequenceNumber.HasValue ? transX.TransactionSequenceNumber.Value : 0;
            int y = transY.TransactionSequenceNumber.HasValue ? transY.TransactionSequenceNumber.Value : 0;

            return x - y;
        }
    }
}