﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using log4net;

namespace WebLINK.Common
{
    public class CleanLogger : ILog
    {
        private ILog _log = null;
        public CleanLogger(ILog log) {
            _log = log;
        }

        public void Debug(object message, Exception exception) 
        {
            _log.Debug(Helper.MaskPossiblySensitiveData((string)message), exception);
        }

        public void Debug(object message)
        {
            _log.Debug(Helper.MaskPossiblySensitiveData((string)message));
        }

        public void DebugFormat(IFormatProvider provider, string format, params object[] args)
        {
            _log.DebugFormat(provider, format, Helper.MaskPossiblySensitiveData(args));
        }

        public void DebugFormat(string format, object arg0, object arg1, object arg2)
        {
            _log.DebugFormat(format, Helper.MaskPossiblySensitiveData((string)arg0), Helper.MaskPossiblySensitiveData((string)arg1), Helper.MaskPossiblySensitiveData((string)arg2));
        }

        public void DebugFormat(string format, object arg0, object arg1)
        {
            _log.DebugFormat(format, Helper.MaskPossiblySensitiveData((string)arg0), Helper.MaskPossiblySensitiveData((string)arg1));
        }

        public void DebugFormat(string format, object arg0)
        {
            _log.DebugFormat(format, Helper.MaskPossiblySensitiveData((string)arg0));
        }

        public void DebugFormat(string format, params object[] args)
        {
            _log.DebugFormat(format, Helper.MaskPossiblySensitiveData(args));
        }

        public void Error(object message, Exception exception)
        {
            _log.Error(Helper.MaskPossiblySensitiveData(message), exception);
        }

        public void Error(object message)
        {
            _log.Error(Helper.MaskPossiblySensitiveData(message));
        }

        public void ErrorFormat(IFormatProvider provider, string format, params object[] args)
        {
            _log.ErrorFormat(provider, format, Helper.MaskPossiblySensitiveData(args));
        }

        public void ErrorFormat(string format, object arg0, object arg1, object arg2)
        {
            _log.ErrorFormat(format, Helper.MaskPossiblySensitiveData((string)arg0), Helper.MaskPossiblySensitiveData((string)arg1), Helper.MaskPossiblySensitiveData((string)arg2));
        }

        public void ErrorFormat(string format, object arg0, object arg1)
        {
            _log.ErrorFormat(format, Helper.MaskPossiblySensitiveData((string)arg0), Helper.MaskPossiblySensitiveData((string)arg1));
        }

        public void ErrorFormat(string format, object arg0)
        {
            _log.ErrorFormat(format, Helper.MaskPossiblySensitiveData((string)arg0));
        }

        public void ErrorFormat(string format, params object[] args)
        {
            _log.ErrorFormat(format, Helper.MaskPossiblySensitiveData(args));
        }

        public void Fatal(object message, Exception exception)
        {
            _log.Fatal(Helper.MaskPossiblySensitiveData(message), exception);
        }

        public void Fatal(object message)
        {
            _log.Fatal(Helper.MaskPossiblySensitiveData(message));
        }

        public void FatalFormat(IFormatProvider provider, string format, params object[] args)
        {
            _log.FatalFormat(provider, format, Helper.MaskPossiblySensitiveData(args));
        }

        public void FatalFormat(string format, object arg0, object arg1, object arg2)
        {
            _log.FatalFormat(format, Helper.MaskPossiblySensitiveData((string)arg0), Helper.MaskPossiblySensitiveData((string)arg1), Helper.MaskPossiblySensitiveData((string)arg2));
        }

        public void FatalFormat(string format, object arg0, object arg1)
        {
            _log.FatalFormat(format, Helper.MaskPossiblySensitiveData((string)arg0), Helper.MaskPossiblySensitiveData((string)arg1));
        }

        public void FatalFormat(string format, object arg0)
        {
            _log.FatalFormat(format, Helper.MaskPossiblySensitiveData((string)arg0));
        }

        public void FatalFormat(string format, params object[] args)
        {
            _log.FatalFormat(format, Helper.MaskPossiblySensitiveData(args));
        }

        public void Info(object message, Exception exception)
        {
            _log.Info(Helper.MaskPossiblySensitiveData(message), exception);
        }

        public void Info(object message)
        {
            _log.Info(Helper.MaskPossiblySensitiveData(message));
        }

        public void InfoFormat(IFormatProvider provider, string format, params object[] args)
        {
            _log.InfoFormat(provider, format, Helper.MaskPossiblySensitiveData(args));
        }

        public void InfoFormat(string format, object arg0, object arg1, object arg2)
        {
            _log.InfoFormat(format, Helper.MaskPossiblySensitiveData((string)arg0), Helper.MaskPossiblySensitiveData((string)arg1), Helper.MaskPossiblySensitiveData((string)arg2));
        }

        public void InfoFormat(string format, object arg0, object arg1)
        {
            _log.InfoFormat(format, Helper.MaskPossiblySensitiveData((string)arg0), Helper.MaskPossiblySensitiveData((string)arg1));
        }

        public void InfoFormat(string format, object arg0)
        {
            _log.InfoFormat(format, Helper.MaskPossiblySensitiveData((string)arg0));
        }

        public void InfoFormat(string format, params object[] args)
        {
            _log.InfoFormat(format, Helper.MaskPossiblySensitiveData(args));
        }

        public bool IsDebugEnabled
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsErrorEnabled
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsFatalEnabled
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsInfoEnabled
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsWarnEnabled
        {
            get { throw new NotImplementedException(); }
        }

        public void Warn(object message, Exception exception)
        {
            _log.Warn(Helper.MaskPossiblySensitiveData(message), exception);
        }

        public void Warn(object message)
        {
            _log.Warn(Helper.MaskPossiblySensitiveData(message));
        }

        public void WarnFormat(IFormatProvider provider, string format, params object[] args)
        {
            _log.WarnFormat(provider, format, Helper.MaskPossiblySensitiveData(args));
        }

        public void WarnFormat(string format, object arg0, object arg1, object arg2)
        {
            _log.WarnFormat(format, Helper.MaskPossiblySensitiveData((string)arg0), Helper.MaskPossiblySensitiveData((string)arg1), Helper.MaskPossiblySensitiveData((string)arg2));
        }

        public void WarnFormat(string format, object arg0, object arg1)
        {
            _log.WarnFormat(format, Helper.MaskPossiblySensitiveData((string)arg0), Helper.MaskPossiblySensitiveData((string)arg1));
        }

        public void WarnFormat(string format, object arg0)
        {
            _log.WarnFormat(format, Helper.MaskPossiblySensitiveData((string)arg0));
        }

        public void WarnFormat(string format, params object[] args)
        {
            _log.WarnFormat(format, Helper.MaskPossiblySensitiveData(args));
        }

        public log4net.Core.ILogger Logger
        {
            get { throw new NotImplementedException(); }
        }

        public log4net.ILog Log
        {
            get { return _log ; }
        }
    }
}