﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace WebLINK.Common
{
    public class IndentedStringBuilder
    {
        StringBuilder sb = null;
        public int IndentLevel { get; set; }
        public bool IgnoreIndent { get; set; }

        public IndentedStringBuilder()
        {
            sb = new StringBuilder();
            IndentLevel = 0;
            IgnoreIndent = false;
        }

        public void AppendLine(string text)
        {
            AddIndent();
            sb.AppendLine(text);
        }

        public void Append(string text)
        {
            AddIndent();
            sb.Append(text);
        }

        public void AppendFormat(string format, params object[] args)
        {
            AddIndent();
            sb.AppendFormat(format, args);
        }

        public override string ToString()
        {
            return sb.ToString();
        }

        private void AddIndent()
        {
            if (IgnoreIndent) return;
            for (int i = 0; i < IndentLevel; i++)
            {
                sb.Append("\t");
            }
        }
    }
}