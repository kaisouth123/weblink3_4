﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace WebLINK.Validation
{
    public static class ValidationHelper
    {
        private static List<string> InvalidRegExStrings = new List<string>
        {
            "javascript",
            "<.+>"
        };

        public static bool ContainsMaliciousData(string value)
        {
            bool containsMaliciousData = false;

            List<string> valuesToCheck = new List<string>();

            valuesToCheck.Add(value);
            valuesToCheck.Add(HttpUtility.HtmlDecode(value));
            valuesToCheck.Add(HttpUtility.UrlDecode(value));

            valuesToCheck.RemoveAll(v => string.IsNullOrEmpty(v));

            foreach (var invalidString in InvalidRegExStrings)
            {
                Regex re = new Regex(invalidString, RegexOptions.Multiline | RegexOptions.IgnoreCase, new TimeSpan(0,0,2));
                
                try
                {
                    foreach (var valueToCheck in valuesToCheck)
                    {
                        if (re.IsMatch(valueToCheck))
                        {
                            containsMaliciousData = true;
                            break;
                        }
                    }

                    if (containsMaliciousData)
                    {
                        break;
                    }
                }
                catch (Exception ex)
                {
                    Global.Log.Error("ContainsMaliciousData(): ", ex);
                    containsMaliciousData = true;
                    break;
                }
            }

            return containsMaliciousData;
        }
    }
}