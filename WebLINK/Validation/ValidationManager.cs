﻿using Bridgepay.Weblink.Core.Helpers;
using Bridgepay.Weblink.Core.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using WebLINK.Common;

namespace WebLINK.Validation
{
    // Todo: This needs to be re-factored! Need an hashtable like error collection or something so there is no possibility of duplicate error messages, etc

    /// <summary>
    /// Handles all input validation based on the state provided by the StateManager
    /// </summary>
    public class ValidationManager
    {
        NameValueCollection data = null;
        StateManager stateManager = null;
        TransactionQueue transactionQueue = null;
        List<string> requiredFields = null;

        List<string> generalErrors = null;
        public List<string> GeneralErrors
        {
            get { return generalErrors; }
            set { generalErrors = value; }
        }

        List<string> invalidFields = new List<string>();
        public List<string> InvalidFields
        {
            get { return invalidFields; }
            set { invalidFields = value; }
        }

        public ValidationManager(NameValueCollection data, StateManager stateManager)
        {
            this.data = data;
            this.stateManager = stateManager;
            transactionQueue = new TransactionQueue(data);
            generalErrors = new List<string>();

            // Parse template required fields
            requiredFields = new List<string>();
            string temp = data[Fields.RequiredFields];
            if (!string.IsNullOrEmpty(temp))
                requiredFields = temp.Split(',').ToList();

            requiredFields = requiredFields.Distinct().ToList();
        }

        public bool HasErrors
        {
            get { return generalErrors.Count != 0; }
        }

        public bool IsValid()
        {
            // Check for invalid data
            CheckForMaliciousData();

            ValidateState();

            ValidateSpecificFields();

            ValidateUnconditionallyRequiredFields();

            if (stateManager.SwipeMethod == SwipeMethod.pgweb)
            {
                ValidatePgWebFields();
            }

            //see if anyone entered PAN-like data in fields that should not contain such
            CheckFieldsForPAN();

            if (stateManager.IsPostBack)
            {
                ValidateTransactionallyRequiredFields();
                ValidateTemplateRequiredFields();
            }

            if (stateManager.IsDCP)
            {
                ValidateDCPMode();
                if ((data[Fields.Token] != null && !data[Fields.Token].Equals("create", StringComparison.CurrentCultureIgnoreCase)))
                    ValidateTransactionallyRequiredFields();
            }

            return (!HasErrors);
        }

        private void ValidatePgWebFields()
        {
            if (string.IsNullOrEmpty(data[Fields.TotalAmt]))
            {
                AddErrorFormat("{0} is a required field when {1} is 'pgweb'", Fields.TotalAmt, Fields.SwipeMethod);
            }

            if (string.IsNullOrEmpty(data[Fields.FailureUrl]))
            {
                AddErrorFormat("{0} is a required field when {1} is 'pgweb'", Fields.FailureUrl, Fields.SwipeMethod);
            }
        }

        private void ValidateState()
        {
            if (stateManager.IsPlainTokenizedTransaction &&
                !string.IsNullOrEmpty(data[Fields.SwipeMethod]))
            {
                AddError("Providing a transaction Token and specifying a SwipeMethod is not supported. Choose one or the other.");
            }
        }

        /// <summary>
        /// Validates specific fields with specific rules
        /// </summary>
        // Todo: Good place to use Jenna's validation lib eventually
        private void ValidateSpecificFields()
        {
            if (!string.IsNullOrEmpty(data[Fields.Mode]) &&
                Array.Find(Enum.GetNames(typeof(Modes)), m => m.Equals((string)data[Fields.Mode], StringComparison.CurrentCultureIgnoreCase)) == null)
            {
                AddErrorFormat("Invalid {0} value specified.", Fields.Mode);
            }

            string transType = data[Fields.TransType];
            if (!string.IsNullOrEmpty(transType))
            {
                List<string> validTransTypes = new List<string>
                {
                    "sale",
                    "auth",
                    "preauth",
                    "return",
                    "credit",
                    "sale-auth"
                };

                if (!validTransTypes.Contains(transType, StringComparer.CurrentCultureIgnoreCase))
                {
                    AddErrorFormat("Invalid {0} value specified.", Fields.TransType);
                }
            }

            string TransCatCode = data[Fields.TransactionCategoryCode];

            if (!string.IsNullOrEmpty(TransCatCode) &&
                !TransCatCode.EqualsAny(new string[] { "B", "R", "I", "H" }))
            {
                AddErrorFormat("{0} contains an invalid value. Valid values are: [Blank], B, R, I, or H.", Fields.TransactionCategoryCode);
            }

            string TransIndustryType = data[Fields.TransactionIndustryType];

            if (!string.IsNullOrEmpty(TransIndustryType) &&
                !TransIndustryType.EqualsAny(new string[] { "RE", "EC", "DM", "RS", "LD", "PT", "WEB", "PPD", "CCD", "POP", "TEL", "C21" }))
            {
                AddErrorFormat("{0} contains an invalid value. Valid values are: [Blank], RE, EC, DM, RS, LD or PT for Credit; [Blank], WEB, POP, TEL, PPD, CCD or C21 for E-Check", Fields.TransactionIndustryType);
            }


            string acctType = data[Fields.AccountType];
            if (!string.IsNullOrEmpty(acctType) &&
                !acctType.EqualsAny(new string[] { "C", "S", "Checking", "Saving", "Savings" }))
            {
                // Todo: Refactor to an "AddInvalidFieldError" method or something
                AddErrorFormat("{0} contains an invalid value. Valid values are: [Blank], C, or S.", Fields.AccountType);
            }

        }

        private void CheckForMaliciousData()
        {
            StringBuilder errorMessage = new StringBuilder("The following fields contain invalid data: ");
            string conn = null;

            foreach (var key in data.AllKeys)
            {
                if (ValidationHelper.ContainsMaliciousData(data[key]))
                {
                    InvalidFields.Add(key);
                }
            }

            if (InvalidFields.Count != 0)
            {
                foreach (var field in InvalidFields)
                {
                    errorMessage.AppendFormat("{0}{1}",
                        conn,
                        field);
                    conn = ",";
                }

                AddError(errorMessage.ToString());
            }

        }

        // These are fields that are always required. Even on the initial post to weblink
        private void ValidateUnconditionallyRequiredFields()
        {
            StringBuilder sb = new StringBuilder();
            string conn = null;
            // Validate fields that are required for every transaction
            foreach (var transaction in transactionQueue.Transactions)
            {
                // Validate authentication...which is either a purchase token or a login/password combo

                bool PurchaseTokenPresent = !string.IsNullOrEmpty(transaction[Fields.PurchaseToken]);
                bool LoginPresent = !string.IsNullOrEmpty(transaction[Fields.Login]);
                bool PasswordPresent = !string.IsNullOrEmpty(transaction[Fields.Password]);
                bool EncryptedPasswordPresent = !string.IsNullOrEmpty(transaction[Fields.PasswordEncrypted]);
                bool CredentialsValid = LoginPresent && (PasswordPresent || EncryptedPasswordPresent);

                if (!PurchaseTokenPresent && !CredentialsValid)
                    AddError("You must provide either a purchase token or login credentials.");
                    

                List<string> splitTransactionRequiredFields = new List<string>();

                if (transactionQueue.HasMultipleTransactions)
                {
                    splitTransactionRequiredFields.Add(Fields.TransactionSequenceNum);
                }

                foreach (string field in splitTransactionRequiredFields)
                {
                    if (string.IsNullOrEmpty(transaction[field]))
                    {
                        sb.AppendFormat("{0}Transaction #{1} {2}", conn, transaction.ActualIndex, field);
                        conn = ",";
                    }
                }

                // Make sure trans seq num is an integer
                int trxSeqNum = 0;
                if (!string.IsNullOrEmpty(transaction[Fields.TransactionSequenceNum]) && !int.TryParse(transaction[Fields.TransactionSequenceNum], out trxSeqNum))
                    AddErrorFormat("Transaction #{0}: {1} must be an integer.", transaction.ActualIndex, Fields.TransactionSequenceNum);
            }

            if (sb.Length != 0)
                AddError("The following fields are required: " + sb.ToString());
        }

        private void ValidateTemplateRequiredFields()
        {
            StringBuilder sb = new StringBuilder();
            string conn = string.Empty;

            // Validate the required fields as defined by the template(if any)
            // except for a few fields as these are required for every transaction and need to be handled
            // differently elsewhere.

            List<string> exceptionFields = new List<string>
            {
                Fields.Password,
                Fields.PasswordEncrypted,
                Fields.Login
            };

            if (stateManager.IsSecureTransaction)
            {
                exceptionFields.Add(Fields.CardNumber);
                exceptionFields.Add(Fields.CardTypeName);
                exceptionFields.Add(Fields.Address1);
                exceptionFields.Add(Fields.Address2);
                exceptionFields.Add(Fields.City);
                exceptionFields.Add(Fields.State);
                exceptionFields.Add(Fields.ZipCode);
                exceptionFields.Add(Fields.FullName);
            }

            if (stateManager.IsUsingPreferredPaymentMethod)
                exceptionFields.Add(Fields.CardTypeName);
            foreach (string field in requiredFields.Except(exceptionFields))
            {
                if (string.IsNullOrEmpty(data[field]))
                {
                    sb.Append(conn + field);
                    conn = ", ";
                }
            }

            if (sb.Length != 0)
                AddError("The following fields are required: " + sb.ToString());

        }
        
        private List<string> GetFieldsToCheckForPAN() {
            List<string> fields = new List<string>();
            fields.Add(Fields.AccountNumber);
            fields.Add(Fields.AccountType);
            fields.Add(Fields.Address1);
            fields.Add(Fields.Address2);           
            fields.Add(Fields.CancelUrl);
            fields.Add(Fields.CardExpMonth);
            fields.Add(Fields.CardExpYear);
            fields.Add(Fields.CardTypeName);
            fields.Add(Fields.CheckNumber);
            fields.Add(Fields.CheckType);
            fields.Add(Fields.City);
            fields.Add(Fields.CompleteUrl);
            fields.Add(Fields.CustomerAccountCode);
            fields.Add(Fields.CustomerEmailTID);
            fields.Add(Fields.CVNum);
            fields.Add(Fields.Description);
            fields.Add(Fields.DL);
            fields.Add(Fields.DOB);
            fields.Add(Fields.Email);
            fields.Add(Fields.EmailFrom);
            fields.Add(Fields.ExpDate);
            fields.Add(Fields.FailureUrl);
            fields.Add(Fields.FullName);
            fields.Add(Fields.InvoiceNum);
            fields.Add(Fields.CustomData);
            fields.Add(Fields.Login);
            fields.Add(Fields.MagData);
            fields.Add(Fields.MerchantEmail);
            fields.Add(Fields.MerchantEmailTID);
            fields.Add(Fields.MerchantName);
            fields.Add(Fields.MICR);
            fields.Add(Fields.Mode);
            fields.Add(Fields.Password);
            fields.Add(Fields.PasswordEncrypted);
            fields.Add(Fields.PaymentFormTID);
            fields.Add(Fields.PaymentType);
            fields.Add(Fields.Phone);
            fields.Add(Fields.PONum);
            fields.Add(Fields.ReceiptTID);
            fields.Add(Fields.ReceiptUrl);
            fields.Add(Fields.RedirectUrl);
            fields.Add(Fields.ReturnFields);
            fields.Add(Fields.ReturnReceiptUrl);
            fields.Add(Fields.SecurityCode);
            fields.Add(Fields.SS);
            fields.Add(Fields.State);
            fields.Add(Fields.StateCode);
            fields.Add(Fields.SwipeMethod);
            fields.Add(Fields.TransactionSequenceNum);
            fields.Add(Fields.TransitNumber);
            fields.Add(Fields.TransType);
            fields.Add(Fields.ZipCode);

            return fields;
        }

        private void CheckFieldsForPAN()
        {
            StringBuilder sb = new StringBuilder();
            string conn = null;

            List<string> FieldsToCheck = GetFieldsToCheckForPAN();
            foreach (string field in FieldsToCheck)
            {
                //see if the data looks like a PAN
                if (MaskingHelper.MaskPossiblySensitiveData(data[field]) != data[field])
                {
                    sb.Append(conn + field);
                    conn = ", ";
                }
            }

            if (sb.Length != 0)
                AddError("The following fields contain invalid data: " + sb.ToString());
        }

        private void ValidateTransactionallyRequiredFields()
        {
            StringBuilder sb = new StringBuilder();
            string conn = null;

            // Validate fields that are always required regardless if they are marked required by the template
            List<string> ImplicitlyRequiredFields = new List<string>();

            if (!stateManager.IsSecureTransaction &&
                !stateManager.IsUsingPreferredPaymentMethod &&
                stateManager.IsCardTransaction)
            {
                ImplicitlyRequiredFields.Add(Fields.CardExpMonth);
                ImplicitlyRequiredFields.Add(Fields.CardExpYear);
            }

            foreach (string field in ImplicitlyRequiredFields)
            {
                // Make sure these werent already validated above! Two of the same error messages is not cool
                if (!requiredFields.Contains(field) && string.IsNullOrEmpty(data[field]))
                {
                    sb.Append(conn + field);
                    conn = ", ";
                }
            }

            // Validate the amount(s) except for TokenizationForm transactions
            if (stateManager.Mode != Modes.TokenizationForm)
            {
                foreach (var transaction in transactionQueue.Transactions)
                {
                    foreach (string key in transaction.Keys)
                    {
                        if (key.StartsWith(Fields.TotalAmt) && string.IsNullOrEmpty(transaction[key]))
                        {
                            sb.AppendFormat("{0}Transaction #{1} {2}", conn, transaction.TransactionSequenceNumber, key);
                            conn = ",";
                        }
                    }
                }
            }

            if (sb.Length != 0)
                AddError("The following fields are required: " + sb.ToString());

            //Validate the tg_trans_hash
            //Doesnt apply for DCP mode
            if (!stateManager.IsDCP && !string.IsNullOrEmpty(data[Fields.TransactionHash]))
            {
                if (!Helper.IsTransactionHashValid(Helper.DecryptTransaction(data[Fields.TransactionHash]), Helper.GetSimpleTransHash(transactionQueue)))
                    //Keep the error message pretty generic to help keep a "hacker" scratching his head on whats really going on.
                    AddError("Your transaction could not be completed at this time, please try again later.");
            }

            //Validate required fields for a sale via a token

            string token = data[Fields.Token];

            // Validate expiration date
            bool validExpDate = true;
            data[Fields.CardExpYear] = data[Fields.CardExpYear].Length == 2 ? (2000 + int.Parse(data[Fields.CardExpYear])).ToString() : data[Fields.CardExpYear];
            string expMonth = data[Fields.CardExpMonth] + string.Empty;
            string expYear = data[Fields.CardExpYear] + string.Empty;
            if (!string.IsNullOrEmpty(expMonth) && !string.IsNullOrEmpty(expYear))
            {
                int year;
                int month;
                int.TryParse(expYear, out year);
                int.TryParse(expMonth, out month);

                if (year < DateTime.Now.Year)
                    validExpDate = false;

                if (year <= DateTime.Now.Year && month < DateTime.Now.Month)
                    validExpDate = false;

                if (month < 1 || month > 12)
                    validExpDate = false;

                if (!validExpDate)
                    AddError("Invalid expiration date.");
            }
        }

        private void ValidateDCPMode()
        {
            // Token creation
            if (data[Fields.Token] != null && data[Fields.Token].Equals("create", StringComparison.CurrentCultureIgnoreCase))
            {
                List<string> fields = new List<string>();

                fields.Add(Fields.CardNumber);

                ValidateListOfRequiredFields(fields);
            }

            // Wallet paymentmethod creation
            if (stateManager.IsCreatingWallet)
            {
                List<string> fields = new List<string>
                {
                    Fields.FullName,
                    Fields.Address1,
                    Fields.City,
                    Fields.State,
                    Fields.ZipCode,
                    Fields.CardTypeName
                };

                ValidateListOfRequiredFields(fields);
            }

            // Validate that they've provided card details
            if (!stateManager.IsUsingPreferredPaymentMethod &&
                !stateManager.IsSecureTransaction)
            {
                List<string> fields = new List<string>
                {
                    Fields.CardExpMonth,
                    Fields.CardExpYear
                };

                if (string.IsNullOrEmpty(data[Fields.Token]))
                {
                    fields.Add(Fields.CardNumber);
                    fields.Add(Fields.CVNum);
                }

                ValidateListOfRequiredFields(fields);
            }
            
        }

        public void AddError(string message)
        {
            generalErrors.Add(message);
        }

        private void ValidateListOfRequiredFields(List<string> requiredFields)
        {
            foreach (string field in requiredFields)
            {
                if (string.IsNullOrEmpty(data[field]))
                    AddErrorFormat("{0} is required.", field);
            }
        }

        public void AddErrorFormat(string message, params object[] args) 
        {
            generalErrors.Add(string.Format(message, args));
        }

        /// <summary>
        /// Takes all r_Message fields and adds them to the error collection.
        /// Used for displaying transaction errors during split transaction processing
        /// </summary>
        /// <param name="data"></param>
        public void AddResultMessages(TransactionQueue transactionQueue)
        {
            foreach (var trans in transactionQueue.Transactions)
            {
                string result = trans[Fields.r_Message];
                if (trans.WasVoided)
                    result += " (Voided)";

                result = string.IsNullOrEmpty(result) ? "N/A" : result;
                AddErrorFormat("Transaction #{0} result: {1}", trans.ActualIndex, result);
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            string conn = null;

            foreach (string error in GeneralErrors)
            {
                sb.AppendFormat("{0}{1}", conn, error);
                conn = ",";
            }

            return sb.ToString();
        }

    }
}