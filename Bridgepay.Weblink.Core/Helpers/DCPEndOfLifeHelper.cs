﻿using Bridgepay.Core.Logging;
using Bridgepay.Weblink.Core.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

namespace Bridgepay.Weblink.Core.Helpers
{
    /// <summary>
    /// DCP mode end-of-life functionality.
    /// </summary>
    public class DCPEndOfLifeHelper
    {
        private List<string> dcpUserWhitelist = new List<string>();
        private DateTime? passThroughDeprecation;
        private DateTime? dcpDeprecation;

        /// <summary>
        /// Instantiates a helper for DCP mode end-of-life.
        /// </summary>
        /// <param name="passThroughUsers">Comma-separated list of users allowed to continue using DCP mode.</param>
        /// <param name="passThroughDeprecationDate">Date when the pass-through users will no longer be allowed to use DCP mode.</param>
        /// <param name="dcpDeprecationDate">Date when DCP mode will be deprecated and only pass-through users allowed.</param>
        public DCPEndOfLifeHelper(string passThroughUsers, string passThroughDeprecationDate, string dcpDeprecationDate)
        {
            if (!string.IsNullOrWhiteSpace(passThroughUsers))
            {
                dcpUserWhitelist = passThroughUsers.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                                                   .Select(s => s.Trim())
                                                   .Where(s => s.Length > 0)
                                                   .ToList();
            }

            if (dcpUserWhitelist.Count <= 0)
            {
                LogProvider.LoggerInstance.Warn("No DCP pass-through users were found.");
            }

            if (!string.IsNullOrWhiteSpace(passThroughDeprecationDate) && DateTime.TryParse(passThroughDeprecationDate, out DateTime tempPassThroughDate))
            {
                passThroughDeprecation = tempPassThroughDate;
            }
            else
            {
                LogProvider.LoggerInstance.Warn("Pass-through deprecation date not found or failed to parse.");
            }
            
            if (!string.IsNullOrWhiteSpace(dcpDeprecationDate) && DateTime.TryParse(dcpDeprecationDate, out DateTime tempDcpDate))
            {
                dcpDeprecation = tempDcpDate;
            }
            else
            {
                LogProvider.LoggerInstance.Warn("DCP deprecation date not found or failed to parse.");
            }

            LogProvider.LoggerInstance.Info($"Pass-through users: {dcpUserWhitelist.Count} Pass-through deprecation: {passThroughDeprecation} DCP deprecation: {dcpDeprecation}");
        }

        /// <summary>
        /// Is a DCP mode transaction allowed?
        /// </summary>
        /// <param name="form">Request form data.</param>
        /// <param name="currentTime">The current time to check against deprecation dates.</param>
        /// <returns>True if a DCP mode transaction is allowed.</returns>
        public bool IsDCPModeAllowed(NameValueCollection form, DateTime currentTime)
        {
            // "DZ says, if they try DCP with a PT, deny it."
            if (!string.IsNullOrWhiteSpace(form[Fields.PurchaseToken]))
            {
                LogProvider.LoggerInstance.Warn("Denied a DCP transaction due to the presence of a purchase token.");
                return false;
            }
            else if (dcpDeprecation == null || dcpDeprecation.Value > currentTime)
            {
                // The configured DCP deprecation date is invalid, missing, or in the future; proceed as normal.
                return true;
            }

            var isWhiteListed = dcpUserWhitelist.Contains(form[Fields.Login]);
            if (!isWhiteListed)
            {
                LogProvider.LoggerInstance.Warn($"Denied a DCP transaction because the pass-through list doesn't contain \"{form[Fields.Login]}\".");
                return false;
            }
            else if (passThroughDeprecation.HasValue && passThroughDeprecation.Value < currentTime)
            {
                LogProvider.LoggerInstance.Warn($"Denied a DCP transaction because the pass-through list expired on {passThroughDeprecation}.");
                return false;
            }

            LogProvider.LoggerInstance.Info($"Allowed a DCP transaction because the pass-through list has not expired and contains \"{form[Fields.Login]}\".");
            return true;            
        }
    }
}
