﻿using Bridgepay.Weblink.Core.Models;
using System;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace Bridgepay.Weblink.Core.Helpers
{
    /// <summary>
    /// Data masking functionality.
    /// </summary>
    /// <remarks>
    /// This class consolidates redundant masking methods the Core.Helper and Common.Helper classes.
    /// </remarks>
    public static class MaskingHelper
    {
        /// <summary>
        /// Scans the testValue for anything possible sensitive data in URL or anything that looks like a PAN
        /// Returns a masked version of the testValue string
        /// </summary>
        /// <param name="testValue"></param>
        /// <param name="key"></param>
        /// <returns>string</returns>
        public static string MaskPossiblySensitiveData(string testValue, string key = null)
        {
            if (!string.IsNullOrWhiteSpace(testValue))
            {
                testValue = MaskSensitiveUrlParameters(testValue, key);

                testValue = MaskPossiblePAN(testValue);
            }

            // Mask Password, PasswordEncrypted, svcpassword, etc.
            if (!string.IsNullOrWhiteSpace(key) && key.IndexOf("password", StringComparison.InvariantCultureIgnoreCase) != -1)
            {
                // Don't do a 1:1 replacement here, no sense in revealing the password length.
                testValue = "************";
            }

            return testValue;
        }

        /// <summary>
        /// Scans the array of parameter objects for anything possible sensitive data to be masked
        /// Returns masked values as an array of objects
        /// </summary>
        /// <param name="args"></param>
        /// <returns>object[]</returns>
        public static object[] MaskPossiblySensitiveData(params object[] args)
        {
            //this method should go through and mask any 16-digit number even if it's embedded in a larger string

            if (args.Count() > 0)
            {
                for (int x = 0; x < args.Count(); x++)
                {
                    string testValue = Convert.ToString(args[x]);
                    args[x] = MaskPossiblySensitiveData(testValue);
                }
            }

            return args;
        }

        /// <summary>
        /// Scans the testValue (A URL) for potentially sensitive data that needs to be masked
        /// Returns a masked version of the testValue URL string
        /// </summary>
        /// <param name="testValue"></param>
        /// <param name="key"></param>
        /// <returns>string</returns>
        public static string MaskSensitiveUrlParameters(string testValue, string key)
        {
            //this method tries to ensure that no sensitive values that were included in a Url passed in by the user get logged

            if ((key != null) && (key.ToLower().Contains("url")))
            {
                //split the url by name/value pairs
                string[] keyPairs = testValue.Split('&');
                foreach (string pair in keyPairs)
                {
                    if (pair.Contains('='))
                    {
                        //split apart the name and the value
                        string[] keyValuePair = pair.Split('=');
                        if (keyValuePair[1] != string.Empty)
                        {
                            //see if the key name contains password
                            if (keyValuePair[0].ToLower().Contains("password"))
                                testValue = testValue.Replace(keyValuePair[1], new string('X', 8));

                            //add other sensitive parameters as they're noted by QA
                        }
                    }
                }
            }

            return testValue;
        }

        /// <summary>
        /// Scans the testValue for anything that looks like a PAN so that it can be properly masked for logging
        /// Returns a masked version of the testValue string
        /// </summary>
        /// <param name="testValue"></param>
        /// <returns>string</returns>
        public static string MaskPossiblePAN(string testValue)
        {
            if (testValue.Length >= 16)
            {
                string[] numbers = Regex.Split(testValue, @"\D+");
                foreach (string value in numbers)
                {
                    if (!string.IsNullOrEmpty(value) && value.Trim().Length >= 16)
                        if (PassesLuhnMod10(value))
                            testValue = testValue.Replace(value, MaskCardNumber(value));
                }
            }

            return testValue;
        }

        /// <summary>
        /// Performs a Luhn algorithm / Mod10 check on the testValue to see if it looks like a PAN
        /// Returns a bool: true = possible PAN; false = not a PAN
        /// </summary>
        /// <param name="testValue"></param>
        /// <returns>bool</returns>
        public static bool PassesLuhnMod10(string testValue)
        {
            int[] DELTAS = new int[] { 0, 1, 2, 3, 4, -4, -3, -2, -1, 0 };
            int checksum = 0;
            char[] chars = testValue.ToCharArray();
            for (int i = chars.Length - 1; i > -1; i--)
            {
                int j = ((int)chars[i]) - 48;
                checksum += j;
                if (((i - chars.Length) % 2) == 0)
                    checksum += DELTAS[j];
            }

            return ((checksum % 10) == 0);
        }

        public static string MaskCardNumber(string cardNumber, int totalCharsToReturn)
        {
            if ((!string.IsNullOrEmpty(cardNumber)) && (cardNumber.Length > 4))
            {
                int maskLength = cardNumber.Length - 4;
                cardNumber = cardNumber.Substring(maskLength, 4).PadLeft(totalCharsToReturn, 'X');
            }
            return (cardNumber);
        }

        public static string MaskCardNumber(string cardNumber)
        {
            return (MaskCardNumber(cardNumber, cardNumber.Length));
        }

        public static void MaskSensitiveData(NameValueCollection data, bool excludePassword = false)
        {
            if (!string.IsNullOrEmpty(data[Fields.CardNumber]))
                data[Fields.CardNumber] = MaskCardNumber(data[Fields.CardNumber]);

            if (!string.IsNullOrEmpty(data[Fields.CVNum]))
                data[Fields.CVNum] = new string('X', (data[Fields.CVNum] + string.Empty).Length);
            if (!string.IsNullOrEmpty(data[Fields.r_AVSMessage]))
                data[Fields.r_AVSMessage] = data[Fields.r_AVSMessage].Replace("'", "%27");

            if (!excludePassword)
            {
                for (int i = 0; i < data.Keys.Count; i++)
                {
                    if (data.GetKey(i).ToLower().Contains(Fields.Password.ToLower())) //&& !data.GetKey(i).StartsWith(Fields.PasswordEncrytped, StringComparison.CurrentCultureIgnoreCase))
                    {
                        data.Set(data.GetKey(i), new string('X', 8));
                    }
                }
            }

            if (!string.IsNullOrEmpty(data[Fields.AccountNumber]))
                data[Fields.AccountNumber] = MaskCardNumber(data[Fields.AccountNumber],
                    data[Fields.AccountNumber].Length);
        }

        public static string MaskSensitiveData(string valueToMask, string valueKey)
        {
            if ((valueKey == Fields.CardNumber) || (valueKey == Fields.PaymentAccountNumber))
            {
                if (!string.IsNullOrEmpty(valueToMask))
                    if (valueToMask.Length > 3)
                    {
                        string mask = new string('X', (valueToMask + string.Empty).Length - 4);
                        return mask + valueToMask.Substring(valueToMask.Length - 4);
                    }
                    else
                        return new string('X', (valueToMask + string.Empty).Length);
            }

            if (valueKey.ToLower().Contains(Fields.Password.ToLower()))
                return new string('X', 8);

            if ((valueKey == Fields.CVNum) || (valueKey == Fields.SecurityCode))
            {
                if (!string.IsNullOrEmpty(valueToMask))
                    return new string('X', (valueToMask + string.Empty).Length);
            }

            if (valueKey == Fields.AccountNumber)
                return MaskCardNumber(valueToMask + string.Empty, valueToMask.Length);

            return valueToMask;
        }

        /// <summary>
        /// Scans the collection of name/values for anything possible sensitive data to be masked
        /// Returns a masked string built from the name value collection
        /// </summary>
        /// <param name="col"></param>
        /// <param name="encode"></param>
        /// <returns>string</returns>
        public static string MaskPossiblySensitiveData(this NameValueCollection col, bool encode = true)
        {
            StringBuilder sb = new StringBuilder();
            string conn = null;

            foreach (string key in col.AllKeys)
            {
                string value = col[key];
                if (encode)
                    value = HttpUtility.UrlEncode(value);
                sb.AppendFormat("{2}{0}={1}", key, MaskPossiblySensitiveData(value, key), conn);
                conn = "&";
            }

            return (sb.ToString());
        }
    }
}
