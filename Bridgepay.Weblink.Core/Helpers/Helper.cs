﻿
using Bridgepay.Weblink.Core.Models;
using System.Collections.Specialized;
using System.Text;
using System.Web;
using Bridgepay.Weblink.Core.Extensions;
using System.Linq;
using System;
using System.Text.RegularExpressions;
using System.Net;
using System.IO;
using Microsoft.Win32;
using System.Collections.Generic;
using Bridgepay.WebLink.Models;

namespace Bridgepay.WebLink.Core.Helpers
{
    public static class Helper
    {
        public static string GetExpDate(NameValueCollection post)
        {
            string expDate = post[Fields.ExpDate];

            if (string.IsNullOrEmpty(expDate))
            {
                expDate = post[Fields.CardExpMonth];
                string expYear = post[Fields.CardExpYear] + string.Empty;
                if (expYear.Length == 4)
                    expDate += expYear.Substring(2, 2);
                else if (expYear.Length == 2)
                    expDate += expYear;
            }

            return (expDate);
        }

        public static string GetLastFour(NameValueCollection post)
        {
            string lastFour = post[Fields.CardNumber];
            if (!string.IsNullOrEmpty(lastFour))
            {
                if (lastFour.Length > 4)
                    lastFour = lastFour.Substring(lastFour.Length - 4);
            }
            return lastFour;
        }

        public static string ToDecimalFromIntHundred(string amount)
        {
            decimal amt = 0;

            amt = decimal.Parse(amount);

            return amt.ToString("#.00").Replace(".", string.Empty);
        }

        public static string ToIntHundredFromDecimal(int amount)
        {
            return (amount / 100d).ToString("#.00");
        }

        public static string GetClientIP()
        {
            //string ipList = HttpContext.Current.Request.ServerVariables["HTTP_X_CLUSTER_CLIENT_IP"];

            string ipList = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipList))
            {
                return ipList.Split(',')[0];
            }

            return HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
        }

        public static string GetPostData(NameValueCollection form)
        {
            StringBuilder sb = new StringBuilder();
            string conn = string.Empty;

            form = new NameValueCollection(form);

            foreach (string key in form.Keys)
            {
                string value = form[key];
                sb.AppendFormat("{2}{0}={1}", key, value, conn);
                conn = "&";
            }

            return (sb.ToString());
        }

        public static bool IsTransactionHashValid(string originalHash, string currentHash)
        {
            NameValueCollection originalData = originalHash.ToNameValueCollection();
            NameValueCollection currentData = currentHash.ToNameValueCollection();

            foreach (string key in originalData)
            {
                if (!originalData[key].Equals(currentData[key], StringComparison.CurrentCultureIgnoreCase))
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Returns a simple hash of the fields of all transactions that would likely be tampered with.
        /// </summary>
        /// <param name="transactionQueue"></param>
        /// <returns></returns>



        /// <summary>
        /// Similar to bool.TryParse except that this treats "1", "on", and "T" as boolean true.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public static bool TryParseBoolEX(string value, out bool result)
        {
            bool rv = false;

            if (!string.IsNullOrEmpty(value))
            {
                switch (value.ToLower())
                {
                    case "1":
                    case "on":
                    case "t":
                        value = "True";
                        break;
                }
            }

            rv = bool.TryParse(value, out result);

            return (rv);
        }

        public static string GetXmlNodeValue(string xml, string tag)
        {
            string rv = string.Empty;

            if (tag.StartsWith("<"))
                tag = tag.TrimStart(new char[] { '<' });

            if (tag.EndsWith(">"))
                tag = tag.TrimEnd(new char[] { '>' });

            int start = xml.IndexOf("<" + tag + ">");
            int end = xml.IndexOf("</" + tag + ">");

            if (start != -1 && end != -1)
                rv = xml.Substring(start + tag.Length + 2, (end - (start + tag.Length + 2)));

            return (rv);
        }

        public static string PostData(string postData, string url)
        {
            string contentType = string.Empty;
            return PostData(postData, url, out contentType);
        }

        public static string PostData(string postData, string url, out string contentType)
        {
            string rv = string.Empty;

            byte[] bytes = Encoding.Default.GetBytes(postData);
            WebRequest request = WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = bytes.Length;
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(bytes, 0, bytes.Length);
            dataStream.Close();

            WebResponse response = null;
            try
            {
                response = request.GetResponse();
            }
            catch (WebException we)
            {
                response = we.Response;
            }
            catch (Exception) { throw; }
            dataStream = response.GetResponseStream();
            StreamReader sr = new StreamReader(dataStream);

            rv = sr.ReadToEnd();
            sr.Close();
            dataStream.Close();

            contentType = response.ContentType;

            response.Close();

            return (rv);
        }

        public static string GETData(NameValueCollection nvc, string url)
        {
            string rv = string.Empty;
            url += "?" + nvc.ToNameValuePairString();

            WebRequest request = WebRequest.Create(url);
            request.Method = "GET";
            Stream dataStream = null;
            WebResponse response = null;

            try
            {
                response = request.GetResponse();
            }
            catch (WebException ex)
            {
                response = ex.Response;
            }

            dataStream = response.GetResponseStream();
            StreamReader sr = new StreamReader(dataStream);

            rv = sr.ReadToEnd();
            sr.Close();
            dataStream.Close();
            response.Close();

            return (rv);
        }

        public static string Base64Encode(string value)
        {
            if (string.IsNullOrEmpty(value))
                return null;
            else
                return Convert.ToBase64String(Encoding.Default.GetBytes(value));
        }

        public static string Base64Decode(string value)
        {
            if (string.IsNullOrEmpty(value))
                return null;
            else
                return Encoding.Default.GetString(Convert.FromBase64String(value));
        }

        /// <summary>
        /// Merges the passed in collection to the collection and returns a new resultant collection. Will not overwrite
        /// null values in the original collection
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        public static void Merge(NameValueCollection first, NameValueCollection second, bool overwriteNulls = false)
        {
            for (int i = 0; i < second.Count; i++)
            {
                if (string.IsNullOrEmpty(first[second.GetKey(i)]) || overwriteNulls)
                    first[second.GetKey(i)] = second.Get(i);
            }
        }

        public static NameValueCollection AppendSuffixIndexToResultFields(NameValueCollection col, int index)
        {
            NameValueCollection newCol = new NameValueCollection();

            foreach (string key in col.Keys)
            {
                string newKey = key;
                if (key.StartsWith("r_"))
                    newKey = key + index.ToString();
                newCol.Add(newKey, col[key]);
            }

            return newCol;
        }

        public static string GetRegistryValue(string subkey, string name)
        {
            RegistryKey key = null;
            string v = null;
            try
            {
                key = Registry.LocalMachine;
                key = key.OpenSubKey(subkey);
                v = (string)key.GetValue(name);
            }
            catch { }
            finally
            {
                if (key != null) try { key.Close(); }
                    catch { }
            }
            return v;
        }

        public static string ToBridgeCommAmount(string amount)
        {
            decimal amt = 0;

            amt = decimal.Parse(amount);

            return amt.ToString("#.00").Replace(".", string.Empty);
        }

        public static string FromBridgeCommAmount(int amount)
        {
            return (amount / 100d).ToString("#.00");
        }

        public static void DumpToFile(NameValueCollection data)
        {
            StreamWriter sw = new StreamWriter(@"c:\temp\data.txt", true);

            foreach (string key in data.Keys)
            {
                sw.WriteLine(string.Format("{0} = {1}", key, data[key]));
            }

            sw.Close();
        }

        public static string GetSimpleTransHash(NameValueCollection data)
        {
            // Grab all of our amounts and username/passwords and the mid. Those probably cover the most likely fields to be tampered with.

            NameValueCollection tempCollection = new NameValueCollection();

            tempCollection.Add(Fields.MerchantAccountCode, data[Fields.MerchantAccountCode]);

            foreach (string key in data.Keys)
            {
                if (key.StartsWithAny(new List<string>
                {
                    Fields.TotalAmt,
                    Fields.Login,
                    Fields.PasswordEncrypted,
                    Fields.Password
                }) && !string.IsNullOrEmpty(data[key]))
                    tempCollection.Add(key, data[key]);
            }

            return tempCollection.ToNameValuePairString();
        }

        public static string GetTransHash(NameValueCollection data)
        {
            SimpleHash simpleHash = new SimpleHash(SimpleHash.HashAlgorithms.SHA512, WebLinkForms.CryptoSecret);
            return simpleHash.ComputeHash(GetSimpleTransHash(data));
        }

        public static string EncryptTransaction(NameValueCollection data)
        {
            return Crypto.EncryptStringAES(GetSimpleTransHash(data), WebLinkForms.CryptoSecret);
        }

        public static string DecryptTransaction(string hash)
        {
            return Crypto.DecryptStringAES(hash, WebLinkForms.CryptoSecret);
        }

        public static string DecryptTransaction(string hash, string secret)
        {
            return Crypto.DecryptStringAES(hash, secret);
        }

        public static string EncryptPassword(string password, string secret)
        {
            return Crypto.EncryptStringAES(password, secret);
        }

        public static string DecryptPassword(string encryptedPassword, string secret)
        {
            return Crypto.DecryptStringAES(encryptedPassword, secret);
        }

        public static string RemoveNonDigits(string requestMessage)
        {
            if (requestMessage == null) return string.Empty;
            return new string(requestMessage.Where(c => char.IsDigit(c)).ToArray());
        }

    }
}
