﻿using Bridgepay.Weblink.Core.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Web;

namespace Bridgepay.Weblink.Core.Extensions
{
    public static class ModelExtensions
    {
        /// <summary>
        /// Returns a collection that includes only the entries specified in the keys collection.
        /// </summary>
        /// <param name="nvc"></param>
        /// <param name="keys"></param>
        /// <returns></returns>
        public static NameValueCollection Subset(this NameValueCollection nvc, List<string> keys)
        {
            NameValueCollection rv = new NameValueCollection();
            foreach (string key in keys)
            {
                if (Array.IndexOf(nvc.AllKeys, key) != -1)
                    rv.Add(key, nvc[key]);
            }
            return (rv);
        }

        public static bool Exists(this NameValueCollection nvc, string key)
        {
            return (Array.IndexOf(nvc.AllKeys, key) != -1);
        }

        /// <summary>
        /// Effectively changes the name of an entries key. If the newName already exists, it is overwritten.
        /// </summary>
        /// <param name="nvc"></param>
        /// <param name="oldName"></param>
        /// <param name="newName"></param>
        public static void RenameValue(this NameValueCollection nvc, string oldName, string newName)
        {
            string value = nvc[oldName];
            nvc.Remove(oldName);
            nvc.Remove(newName);
            nvc.Add(newName, value);
        }

        public static void Coalesce(this NameValueCollection nvc, string destinationField, params string[] sourceField)
        {
            //Only modify the destinationField if it isnt already allocated
            if (string.IsNullOrEmpty(nvc[destinationField]))
            {
                foreach (string field in sourceField)
                {
                    if (!string.IsNullOrEmpty(nvc[field]))
                    {
                        nvc[destinationField] = nvc[field];
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Gets a value from a NameValueCollection. Returns string.empty rather than null however.
        /// </summary>
        /// <param name="col"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public static string GetValue(this NameValueCollection col, string field, string defaultValue = "")
        {
            return string.IsNullOrEmpty(col[field]) ? defaultValue : col[field];
        }

        public static string ToNameValuePairString(this NameValueCollection col, bool encode = true)
        {
            StringBuilder sb = new StringBuilder();
            string conn = null;

            foreach (string key in col.AllKeys)
            {
                string value = col[key];
                if (encode)
                    value = HttpUtility.UrlEncode(value);
                sb.AppendFormat("{2}{0}={1}", key, value, conn);
                conn = "&";
            }

            return (sb.ToString());
        }

        /// <summary>
        /// Filters a NameValueCollection by an expression. Much like IEnumerable.Except()
        /// </summary>
        /// <param name="col"></param>
        /// <param name="expr"></param>
        /// <returns></returns>
        public static NameValueCollection Except(this NameValueCollection col, Expression<Func<string, bool>> expr)
        {
            NameValueCollection newCol = new NameValueCollection();

            foreach (string key in col)
            {
                if (expr.Compile().Invoke(key))
                    newCol.Add(key, col[key]);
            }

            return newCol;
        }

        public static NameValueCollection ToNameValueCollection(this string s)
        {
            NameValueCollection nvc = new NameValueCollection();

            string[] tokens = s.Split(new char[] { '&' });

            foreach (string token in tokens)
            {
                string[] subtokens = token.Split(new char[] { '=' });
                if (subtokens.Length == 2)
                {
                    nvc.Add(subtokens[0], HttpUtility.UrlDecode(subtokens[1]));
                }
            }

            return nvc;
        }

        public static void RemoveResultFields(this NameValueCollection data)
        {
            for (int i = data.Count - 1; i >= 0; i--)
            {
                string key = data.GetKey(i);
                if (key.StartsWithAny(new List<string> { "r_", "rv_" }))
                    data.Remove(key);
            }
        }

        /// <summary>
        /// Returns the last four from the PAN, if possible.
        /// </summary>
        /// <param name="col"></param>
        /// <returns></returns>
        public static string GetLastFour(this NameValueCollection col)
        {
            var PAN = col.GetValue(Fields.CardNumber);

            if (PAN != null &&
                PAN.Length >= 4)
            {
                return PAN.Substring(PAN.Length - 4);
            }

            return string.Empty;
        }

        public static string Join<T>(this IEnumerable<T> i, string delimitter)
        {
            StringBuilder sb = new StringBuilder();
            string conn = null;

            foreach (T item in i)
            {
                sb.Append(conn + item.ToString());
                conn = delimitter;
            }

            return sb.ToString();
        }

        public static bool EqualsAny(this string s, params string[] values)
        {
            bool rv = false;
            foreach (string val in values)
            {
                if (s.Equals(val, StringComparison.OrdinalIgnoreCase))
                {
                    rv = true;
                    break;
                }
            }

            return rv;
        }

        public static bool StartsWithAny(this string s, List<string> list)
        {
            foreach (var item in list)
            {
                if (s.StartsWith(item))
                    return true;
            }

            return false;
        }

        public static string TrimTrailingDigits(this string s)
        {
            return s.TrimEnd(new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' });
        }

        /// <summary>
        /// Converts an IEnumerable<T> to a datatable
        /// </summary>
        public static DataTable ToDataTable<T>(this IEnumerable<T> items)
        {
            var tb = new DataTable(typeof(T).Name);

            PropertyInfo[] props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (PropertyInfo prop in props)
            {
                Type t = GetCoreType(prop.PropertyType);
                tb.Columns.Add(prop.Name, t);
            }


            foreach (T item in items)
            {
                var values = new object[props.Length];

                for (int i = 0; i < props.Length; i++)
                {
                    values[i] = props[i].GetValue(item, null);
                }

                tb.Rows.Add(values);
            }

            return tb;
        }

        /// <summary>
        /// Determine of specified type is nullable
        /// </summary>
        public static bool IsNullable(Type t)
        {
            return !t.IsValueType || (t.IsGenericType && t.GetGenericTypeDefinition() == typeof(Nullable<>));
        }

        /// <summary>
        /// Return underlying type if type is Nullable otherwise return the type
        /// </summary>
        public static Type GetCoreType(Type t)
        {
            if (t != null && IsNullable(t))
            {
                if (!t.IsValueType)
                {
                    return t;
                }
                else
                {
                    return Nullable.GetUnderlyingType(t);
                }
            }
            else
            {
                return t;
            }
        }
    }
}
