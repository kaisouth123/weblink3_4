﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.Weblink.Core.Logging
{
    public class TimeStampLogger : ITimeStampLogger
    {
        private string id;
        private List<DateTime> events;
        private ICleanLogger _log;

        public TimeStampLogger(ICleanLogger log)
        {
            events = new List<DateTime>();
            _log = log;
            Guid g = Guid.NewGuid();

            id = g.ToString().Split(new char[] { '-' })[4];
        }

        public void LogIt(string message)
        {
            DateTime eventTime = DateTime.Now;
            string timeSinceLastEvent = null;

            if (events.Count != 0)
            {
                DateTime PriorEventDateTime = events[events.Count - 1];
                timeSinceLastEvent = string.Format("Miliseconds since last event: {0}", (eventTime - PriorEventDateTime).TotalMilliseconds);
            }

            _log.InfoFormat("\r\nTimestamp log ID: {0}\r\n{1}\r\n{2}",
                id,
                message,
                timeSinceLastEvent);

            events.Add(eventTime);
        }
    }
}
