﻿using log4net;
using log4net.Config;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Bridgepay.Weblink.Core.Logging
{
    public static class WeblinkLog
    {
        public static ILog log;
        static WeblinkLog()
        {
            string directory = AppDomain.CurrentDomain.RelativeSearchPath ?? AppDomain.CurrentDomain.BaseDirectory;
            string logFile = directory + "\\" + @"log4net.config";
            if (File.Exists(logFile))
            {
                FileInfo info = new FileInfo(logFile);
                XmlConfigurator.ConfigureAndWatch(info);
            }

            log = LogManager.GetLogger(typeof(HttpApplication));
        }



        [MethodImpl(MethodImplOptions.NoInlining)]
        public static string GetCurrentMethod()
        {
            StackTrace st = new StackTrace();
            StackFrame sf = st.GetFrame(1);

            return sf.GetMethod().Name;
        }
    }
}

