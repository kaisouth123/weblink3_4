﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using log4net;
using Bridgepay.WebLink.Core.Helpers;
using Bridgepay.Core.Logging;
using Bridgepay.Weblink.Core.Helpers;

namespace Bridgepay.Weblink.Core.Logging
{
    public class CleanLogger : ICleanLogger 
    {
        Logger _log;
        ILog _testLog;

        public CleanLogger()
        {
            _testLog = WeblinkLog.log;
            LogProvider.Configure(null);
            _log = LogProvider.LoggerInstance;
        }

        public CleanLogger(Logger log)
        {

            if (_log == null)
                _log = LogProvider.LoggerInstance;
            else
                _log = log;
        }

        public void Debug(string message, Exception exception) 
        {
            _log.Debug(message.ToString(), exception, EventId.Default);
        }

        public void Debug(string message)
        {
            _log.Debug(MaskingHelper.MaskPossiblySensitiveData(message), EventId.Default);
        }

        public void DebugFormat(string format, EventId eventId = EventId.Default, params object[] args )
        {
            _log.DebugFormat(format, eventId, MaskingHelper.MaskPossiblySensitiveData(args));
        }

        public void DebugFormat(string format, object arg0, object arg1, object arg2, EventId eventId = EventId.Default)
        {
            _log.DebugFormat(format, eventId, MaskingHelper.MaskPossiblySensitiveData((string)arg0), MaskingHelper.MaskPossiblySensitiveData((string)arg1), MaskingHelper.MaskPossiblySensitiveData((string)arg2));
        }

        public void DebugFormat(string format, object arg0, object arg1, EventId eventId = EventId.Default)
        {
            _log.DebugFormat(format, eventId, MaskingHelper.MaskPossiblySensitiveData((string)arg0), MaskingHelper.MaskPossiblySensitiveData((string)arg1));
        }

        public void DebugFormat(string format, object arg0, EventId eventId = EventId.Default)
        {
            _log.DebugFormat(format, eventId, MaskingHelper.MaskPossiblySensitiveData((string)arg0));
        }

        public void Error(string message, Exception exception, EventId eventId = EventId.Default)
        {
            _log.Error(MaskingHelper.MaskPossiblySensitiveData((string)message), exception, eventId);
        }

        public void Error(string message)
        {
            _log.Error(MaskingHelper.MaskPossiblySensitiveData(message));
        }

        public void ErrorFormat(string format, EventId eventId = EventId.Default, params object[] args)
        {
            _log.ErrorFormat(format, eventId, MaskingHelper.MaskPossiblySensitiveData(args));
        }

        public void ErrorFormat(string format, object arg0, object arg1, object arg2, EventId eventId = EventId.Default)
        {
            _log.ErrorFormat(format, eventId, MaskingHelper.MaskPossiblySensitiveData((string)arg0), MaskingHelper.MaskPossiblySensitiveData((string)arg1), MaskingHelper.MaskPossiblySensitiveData((string)arg2));
        }

        public void ErrorFormat(string format, object arg0, object arg1, EventId eventId = EventId.Default)
        {
            _log.ErrorFormat(format, eventId, MaskingHelper.MaskPossiblySensitiveData((string)arg0), MaskingHelper.MaskPossiblySensitiveData((string)arg1));
        }

        public void ErrorFormat(string format, object arg0, EventId eventId = EventId.Default)
        {
            _log.ErrorFormat(format, eventId, MaskingHelper.MaskPossiblySensitiveData((string)arg0));
        }

        

        public void Info(string message, Exception exception, EventId eventId = EventId.Default)
        {
            _log.Info(MaskingHelper.MaskPossiblySensitiveData(message), exception, eventId);
        }

        public void Info(string message)
        {
            _log.Info(MaskingHelper.MaskPossiblySensitiveData(message));
        }

        public void InfoFormat(string format, EventId eventId = EventId.Default, params object[] args)
        {
            _log.InfoFormat(format, eventId, MaskingHelper.MaskPossiblySensitiveData(args));
        }

        public void InfoFormat(string format, object arg0, object arg1, object arg2, EventId eventId = EventId.Default)
        {
            _log.InfoFormat(format, eventId, MaskingHelper.MaskPossiblySensitiveData((string)arg0), MaskingHelper.MaskPossiblySensitiveData((string)arg1), MaskingHelper.MaskPossiblySensitiveData((string)arg2));
        }

        public void InfoFormat(string format, object arg0, object arg1, EventId eventId = EventId.Default)
        {
            _log.InfoFormat(format, eventId, MaskingHelper.MaskPossiblySensitiveData((string)arg0), eventId, MaskingHelper.MaskPossiblySensitiveData((string)arg1));
        }

        public void InfoFormat(string format, object arg0, EventId eventId = EventId.Default)
        {
            _log.InfoFormat(format, eventId, MaskingHelper.MaskPossiblySensitiveData((string)arg0));
        }


        public bool IsDebugEnabled
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsErrorEnabled
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsFatalEnabled
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsInfoEnabled
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsWarnEnabled
        {
            get { throw new NotImplementedException(); }
        }

        public void Warn(string message, Exception exception, EventId eventId = EventId.Default)
        {
            _log.Warn(MaskingHelper.MaskPossiblySensitiveData(message), exception, eventId);
        }

        public void Warn(string message, EventId eventId = EventId.Default)
        {
            _log.Warn(MaskingHelper.MaskPossiblySensitiveData(message));
        }

        public void WarnFormat(string format, EventId eventId = EventId.Default, params object[] args)
        {
            _log.WarnFormat(format, eventId, MaskingHelper.MaskPossiblySensitiveData(args));
        }

        public void WarnFormat(string format, object arg0, object arg1, object arg2, EventId eventId = EventId.Default)
        {
            _log.WarnFormat(format, eventId, MaskingHelper.MaskPossiblySensitiveData((string)arg0), MaskingHelper.MaskPossiblySensitiveData((string)arg1), MaskingHelper.MaskPossiblySensitiveData((string)arg2));
        }

        public void WarnFormat(string format, object arg0, object arg1, EventId eventId = EventId.Default)
        {
            _log.WarnFormat(format, eventId, MaskingHelper.MaskPossiblySensitiveData((string)arg0), MaskingHelper.MaskPossiblySensitiveData((string)arg1));
        }

        public void WarnFormat(string format, object arg0, EventId eventId = EventId.Default)
        {
            _log.WarnFormat(format, eventId, MaskingHelper.MaskPossiblySensitiveData((string)arg0));
        }
    }
}