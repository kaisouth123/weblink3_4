﻿using Bridgepay.Core.Logging;
using System;
namespace Bridgepay.Weblink.Core.Logging
{
    public interface ICleanLogger
    {
        void Debug(string message);
        void Debug(string message, Exception exception);
        void DebugFormat(string format, global::Bridgepay.Core.Logging.EventId eventId = EventId.Default, params object[] args);
        void DebugFormat(string format, object arg0, global::Bridgepay.Core.Logging.EventId eventId = EventId.Default);
        void DebugFormat(string format, object arg0, object arg1, global::Bridgepay.Core.Logging.EventId eventId = EventId.Default);
        void DebugFormat(string format, object arg0, object arg1, object arg2, global::Bridgepay.Core.Logging.EventId eventId = EventId.Default);
        void Error(string message);
        void Error(string message, Exception exception, global::Bridgepay.Core.Logging.EventId eventId = EventId.Default);
        void ErrorFormat(string format, global::Bridgepay.Core.Logging.EventId eventId = EventId.Default, params object[] args);
        void ErrorFormat(string format, object arg0, global::Bridgepay.Core.Logging.EventId eventId = EventId.Default);
        void ErrorFormat(string format, object arg0, object arg1, global::Bridgepay.Core.Logging.EventId eventId = EventId.Default);
        void ErrorFormat(string format, object arg0, object arg1, object arg2, global::Bridgepay.Core.Logging.EventId eventId = EventId.Default);
        void Info(string message);
        void Info(string message, Exception exception, global::Bridgepay.Core.Logging.EventId eventId = EventId.Default);
        void InfoFormat(string format, global::Bridgepay.Core.Logging.EventId eventId = EventId.Default, params object[] args);
        void InfoFormat(string format, object arg0, global::Bridgepay.Core.Logging.EventId eventId = EventId.Default);
        void InfoFormat(string format, object arg0, object arg1, global::Bridgepay.Core.Logging.EventId eventId = EventId.Default);
        void InfoFormat(string format, object arg0, object arg1, object arg2, global::Bridgepay.Core.Logging.EventId eventId = EventId.Default);
        bool IsDebugEnabled { get; }
        bool IsErrorEnabled { get; }
        bool IsFatalEnabled { get; }
        bool IsInfoEnabled { get; }
        bool IsWarnEnabled { get; }
        void Warn(string message, global::Bridgepay.Core.Logging.EventId eventId = EventId.Default);
        void Warn(string message, Exception exception, global::Bridgepay.Core.Logging.EventId eventId = EventId.Default);
        void WarnFormat(string format, global::Bridgepay.Core.Logging.EventId eventId = EventId.Default, params object[] args);
        void WarnFormat(string format, object arg0, global::Bridgepay.Core.Logging.EventId eventId = EventId.Default);
        void WarnFormat(string format, object arg0, object arg1, global::Bridgepay.Core.Logging.EventId eventId = EventId.Default);
        void WarnFormat(string format, object arg0, object arg1, object arg2, global::Bridgepay.Core.Logging.EventId eventId = EventId.Default);
    }
}

