﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.Weblink.Core.Interfaces
{
    public interface IResponseFormatter
    {
        IResponse FormatResponse();
    }
}
