﻿using System.Collections.Specialized;

namespace Bridgepay.Weblink.Core.Interfaces
{
    public interface IRequestFormatter
    {
        string FormatRequest(NameValueCollection data, string clientIdentifier, string referenceNumber);
    }
}
