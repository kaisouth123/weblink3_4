﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.Weblink.Core.Interfaces
{
    public interface IResponse
    {
        string Result { get; set; }
        string Message { get; set; }
        string MessageEx { get; set; }
        string Id { get; set; }
    }
}
