﻿using Bridgepay.Weblink.Core.Models;
using Bridgepay.WebLink.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml.Linq;

namespace Bridgepay.Weblink.Core.Models
{
    public class TransactionResponse : BaseResponse
    {
        public string AuthCode { get; set; }
        public string HostCode { get; set; }
        public string CardType { get; set; }
        public bool? IsCommercialCard { get; set; }
        public string Token { get; set; }
        public string TokenKey { get; set; }
        public string CardNumber { get; set; }
        public string ExpDate { get; set; }
        public string NameOnCard { get; set; }
        public string Street { get; set; }
        public string Zip { get; set; }
        public string LastFour { get; set; }
        public string AuthorizedAmount { get; set; }
        public string OriginalAmount { get; set; }
        public string ActualAmount { get; set; }
        public string AVSMessage { get; set; }
        public string AVSResult { get; set; }
        public string CVMessage { get; set; }
        public string CVResult { get; set; }
        public ServiceFeeResponse ServiceFeeResponse { get; set; }

        public TransactionResponse()
        {
            ServiceFeeResponse = new ServiceFeeResponse();
        }

        public void MergeResults(ref TransactionData nvc, bool isVoid = false)
        {
            if (isVoid)
            {
                nvc[Fields.rv_AuthCode] = AuthCode;
                nvc[Fields.rv_HostCode] = HostCode;
                nvc[Fields.rv_Id] = Id;
                nvc[Fields.rv_Message] = Message;
                nvc[Fields.rv_Result] = Result.ToString();
            }
            else
            {
                nvc[Fields.r_AuthCode] = AuthCode;
                nvc[Fields.r_HostCode] = HostCode;
                nvc[Fields.r_Message] = Message;
                nvc[Fields.r_MessageEx] = MessageEx;
                nvc[Fields.r_Result] = Result;
                nvc[Fields.r_ExpDate] = ExpDate ?? Helper.GetExpDate(nvc);
                nvc[Fields.r_Id] = Id;
                nvc[Fields.r_NameOnCard] = NameOnCard;
                nvc[Fields.r_Street] = Street;
                nvc[Fields.r_Token] = Token;
                nvc[Fields.r_TokenKey] = TokenKey;
                nvc[Fields.r_Zip] = Zip;
                nvc[Fields.r_CardType] = CardType;
                nvc[Fields.r_LastFour] = LastFour;
                nvc[Fields.r_AuthorizedAmount] = AuthorizedAmount;
                nvc[Fields.r_OriginalAmount] = OriginalAmount;
                nvc[Fields.r_ActualAmount] = ActualAmount;
                nvc[Fields.r_SvcAuthCode] = ServiceFeeResponse.AuthCode;
                nvc[Fields.r_SvcHostCode] = ServiceFeeResponse.HostCode;
                nvc[Fields.r_SvcMessage] = ServiceFeeResponse.Message;
                nvc[Fields.r_SvcMessageEx] = ServiceFeeResponse.MessageEx;
                nvc[Fields.r_SvcResult] = ServiceFeeResponse.Result;
                nvc[Fields.r_SvcOriginalAmount] = ServiceFeeResponse.OriginalAmount;
                nvc[Fields.r_SvcAuthorizedAmount] = ServiceFeeResponse.AuthorizedAmount;
                nvc[Fields.r_SvcActualAmount] = ServiceFeeResponse.ActualAmount;

                if (AVSMessage != null && AVSMessage.Contains("'"))
                {
                    nvc[Fields.r_AVSMessage] = HttpUtility.HtmlEncode(AVSMessage);
                }
                else
                {
                    nvc[Fields.r_AVSMessage] = AVSMessage;
                }

                nvc[Fields.r_AVSResult] = AVSResult;
                nvc[Fields.r_CVMessage] = CVMessage;
                nvc[Fields.r_CVResult] = CVResult;
            }
        }
    }
}