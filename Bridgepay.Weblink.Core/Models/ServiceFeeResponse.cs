﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.Weblink.Core.Models
{
    public class ServiceFeeResponse
    {
        public string AuthCode { get; set; }
        public string HostCode { get; set; }
        public string Message { get; set; }
        public string MessageEx { get; set; }
        public string Result { get; set; }
        public string ServiceID { get; set; }
        public string OriginalAmount { get; set; }
        public string AuthorizedAmount { get; set; }
        public string ActualAmount { get; set; }
    }
}
