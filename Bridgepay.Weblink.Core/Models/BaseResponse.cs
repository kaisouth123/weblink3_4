﻿using Bridgepay.Weblink.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace Bridgepay.Weblink.Core.Models
{
    public class BaseResponse : IResponse
    {
        public string Result { get; set; }
        public string Message { get; set; }
        public string MessageEx { get; set; } // Meant more for detailed error messages, etc
        public string Id { get; set; }

        public static void AppendToNameValueCollection<T>(T obj, ref TransactionData nvc) where T:BaseResponse
        {
            foreach(var prop in obj.GetType().GetProperties())
            {
                nvc[prop.Name] = prop.GetValue(obj, null).ToString();
            }
        }

    }
}