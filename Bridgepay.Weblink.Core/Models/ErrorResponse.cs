﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace Bridgepay.Weblink.Core.Models
{
    public class ErrorResponse : BaseResponse
    {
        public string MID { get; set; }
        public string MerchantName { get; set; }
        public NameValueCollection ExtraData { get; set; }

        public ErrorResponse()
        {
            ExtraData = new NameValueCollection();
        }
    }
}