﻿using Bridgepay.Weblink.Core.Extensions;
using Bridgepay.Weblink.Core.Models;
using Bridgepay.WebLink.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace Bridgepay.Weblink.Core.Models
{
    /// <summary>
    /// A class that holds the related data for a single transaction. Derived from NameValueCollection
    /// </summary>
    public class TransactionData : NameValueCollection
    {
        public TransactionData() : base() { }
        public TransactionData(NameValueCollection col, string cryptoSecret) : base(col) 
        {
            ScrubData(cryptoSecret);
        }

        public bool HasValidationErrors
        {
            get { return !string.IsNullOrEmpty(base[Fields.r_Errors]); }
        }

        public int? Result
        {
            get
            {
                if (!string.IsNullOrEmpty(base[Fields.r_Result]))
                {
                    int result;
                    if (int.TryParse(base[Fields.r_Result], out result))
                        return result;
                    else
                        return null;
                }
                else
                {
                    return null;
                }
            }
        }

        public int? TransactionSequenceNumber
        {
            get
            {
                if (!string.IsNullOrEmpty(base[Fields.TransactionSequenceNum]))
                {
                    int result;
                    if (int.TryParse(base[Fields.TransactionSequenceNum], out result))
                        return result;
                    else
                        return null;
                }
                else
                {
                    return null;
                }
            }
        }

        public bool WasVoided
        {
            get { return base[Fields.rv_Result] == "0"; }
        }

        // The 1 based index of where this transaction came in the post data
        // Can differ from the TransactionSequenceNumber in cases where the first transaction passed in had a trxn seq number of 2
        // and the second trxn passed in had a trxn seq number of 1, etc...
        private int actualIndex = 0;
        public int ActualIndex
        {
            get { return actualIndex; }
            set { actualIndex = value; }
        }

        /// <summary>
        /// The main id for the transaction. "PNRef" in TPI land.
        /// </summary>
        public int? Id
        {
            get
            {
                if (!string.IsNullOrEmpty(base[Fields.r_Id]))
                {
                    int result;
                    if (int.TryParse(base[Fields.r_Id], out result))
                        return result;
                    else
                        return null;
                }
                else
                {
                    return null;
                }
            }
        }

        public void AddValidationError(string error)
        {
            string errors = base[Fields.r_Errors];
            if (!string.IsNullOrEmpty(errors))
                errors += ",";

            errors += error;

            base[Fields.r_Errors] = errors;
        }

        /// <summary>
        /// Performs any pre-processing or scrubbing of the data. ie: decrypting of passwords, trimming comma's from the amounts, whatever...
        /// </summary>
        private void ScrubData(string cryptoSecret)
        {
            string encryptedPassword = base[Fields.PasswordEncrypted];

            if (!string.IsNullOrEmpty(encryptedPassword))
            {
                try
                {
                    base[Fields.Password] = Helper.DecryptPassword(encryptedPassword, cryptoSecret);
                }
                catch (Exception)
                {
                    AddValidationError("There was a problem decrypting the password for transaction #" + ActualIndex);
                }
            }

            // Strip comma's from the amount
            base[Fields.TotalAmt] = (base[Fields.TotalAmt] + string.Empty).Replace(",", "");
        }

        /// <summary>
        /// Adds the result fields (r_XXXXX) to the passed in data collection
        /// </summary>
        /// <param name="data"></param>
        public void MergeTransactionResults(NameValueCollection data)
        {
            foreach (string key in base.Keys)
            {
                if (key.StartsWithAny(new List<string> { "r_", "rv_" }))
                    data[GetNormalizedFieldName(key, ActualIndex)] = base[key];
            }

            // If r_CardType is empty, and its available in the wallet data, then populate r_CardType from there
            if (string.IsNullOrEmpty(data[GetNormalizedFieldName(Fields.r_CardType, ActualIndex)]) &&
                !string.IsNullOrEmpty(data[Fields.WalletCardType]))
            {
                data[GetNormalizedFieldName(Fields.r_CardType, ActualIndex)] = data[Fields.WalletCardType];
            }
        }

        /// <summary>
        /// Returns the normalized field name for a given field and transaction index. 
        /// i.e. For the field Login and index of 1, "Login" is returned, for index of 2, "Login1" is returned and so on...
        /// </summary>
        /// <param name="field"></param>
        /// <param name="transactionIndex"></param>
        /// <returns></returns>
        public static string GetNormalizedFieldName(string field, int transactionIndex)
        {
            return field + (transactionIndex == 1 ? "" : (transactionIndex - 1).ToString());
        }

        public string GetNormalizedFieldName(string field)
        {
            return GetNormalizedFieldName(field, this.ActualIndex);
        }

    }
}