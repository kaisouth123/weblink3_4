<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:output method="xml" indent="yes"/>
   <xsl:template match="/">
      <requestHeader>
         <ClientIdentifier><xsl:value-of select="requestHeader/ClientIdentifier"/></ClientIdentifier>
         <TransactionID><xsl:value-of select="requestHeader/TransactionID"/></TransactionID>
         <RequestDateTime><xsl:value-of select="requestHeader/RequestDateTime"/></RequestDateTime>
         <RequestType>001</RequestType>
		 <xsl:if test="requestHeader/requestMessage/PurchaseToken = ''">
         <User><xsl:value-of select="requestHeader/User"/></User>
         <Password><xsl:value-of select="requestHeader/Password"/></Password>
		 </xsl:if>
		 <requestMessage>
		    <xsl:if test="requestHeader/requestMessage/PurchaseToken != ''">
			<PurchaseToken><xsl:value-of select="requestHeader/requestMessage/PurchaseToken"/></PurchaseToken>
			</xsl:if>
		    <PaymentAccountNumber><xsl:value-of select="requestHeader/requestMessage/PaymentAccountNumber"/></PaymentAccountNumber>
			<ExpirationDate><xsl:value-of select="requestHeader/requestMessage/ExpirationDate"/></ExpirationDate>
		 </requestMessage>
    </requestHeader>
</xsl:template>
</xsl:stylesheet>